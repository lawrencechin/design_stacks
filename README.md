# Design Stacks

![home](./readme_images/home.jpg)

A web app for collecting design-related images, bookmarks and rss feeds written in **php**. 

## More Images
### Home Page

The homepage allows you to change the *wallpaper* of the site (selected from images in your collections) and change the font for the site.

![Home Wallpapers](./readme_images/home_wallpapers.jpg)
![Home Bookmarks](./readme_images/home_booksmarks.jpg)

### Research/Image Collections

Images are added to albums and sorted by set categories. 

![Research albums](./readme_images/research.jpg)
![Research Edit Albums](./readme_images/research02.jpg)
![Images in Album](./readme_images/research03.jpg)
![Image Viewer](./readme_images/research04.jpg)

### Bookmarks

Bookmarks can be viewed within the app.

![Bookmarks](./readme_images/bookmarks01.jpg)
![Bookmarks in-app browser](./readme_images/bookmarks02.jpg)

### Feeds


![Feeds](./readme_images/feeds01.jpg)
![Feeds in-app browser](./readme_images/feeds02.jpg)

### Temp Folder

A place to quickly upload images without adding to an album for processing later.

![Temp](./readme_images/temp01.jpg)
![Temp Add to Album](./readme_images/temp02.jpg)
