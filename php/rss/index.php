<?php
include_once '../.././db/database.php';
// Include SimplePie
// Located in the parent directory
include_once('autoloader.php');
include_once('./idn/idna_convert.class.php');

$county;

function getCount(){
	global $dbh;
	global $county;
	$feed_count_query ="SELECT Count(*) FROM Feed_List WHERE Read == 0";
	$result = $dbh->query($feed_count_query);
	$feed_count = $result->fetch(PDO::FETCH_NUM);

	$county = $feed_count[0];
}

$handshake = $_GET['password'];

if(!isset($handshake) || $handshake != "chang01ChanG"){
	echo json_encode(array('msg'=>'Password error or not entered', 'error'=>true, 'count'=> 0));
}else{
	//Fetch Feeds from Database
	$query = "SELECT ID, CatID, URL FROM Feed_URLS";

	//Loop through feeds and create full array and just urls for Simple Pie usage
	foreach($dbh->query($query, PDO::FETCH_ASSOC) as $feeds){
		$feedArray[] = $feeds;
		$urlArray[] = $feeds['URL'];
	};

	// Create a new instance of the SimplePie object
	$feed = new SimplePie();
	$feed->set_feed_url($urlArray);

	//Php complains without this being set
	date_default_timezone_set('Europe/London');

	// Initialize the whole SimplePie object.
	$success = $feed->init();

	// This function will grab the proper character encoding, as well as set the content type to text/html.
	$feed->handle_content_type();

	//Get all relevant data from feed and prepare db query

	if($success){
		foreach($feed->get_items() as $item){
			$title = $item->get_title();
			$id = $item->get_id(true);
			$date = $item->get_date();
			//Convert string date into sql format used for sorting YYYY-MM-DD HH:MM
			//Using both to avoid having to run a function afterwards converting between sorting date and display date
			$sqlDate = $item->get_date("Y-m-d H:i");
			//Remove junk from short description
			$description = strip_tags($item->get_description(true));
			$content = $item->get_content();
			$perma = $item->get_permalink();
			//Get feed url to match against feed array
			$sourceURL = $item->get_feed()->subscribe_url();

			//Loop through feed array and match feed ID to feed item
			foreach($feedArray as $feedInfo){
				if(strcmp($sourceURL, $feedInfo['URL']) === 0){
					$URLID = $feedInfo['ID'];
				}
			}

			$itemArray[] = ["ID" => $id, "Title" => $title, "PostDate" => $date, "Description" => $description , "Content" => $content, 'Read' => 0, 'Perma' => $perma, 'URLID' => $URLID, 'sortDate' => $sqlDate];	
		}
	}

	try {
		//Ignore content that already exists in table
	    $query = "INSERT OR IGNORE INTO Feed_List(ID, PostDate, Title, Description, Content, Read, Perma, URLID, sortDate) VALUES(:ID, :PostDate, :Title, :Description, :Content, :Read, :Perma, :URLID, :sortDate)";

	    $preparedQ = $dbh->prepare($query);

	    foreach($itemArray as $dbInsert){
	    	$preparedQ->execute($dbInsert);
	    }

	}catch(PDOException $e){
		echo json_encode(array('msg'=>$e->getMessage(), 'error'=>true, 'count'=>0));
	}
	try{
		//cleanup database: we can't delete the READ rows becasue all that will happen is that they will get reinserted when polling the feeds and return as new items so we leave the ID and Read fields. We then check for READ items older than a week and blank all the other fields before VACUUM'ing the database to release free pages
		$lastWeek  = date("Y-m-d H:i", strtotime("-1 week"));

		$clearDb = "UPDATE Feed_List SET PostDate = '', Title = '', Description = '', Content = '', Perma = '', URLID = 0, sortDate = '' WHERE sortDate < '$lastWeek' AND Read = 1";

		$sendQuery = "VACUUM";

		$dbh->exec($clearDb);
		$dbh->exec($sendQuery);

		getCount();
	    echo json_encode(array('msg'=>'Cool', 'error'=>false, 'count'=> $county));
		
	}catch(PDOException $e){
		echo json_encode(array('msg'=>$e->getMessage(), 'error'=>true, 'count'=>0));
	}
}

?>