a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:7:"

    
";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:294:"
    
    
    
    
    
    
    
    
    

	    
	
	    
	
	    
	
	    
    
	    
	
		
    
    
    
    
	
	    
	
	    
	
	    
	
		
    
    
    
    
	
	    
	
	    
	
		
    
    
    
    
	
	    
	
	    
	
	    
	
		
    
    
	
	    
	
		
    
    
    
    
	
	    
	
    
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:4:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"A List Apart: The Full Feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:21:"http://alistapart.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:64:"Articles, columns, and blog posts for people who make web sites.";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:24:{i:0;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:44:"Shellshock: A Bigger Threat than Heartbleed?";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:12:"Tim Murtaugh";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:104:"http://feedproxy.google.com/~r/alistapart/main/~3/f1NjOGbys4Q/shellshock-a-bigger-threat-than-heartbleed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:74:"http://alistapart.com/blog/post/shellshock-a-bigger-threat-than-heartbleed";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1336:"<p>Time to update those Linux servers again. A newly-discovered Linux flaw may be more pervasive, and more dangerous, than last spring&#8217;s Heartbleed.</p>

<figure class="quote">
<blockquote>
<p>A newly discovered security bug in a widely used piece of Linux software, known as “Bash,” could pose a bigger threat to computer users than the “Heartbleed” bug that surfaced in April, cyber experts warned on Wednesday.</p>
<p>...</p>
<p>Hackers can exploit a bug in Bash to take complete control of a targeted system, security experts said. The “Heartbleed” bug allowed hackers to spy on computers, but not take control of them.</p>
</blockquote>
<figcaption><a href="http://recode.net/2014/09/24/bash-software-bug-may-pose-bigger-threat-than-heartbleed/">“Bash” Software Bug May Pose Bigger Threat Than “Heartbleed”</a>, <cite>Re/code</a></figcaption>
</figure>

<p>This new vulnerability, being called Shellshock, has been found in use on public servers, meaning the threat is not theoretical. A patch has been released, but <a href="http://arstechnica.com/security/2014/09/bug-in-bash-shell-creates-big-security-hole-on-anything-with-nix-in-it/">according to Ars Technica</a>, it&#8217;s unfortunately incomplete.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/f1NjOGbys4Q" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-25T17:00:13+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:74:"http://alistapart.com/blog/post/shellshock-a-bigger-threat-than-heartbleed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:72:"Antoine Lefeuvre on The Web, Worldwide: The Culinary Model of Web Design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:17:"Antoine Lefeuvre ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:94:"http://feedproxy.google.com/~r/alistapart/main/~3/lkfXOiZW4Ck/the-culinary-model-of-web-design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:61:"http://alistapart.com/column/the-culinary-model-of-web-design";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6497:"<p>We call ourselves information architects, web designers or content strategists, among other <a href="http://css-tricks.com/job-titles-in-the-web-industry/">job titles in the industry</a>, including the occasional PHP ninja or SEO rockstar. The web does owe a lot to fields like architecture, industrial design, or marketing. I still haven’t met an interaction cook or <i>maitre d’optimization</i>, though. No web makers turn to chefs for inspiration, one might say.</p>

<p>Well, some do. Let me take you, <i>s’il vous plaît</i>, to Lyon, France, where people think sliced bread is the greatest thing since the internet.</p>

<p>Just a hundred miles from the web’s birthplace at CERN in Geneva lies Lyon, France’s second biggest city. It’s no internet mecca, but that doesn’t mean there are no lessons to be learned from how people make the web there. Unlike many places in the world where the latest new thing is everyone’s obsession, entrepreneurs in Lyon are quite interested in… the nineteenth century! What they’re analyzing is their city’s greatest success, its cuisine.</p>

<p>If Lyon’s food scene today is one the world’s best—<a href="http://edition.cnn.com/2014/04/24/travel/lyon-outshines-paris/index.html">even outshining Paris’ according to CNN</a>, this is thanks to the <a href="http://awomansparis.com/2010/11/18/women-chefs-les-meres-lyonnaises/"><i>Mères lyonnaises</i> movement</a>. These “mothers” were house cooks for Lyon’s rich people, who decided to emancipate and launch their own start-ups: humble restaurants aiming at top-quality food, not fanciness. The movement begun in the nineteenth century only grew bigger in the twentieth, when the Mères passed on their skills and values to the next generation. Their most famous heir is superstar chef Paul Bocuse, who has held the Michelin three-star rating longer than any other, and who began as the apprentice of Mère Eugénie Brazier, the <a href="http://www.thedailybeast.com/articles/2014/03/26/la-m-re-brazier-the-queen-of-the-french-kitchen.html">mother of modern French cooking</a> and one of the very first three-star chefs in 1928. “There’s a real parallel between the ecosystem the Mères started and what we want to achieve,” says Grégory Palayer, president of the aptly named local trade association <a href="http://www.lacuisineduweb.com">La Cuisine du Web</a>. To recreate the Mères’ recipe for success, the <i>toqués</i>—the nickname meaning both “chef’s hat” and “crazy” that’s given to La Cuisine du Web members—have identified its ingredients: networking, media support, funding, and transmitting skills and knowledge. Not to mention a secret plus: joie de vivre. “Parisians and Europeans are often surprised to see we can spend two hours having lunch,” says Grégory. “This is how we conduct business here!”</p>

<p>Lyon’s designers too have their nineteenth-century hero in Auguste Escoffier, the celebrity chef of his age. He began his career as a kitchen boy in his uncle’s restaurant and ended up running the kitchens in London’s most luxurious hotels. Renowned as “the Chef of Kings and the King of Chefs,” Escoffier was also a serial designer: his creations include Peach Melba, Crêpe Suzette, and the <i>Cuisine classique</i> style. He even experimented in a culinary form of <a href="http://archive.wired.com/culture/design/magazine/17-03/dp_intro">design under constraint</a> while in the army during the 1870 Franco-Prussian War, using horse meat for ordinary meals to save scarce beef for the wounded, and inventing 1,001 recipes with turnip, the only readily available vegetable on the front lines. Escoffier did much to improve and structure his industry. He was the first head of the <a href="http://www.worldchefs.org"><abbr title="World Association of Chefs Societies">WACS</abbr></a>, the chefs’ W3C, and revolutionized not only French cooking, but the way restaurants worldwide are run, by championing documentation, standardization, and professionalism.</p>

<p>In his talk “<a href="http://slideshare.net/guillaumeberry/interaction-bchamel">Interaction Béchamel</a>” at the Interaction 14 conference in Amsterdam, Lyon’s <abbr title="Interaction Design Association">IxDA</abbr> leader Guillaume Berry explained how the life and work of Escoffier could influence web design. Guillaume comes from a family of food lovers and makers. Himself a visual designer and an amateur cook, he is greatly inspired in his daily work by cuisine. “It’s all about quality ingredients and preparing them. I’ve realized this while chopping vegetables—a task often neglected or disliked.” The web’s raw ingredients are copy, images, videos: “Even a starred chef won’t be able to cook a proper dish with low-quality ingredients. Don’t expect a web designer to do wonders without great content.”</p>

<p>Just as Escoffier took Ritz customers on a kitchen tour, Guillaume recommends explaining to your clients how their site or app has been cooked. The more open and understood our design processes are, the more their value will be recognized. Have you ever been running late and prepared dinner in a rush? I have and it was, unsurprisingly, a disaster. So tell your clients their website is nothing but a good meal; it takes time to make it a memorable experience.</p>

<p>Looking back at other industries helps us see what’s ahead in ours. What could be the web’s answer to slow food, organic farming, or rawism? “How many interactions a day is it healthy for us to have?” asks Guillaume. He adds, “Cooks have a huge responsibility because depending on how they prepare the food they can make people sick.” Are we designers that powerful? Oh yes, and more—<a href="http://vimeo.com/68470326">we destroyed the world</a>, after all.</p>

<p>No, the web industry isn’t free of junk food. When we create apps that make a smartphone obsolete after two years: junk food. When we believe email is dead and Facebook is the new communication standard: junk food. When we design only for the latest browsers and fastest connections: junk food.</p>

<p>If we’re ready to move from “more” to “better,” let’s remember these simple rules from Eugénie Brazier: 1. Pick your ingredients very carefully; 2. Home-made first; 3. A flashy presentation won’t save a poor dish.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/lkfXOiZW4Ck" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:156:"<a href="/topic/industry-business">Industry & Business</a>, <a href="/topic/user-experience">User Experience</a>, <a href="/topic/creativity">Creativity</a>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-25T12:29:43+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:61:"http://alistapart.com/column/the-culinary-model-of-web-design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"It Was Just A Thing";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:17:"Anthony Colangelo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:81:"http://feedproxy.google.com/~r/alistapart/main/~3/-0vaScmb-yk/it-was-just-a-thing";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:51:"http://alistapart.com/blog/post/it-was-just-a-thing";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2329:"<p>A little less than two months ago, I wrote about <a href="http://alistapart.com/blog/post/the-most-dangerous-word-in-software-development">the most dangerous word in software development</a>: just. A lot of assumptions hide behind that seemingly harmless word, but there’s another side to it.</p>

<p>“It was just a thing we built to deploy our work to staging.”</p>

<p>“It was just a little plugin we built to handle responsive tab sets.”</p>

<p>“It was just a way to text a bunch of our friends at the same time.”</p>

<p>Some of the best and most useful things we build have humble beginnings. Small side projects start with a sapling of an idea—something that can be built in a weekend, but will make our work a little easier, our lives a little better.</p>

<p>We focus on solving a very specific problem, or fulfilling a very specific need. Once we start using the thing we’ve built, we realize its full potential. We refine our creation until it becomes something bigger and better. By building, using, and refining, we avoid the pitfalls of assumptions made by the harmful use of the word “just” <a href="http://alistapart.com/blog/post/the-most-dangerous-word-in-software-development">that I warned about</a>:</p>

<figure class="quote">
<blockquote>
Things change when something moves from concept to reality. As <a href="https://twitter.com/dwiskus">Dave Wiskus</a> said on a <a href="http://www.imore.com/debug-37-simmons-wiskus-gruber-and-vesper-sync">recent episode of Debug</a>, “everything changes when fingers hit glass.”
</blockquote>
</figure>

<p>But the people who build something shouldn’t be the only ones who shape its future. When Twitter was founded, it was just a way to text a bunch of friends at once. The way that people used Twitter in the early days helped determine its future. Retweets, @username mentions, and hashtags became official parts of Twitter because of those early usage patterns.</p>

<p>Embrace the small, simple, focused start, and get something into people’s hands. Let usage patterns inform refinements, validate assumptions, and guide you to future success. It’s more than okay to start by building “just a thing”—in fact, I suggest it.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/-0vaScmb-yk" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-24T12:30:16+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:51:"http://alistapart.com/blog/post/it-was-just-a-thing";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:47:"
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:4:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:31:"This week&#039;s sponsor: Asana";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:69:"http://feedproxy.google.com/~r/alistapart/main/~3/a0i8OgvPd3Q/1piltgH";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:22:"http://synd.co/1piltgH";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:302:"<p><a href="http://synd.co/1piltgH">Asana&#8217;s</a> new iOS 8 app now available! Use Asana to organize team tasks and conversations on web and mobile. <a href="http://synd.co/1piltgH">Sign up free.</a></p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/a0i8OgvPd3Q" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-18T18:28:17+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:22:"http://synd.co/1piltgH";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:31:"Getting Started With CSS Audits";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:15:"Susan Robertson";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:93:"http://feedproxy.google.com/~r/alistapart/main/~3/LmIbtBtJWSo/getting-started-with-css-audits";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:63:"http://alistapart.com/blog/post/getting-started-with-css-audits";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2541:"<p>This week I wrote about conducting <a href="http://alistapart.com/article/css-audits-taking-stock-of-your-code">CSS audits</a> to organize your code, keeping it clean and performant—resulting in faster sites that are easier to maintain. Now that you understand the hows and whys of auditing, let’s take a look at some more resources that will help you maintain your CSS architecture. Here are some I’ve recently discovered and find helpful.</p>

<h2>Organizing CSS</h2>
<ul>
<li><a href="http://csswizardry.com">Harry Roberts</a> has put together a fantastic resource for thinking about how to write large CSS systems, <a href="http://cssguidelin.es">CSS Guidelines</a>.</li>
<li>Interested in making the style guide part of the audit easier? <a href="https://github.com/davidhund/styleguide-generators">This Github repo</a> includes a whole bunch of info on different generators.</li>
</ul>

<h2>Help from task runners</h2>
<p>Do you like task runners such as grunt or gulp? Andy Osmani’s tutorial walks through using all kinds of task runners to find unused CSS selectors: <a href="http://addyosmani.com/blog/removing-unused-css/">Spring Cleaning Unused CSS Selectors</a>.</p>

<h2>Accessibility</h2>
<p>Are you interested in auditing for accessibility as well (hopefully you are!)? There are tools for that, too. <a href="http://substantial.com/blog/2014/07/22/how-i-audit-a-website-for-accessibility/">This article helps you audit your site for accessibility</a>— it’s a great outline of exactly how to do it. </p>

<h2>Performance</h2>
<ul>
<li><a href="http://www.sitepoint.com/complete-guide-reducing-page-weight/">Sitepoint</a> takes a look at trimming down overall page weight, which would optimize your site quite a bit.</li>
<li>Google Chrome’s dev tools include a built-in <a href="https://developer.chrome.com/devtools#audits">audit tool</a>, which suggests ways you could improve performance. A great article on <a href="http://www.html5rocks.com/en/tutorials/developertools/auditpanel/">HTML5 Rocks</a> goes through this tool in depth.</li>
</ul>

<p>With these tools, you’ll be better prepared to clean up your CSS, optimize your site, and make the entire experience better for users. When talking about auditing code, many people are focusing on performance, which is a great benefit for all involved, but don&#8217;t forget that maintainability and speedier development time come along with a faster site.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/LmIbtBtJWSo" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-18T12:30:31+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:63:"http://alistapart.com/blog/post/getting-started-with-css-audits";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"Client Education and Post-Launch Success";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:11:"Drew Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:102:"http://feedproxy.google.com/~r/alistapart/main/~3/sOq6RG3zlkg/client-education-and-post-launch-success";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:70:"http://alistapart.com/article/client-education-and-post-launch-success";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:9032:"<p>What our clients do with their websites is just as important as the websites themselves. We may pride ourselves on building a great product, but it’s ultimately up to the client to see it succeed or fail. Even the best website can become neglected, underused, or messy without a little education and training.</p>

<p>Too often, my company used to create amazing tools for clients and then send them out into the world without enough guidance. We’d watch our sites slowly become stale, and we’d see our strategic content overwritten with fluffy filler.</p>

<p>It was no one’s fault but our own.</p>

<p>As passionate and knowledgeable web enthusiasts, it’s literally our job to help our clients succeed in any way we can, even after launch. Every project is an opportunity to educate clients and build a mutually beneficial learning experience.</p>

<h2>Meeting in the middle</h2>

<p>If we want our clients to use our products to their full potential, we have to meet them in the middle. We have to balance our technical expertise with their existing processes and skills. </p>

<p>At my company, <a href="http://brolik.com/">Brolik</a>, we learned this the hard way.</p>

<p>We had a financial client whose main revenue came from selling in-depth PDF reports. Customers would select a report, generating an email to an employee who would manually create and email an unprotected PDF to the customer. The whole process would take about two days.</p>

<p>To make the process faster and more secure, we built an advanced, password-protected portal where their customers could purchase and access only the reports they’d paid for. The PDFs themselves were generated on the fly from the content management system. They were protected even after they were downloaded and only viewable with a unique username and password generated with the PDF.</p>

<p>The system itself was technically advanced and thoroughly solved our client’s needs. When the job was done, we patted ourselves on the back, added the project to our portfolio, and moved on to the next thing.</p>

<p>The client, however, was generally confused by the system we’d built. They didn’t quite know how to explain it to their customers. Processes had been automated to the point where they seemed untrustworthy. After about a month, they asked us if we’d revert back to their previous system.<br />
 <br />
We had created too large of a process change for our client. We upended a large part of their business model without really considering whether they were ready for a new approach. </p>

<p>From that experience, we learned not only to create online tools that complement our clients’ existing business processes, but also that we can be instrumental in helping clients embrace new processes. We now see it as part of our job to educate our clients and explain the technical and strategic thought behind all of our decisions.</p>

<h2>Leading by example</h2>

<p>We put this lesson to work on a more recent project, developing a site-wide content tagging system where images, video, and other media could be displayed in different ways based on how they were tagged. </p>

<p>We could have left our clients to figure out this new system on their own, but we wanted to help them adopt it. So we pre-populated content and tags to demonstrate functionality. We walked through the tagging process with as many stakeholders as we could. We even created a PDF guide to explain the how and why behind the new system. </p>

<p>In this case, our approach worked, and the client’s cumbersome media management time was significantly reduced. The difference between the outcome of the two projects was simply education and support.</p>

<p>Education and support can, and usually does, take the form of setting an example. Some clients may not fully understand the benefits of a content strategy, for instance, so you have to show them results. Create relevant and well-written sample blog posts for them, and show how they can drive website traffic. Share articles and case studies that relate to the new tools you’re building for them. Show them that you’re excited, because excitement is contagious. If you’re lucky and smart enough to follow <a href="/article/living-up-to-your-business-ideals">Geoff Dimasi’s advice</a> and work with clients who align with your values, this process will be automatic, because you’ll already be invested in their success.</p>

<p>We should be teaching our clients to use their website, app, content management system, or social media correctly and wisely. The more adept they are at putting our products to use, the better our products perform.</p>

<h2>Dealing with budgets</h2>

<p>Client education means new deliverables, which have to be prepared by those directly involved in the project. Developers, designers, project managers, and other team members are responsible for creating the PDFs, training workshops, interactive guides, and other educational material.</p>

<p>That means more organizing, writing, designing, planning, and coding—all things we normally bill for, but now we have to bill in the name of client education.</p>

<p>Take this into account at the beginning of a project. The amount of education a client needs can be a consideration for taking a job at all, but it should at least factor into pricing. Hours spent helping your client use your product is billable time that you shouldn’t give away for free. </p>

<p>At Brolik, we’ve helped a range of clients—from those who have “just accepted that the Web isn’t a fad” (that’s an actual quote from 2013), to businesses that have a team of in-house developers. We consider this information and price accordingly, because it directly affects the success of the entire product and partnership. If they need a lot of education but they’re not willing to pay for it, it may be smart to pass on the job.</p>

<p>Most clients actually understand this. Those who are interested in improving their business are interested in improving themselves as well. This is the foundation for a truly fulfilling and mutually beneficial client relationship. Seek out these relationships.</p>

<p>It’s sometimes challenging to justify a “client education” line item in your proposals, however. If you can’t, try to at least work some wiggle room into your price. More specifically, try adding a 10 percent contingency for “Support and Training” or “Onboarding.”</p>

<p>If you can’t justify a price increase at all, but you still want the job, consider factoring in a few client education hours and their opportunity cost as part of your company’s overall marketing budget. Teaching your client to use your product is your responsibility as a digital business.</p>

<h2>This never ends (hopefully)</h2>

<p>What’s better than arming your clients with knowledge and tools, pumping them up, and then sending them out into the world to succeed? Venturing out with them!</p>

<p>At Brolik, we’ve started signing clients onto digital strategy retainers once their websites are completed. Digital strategy is an overarching term that covers anything and everything to grow a business online. Specifically for us, it includes audience research, content creation, SEO, search and display advertising, website maintenance, social media, and all kinds of analysis and reporting.</p>

<p>This allows us to continue to educate (and learn) on an ongoing basis. It keeps things interesting—and as a bonus, we usually upsell more work.</p>

<p>We’ve found that by fostering collaboration post-launch, we not only help our clients use our product more effectively and grow their business, but we also alleviate a lot of the panic that kicks in right before a site goes live. They know we’ll still be there to fix, tweak, analyze, and even experiment.</p>

<p>This ongoing digital strategy concept was so natural for our business that it’s surprising it took us so long to implement it. After 10 years making websites, we’ve only offered digital strategy for the last two, and it’s already driving 50 percent of our revenue.</p>

<h2>It pays to be along for the ride</h2>

<p>The extra effort required for client education is worth it. By giving our clients the tools, knowledge, and passion they need to be successful with what we’ve built for them, we help them improve their business.</p>

<p>Anything that drives their success ultimately drives ours. When the tools we build work well for our clients, they return to us for more work. When their websites perform well, our portfolios look better and live longer. Overall, when their business improves, it reflects well on us. </p>

<p>A fulfilling and mutually beneficial client relationship is good for the client and good for future business. It’s an area where we can follow our passion and do what’s right, because we get back as much as we put in.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/sOq6RG3zlkg" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-16T14:00:55+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:70:"http://alistapart.com/article/client-education-and-post-launch-success";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"CSS Audits: Taking Stock of Your Code";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:15:"Susan Robertson";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"http://feedproxy.google.com/~r/alistapart/main/~3/LD5TSciKp7I/css-audits-taking-stock-of-your-code";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:66:"http://alistapart.com/article/css-audits-taking-stock-of-your-code";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:11660:"<p>Most people aren’t excited at the prospect of auditing code, but it’s become one of my favorite types of projects. A CSS audit is really detective work. You start with a site’s code and dig deeper: you look at how many stylesheets are being called, how that affects site performance, and how the CSS itself is written. Your goal is to look for ways to improve on what’s there—to sleuth out fixes to make your codebase better and your site faster.</p>

<p>I&#8217;ll share tips on how to approach your own audit, along with the advantages of taking a full inventory of your CSS and various tools.</p>

<h2>Benefits of an audit</h2>

<p>An audit helps you to organize your code and eliminate repetition. You don’t write any code during an audit; you simply take stock of what’s there and document recommendations to pass off to a client or discuss with your team. These recommendations ensure new code won’t repeat past mistakes. Let’s take a closer look at other benefits:</p>

<ul>
<li><strong>Reduce file sizes.</strong> A complete overview of the CSS lets you take the time to find ways to refactor the code: to clean it up and perhaps cut down on the number of properties. You can also hunt for any odds and ends, such as outdated versions of browser prefixes, that aren’t in use anymore. Getting rid of unused or unnecessary code trims down the file people have to download when they visit your site.</li> 
<li><strong>Ensure consistency with guidelines.</strong> As you audit, create documentation regarding your styles and what’s happening with the site or application. You could make a formal style guide, or you could just write out recommendations to note how different pieces of your code are used. Whatever form your documentation takes, it’ll save anyone coming onto your team a lot of time and trouble, as they can easily familiarize themselves with your site’s CSS and architecture.</li> 
<li><strong>Standardize your code.</strong> Code organization—which certainly attracts differing opinions—is essential to keeping your codebase more maintainable into the future. For instance, if you choose to alphabetize your properties, you can readily spot duplicates, because you’d end up with two sets of margin properties right next to each other. Or you may prefer to group properties according to their function: positioning, box model-related, etc. Having a system in place helps you guard against repetition.</li>
<li><strong>Increase performance.</strong> I’ve saved the best for last. Auditing code, along with combining and zipping up stylesheets, leads to markedly faster site speeds. For example, <a href="http://csswizardry.com">Harry Roberts</a>, a front-end architect in the UK who conducts regular audits, told me about a site he recently worked on:

<figure class="quote">
<blockquote>I rebuilt <a href="https://www.fasetto.com/">Fasetto.com</a> with a view to improving its performance; it went from <a href="https://web.archive.org/web/20140103113604/http://www.fasetto.com/">27 separate stylesheets</a> for a single-page site (mainly UI toolkits like Bootstrap, etc.) down to just one stylesheet (which is actually minified and inlined, to save on the HTTP request), which weighs in at just 5.4 kB post-gzip.</blockquote>
</figure><p>	</p>

<p>This is a huge win, especially for people on slower connections—but everyone gains when sites load quickly.</p><p></li></p>
</ul>

<h2>How to audit: take inventory</h2>

<p>Now that audits have won you over, how do you go about doing one? I like to start with a few tools that provide an overview of the site’s current codebase. You may approach your own audit differently, based on your site’s problem areas or your philosophy of how you write code (whether <a href="http://oocss.org">OOCSS</a> or <a href="http://bem.info/method">BEM</a>). The important thing is to keep in mind what will be most useful to you and your own site. </p>

<p>Once I’ve diagnosed my code through tools, I examine it line by line.</p>

<h3>Tools</h3>

<p>The first tool I reach for is Nicole Sullivan’s invaluable <a href="https://github.com/stubbornella/type-o-matic">Type-o-matic</a>, an add-on for Firebug that generates a JSON report of all the type styles in use across a site. As an added bonus, Type-o-matic creates a visual report as it runs. By looking at both reports, you know at a glance when to combine type styles that are too similar, eliminating unnecessary styles. I’ve found that the detail of the JSON report makes it easy to see how to create a more reusable type system.</p>

<p>In addition to Type-o-matic, I run <a href="http://csslint.net">CSS Lint</a>, an extremely flexible tool that flags a wide range of potential bugs from missing fallback colors to shorthand properties for better performance. To use CSS Lint, click the arrow next to the word “Lint” and choose the options you want. I like to check for repeated properties or too many font sizes, so I always run Maintainability &amp; Duplication along with Performance. CSS Lint then returns recommendations for changes; some may be related to known issues that will break in older browsers and others may be best practices (as the tool sees them). CSS Lint isn’t perfect. If you run it leaving every option checked, you are bound to see things in the end report that you may not agree with, like warnings for IE6. That said, this is a quick way to get a handle on the overall state of your CSS.</p>

<p>Next, I search through the CSS to review how often I repeat common properties, like <code>float</code> or <code>margin</code>. (If you’re comfortable with the command line, type <code>grep</code> along with instructions and plug in something like <code>grep &#8220;float&#8221; styles/styles.scss</code> to find all instances of <code>&#8220;float&#8221;</code>.) Note any properties you may cut or bundle into other modules. Trimming your properties is a balancing act: to reduce the number of repeated properties, you may need to add more classes to your HTML, so that&#8217;s something you&#8217;ll need to gauge according to your project.</p>

<p>I like to do this step by hand, as it forces me to walk through the CSS on my own, which in turn helps me better understand what’s going on. But if you’re short on time, or if you’re not yet comfortable with the command line, tools can smooth the way:</p>

<ul>
<li><a href="http://atomeye.com/css-dig.html">CSS Dig</a> is an automated script that runs through all of your code to help you see it visually. A similar tool is <a href="http://www.stylestats.org/">StyleStats</a>, where you type in a url to survey its CSS.</li>
<li><a href="https://github.com/SlexAxton/css-colorguard">CSS Colorguard</a> is a brand-new tool that runs on Node and outputs a report based on your colors, so you know if any colors are too alike. This helps limit your color palette, making it easier to maintain in the future.</li> 
<li><a href="https://addons.mozilla.org/en-US/firefox/addon/dust-me-selectors">Dust-Me Selectors</a> is an add-on for Firebug in Firefox that finds unused selectors.</li>
</ul>

<h3>Line by line</h3>

<p>After you run your tools, take the time to read through the CSS; it’s worth it to get a real sense of what’s happening. For instance, comments in the code—that tools miss—may explain why some quirk persists.</p>

<p>One big thing I double-check is the <a href="http://smacss.com/book/applicability">depth of applicability</a>, or how far down an attribute string applies. Does your CSS rely on a lot of specificity? Are you seeing long strings of selectors, either in the style files themselves or in the output from a preprocessor? A high depth of applicability means your code will require a very specific HTML structure for styles to work. If you can scale it back, you’ll get more reusable code and speedier performance.</p>

<h2>Review and recommend</h2>

<p>Now to the fun part. Once you have all your data, you can figure out how to improve the CSS and make some recommendations.</p>

<p>The recommendation document doesn’t have to be heavily designed or formatted, but it should be easy to read. Splitting it into two parts is a good idea. The first consists of your review, listing the things you’ve found. If you refer to the results of CSS Lint or Type-o-matic, be sure to include either screenshots or the JSON report itself as an attachment. The second half contains your actionable recommendations to improve the code. This can be as simple as a list, with items like “Consolidate type styles that are closely related and create mixins for use sitewide.”</p>

<p>As you analyze all the information you’ve collected, look for areas where you can:</p>

<ul>
<li><strong>Tighten code.</strong> Do you have four different sets of styles for a call-out box, several similar link styles, or way too many exceptions to your standard grid? These are great candidates for repeatable modular styles. To make consolidation even easier, you could use a preprocessor like Sass to turn them into <a href="http://oocss.org/spec/css-mixins.html">mixins</a> or <a href="http://sass-lang.com/guide">extend</a>, allowing styles to be applied when you call them on a class. (Just check that the outputted code is sensible too.)</li>

<li><strong>Keep code consistent.</strong> A good audit makes sure the code adheres to its own philosophy. If your CSS is written based on a particular approach, such as BEM or OOCSS, is it consistent? Or do styles veer from time to time, and are there acceptable deviations? Make sure you document these exceptions, so others on your team are aware.</li>
</ul>

<p>If you’re working with a client, it’s also important to explain the approaches you favor, so they understand where you’re coming from—and what things you may consider as issues with the code. For example, I prefer OOCSS, so I tend to push for more modularity and reusability; a few classes stacked up (if you aren’t using a preprocessor) don’t bother me. Making sure your client understands the context of your work is particularly crucial when you’re not on the implementation team.<br /></p><h2>Hand off to the client</h2>

<p>You did it! Once you’ve written your recommendations (and taken some time to think on them and ensure they’re solid), you can hand them off to the client—be prepared for any questions they may have. If this is for your team, congratulations: get cracking on your list.</p>

<p>But wait—an audit has even more rewards. Now that you’ve got this prime documentation, take it a step further: use it as the springboard to talk about how to maintain your CSS going forward. If the same issues kept popping up throughout your code, document how you solved them, so everyone knows how to proceed in the future when creating new features or sections. You may turn this document into a <a href="/article/creating-style-guides">style guide</a>. Another thing to consider is how often to revisit your audit to ensure your codebase stays squeaky clean. The timing will vary by team and project, but set a realistic, regular schedule—this a key part of the auditing process.</p>

<p>Conducting an audit is a vital first step to keeping your CSS lean and mean. It also helps your documentation stay up to date, allowing your team to have a good handle on how to move forward with new features. When your code is structured well, it’s more performant—and everyone benefits. So find the time, grab your best sleuthing hat, and get started.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/LD5TSciKp7I" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-16T14:00:11+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:66:"http://alistapart.com/article/css-audits-taking-stock-of-your-code";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:73:"Rian van der Merwe on A View from a Different Valley: Work Life Imbalance";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:18:"Rian Van Der Merwe";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:81:"http://feedproxy.google.com/~r/alistapart/main/~3/y5ED310PGQE/work-life-imbalance";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:48:"http://alistapart.com/column/work-life-imbalance";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6413:"<p>I’m old enough to remember when laptops entered the workforce. It was an amazing thing. At first only the select few could be seen walking around with their giant black IBMs and silver Dells. It took a few years, but eventually every new job came with the question we all loved to hear: “desktop or laptop?”</p>

<p>I was so happy when I got my first laptop at work. “Man,” I thought, “now I can work anywhere, any time!” It was fun for a while, until I realized that now I could work anywhere, any time. Slowly our office started to reflect this newfound freedom. Work looked less and less like work, and more and more like home. Home offices became a big thing, and it’s now almost impossible to distinguish between home offices of famous designers and the workspaces (I don’t think we even call them “offices” any more) of most startups.</p>

<h2>Work and life: does it blend?</h2>

<p>There is a blending of work and life that woos us with its promise of barbecues at work and daytime team celebrations at movie theaters, but we’re paying for it in another way: a complete eradication of the line between home life and work life. “Love what you do,” we say. “Get a job you don’t want to take a vacation from,” we say—and we sit back and watch the retweets stream in.</p>

<p>I don’t like it.</p>

<p>I don’t like it for two reasons.</p>

<h3>It makes us worse at our jobs</h3>

<p>There’s plenty of research that shows when employers place strict limits on messaging, employees are happier and enjoy their work more. <i>And</i> productivity isn’t affected negatively at all. Clive Thompson’s <a href="http://www.motherjones.com/environment/2014/04/smartphone-addiction-research-work-email">article about this for Mother Jones</a> is a great overview of what we know about the handful of experiments that have been done to research the effects of messaging limits.</p>

<p>But that’s not even the whole story. It’s not just that constantly thinking about work makes us more stressed, it’s also that our fear of doing nothing—of not being productive every second of the day—is hurting us as well (we’ll talk about side projects another time). There’s plenty of research about this as well, but let’s stick with Jessica Stillman’s <a href="http://www.inc.com/jessica-stillman/bored-at-work-good.html">Bored at Work? Good</a>. It’s a good overview of what scientists have found on the topic of giving your mind time to rest. In short, being idle tells your brain that it’s in need of something different, which stimulates creative thinking. So it’s something to be sought out and cherished—not something to be shunned.</p>

<figure class="quote">
<blockquote>
<p>Sometimes when things clear away and you’re not watching anything and you’re in your car and you start going, oh no, here it comes, that I’m alone, and it starts to visit on you, just this sadness. And that’s why we text and drive. People are willing to risk taking a life and ruining their own because they don’t want to be alone for a second because it’s so hard.</p>
</blockquote>
<figcaption><a href="https://www.youtube.com/watch?v=5HbYScltf1c">Louis C. K.</a></figcaption>
</figure>

<h3>It teaches that boundaries are bad</h3>

<p>The second problem I have with our constant pursuit of the productivity train is that it teaches us that setting boundaries to spend time with our friends and family = laziness. I got some raised eyebrows at work recently when I declined an invitation to watch a World Cup game in a conference room. But here’s the thing. If I watch the World Cup game with a bunch of people at work today, guess what I have to do tonight? I have to work to catch up, instead of spending time with my family. And that is not ok with me.</p>

<p>I have a weird rule about this. Work has me—completely—between the hours of 8:30 a.m. and 6:00 p.m. It has 100 percent of my attention. But outside of those hours I consider it part of being a sane and good human to give my kids a bath, chat to my wife, read, and reflect on the day that’s past and the one that’s coming—without the pressure of having to be online all the time. I swear it makes me a better (and more productive) employee, but I can’t shake the feeling that I shouldn’t be writing this down because you’re just going to think I’m lazy.</p>

<p>But hey, I’m going to face my fear and just come right out and say it: I try not to work nights. There. That felt good.</p>

<p>It doesn’t always work out, and of course there are times when a need is pressing and I take care of it at night. I don’t have a problem with that. But I don’t sit and do email for hours every night. See, the time I spend with people is what gives my work meaning. I do what I do for <i>them</i>—for the people in my life, the people I know, and the people I don’t. If we never spend time away from our work, how can we understand the world and the people we make things for?</p>

<figure class="quote">
<blockquote>
<p>Of course, the remaking of the contemporary tech office into a mixed work-cum-leisure space is not actually meant to promote leisure. Instead, the work/leisure mixing that takes place in the office mirrors what happens across digital, social and professional spaces. Work has seeped into our leisure hours, making the two tough to distinguish.</p>
</blockquote>
<figcaption>Kate Losse, <cite><a href="http://aeon.co/magazine/culture/what-tech-offices-tell-us-about-the-future-of-work/">Tech aesthetics</a></cite></figcaption>
</figure>

<h2>Permission to veg out</h2>

<p>So I guess this column is my attempt to give you permission to do nothing every once in a while. Not to be lazy, or not do your job. But to take the time you need to get <i>better</i> at what you do, and enjoy it a lot more.</p>

<p>As this column evolves, I think this is what I’ll be talking about a lot. How to make the hours we have at work count more. How to think of what we do not as <i>the tech business</i> but <i>the people business</i>. How to give ourselves permission to experience the world around us and get inspiration for our work from that. How to be <a href="http://en.wikipedia.org/wiki/Flâneur">flâneur</a>: wandering around with eyes wide open to inspiration.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/y5ED310PGQE" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:42:"<a href="/topic/creativity">Creativity</a>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-11T12:30:06+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:48:"http://alistapart.com/column/work-life-imbalance";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:15:"Awkward Cousins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:17:"Anthony Colangelo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:77:"http://feedproxy.google.com/~r/alistapart/main/~3/EHHe_O6Fs_I/awkward-cousins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:47:"http://alistapart.com/blog/post/awkward-cousins";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:3614:"<p>As an industry, we’re historically terrible at drawing lines between things. We try to segment devices based on screen size, but that doesn’t take into account hardware functionality, form factor, and usage context, for starters. The laptop I’m writing this on has the same resolution as a 1080p television. They’d be lumped into the same screen-size–dependent groups, but they are two totally different device classes, so how do we determine what goes together?</p>

<p>That’s a simple example, but it points to a larger issue. We so desperately want to draw lines between things, but there are often too many variables to make those lines clean.</p>

<p>Why, then, do we draw such strict lines between our roles on projects? What does the area of overlap between a designer and front-end developer look like? A front- and back-end developer? A designer and back-end developer? The old thinking of defined roles is certainly loosening up, but we still have a long way to go.</p>

<p>The chasm between roles that is most concerning is the one between web designers/developers and native application designers/developers. We often choose a camp early on and stick to it, which is a mindset that may have been fueled by the false “native vs. web” battle a few years ago. It was positioned as an either-or decision, and hybrid approaches were looked down upon.</p>

<p>The two camps of creators are drifting farther and farther apart, even as the products are getting closer and closer. <a href="https://twitter.com/gruber">John Gruber</a> <a href="http://daringfireball.net/2014/04/rethinking_what_we_mean_by_mobile_web">best described the overlap that users see</a>:</p>

<figure class="quote">
<blockquote>
<p>When I’m using Tweetbot, for example, much of my time in the app is spent reading web pages rendered in a web browser. Surely that’s true of mobile Facebook users, as well. What should that count as, “app” or “web”?</p>

<p>I publish a website, but tens of thousands of my most loyal readers consume it using RSS apps. What should they count as, “app” or “web”?.</p>
</blockquote>
</figure>

<p>The people using the things we build don’t see the divide as harshly as we do, if at all. More importantly, the development environments are becoming more similar, as well. <a href="https://developer.apple.com/swift/">Swift</a>, Apple’s brand new programming language for iOS and Mac development, has a strong resemblance to the languages we know and love on the web, and that’s no accident. One of Apple’s top targets for Swift, if not the top target, is the web development community. It’s a massive, passionate, and talented pool of developers who, largely, have not done iOS or Mac work—yet.</p>

<p>As someone who spans the divide regularly, it’s sad to watch these two communities keep at arm’s length like awkward cousins at a family reunion. We have so much in common—interests, skills, core values, and a ton of technological ancestry. The difference between the things we build is shrinking in the minds of our shared users, and the ways we build those things are aligning. I dream of the day when we get over our poorly drawn lines and become the big, happy community I know we can be.</p>

<p>At the very least, please <a href="http://inessential.com/">start</a> <a href="http://betterelevation.com/">reading</a> <a href="http://www.marco.org/">each</a> <a href="http://shapeof.com/">other’s</a> <a href="http://indiestack.com/">blogs</a>.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/EHHe_O6Fs_I" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:168:"<a href="/topic/application-development">Application Development</a>, <a href="/topic/industry">Industry</a>, <a href="/topic/mobile-multidevice">Mobile/Multidevice</a>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-08T14:00:22+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:47:"http://alistapart.com/blog/post/awkward-cousins";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:46:"Watch: A New Documentary About Jeffrey Zeldman";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:22:"Sara Wachter-Boettcher";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:89:"http://feedproxy.google.com/~r/alistapart/main/~3/A3SQgQU507M/jeffrey-zeldman-documentary";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:59:"http://alistapart.com/blog/post/jeffrey-zeldman-documentary";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1311:"<figure class="quote">
	<blockquote>You keep it by giving it away.</blockquote><figcaption>Jeffrey Zeldman</figcaption>
</figure>
<p>It&#8217;s a philosophy that&#8217;s always guided us at <cite>A List Apart</cite>: that we all learn more—and are more successful—when we share what we know with anyone who wants to listen. And it comes straight from our publisher, Jeffrey Zeldman. </p>

<p>For 20 years, he&#8217;s been sharing everything he can with us, the people who make websites—from advice on table layouts in the &#8216;90s to <cite>Designing With Web Standards</cite> in the 2000s to educating the next generation of designers today.&nbsp;   </p>

<p>Our friends at <a href="http://lynda.com">Lynda.com</a> just released a documentary highlighting Jeffrey&#8217;s two decades of designing, organizing, and most of all sharing on the web. You should watch it. </p>

<figure>
<iframe src="//player.vimeo.com/video/104641191" width="696" height="391" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="http://vimeo.com/104641191">Jeffrey Zeldman: 20 years of Web Design and Community</a> from <a href="http://vimeo.com/lyndavimeo">lynda.com</a>.</p>
</figure><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/A3SQgQU507M" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-04T13:30:29+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:59:"http://alistapart.com/blog/post/jeffrey-zeldman-documentary";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:10;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"Git: The Safety Net for Your Projects";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:15:"Tobias Günther";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"http://feedproxy.google.com/~r/alistapart/main/~3/QXYwqk3T2cE/git-the-safety-net-for-your-projects";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:66:"http://alistapart.com/article/git-the-safety-net-for-your-projects";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:16174:"<p>I remember January 10, 2010, rather well: it was the day we lost a project&#8217;s complete history. We were using Subversion as our version control system, which kept the project&#8217;s history in a central repository on a server. And we were backing up this server on a regular basis—at least, we thought we were. The server broke down, and then the backup failed. Our project wasn&#8217;t completely lost, but all the historic versions were gone. </p>

<p>Shortly after the server broke down, we switched to Git. I had always seen version control as torturous; it was too complex and not useful enough for me to see its value, though I used it as a matter of duty. But once we’d spent some time on the new system, and I began to understand just how helpful Git could be. Since then, it has saved my neck in many situations.</p>

<p>During the course of this article, I&#8217;ll walk through how Git can help you avoid mistakes—and how to recover if they&#8217;ve already happened.</p>

<h2>Every teammate is a backup</h2>

<p>Since Git is a distributed version control system, every member of our team that has a project cloned (or &#8220;checked out,&#8221; if you&#8217;re coming from Subversion) automatically has a backup on his or her disk. This backup contains the latest version of the project, as well as its complete history.</p>

<p>This means that should a developer&#8217;s local machine or even our central server ever break down again (and the backup not work for any reason), we&#8217;re up and running again in minutes: any local repository from a teammate&#8217;s disk is all we need to get a fully functional replacement.</p>

<h2>Branches keep separate things separate</h2>

<p>When my more technical colleagues told me about how &#8220;cool&#8221; branching in Git was, I wasn&#8217;t bursting with joy right away. First, I have to admit that I didn&#8217;t really understand the advantages of branching. And second, coming from Subversion, I vividly remembered it being a complex and error-prone procedure. With some bad memories, I was anxious about working with branches and therefore tried to avoid it whenever I could.</p>

<p>It took me quite a while to understand that branching and merging work completely differently in Git than in most other systems—especially regarding its ease of use! So if you learned the concept of branches from another version control system (like Subversion), I recommend you forget your prior knowledge and start fresh. Let&#8217;s start by understanding why branches are so important in the first place.</p>

<h3>Why branches are essential</h3>

<p>Back in the days when I <em>didn&#8217;t</em> use branches, working on a new feature was a mess. Essentially, I had the choice between two equally bad workflows:</p>

<p>(a) I already knew that creating small, granular commits with only a few changes was a good version control habit. However, if I did this while developing a new feature, every commit would mingle my half-done feature with the main code base until I was done. It wasn&#8217;t very pleasant for my teammates to have my unfinished feature introduce bugs into the project.</p>

<p>(b) To avoid getting my work-in-progress mixed up with other topics (from colleagues or myself), I&#8217;d work on a feature in my separate space. I would create a copy of the project folder that I could work with quietly—and only commit my feature once it was complete. But committing my changes only at the end produced a single, giant, bloated commit that contained all the changes. Neither my teammates nor I could understand what exactly had happened in this commit when looking at it later.</p>

<p>I slowly understood that I had to make myself familiar with branches if I wanted to improve my coding.</p>

<h3>Working in contexts</h3>

<p>Any project has multiple contexts where work happens; each feature, bug fix, experiment, or alternative of your product is actually a context of its own. It can be seen as its own &#8220;topic,&#8221; clearly separated from other topics.</p>

<p>If you don&#8217;t separate these topics from each other with branching, you will inevitably increase the risk of problems. Mixing different topics in the same context:</p>

<ul>
<li>makes it hard to keep an overview—and with a lot of topics, it becomes almost impossible;</li>
<li>makes it hard to undo something that proved to contain a bug, because it&#8217;s already mingled with so much other stuff;</li>
<li>doesn&#8217;t encourage people to experiment and try things out, because they&#8217;ll have a hard time getting experimental code out of the repository once it&#8217;s mixed with stable code.</li>
</ul>

<p>Using branches gave me the confidence that I couldn&#8217;t mess up. In case things went wrong, I could always go back, undo, start fresh, or switch contexts.</p>

<h3>Branching basics</h3>

<p>Branching in Git actually only involves a handful of commands. Let&#8217;s look at a basic workflow to get you started.</p>

<p>To create a new branch based on your current state, all you have to do is pick a name and execute a single command on your command line. We&#8217;ll assume we want to start working on a new version of our contact form, and therefore create a new branch called &#8220;contact-form&#8221;:</p>

<pre>
<code>$ git branch contact-form</code>
</pre>

<p>Using the <code>git branch</code> command without a name specified will list all of the branches we currently have (and the &#8220;-v&#8221; flag provides us with a little more data than usual):</p>

<pre>
<code>$ git branch -v</code>
</pre>

<figure>
<img src="http://d.alistapart.com/402/branch-listing.jpg" alt="Git screen showing the current branches of contact-form.">
</figure>

<p>You might notice the little asterisk on the branch named &#8220;master.&#8221; This means it&#8217;s the currently active branch. So, before we start working on our contact form, we need to make this our active context:</p>

<pre>
<code>$ git checkout contact-form</code>
</pre>

<p>Git has now made this branch our current working context. (In Git lingo, this is called the &#8220;HEAD branch&#8221;). All the changes and every commit that we make from now on will only affect this single context—other contexts will remain untouched. If we want to switch the context to a different branch, we’ll simply use the <code>git checkout</code> command again.</p>

<p>In case we want to integrate changes from one branch into another, we can &#8220;merge&#8221; them into the current working context. Imagine we&#8217;ve worked on our &#8220;contact-form&#8221; feature for a while, and now want to integrate these changes into our &#8220;master&#8221; branch. All we have to do is switch back to this branch and call git merge:</p>

<pre>
<code>$ git checkout master
$ git merge contact-form</code>
</pre>

<h3>Using branches</h3>

<p>I would strongly suggest that you use branches extensively in your day-to-day workflow. Branches are one of the core concepts that Git was built around. They are extremely cheap and easy to create, and simple to manage—and there are <a href="http://www.git-tower.com/learn/ebook/command-line/branching-merging/branching-can-change-your-life">plenty of resources</a> out there if you’re ready to learn more about using them.</p>

<h2>Undoing things</h2>

<p>There&#8217;s one thing that I’ve learned as a programmer over the years: mistakes happen, no matter how experienced people are. You can&#8217;t avoid them, but you can have tools at hand that help you recover from them.</p>

<p>One of Git&#8217;s greatest features is that you can undo almost anything. This gives me the confidence to try out things without fear—because, so far, I haven&#8217;t managed to <em>really</em> break something beyond recovery.</p>

<h3>Amending the last commit</h3>

<p>Even if you craft your commits very carefully, it&#8217;s all too easy to forget adding a change or mistype the message. With the <code>&#8212;amend</code> flag of the <code>git commit</code> command, Git allows you to change the <em>very last</em> commit, and it’s a very simple fix to execute. For example, if you forgot to add a certain change and also made a typo in the commit subject, you can easily correct this:</p>

<pre>
<code>$ git add <b>some/changed/files</b>
$ git commit --amend -m "The message, this time without typos"</code>
</pre>

<p>There&#8217;s only one thing you should keep in mind: you should never amend a commit that has already been pushed to a remote repository. Respecting this rule, the “amend” option is a great little helper to fix the last commit.</p>

<p>(For more detail about the <code>amend</code> option, I recommend Nick Quaranto’s <a href="http://gitready.com/advanced/2009/01/12/fixing-broken-commit-messages.html">excellent walkthrough</a>.)</p>

<h3>Undoing local changes</h3>

<p>Changes that haven&#8217;t been committed are called “local.” All the modifications that are currently present in your working directory are &#8220;local&#8221; uncommitted changes.</p>

<p>Discarding these changes can make sense when your current work is&#8230; well&#8230; worse than what you had before. With Git, you can easily undo local changes and start over with the last committed version of your project.</p>

<p>If it&#8217;s only a single file that you want to restore, you can use the <code>git checkout</code> command:</p>

<pre>
<code>$ git checkout -- <b>file/to/restore</b></code>
</pre>

<p>Don&#8217;t confuse this use of the <code>checkout</code> command with switching branches (see above). If you use it with two dashes and (separated with a space!) the path to a file, it will discard the uncommitted changes in a given file.</p>

<p>On a bad day, however, you might even want to discard all your local changes and restore the complete project:</p>

<pre>
<code>$ git reset --hard HEAD</code>
</pre>

<p>This will replace all of the files in your working directory with the last committed revision. Just as with using the checkout command above, this will discard the local changes.</p>

<p>Be careful with these operations: since local changes haven&#8217;t been checked into the repository, there is no way to get them back once they are discarded!</p>

<h3>Undoing committed changes</h3>

<p>Of course, undoing things is not limited to local changes. You can also undo certain commits when necessary—for example, if you’ve introduced a bug.</p>

<p>Basically, there are two main commands to undo a commit:</p>

<h4>(a) git reset</h4>

<figure>
<img src="http://d.alistapart.com/402/reset-concept.jpg" alt="Illustration showing how the `git reset` command works.">
</figure>

<p>The <code>git reset</code> command really turns back time. You tell it which version you want to return to and it restores exactly this state—undoing all the changes that happened after this point in time. Just provide it with the hash ID of the commit you want to return to:</p>

<pre>
<code>$ git reset -- hard 2be18d9</code>
</pre>

<p>The <code>&#8212;hard</code> option is the easiest and cleanest approach, but it also wipes away all local changes that you might still have in your working directory. So, before doing this, make sure there aren&#8217;t any local changes you&#8217;ve set your heart on.</p>

<h4>(b) git revert</h4>

<figure>
<img src="http://d.alistapart.com/402/revert-concept.jpg" alt="Illustration showing how the `git revert` command works.">
</figure>

<p>The <code>git revert</code> command is used in a different scenario. Imagine you have a commit that you don&#8217;t want anymore—but the commits that came afterwards still make sense to you. In that case, you wouldn&#8217;t use the <code>git reset</code> command because it would undo all those later commits, too!</p>

<p>The <code>revert</code> command, however, only reverts the <em>effects</em> of a certain commit. It doesn’t remove any commits, like <code>git reset</code> does. Instead, it even creates a <em>new</em> commit; this new commit introduces changes that are just the opposite of the commit to be reverted. For example, if you deleted a certain line of code, <code>revert</code> will create a new commit that introduces exactly this line, again.</p>

<p>To use it, simply provide it with the hash ID of the commit you want reverted:</p>

<pre>
<code>$ git revert 2be18d9</code>
</pre>

<h2>Finding bugs</h2>

<p>When it comes to finding bugs, I must admit that I&#8217;ve wasted quite some time stumbling in the dark. I often knew that it <em>used</em> to work a couple of days ago—but I had no idea <em>where exactly</em> things went wrong. It was only when I found out about <code>git bisect</code> that I could speed up this process a bit. With the <code>bisect</code> command, Git provides a tool that helps you find the commit that introduced a problem.</p>

<p>Imagine the following situation: we know that our current version (tagged &#8220;2.0&#8221;) is broken. We also know that a couple of commits ago (our version &#8220;1.9&#8221;), everything was fine. The problem must have occurred somewhere in between.</p>

<figure>
<img src="http://d.alistapart.com/402/bisect-01.jpg" alt="Illustration showing the commits between working and broken versions.">
</figure>

<p>This is already enough information to start our bug hunt with <code>git bisect</code>:</p>

<pre>
<code>$ git bisect start
$ git bisect bad
$ git bisect good v1.9</code>
</pre>

<p>After starting the process, we told Git that our current commit contains the bug and therefore is &#8220;bad.&#8221; We then also informed Git which previous commit is definitely working (as a parameter to <code>git bisect good</code>).</p>

<p>Git then restores our project in the <em>middle</em> between the known good and known bad conditions:</p>

<figure>
<img src="http://d.alistapart.com/402/bisect-02.jpg" alt="Illustration showing that the bisect begins between the versions.">
</figure>

<p>We now test this version (for example, by running unit tests, building the app, deploying it to a test system, etc.) to find out if this state works—or already contains the bug. As soon as we know, we tell Git again—either with <code>git bisect bad</code> or <code>git bisect good</code>.</p>

<p>Let&#8217;s assume we said that this commit was still &#8220;bad.&#8221; This effectively means that the bug must have been introduced even earlier—and Git will again narrow down the commits in question:</p>

<figure>
<img src="http://d.alistapart.com/402/bisect-03.jpg" alt="Illustration showing how additional bisects will narrow the commits further.">
</figure>

<p>This way, you&#8217;ll find out very quickly where exactly the problem occurred. Once you know this, you need to call <code>git bisect reset</code> to finish your bug hunt and restore the project&#8217;s original state.</p>

<h2>A tool that can save your neck</h2>

<p>I must confess that my first encounter with Git wasn&#8217;t love at first sight. In the beginning, it felt just like my other experiences with version control: tedious and unhelpful. But with time, the practice became intuitive, and gained my trust and confidence.</p>

<p>After all, mistakes happen, no matter how much experience we have or how hard we try to avoid them. What separates the pro from the beginner is preparation: having a system in place that you can trust in case of problems. It helps you stay on top of things, especially in complex projects. And, ultimately, it helps you become a better professional.</p>

<h2>References</h2>

<ul>
<li>Feel free to <a href="http://www.git-tower.com/learn/ebook/command-line/advanced-topics/undoing-things">learn more about amending, reverting, and resetting commits</a>.</li>
<li>Make yourself familiar with &#8220;git bisect&#8221; with this <a href="http://www.metaltoad.com/blog/beginners-guide-git-bisect-process-elimination">detailed example</a>.</li>
<li>A detailed <a href="http://www.git-tower.com/learn/ebook/command-line/branching-merging/branching-can-change-your-life">introduction to branching</a>.</li>
</ul><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/QXYwqk3T2cE" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-02T14:00:58+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:66:"http://alistapart.com/article/git-the-safety-net-for-your-projects";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:11;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:36:"Running Code Reviews with Confidence";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:23:"Emma Jane Hogbin Westby";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:98:"http://feedproxy.google.com/~r/alistapart/main/~3/2RHB5rIvr6A/running-code-reviews-with-confidence";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:66:"http://alistapart.com/article/running-code-reviews-with-confidence";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:16839:"<p>Growing up, I learned there were two kinds of reviews I could seek out from my parents. One parent gave reviews in the form of a shower of praise. The other parent, the one with a degree from the Royal College of Art, would put me through a design crit. Today the reviews I seek are for my code, not my horse drawings, but it continues to be a process I both dread and crave.</p>

<p>In this article, I&#8217;ll describe my battle-tested process for conducting code reviews, highlighting the questions you should ask during the review process as well as the necessary version control commands to download and review someone&#8217;s work. I&#8217;ll assume your team uses Git to store its code, but the process works much the same if you&#8217;re using any other source control system.</p>

<p>Completing a peer review is time-consuming. In the last project where I introduced mandatory peer reviews, the senior developer and I estimated that it doubled the time to complete each ticket. The reviews introduced more context-switching for the developers, and were a source of increased frustration when it came to keeping the branches up to date while waiting for a code review. </p>

<p>The benefits, however, were huge. Coders gained a greater understanding of the whole project through their reviews, reducing silos and making onboarding easier for new people. Senior developers had better opportunities to ask why decisions were being made in the codebase that could potentially affect future work. And by adopting an ongoing peer review process, we reduced the amount of time needed for human quality assurance testing at the end of each sprint.</p>

<p>Let’s walk through the process. Our first step is to figure out exactly what we’re looking for.</p>

<h2>Determine the purpose of the proposed change</h2>

<p>Our code review should always begin in a ticketing system, such as Jira or GitHub. It doesn&#8217;t matter if the proposed change is a new feature, a bug fix, a security fix, or a typo: every change should start with a description of why the change is necessary, and what the desired outcome will be once the change has been applied. This allows us to accurately assess when the proposed change is complete. </p>

<p>The ticketing system is where you&#8217;ll track the discussion about the changes that need to be made after reviewing the proposed work. From the ticketing system, you’ll determine which branch contains the proposed code. Let’s pretend the ticket we’re reviewing today is 61524—it was created to fix a broken link in our website. It could just as equally be a refactoring, or a new feature, but I’ve chosen a bug fix for the example. No matter what the nature of the proposed change is, having each ticket correspond to only one branch in the repository will make it easier to review, and close, tickets.</p>

<p>Set up your local environment and ensure that you can reproduce what is currently the live site—complete with the broken link that needs fixing. When you apply the new code locally, you want to catch any regressions or problems it might introduce. You can only do this if you know, for sure, the difference between what is old and what is new.</p>

<h2>Review the proposed changes</h2>

<p>At this point you&#8217;re ready to dive into the code. I&#8217;m going to assume you&#8217;re working with Git repositories, on a branch-per-issue setup, and that the proposed change is part of a remote team repository. Working directly from the command line is a good universal approach, and allows me to create copy-paste instructions for teams regardless of platform.</p>

<p>To begin, update your local list of branches.</p>

<pre>
<code>git fetch</code>
</pre>

<p>Then list all available branches.</p>

<pre>
<code>git branch -a</code>
</pre>

<p>A list of branches will be displayed to your terminal window. It may appear something like this:</p>

<pre>
<code>* master
remotes/origin/master
remotes/origin/HEAD -> origin/master
remotes/origin/61524-broken-link</code>
</pre>

<p>The <code>*</code> denotes the name of the branch you are currently viewing (or have “checked out”). Lines beginning with <code>remotes/origin</code> are references to branches we’ve downloaded. We are going to work with a new, local copy of branch <code>61524-broken-link</code>.</p>

<p>When you clone your project, you&#8217;ll have a connection to the remote repository as a whole, but you won&#8217;t have a read-write relationship with each of the individual branches in the remote repository. You&#8217;ll make an explicit connection as you switch to the branch. This means if you need to run the command <code>git push</code> to upload your changes, Git will know which remote repository you want to publish your changes to.</p>

<pre>
<code>git checkout --track origin/61524-broken-link</code>
</pre>

<p>Ta-da! You now have your own copy of the branch for ticket 61524, which is connected (“tracked”) to the origin copy in the remote repository. You can now begin your review!</p>

<p>First, let&#8217;s take a look at the commit history for this branch with the command <code>log</code>.</p>

<pre>
<code>git log master..</code>
</pre>

<p>Sample output:</p>

<pre>
<code>Author: emmajane <emma@emmajane.net>
Date: Mon Jun 30 17:23:09 2014 -0400

Link to resources page was incorrectly spelled. Fixed.

Resolves #61524.</code>
</pre>

<p>This gives you the full log message of all the commits that are in the branch <code>61524-broken-link</code>, but are not also in the <code>master</code> branch. Skim through the messages to get a sense of what&#8217;s happening.</p>

<p>Next, take a brief gander through the commit itself using the <code>diff</code> command. This command shows the difference between two snapshots in your repository. You want to compare the code on your checked-out branch to the branch you’ll be merging “to”—which conventionally is the <code>master</code> branch.</p>

<pre>
<code>git diff master</code>
</pre>

<h3>How to read patch files</h3>

<p>When you run the command to output the difference, the information will be presented as a patch file. Patch files are ugly to read. You&#8217;re looking for lines beginning with <code>+</code> or <code>-</code>. These are lines that have been added or removed, respectively. Scroll through the changes using the up and down arrows, and press <code>q</code> to quit when you’ve finished reviewing. If you need an even more concise comparison of what&#8217;s happened in the patch, consider modifying the diff command to list the changed files, and then look at the changed files one at a time:</p>

<pre>
<code>git diff master --name-only
git diff master &lt;filename></code>
</pre>

<p>Let&#8217;s take a look at the format of a patch file.</p>

<pre>
<code>diff --git a/about.html b/about.html
index a3aa100..a660181 100644
	--- a/about.html
	+++ b/about.html
@@ -48,5 +48,5 @@
	(2004-05)

- A full list of &lt;a href="emmajane.net/events">public 
+ A full list of &lt;a href="http://emmajane.net/events">public 
presentations and workshops&lt;/a> Emma has given is available</code>
</pre>

<p>I tend to skim past the metadata when reading patches and just focus on the lines that start with <code>-</code> or <code>+</code>. This means I start reading at the line immediate following <code>@@</code>. There are a few lines of context provided leading up to the changes. These lines are indented by one space each. The changed lines of code are then displayed with a preceding <code>-</code> (line removed), or <code>+</code> (line added).</p>

<h3>Going beyond the command line</h3>

<p>Using a Git repository browser, such as gitk, allows you to get a slightly better visual summary of the information we&#8217;ve looked at to date. The version of Git that Apple ships with does not include gitk—I used <a href="http://brew.sh/">Homebrew</a> to re-install Git and get this utility. Any repository browser will suffice, though, and there are many <a href="http://git-scm.com/downloads/guis">GUI clients available on the Git website</a>.</p>

<pre>
<code>gitk</code>
</pre>

<p>When you run the command <code>gitk</code>, a graphical tool will launch from the command line. An example of the output is given in the following screenshot. Click on each of the commits to get more information about it. Many ticket systems will also allow you to look at the changes in a merge proposal side-by-side, so if you’re finding this cumbersome, click around in your ticketing system to find the comparison tools they might have—I know for sure GitHub offers this feature.</p>

<figure>
<img src="http://d.alistapart.com/402/gitk.jpg" alt="Screenshot of the gitk repository browser.">
</figure>

<p>Now that you&#8217;ve had a good look at the code, jot down your answers to the following questions:</p>

<ol>
<li>Does the code comply with your project&#8217;s identified coding standards?</li>
<li>Does the code limit itself to the scope identified in the ticket?</li>
<li>Does the code follow industry best practices in the most efficient way possible?</li>
<li>Has the code been implemented in the best possible way according to all of your internal specifications? It&#8217;s important to separate your preferences and stylistic differences from actual problems with the code.</li>
</ol>

<h2>Apply the proposed changes</h2>

<p>Now is the time to start up your testing environment and view the proposed change in context. How does it look? Does your solution match what the coder thinks they&#8217;ve built? If it doesn&#8217;t look right, do you need to clear the cache, or perhaps rebuild the Sass output to update the CSS for the project?</p>

<p>Now is the time to also test the code against whatever test suite you use.</p>

<ol>
<li>Does the code introduce any regressions?</li>
<li>Does the new code perform as well as the old code? Does it still fall within your project&#8217;s performance budget for download and page rendering times?</li>
<li>Are the words all spelled correctly, and do they follow any brand-specific guidelines you have?</li>
</ol>

<p>Depending on the context for this particular code change, there may be other obvious questions you need to address as part of your code review.</p>

<p>Do your best to create the most comprehensive list of everything you can find wrong (and right) with the code. It&#8217;s annoying to get dribbles of feedback from someone as part of the review process, so we&#8217;ll try to avoid &#8220;just one more thing&#8221; wherever we can.</p>

<h2>Prepare your feedback</h2>

<p>Let&#8217;s assume you&#8217;ve now got a big juicy list of feedback. Maybe you have no feedback, but I doubt it. If you&#8217;ve made it this far in the article, it&#8217;s because you love to comb through code as much as I do. Let your freak flag fly and let&#8217;s get your review structured in a usable manner for your teammates. </p>

<p>For all the notes you&#8217;ve assembled to date, sort them into the following categories:</p>

<ol>
<li>The code is broken. It doesn&#8217;t compile, introduces a regression, it doesn&#8217;t pass the testing suite, or in some way actually fails demonstrably. These are problems which absolutely must be fixed.</li>
<li>The code does not follow best practices. You have some conventions, the web industry has some guidelines. These fixes are pretty important to make, but they may have some nuances which the developer might not be aware of.</li>
<li>The code isn&#8217;t how you would have written it. You&#8217;re a developer with battle-tested opinions, and you know you’re right, you just haven’t had the chance to update the Wikipedia page yet to prove it.</li>
</ol>

<h2>Submit your evaluation</h2>

<p>Based on this new categorization, you are ready to engage in passive-aggressive coding. If the problem is clearly a typo and falls into one of the first two categories, go ahead and fix it. Obvious typos don&#8217;t really need to go back to the original author, do they? Sure, your teammate will be a little embarrassed, but they&#8217;ll appreciate you having saved them a bit of time, and you’ll increase the efficiency of the team by reducing the number of round trips the code needs to take between the developer and the reviewer. </p>

<p>If the change you are itching to make falls into the third category: stop. Do not touch the code. Instead, go back to your colleague and get them to describe their approach. Asking “why” might lead to a really interesting conversation about the merits of the approach taken. It may also reveal limitations of the approach to the original developer. By starting the conversation, you open yourself to the possibility that just maybe your way of doing things isn&#8217;t the only viable solution.</p>

<p>If you needed to make any changes to the code, they should be absolutely tiny and minor. You should not be making substantive edits in a peer review process. Make the tiny edits, and then add the changes to your local repository as follows:</p>

<pre>
<code>git add .
git commit -m "[#61524] Correcting &lt;list problem> identified in peer review."</code>
</pre>

<p>You can keep the message brief, as your changes should be minor. At this point you should push the reviewed code back up to the server for the original developer to double-check and review. Assuming you&#8217;ve set up the branch as a tracking branch, it should just be a matter of running the command as follows:</p>

<pre>
<code>git push</code>
</pre>

<p>Update the issue in your ticketing system as is appropriate for your review. Perhaps the code needs more work, or perhaps it was good as written and it is now time to close the issue queue.</p>

<p>Repeat the steps in this section until the proposed change is complete, and ready to be merged into the main branch.</p>

<h2>Merge the approved change into the trunk</h2>

<p>Up to this point you&#8217;ve been comparing a ticket branch to the master branch in the repository. This main branch is referred to as the &#8220;trunk&#8221; of your project. (It’s a tree thing, not an elephant thing.) The final step in the review process will be to merge the ticket branch into the trunk, and clean up the corresponding ticket branches.</p>

<p>Begin by updating your master branch to ensure you can publish your changes after the merge.</p>

<pre>
<code>git checkout master
git pull origin master</code>
</pre>

<p>Take a deep breath, and merge your ticket branch back into the main repository. As written, the following command will not create a new commit in your repository history. The commits will simply shuffle into line on the master branch, making <code>git log &#8722;&#8722;graph</code> appear as though a separate branch has never existed. If you would like to maintain the illusion of a past branch, simply add the parameter <code>&#8722;&#8722;no-ff</code> to the merge command, which will make it clear, via the graph history and a new commit message, that you have merged a branch at this point. Check with your team to see what&#8217;s preferred.</p>

<pre>
<code>git merge 61524-broken-link</code>
</pre>

<p>The merge will either fail, or it will succeed. If there are no merge errors, you are ready to share the revised master branch by uploading it to the central repository.</p>

<pre>
<code>git push</code>
</pre>

<p>If there are merge errors, the original coders are often better equipped to figure out how to fix them, so you may need to ask them to resolve the conflicts for you.</p>

<p>Once the new commits have been successfully integrated into the master branch, you can delete the old copies of the ticket branches both from your local repository and on the central repository. It&#8217;s just basic housekeeping at this point.</p>

<pre>
<code>git branch -d 61524-broken-link
git push origin --delete 61524-broken-link</code>
</pre>

<h2>Conclusion</h2>

<p>This is the process that has worked for the teams I&#8217;ve been a part of. Without a peer review process, it can be difficult to address problems in a codebase without blame. With it, the code becomes much more collaborative; when a mistake gets in, it&#8217;s because we both missed it. And when a mistake is found before it&#8217;s committed, we both breathe a sigh of relief that it was found when it was. </p>

<p>Regardless of whether you&#8217;re using Git or another source control system, the peer review process can help your team. Peer-reviewed code might take more time to develop, but it contains fewer mistakes, and has a strong, more diverse team supporting it. And, yes, I’ve been known to learn the habits of my reviewers and choose the most appropriate review style for my work, just like I did as a kid.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/2RHB5rIvr6A" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-02T13:58:21+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:66:"http://alistapart.com/article/running-code-reviews-with-confidence";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:12;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:63:"Rachel Andrew on the Business of Web Dev: Getting to the Action";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:13:"Rachel Andrew";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://feedproxy.google.com/~r/alistapart/main/~3/VCIPQgTpPaQ/getting-to-the-action";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:50:"http://alistapart.com/column/getting-to-the-action";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4503:"<p>Freelancers and self-employed business owners can choose from a huge number of conferences to attend in any given year. There are hundreds of industry podcasts, a constant stream of published books, and a never-ending supply of sites all giving advice. It is very easy to spend a lot of valuable time and money just attending, watching, reading, listening and hoping that somehow all of this good advice will take root and make our business a success.</p>

<p>However, all the good advice in the world won’t help you if you don’t act on it. While you might leave that expensive conference feeling great, did your attendance create a lasting change to your business? I was thinking about this subject while listening to <a href="http://workingoutpodcast.com/2014/08/06/14-not-following-through.html">episode 14 of the Working Out podcast</a>, hosted by Ashley Baxter and Paddy Donnelly. They were talking about following through, and how it is possible to “nod along” to good advice but never do anything with it.</p>

<p>If you have ever been sent to a conference by an employer, you may have been expected to report back. You might even have been asked to present to your team on the takeaway points from the event. As freelancers and business owners, we don’t have anyone making us consolidate our thoughts in that way. It turns out that the way I work gives me a fairly good method of knowing which things are bringing me value.</p>

<h2>Tracking actionable advice</h2>

<p>I’m a fan of the <a href="http://en.wikipedia.org/wiki/Getting_Things_Done">Getting Things Done</a> technique, and live by my to-do lists. I maintain a Someday/Maybe list in OmniFocus into which I add items that I want to do or at least investigate, but that aren’t a project yet.</p>

<p>If a podcast is worth keeping on my playlist, there will be items entered linking back to certain episodes. Conference takeaways might be a link to a site with information that I want to read. It might be an idea for an article to write, or instructions on something very practical such as setting up an analytics dashboard to better understand some data. The first indicator of a valuable conference is how many items I add during or just after the event.</p>

<p>Having a big list of things to do is all well and good, but it’s only one half of the story. The real value comes when I do the things on that list, and can see whether they were useful to my business. Once again, my GTD lists can be mined for that information.</p>

<p>When tickets go on sale for that conference again, do I have most of those to-do items still sat in Someday/Maybe? Is that because, while they sounded like good ideas, they weren’t all that relevant? Or, have I written a number of blog posts or had several articles published on themes that I started considering off the back of that conference? Did I create that dashboard, and find it useful every day? Did that speaker I was introduced to go on to become a friend or mentor, or someone I’ve exchanged emails with to clarify a topic I’ve been thinking about?</p>

<p>By looking back over my lists and completed items, I can start to make decisions about the real value to my business and life of the things I attend, read, and listen to. I’m able to justify the ticket price, time, and travel costs by making that assessment. I can feel confident that I’m not spending time and money just to feel as if I’m moving forward, yet gaining nothing tangible to show for it.</p>

<h2>A final thought on value</h2>

<p>As entrepreneurs, we have to make sure we are spending our time and money on things that will give us the best return. All that said, it is important to make time in our schedules for those things that we just <strong>enjoy</strong>, and in particular those things that do motivate and inspire us. I don’t think that <em>every</em> book you read or event you attend needs to result in a to-do list of actionable items.</p>

<p>What we need as business owners, and as people, is <em>balance</em>. We need to be able to see that the things we are doing are moving our businesses forward, while also making time to be inspired and refreshed to get that actionable work done.</p><h3>Footnotes</h3><ul class="the-footnotes"><li id="note1">1. Have any favorite hacks for getting maximum value from conferences, workshops, and books? Tell us in the comments!</li></ul><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/VCIPQgTpPaQ" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:92:"<a href="/topic/business">Business</a>, <a href="/topic/workflow-tools">Workflow & Tools</a>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-08-28T12:30:31+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:50:"http://alistapart.com/column/getting-to-the-action";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:13;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"10 Years Ago in ALA: Pocket Sized Design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:14:"Ethan Marcotte";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"http://feedproxy.google.com/~r/alistapart/main/~3/zGr02UT5rdI/10-years-ago-in-ala-pocket-sized-design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:71:"http://alistapart.com/blog/post/10-years-ago-in-ala-pocket-sized-design";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4903:"<p>The web doesn’t do “age” especially well. Any blog post or design article more than a few years old gets a raised eyebrow—heck, most people I meet haven’t read John Allsopp’s “<a href="http://www.alistapart.com/articles/dao">A Dao of Web Design</a>” or Jeffrey Zeldman’s “<a href="http://www.alistapart.com/articles/tohell/">To Hell With Bad Browsers</a>,” both as relevant to the web today as when they were first written. Meanwhile, I’ve got books on my shelves older than I am; most of my favorite films came out before I was born; and my iTunes library is riddled with music that’s decades, if not centuries, old.</p>

<p>(No, I don’t get invited to many parties. Why do you ask oh I get it)</p>

<p>So! It’s probably easy to look at “<a href="http://alistapart.com/article/pocket">Pocket-Sized Design</a>,” a lovely article by Jorunn Newth and Elika Etemad that just turned 10 years old, and immediately notice where it’s beginning to show its age. Written at a time when few sites were standards-compliant, and even fewer still were mobile-friendly, Newth and Etemad were urging us to think about life beyond the desktop. And when I first re-read it, it’s easy to chuckle at the points that feel like they’re from another age: there’s plenty of talk of screens that are “only 120-pixels wide”; of inputs driven by stylus, rather than touch; and of using the now-basically-defunct <code>handheld</code> media type for your CSS. Seems a bit quaint, right?</p>

<p>And yet.</p>

<p>Looking past a few of the details, it’s remarkable how well the article’s aged. Modern users may (or may not) manually “turn off in-line image loading,” but they <em>may</em> choose to use a mobile browser that <a href="http://www.opera.com/turbo">dramatically compresses your images</a>. We may scoff at the idea of someone browsing with a stylus, but handheld video game consoles are <a href="https://speakerdeck.com/anna/playing-with-game-console-browsers">impossibly popular when it comes to browsing the web</a>. And while there’s plenty of excitement in our industry for the latest versions of iOS and Android, running on the latest hardware, most of the web’s growth is happening on <a href="http://www.cnbc.com/id/101910355">cheaper hardware</a>, over <a href="http://www.ericsson.com/res/docs/2014/ericsson-mobility-report-june-2014.pdf" title="Ericsson’s 2014 Mobility Report (PDF)">slower networks</a> (PDF), and via <a href="https://twitter.com/scottjehl/status/344183854698991616">slim</a> <a href="https://twitter.com/scottjehl/status/332875425804079104">data</a> <a href="https://twitter.com/scottjehl/status/344183854698991616">plans</a>—so yes, 10 years on, it’s still true that “downloading to the device is likely to be [expensive], the processors are slow, and the memory is limited.”</p>

<p>In the face of all of that, what I love about Newth and Etemad’s article is just how sensible their solutions are. Rather than suggesting slimmed-down mobile sites, or investing in some device detection library, they take a decidedly standards-focused approach: </p>

<figure class="quote">
<blockquote>
Linearizing the page into one column works best when the underlying document structure has been designed for it. Structuring the document according to this logic ensures that the page organization makes sense not only in Opera for handhelds, but also in non-CSS browsers on both small devices and the desktop, in voice browsers, and in terminal-window browsers like Lynx.
</blockquote>
</figure>

<p>In other words, by thinking about the needs of the small screen first, you can layer on more complexity from there. And if you’re hearing shades of <a href="http://www.abookapart.com/products/mobile-first" title="Luke Wroblewski’s book, Mobile First">mobile first</a> and <a href="http://alistapart.com/article/testdriven" title="Scott Jehl’s article for A List Apart, “Test-Driven Progressive Enhancement”">progressive enhancement</a> here, you’d be right: they’re treating their markup—their <em>content</em>—as a foundation, and gently layering styles atop it to make it accessible to more devices, more places than ever before.</p>

<p>So, no: we aren’t using <code>@media handheld</code> or <code>display: none</code> for our small screen-friendly styles—but I don’t think that’s really the point of Newth and Etemad’s essay. Instead, they’re putting forward a process, a <em>framework</em> for designing beyond the desktop. What they’re arguing is for a truly <a href="http://trentwalton.com/2014/03/10/device-agnostic/">device-agnostic</a> approach to designing for the web, one that’s as relevant today as it was a decade ago.</p>

<p><i>Plus ça change, plus c’est la même chose.</i></p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/zGr02UT5rdI" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-08-25T12:30:30+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:71:"http://alistapart.com/blog/post/10-years-ago-in-ala-pocket-sized-design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:14;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:60:"Dependence Day: The Power and Peril of Third-Party Solutions";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:13:"Scott Fennell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:121:"http://feedproxy.google.com/~r/alistapart/main/~3/ghdCO-iH_Kc/dependence-day-the-power-and-peril-of-third-party-solutions";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:89:"http://alistapart.com/article/dependence-day-the-power-and-peril-of-third-party-solutions";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:16349:"<p>“Why don’t we just use this plugin?” That’s a question I started hearing a lot in the heady days of the 2000s, when open-source <abbr title="Content Management Systems">CMSes</abbr> were becoming really popular. We asked it optimistically, full of hope about the myriad solutions only a download away. As the years passed, we gained trustworthy libraries and powerful communities, but the graveyard of crufty code and abandoned services grew deep. Many solutions were easy to install, but difficult to debug. Some providers were eager to sell, but loath to support. </p>

<p>Years later, we’re still asking that same question—only now we’re less optimistic and even more dependent, and I’m scared to engage with anyone smart enough to build something I can’t. The emerging challenge for today’s dev shop is knowing how to take control of third-party relationships—and when to avoid them. I’ll show you my approach, which is to ask a different set of questions entirely. </p>

<h2>A web of third parties</h2>
<p>I should start with a broad definition of what it is to be third party: If it’s a person and I don’t compensate them for the bulk of their workload, they’re third party. If it’s a company or service and I don’t control it, it’s third party. If it’s code and my team doesn’t grasp every line of it, it’s third party. </p>

<p>The third-party landscape is rapidly expanding. Github has grown to almost <a href="https://github.com/about/press">7 million users</a> and the WordPress plugin repo is approaching <a href="https://wordpress.org/plugins/">1 billion downloads</a>. Many of these solutions are easy for clients and competitors to implement; meanwhile, I’m still in the lab debugging my custom code. The idea of selling original work seems oddly&hellip;old-fashioned. </p>

<p>Yet with so many third-party options to choose from, there are more chances than ever to veer off-course. </p>

<h2>What could go wrong?</h2>
<p>At a meeting a couple of years ago, I argued against using an external service to power a search widget on a client project. “We should do things ourselves,” I said. Not long after this, on the very same project, I argued in favor of a using a third party to consolidate <abbr title="Really Simple Syndication">RSS</abbr> feeds into a single document. “Why do all this work ourselves,” I said, “when this problem has already been solved?” My inconsistency was obvious to everyone. Being dogmatic about <em>not</em> using a third party is no better than flippantly jumping in with one, and I had managed to do both at once! </p>

<p>But in one case, I believed the third party was worth the risk. In the other, it wasn’t. I just didn’t know how to communicate those thoughts to my team.</p>

<p>I needed, in the parlance of our times, a <em>decision-making framework</em>. To that end, I’ve been maintaining a collection of points to think through at various stages of engagement with third parties. I’ll tour through these ideas using the search widget and the RSS digest as examples. </p>

<h2>The difference between a request and a goal</h2>

<p>This point often reveals false assumptions about what a client or stakeholder wants. In the case of the search widget, we began researching a service that our client specifically requested. Fitted with ajax navigation, full-text searching, and automated crawls to index content, it seemed like a lot to live up to. But when we asked our clients what <em>exactly</em> they were trying to do, we were surprised: they were entirely taken by the typeahead functionality; the other features were of very little perceived value. </p>

<p>In the case of the RSS “smusher,” we already had an in-house tool that took an array of feed URLs and looped through them in order, outputting <em>x</em> posts per feed in some bespoke format. They’re too good for our beloved multi-feed widget? But actually, the client had a distinctly different and worthwhile vision: they wanted <em>x</em> results from their array of sites in total, and they wanted them ordered by publication date, not grouped by site. I conceded. </p>

<p>It might seem like an obvious first step, but I have seen projects set off in the wrong direction because the end goal is unknown. In both our examples now, we’re clear about that and we’re ready to evaluate solutions. </p>

<h2>To dev or to download</h2>
<p>Before deciding to use a third party, I find that I first need to examine my own organization, often in four particular ways: strengths, weaknesses, betterment, and mission. </p>

<h3>Strengths and weaknesses</h3>
<p>The search task aligned well with our strengths because we had good front-end developers and were skilled at extending our CMS. So when asked to make a typeahead search, we felt comfortable betting on ourselves. Had we done it before? Not exactly, but we could think through it. </p>

<p>At that same time, backend infrastructure was a weakness for our team. We had happened to have a lot of turnover among our sysadmins, and at times it felt like we weren’t equipped to hire that sort of talent. As I was thinking through how we might build a feed-smusher of our own, I felt like I was tempting a weak underbelly. Maybe we’d have to set up a cron job to poll the desired URLs, grab feed content, and store that on our servers. Not rocket science, but cron tasks in particular were an albatross for us. </p>

<h3>Betterment of the team</h3>
<p>When we set out to achieve a goal for a client, it’s more than us doing work: it’s an opportunity for our team to better themselves by learning new skills. The best opportunities for this are the ones that present challenging but attainable tasks, which create <a href="http://www.reddit.com/r/incremental_games/comments/1xiiyp/mechanics_of_incremental_games_3_reward_frequency/">incremental rewards</a>. <a href="http://www.edutopia.org/blog/video-games-learning-student-engagement-judy-willis">Some researchers</a> cite this effect as a factor in gaming addiction. I’ve felt this myself when learning new things on a project, and those are some of my favorite work moments ever. Teams appreciate this and there is an organizational cost in missing a chance to pay them to learn. The typeahead search project looked like it could be a perfect opportunity to boost our skill level. </p>

<h3>Organizational mission</h3>
<p>If a new project aligns well with our mission, we’re going to resell it many times. It’s likely that we’ll want our in-house dev team to iterate on it, tailoring it to our needs. Indeed, we’ll have the budget to do so if we’re selling it a lot. No one had asked us for a feed-smusher before, so it didn’t seem reasonable to dedicate an <abbr title="Research and Development">R&amp;D</abbr> budget to it. In contrast, several other clients were interested in more powerful site search, so it looked like it would be time well spent. </p>

<p>We’ve now clarified our end goals and we’ve looked at how these projects align with our team. Based on that, we’re doing the search widget ourselves, and we’re outsourcing the feed-smusher. Now let’s look more closely at what happens next for both cases.</p>

<h2>Evaluating the unknown</h2>
<p>The frustrating thing about working with third parties is that the most important decisions take place when we have the least information. But there are some things we can determine before committing. Familiarity, vitality, extensibility, branding, and Service Level Agreements (SLAs) are all observable from afar. </p>

<h3>Familiarity: is there a provider we already work with?</h3>
<p>Although we’re going to increase the number of third-party <em>dependencies</em>, we’ll try to avoid increasing the number of third-party <em>relationships</em>. </p>

<p>Working with a known vendor has several potential benefits: they may give us volume pricing. Markup and style are likely to be consistent between solutions. And we just know them better than we’d know a new service. </p>

<h3>Vitality: will this service stick around?</h3>
<p>The worst thing we could do is get behind a service, only to have it shut down next month. A service with high vitality will likely (and rightfully) brag about enterprise clients by name. If it’s open source, it will have a passionate community of contributors. On the other hand, it could be advertising a shutdown. More often, it’s somewhere in the middle. Noting how often the service is updated is a good starting point in determining vitality. </p>

<h3>Extensibility: can this service adapt as our needs change?</h3>
<p>Not only do we have to evaluate the core service, we have to see how extensible it is by digging into its API. If a service is extensible, it’s more likely to fit for the long haul. </p>

<p>APIs can also present new opportunities. For example, imagine selecting an email-marketing provider with an API that exposes campaign data. This might allow us to build a dashboard for campaign performance in our CMS—a unique value-add for our clients, and a chance to keep our in-house developers invested and excited about the service.</p>

<h3>Branding: is theirs strong, or can you use your own?</h3>
<p>White-labeling is the practice of reselling a service with your branding instead of that of the original provider. For some companies, this might make good sense for marketing. I tend to dislike white-labeling. Our clients trust us to make choices, and we should be proud to display what those choices are. Either way, you want to ensure you’re comfortable with the brand you’ll be using.</p>

<h3>SLAs: what are you getting, beyond uptime?</h3>
<p>For client-side products, browser support is a factor: every external dependency represents another layer that could abandon older browsers before we’re ready. There’s also accessibility. Does this new third-party support users with accessibility needs to the degree that we require? Perhaps most important of all is support. Can we purchase a priority support plan that offers fast and in-depth help? </p>

<p>In the case of our feed-smusher service, there was no solution that ran the table. The most popular solution actually had a shutdown notice! There were a couple of smaller providers available, but we hadn’t worked with either before. Browser support and accessibility were moot since we’d be parsing the data and displaying it ourselves. The uptime concern was also diminished because we’d be sure to cache the results locally. Anyway, with viable candidates in hand, we can move on to more productive concerns than dithering between two similar solutions. </p>

<h2>Relationship maintenance</h2>
<p>If someone else is going to do the heavy lifting, I want to assume as much of the remaining burden as possible. Piloting, data collection, documentation, and in-house support are all valuable opportunities to buttress this new relationship. </p>

<p>As exciting as this new relationship is, we don’t want to go dashing out of the gates just yet. Instead, we’ll target clients for piloting and quarantine them before unleashing it any further. Cull suggestions from team members to determine good candidates for piloting, garnering a mix of edge-cases and the norm. </p>

<p>If the third party happens to collect data of any kind, we should also have an automated way to import a copy of it—not just as a backup, but also as a cached version we can serve to minimize latency. If we are serving a popular dependency from a CDN, we want to send a local version if that call should fail. </p>

<p>If our team doesn’t have a well-traveled directory of provider relationships, the backstory can get lost. Let a few months pass, throw in some personnel turnover, and we might forget why we even use a service, or why we opted for a particular package. Everyone on our team should know where and how to learn about our third-party relationships. </p>

<p>We don’t need every team member to be an expert on the service, yet we don’t want to wait for a third-party support staff to respond to simple questions. Therefore, we should elect an in-house subject-matter expert. It doesn’t have to be a developer. We just need somebody tasked with monitoring the service at regular intervals for API changes, shutdown notices, or new features. They should be able to train new employees and route more complex support requests to the third party. </p>

<p>In our RSS feed example, we knew we’d read their output into our database. We documented this relationship in our team’s most active bulletin, our <abbr title="Customer Relationship Management">CRM</abbr> software. And we made managing external dependencies a primary part of one team member’s job. </p>

<h2><abbr title="Do It Yourself">DIY</abbr>: a third party waiting to happen?</h2>
<p>Stop me if you’ve heard this one before: a prideful developer assures the team that they can do something themselves. It’s a complex project. They make something and the company comes to rely on it. Time goes by and the in-house product is doing fine, though there is a maintenance burden. Eventually, the developer leaves the company. Their old product needs maintenance, no one knows what to do, and since it’s totally custom, there is no such thing as a community for it. </p>

<p>Once you decide to build something in-house, how can you prevent that work from devolving into a resented, alien dependency?&nbsp; </p>

<ul>
<li><b>Consider pair-programming.</b> What better way to ensure that multiple people understand a product, than to have multiple people build it?</li>
<li><b>“Job-switch Tuesdays.”</b> When feasible, we have developers switch roles for an entire day. Literally, in our ticketing system, it’s as though one person is another. It’s a way to force cross-training without doubling the hours needed for a task.</li>
<li><b>Hold code reviews</b> before new code is pushed. This might feel slightly intrusive at first, but that passes. If it’s not readable, it’s not deployable. If you have project managers with a technical bent, empower them to ask questions about the code, too.</li>
<li><b>Bring moldy code into light</b> by displaying it as <a href="http://www.phpdoc.org/">phpDoc</a>, <a href="http://usejsdoc.org/">JSDoc</a>, or similar.</li>
<li><b>Beware the big.</b> Create hourly estimates in <a href="http://en.wikipedia.org/wiki/Planning_poker">Fibonacci increments</a>. As a project gets bigger, so does its level of uncertainty. The Fibonacci steps are biased against under-budgeting, and also provide a cue to opt out of projects that are too difficult to estimate. In that case, it&#8217;s likely better to toe-in with a third party instead of blazing into the unknown by yourself.</li>
</ul>

<p>All of these considerations apply to our earlier example, the typeahead search widget. Most germane is the provision to “beware the big.” When I say “big,” I mean that relative to what usually works for a given team. In this case, it was a deliverable that felt very familiar in size and scope: we were being asked to extend an open-source CMS. If instead we had been asked to <em>make</em> a CMS, alarms would have gone off. </p>

<h2>Look before you leap, and after you land</h2>
<p>It’s not that third parties are bad <em>per se</em>. It’s just that the modern web team strikes me as a strange place: not only do we stand on the shoulders of giants, we do so without getting to know them first—and we hoist our organizations and clients up there, too. </p>

<p>Granted, there are many things you shouldn’t do yourself, and it’s possible to hurt your company by trying to do them—<a href="http://en.wikipedia.org/wiki/Not_invented_here">NIH</a> is a problem, not a goal. But when teams err too far in the other direction, developers become disenfranchised, components start to look like spare parts, and clients pay for solutions that aren’t quite right. Using a third party versus staying in-house is a <em>big</em> decision, and we need to think hard before we make it. Use my line of questions, or come up with one that fits your team better. After all, you’re your own best dependency.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/ghdCO-iH_Kc" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-08-19T14:00:34+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:89:"http://alistapart.com/article/dependence-day-the-power-and-peril-of-third-party-solutions";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:15;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:54:"One Step Ahead: Improving Performance with Prebrowsing";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:19:"Santiago Valdarrama";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:115:"http://feedproxy.google.com/~r/alistapart/main/~3/McnEJXwo09Q/one-step-ahead-improving-performance-with-prebrowsing";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:83:"http://alistapart.com/article/one-step-ahead-improving-performance-with-prebrowsing";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:11726:"<p>We all want our websites to be fast. We optimize images, create CSS sprites, use CDNs, cache aggressively, and gzip and minimize static content. We use every trick in the book. </p>

<p>But we can still do more. If we want faster outcomes, we have to think differently. What if, instead of leaving our users to stare at a spinning wheel, waiting for content to be delivered, we could predict where they wanted to go next? What if we could have that content ready for them before they even ask for it?</p>

<p>We tend to see the web as a reactive model, where every action causes a reaction. Users click, then we take them to a new page. They click again, and we open another page. But we can do better. We can be proactive with prebrowsing.</p>

<h2>The three big techniques</h2>

<p>Steve Souders coined the term prebrowsing (from <em>predictive browsing</em>) in one of his <a href="http://www.stevesouders.com/blog/2013/11/07/prebrowsing/">articles</a> late last year. Prebrowsing is all about anticipating where users want to go and preparing the content ahead of time. It’s a big step toward a faster and less visible internet.</p>

<p>Browsers can analyze patterns to predict where users are going to go next, and start DNS resolution and TCP handshakes as soon as users hover over links. But to get the most out of these improvements, we can enable prebrowsing on our web pages, with three techniques at our disposal:</p><ul>
<li>DNS prefetching</li>
<li>Resource prefetching</li>
<li>Prerendering</li>
</ul>
<p>Now let’s dive into each of these separately.</p>

<h2>DNS prefetching</h2>

<p>Whenever we know our users are likely to request a resource from a different domain than our site, we can use DNS prefetching to warm the machinery for opening the new URL. The browser can pre-resolve the DNS for the new domain ahead of time, saving several milliseconds when the user actually requests it. We are anticipating, and preparing for an action.</p>

<p>Modern browsers are very good at parsing our pages, looking ahead to pre-resolve all necessary domains ahead of time. Chrome goes as far as keeping an internal list with all related domains every time a user visits a site, pre-resolving them when the user returns (you can see this list by navigating to chrome://dns/ in your Chrome browser). However, sometimes access to new URLs may be hidden behind redirects or embedded in JavaScript, and that’s our opportunity to help the browser.</p>

<p>Let’s say we are downloading a set of resources from the domain cdn.example.com using a JavaScript call after a user clicks a button. Normally, the browser would have to resolve the DNS at the time of the click, but we can speed up the process by including a <code>dns-prefetch</code> directive in the <code>head</code> section of our page:</p>

<pre>
<code class="markup">&lt;link rel="dns-prefetch" href="http://cdn.example.com"&gt;</code>
</pre>

<p>Doing this informs the browser of the existence of the new domain, and it will combine this hint with its own pre-resolution algorithm to start a DNS resolution as soon as possible. The entire process will be faster for the user, since we are shaving off the time for DNS resolution from the operation. (Note that browsers do not guarantee that DNS resolution will occur ahead of time; they simply use our hint as a signal for their own internal pre-resolution algorithm.)</p>

<p>But exactly how much faster will pre-resolving the DNS make things? In your Chrome browser, open chrome://histograms/DNS and search for DNS.PrefetchResolution. You’ll see a table like this:</p>

<figure><img src="http://d.alistapart.com/401/fig1-o.jpg" alt="Histogram for DNS.PrefetchResolution"></figure>

<p>This histogram shows my personal distribution of latencies for DNS prefetch requests. On my computer, for 335 samples, the average time is 88 milliseconds, with a median of approximately 60 milliseconds. Shaving 88 milliseconds off every request our website makes to an external domain? That’s something to celebrate.</p>

<p>But what happens if the user never clicks the button to access the cdn.example.com domain? Aren’t we pre-resolving a domain in vain? We are, but luckily for us, DNS prefetching is a very low-cost operation; the browser will need to send only a few hundred bytes over the network, so the risk incurred by a preemptive DNS lookup is very low. That being said, don’t go overboard when using this feature; prefetch only domains that you are confident the user will access, and let the browser handle the rest.</p>

<p>Look for situations that might be good candidates to introduce DNS prefetching on your site:</p><ul>
<li>Resources on different domains hidden behind 301 redirects</li>
<li>Resources accessed from JavaScript code</li>
<li>Resources for analytics and social sharing (which usually come from different domains)</li>
</ul>
<p>DNS prefetching is currently supported on <a href="http://msdn.microsoft.com/en-us/library/ie/dn265039(v=vs.85).aspx">IE11</a>, <a href="http://www.chromium.org/developers/design-documents/dns-prefetching">Chrome</a>, Chrome Mobile, Safari, <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Controlling_DNS_prefetching">Firefox</a>, and Firefox Mobile, which makes this feature widespread among current browsers. Browsers that don’t currently support DNS prefetching will simply ignore the hint, and DNS resolution will happen in a regular fashion.</p>

<h2>Resource prefetching</h2>

<p>We can go a little bit further and predict that our users will open a specific page in our own site. If we know some of the critical resources used by this page, we can instruct the browser to prefetch them ahead of time:<br /></p><pre>
<code>&lt;link rel="prefetch" href="http://cdn.example.com/library.js"&gt;</code>
</pre>

<p>The browser will use this instruction to prefetch the indicated resources and store them on the local cache. This way, as soon as the resources are actually needed, the browser will have them ready to serve.</p>

<p>Unlike DNS prefetching, resource prefetching is a more expensive operation; be mindful of how and when to use it. Prefetching resources can speed up our websites in ways we would never get by merely prefetching new domains—but if we abuse it, our users will pay for the unused overhead.</p>

<p>Let’s take a look at the average response size of some of the most popular resources on a web page, courtesy of the <a href="http://httparchive.org/interesting.php#responsesizes">HTTP Archive</a>:</p>

<figure><img src="http://d.alistapart.com/401/fig2-o.jpg" alt="Chart of average response size of web page resources"></figure>

<p>On average, prefetching a script file (like we are doing on the example above) will cause 16kB to be transmitted over the network (without including the size of the request itself). This means that we will save 16kB of downloading time from the process, plus server response time, which is amazing—provided it’s later accessed by the user. If the user never accesses the file, we actually made the entire workflow slower by introducing an unnecessary delay.</p>

<p>If you decide to use this technique, prefetch only the most important resources, and make sure they are cacheable by the browser. Images, CSS, JavaScript, and font files are usually good candidates for prefetching, but HTML responses are not since they aren’t cacheable.</p>

<p>Here are some situations where, due to the likelihood of the user visiting a specific page, you can prefetch resources ahead of time:</p><ul>
<li>On a login page, since users are usually redirected to a welcome or dashboard page after logging in</li>
<li>On each page of a linear questionnaire or survey workflow, where users are visiting subsequent pages in a specific order</li>
<li>On a multi-step animation, since you know ahead of time which images are needed on subsequent scenes</li>
</ul>
<p>Resource prefetching is currently supported on <a href="http://msdn.microsoft.com/en-us/library/ie/dn265039(v=vs.85).aspx">IE11</a>, <a href="https://developers.google.com/chrome/whitepapers/prerender?csw=1">Chrome</a>, Chrome Mobile, <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Link_prefetching_FAQ">Firefox</a>, and Firefox Mobile. (To determine browser compatibility, you can run a quick browser test on <a href="http://prebrowsing.com">prebrowsing.com</a>.)</p>

<h2>Prerendering</h2>
<p>What about going even further and asking for an entire page? Let’s say we are <em>absolutely</em> sure that our users are going to visit the about.html page in our site. We can give the browser a hint:</p>

<pre>
<code>&lt;link rel="prerender" href="http://example.com/about.html"&gt;</code>
</pre>

<p>This time the browser will download and render the page in the background ahead of time, and have it ready for the user as soon as they ask for it. The transition from the current page to the prerendered one would be instantaneous.</p>

<p>Needless to say, prerendering is the most risky and costly of these three techniques. Misusing it can cause major bandwidth waste—especially harmful for users on mobile devices. To illustrate this, let’s take a look at this chart, also courtesy of the <a href="http://httparchive.org/trends.php#bytesTotal&amp;reqTotal">HTTP Archive</a>:</p>

<figure><img src="http://d.alistapart.com/401/fig3-o.jpg" alt="Graph of total transfer size and total requests to render a web page"></figure>

<p>In June of this year, the average number of requests to render a web page was 96, with a total size of 1,808kB. So if your user ends up accessing your prerendered page, then you’ve hit the jackpot: you’ll save the time of downloading almost 2,000kB, plus server response time. But if you’re wrong and your user never accesses the prerendered page, you’ll make them pay a very high cost.</p>

<p>When deciding whether to prerender entire pages ahead of time, consider that Google prerenders the top results on its search page, and Chrome prerenders pages based on the historical navigation patterns of users. Using the same principle, you can detect common usage patterns and prerender target pages accordingly. You can also use it, just like resource prefetching, on questionnaires or surveys where you know users will complete the workflow in a particular order.</p>

<p>At this time, prerendering is only supported on <a href="http://msdn.microsoft.com/en-us/library/ie/dn265039(v=vs.85).aspx">IE11</a>, <a href="https://developers.google.com/chrome/whitepapers/prerender?csw=1">Chrome</a>, and Chrome Mobile. Neither Firefox nor Safari have added support for this technique yet. (And as with resource prefetching, you can check <a href="http://prebrowsing.com">prebrowsing.com</a> to test whether this technique is supported in your browser.)</p>

<h2>A final word</h2>
<p>Sites like <a href="http://googlewebmastercentral.blogspot.com/2011/06/announcing-instant-pages.html">Google</a> and <a href="http://blogs.bing.com/search/2013/10/14/a-deeper-look-at-task-completion/">Bing</a> are using these techniques extensively to make search instant for their users. Now it’s time for us to go back to our own sites and take another look. Can we make our experiences better and faster with prefetching and prerendering?</p>

<p>Browsers are already working behind the scenes, looking for patterns in our sites to make navigation as fast as possible. Prebrowsing builds on that: we can combine the insight we have on our own pages with further analysis of user patterns. By helping browsers do a better job, we speed up and improve the experience for our users.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/McnEJXwo09Q" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-08-19T14:00:16+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:83:"http://alistapart.com/article/one-step-ahead-improving-performance-with-prebrowsing";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:16;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"Valediction";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:15:"Jeffrey Zeldman";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:73:"http://feedproxy.google.com/~r/alistapart/main/~3/v0JRauu8zwQ/valediction";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:43:"http://alistapart.com/blog/post/valediction";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2177:"<p>When I first met <a href="http://bearskinrug.co.uk/">Kevin Cornell</a> in the early 2000s, he was employing his illustration talent mainly to draw caricatures of his fellow designers at a small Philadelphia design studio. Even in that rough, dashed-off state, his work floored me. It was as if <a href="http://www.charlesaddams.com">Charles Addams</a> and my favorite <cite>Mad Magazine</cite> illustrators from the 1960s had blended their DNA to spawn the perfect artist.</p>

<p>Kevin would deny that label, but artist he is. For there is a vision in his mind, a way of seeing the world, that is unlike anyone else’s—and he has the gift to make you see it too, and to delight, inspire, and challenge you with what he makes you see.</p>

<p>Kevin was part of a small group of young designers and artists who had recently completed college and were beginning to establish careers. Others from that group included <a href="http://v5.robweychert.com/about/">Rob Weychert</a>, <a href="http://mattsutter.com">Matt Sutter</a>, and <a href="http://jasonsantamaria.com">Jason Santa Maria</a>. They would all go on to do fine things in our industry. </p>

<p>It was Jason who brought Kevin on as house illustrator during the <a href="http://alistapart.com/article/ala40"><cite>A List Apart</cite> 4.0 brand overhaul</a> in 2005, and Kevin has worked his strange magic for us ever since. If you’re an <cite>ALA</cite> reader, you know how he translates the abstract web design concepts of our articles into concrete, witty, and frequently absurd situations. Above all, he is a storyteller—if pretentious designers and marketers haven’t sucked all the meaning out of that word. </p>

<p>For nearly 10 years, Kevin has taken our well-vetted, practical, frequently technical web design and development pieces, and elevated them to the status of classic <cite>New Yorker</cite> articles. Tomorrow he publishes his last new illustrations with us. There will never be another like him. And for whatever good it does him, Kevin Cornell has my undying thanks, love, and gratitude. </p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/v0JRauu8zwQ" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-08-18T12:30:49+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:43:"http://alistapart.com/blog/post/valediction";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:17;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"My Favorite Kevin Cornell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:9:"ALA Staff";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:87:"http://feedproxy.google.com/~r/alistapart/main/~3/kLBd_yugW7w/my-favorite-kevin-cornell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:57:"http://alistapart.com/blog/post/my-favorite-kevin-cornell";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:15933:"<p>After 200 issues—yes, <em>two hundred</em>—Kevin Cornell is retiring from his post as <cite>A List Apart</cite>’s staff illustrator. Tomorrow’s issue will be the last one featuring new illustrations from him.</p>

<p>Sob.</p>

<p>For years now, we’ve eagerly awaited Kevin’s illustrations each issue, opening his files with all the patience of a kid tearing into a new LEGO set.</p>

<p>But after nine years and more than a few lols, it’s time to give Kevin’s beautifully deranged brain a rest.</p>

<p>We’re still figuring out what comes next for <cite>ALA</cite>, but while we do, we’re sending Kevin off the best way we know how: by sharing a few of our favorite illustrations. Read on for stories from <cite>ALA</cite> staff, past and present—and join us in thanking Kevin for his talent, his commitment, and his uncanny ability to depict seemingly any concept using animals, madmen, and circus figures.<br />
—</p>

<figure><img src="http://alistapart.com/d/thedisciplineofcontentstrategy/discipline-content-strategy.jpg" alt=""></figure>

<p>Of all the things I enjoyed about working on <cite>A List Apart</cite>, I loved anticipating the reveal: seeing Kevin’s illos for each piece, just before the issue went live. Every illustration was always a surprise—even to the staff. My favorite, hands-down, was his artwork for “<a href="http://alistapart.com/article/thedisciplineofcontentstrategy">The Discipline of Content Strategy</a>,” by Kristina Halvorson. In 2008, content was web design&#8217;s “elephant in the room” and Kevin’s visual metaphor nailed it. In a drawing, he encapsulated thoughts and feelings many had within the industry but were unable to articulate. That’s the mark of a master.</p>

<p>—Krista Stevens, <em>Editor-in-chief, 2006–2012</em></p>

<figure><img src="http://alistapart.com/d/future-ready-content/future-ready-content.jpg" alt=""></figure>

<p>In the fall of 2011, I submitted my first article to <cite>A List Apart</cite>. I was terrified: I didn’t know anyone on staff. The authors’ list read like a who’s who of web design. The archives were intimidating. But I had ideas, dammit. I hit send.</p>

<p>I told just one friend what I’d done. His eyes lit up. “Whoa. You&#8217;d get a Kevin Cornell!” he said.</p>

<p>Whoa indeed. I might get a Kevin Cornell?! I hadn’t even thought about that yet.</p>

<p>Like Krista, I fell in love with Kevin’s illustration for “The Discipline of Content Strategy”—an illustration that meant the world to me as I helped my clients see their own content elephants. The idea of having a Cornell of my own was exciting, but terrifying. Could I possibly write something worthy of his illustration?</p>

<p>Months later, there it was on the screen: little modular sandcastles illustrating my <a href="http://alistapart.com/article/future-ready-content">article on modular content</a>. I was floored.</p>

<p>Now, after two years as <cite>ALA</cite>’s editor in chief, I’ve worked with Kevin through dozens of issues. But you know what? I’m just as floored as ever.</p>

<p>Thank you, Kevin, you brilliant, bizarre, wonderful friend.</p>

<p>—Sara Wachter-Boettcher, <em>Editor-in-chief</em></p>

<figure><img src="http://alistapart.com/d/accessibilityseo/html_with_hat.jpg" alt=""></figure>

<p>It&#8217;s impossible for me to choose a favorite of Kevin’s body of work for <cite>ALA</cite>, because my favorite Cornell illustration is the witty, adaptable, humane language of characters and symbols underlying his years of work. If I <em>had</em> to pick a single illustration to represent the evolution of his visual language, I think it would be the hat-wearing nested egg with the winning smile that opened Andy Hagen’s “<a href="http://alistapart.com/article/accessibilityseo">High Accessibility is Effective Search Engine Optimization</a>.” An important article but not, perhaps, the juiciest title <cite>A List Apart</cite> has ever run…and yet there’s that little egg, grinning in his slightly dopey way.</p>

<p>If my memory doesn’t fail me, this is the second appearance of the nested Cornell egg—we saw the first a few issues before in <a href="http://alistapart.com/article/pdf_accessibility">Issue 201</a>, where it represented the nested components of an HTML page. When it shows up here, in Issue 207, we realize that the egg wasn’t a cute one-off, but the first syllable of a visual language that we’ll see again and again through the years. And what a language! Who else could make semantic markup seem not just clever, but shyly adorable?</p>

<p>A wander through the <cite>ALA</cite> archives provides a view of Kevin’s changing style, but something visible only backstage was his startlingly quick progression from reading an article to sketching initial ideas in conversation with then-creative director Jason Santa Maria to turning out a lovely miniature—and each illustration never failed to make me appreciate the article it introduced in a slightly different way. When I was at <cite>ALA</cite>, Kevin’s unerring eye for the important detail as a reader astonished me almost as much as his ability to give that (often highly technical, sometimes very dry) idea a playful and memorable visual incarnation. From the very first time his illustrations hit the <cite>A List Apart</cite> servers he’s shared an extraordinary gift with its readers, and as a reader, writer, and editor, I will always count myself in his debt.</p>

<p>—Erin Kissane, <em>Editor-in-chief, contributing editor, 1999–2009</em></p>

<figure><img src="http://alistapart.com/d/what-i-learned-about-the-web-in-2011/what-i-learned-about-the-web-in-2011.jpg" alt=""></figure>

<p>So much of what makes Kevin’s illustrations work are the gestures. The way the figure sits a bit slouched, but still perched on gentle tippy toes, determinedly occupied pecking away on his phone. With just a few lines, Kevin captures a mood and moment anyone can feel.</p>

<p>—Jason Santa Maria, <em>Former creative director</em></p>

<figure><img src="http://alistapart.com/d/ALA368_alaredesign_300.png" alt=""></figure>

<p>I’ve had the pleasure of working with Kevin on the illustrations for each issue of <cite>A List Apart</cite> since we launched the latest site redesign in early 2013. By working, I mean replying to his email with something along the lines of “Amazing!” when he sent over the illustrations every couple of weeks.</p>

<p>Prior to launching the new design, I had to go through the backlog of Kevin’s work for <cite>ALA</cite> and do the production work needed for the new layout. This bird’s eye view gave me an appreciation of the ongoing metaphorical world he had created for the magazine—the <a href="http://alistapart.com/article/putyourcontentinmypocket">birds</a>, <a href="http://alistapart.com/article/thedisciplineofcontentstrategy">elephants</a>, <a href="http://alistapart.com/article/semanticsinhtml5">weebles</a>, <a href="http://alistapart.com/article/audiences-outcomes-and-determining-user-needs">mad scientists</a>, <a href="http://alistapart.com/article/usable-yet-useless-why-every-business-needs-product-discovery">ACME products</a>, and other bits of amusing weirdness that breathed life into the (admittedly, sometimes) dry topics covered.</p>

<p>If I had to pick a favorite, it would probably be the illustration that accompanied the unveiling of the redesign, <a href="http://alistapart.com/article/a-list-apart-relaunches-new-features-new-design"><cite>A List Apart</cite> 5.0</a>. The shoe-shine man carefully working on his own shoes was the perfect metaphor for both the idea of design as craft and the back-stage nature of the profession—working to make others shine, so to speak. It was a simple and humble concept, and I thought it created the perfect tone for the launch.</p>

<p>—Mike Pick, <em>Creative director</em></p>

<figure><img src="http://alistapart.com/d/ALA383_sustainabledesign_300.png" alt=""></figure>

<p>So I can’t pick one favorite illustration that Kevin’s done. I just can’t. I could prattle on about <a href="http://alistapart.com/article/the-web-aesthetic" title="Paul Robert Lloyd’s “The Web Aesthetic”">this</a>, <a href="http://alistapart.com/article/everything-in-its-right-pace" title="Hannah Donovan’s “Everything in its Right Pace”">that</a>, or <a href="http://alistapart.com/article/orbital-content" title="Cameron Koczon’s “Orbital Content”">that other one</a>, and tell you everything I love about each of ’em. I mean, hell: I still have a print of the illustration he did for <a href="http://alistapart.com/article/whereourstandardswentwrong">my very first ALA article</a>. (The illustration is, of course, far stronger than the essay that follows it.)</p>

<p>But his illustration for <a href="http://alistapart.com/article/sustainable-web-design">James Christie’s excellent “Sustainable Web Design”</a> is a perfect example of everything I love about Kevin’s ALA work: how he conveys emotion with a few deceptively simple lines; the humor he finds in contrast; the occasional chicken. Like most of Kevin’s illustrations, I’ve seen it whenever I reread the article it accompanies, and I find something new to enjoy each time.</p>

<p>It’s been an honor working alongside your art, Kevin—and, on a few lucky occasions, having my words appear below it.</p>

<p>Thanks, Kevin.</p>

<p>—Ethan Marcotte, <em>Technical editor</em></p>

<figure><img src="http://alistapart.com/d/orbital-content/orbital-content.jpg" alt=""></figure>

<p>Kevin’s illustration for Cameron Koczon’s “<a href="http://alistapart.com/article/orbital-content">Orbital Content</a>” is one of the best examples I can think of to show off his considerable talent. Those balloons are just perfect: vaguely reminiscent of cloud computing, but tethered and within arm’s reach, and evoking the fun and chaos of carnivals and county fairs. No other illustrator I’ve ever worked with is as good at translating abstract concepts into compact, visual stories. <cite>A List Apart</cite> won’t be the same without him.</p>

<p>—Mandy Brown, <em>Former contributing editor</em></p>

<figure class="responsive-hero" data-picture data-alt="">
<div data-src="http://d.alistapart.com/_made/d/misc-images/rwd-1_2577_1165_60.jpg" ></div>
<div data-src="http://d.alistapart.com/_made/d/misc-images/rwd-2_1400_761_60.jpg" data-media="(max-width: 1400px)"></div>
<div data-src="http://d.alistapart.com/_made/d/misc-images/rwd-3_960_690_60.jpg" data-media="(max-width: 960px)"></div>
<div data-src="http://d.alistapart.com/_made/d/misc-images/rwd-4_450_736_60.jpg" data-media="(max-width: 450px)"></div>
<noscript><img src="http://d.alistapart.com/_made/d/ALA306_respdesign_300_960_439_10.jpg" alt=""></noscript>
</figure>

<p>Kevin has always had what seems like a preternatural ability to take an abstract technical concept and turn it into a <a href="http://alistapart.com/article/good-help-is-hard-to-find">clear and accessible illustration</a>.</p>

<p>For me, my favorite pieces are the ones he did for the 3rd anniversary of the original “<a href="http://alistapart.com/article/responsive-web-design">Responsive Web Design</a>” article…the web’s first “responsive” illustration? <em>Try squishing your browser here to see it in action—Ed</em></p>

<p>—Tim Murtaugh, <em>Technical director</em></p>

<figure><img src="http://alistapart.com/d/12lessonsCSSandstandards/twelve_lessons.jpg" alt=""></figure>

<p>I think it may be impossible for me to pick just one illustration of Kevin’s that I really like. Much like trying to pick your one favorite album or that absolutely perfect movie, picking a true favorite is simply folly. You can whittle down the choices, but it’s guaranteed that the list will be sadly incomplete and longer (much longer) than one.</p>

<p>If held at gunpoint, however ridiculous that sounds, and asked which of Kevin’s illustrations is my favorite, close to the top of the list would definitely be “<a href="http://alistapart.com/article/12lessonsCSSandstandards">12 Lessons for Those Afraid of CSS Standards</a>.” It’s just so subtle, and yet so pointed.</p>

<p>What I personally love the most about Kevin’s work is the overall impact it can have on people seeing it for the first time. It has become commonplace within our ranks to hear the phrase, “This is my new favorite Kevin Cornell illustration” with the publishing of each issue. And rightly so. His wonderfully simple style (which is also deceptively clever and just so smart) paired with the fluidity that comes through in his brush work is magical. Case in point for me would be his piece for “<a href="http://alistapart.com/article/the-problem-with-passwords">The Problem with Passwords</a>” which just speaks volumes about the difficulty and utter ridiculousness of selecting a password and security question.</p>

<p>We, as a team, have truly been spoiled by having him in our ranks for as long as we have. Thank you Kevin.</p>

<p>—Erin Lynch, <em>Production manager</em></p>

<figure><img src="http://alistapart.com/d/content-modelling-a-master-skill/content-modelling-a-master-skill.jpg" alt=""></figure>

<p>The elephant was my first glimpse at Kevin&#8217;s elegantly whimsical visual language. I first spotted it, a patient behemoth being studied by nonplussed little figures, atop Kristina Halvorson’s “<a href="http://alistapart.com/article/thedisciplineofcontentstrategy">The Discipline of Content Strategy</a>,” which made no mention of elephants at all. Yet the elephant added to my understanding: content owners from different departments focus on what&#8217;s nearest to them. The content strategist steps back to see the entire thing.</p>

<p>When Rachel Lovinger wrote about “<a href="http://alistapart.com/article/content-modelling-a-master-skill">Content Modelling</a>,” the elephant made a reappearance as a yet-to-be-assembled, stylized elephant doll. The unflappable elephant has also been the mascot of product development at the hands of a team trying to <a href="http://alistapart.com/article/connected-ux">construct it from user research</a>, strutted its stuff as <a href="http://alistapart.com/article/content-strategist-as-digital-curator">curated content</a>, enjoyed the diplomatic <a href="http://alistapart.com/article/tinker-tailor-content-strategist">guidance of a ringmaster</a>, and been <a href="http://alistapart.com/article/seeing-the-elephant-defragmenting-user-research">impersonated by a snake</a> to tell us that busting silos is helped by a better understanding of others’ discourse conventions.</p>

<p>The delight in discovering Kevin&#8217;s visual rhetoric doesn&#8217;t end there. With <a href="http://alistapart.com/article/avoidedgecases">doghouses</a>, <a href="http://alistapart.com/article/hattrick">birdhouses</a>, and <a href="http://alistapart.com/article/growing-your-design-business">fishbowls</a>, Kevin speaks of environments for users and workers. With <a href="http://alistapart.com/article/a-pixel-identity-crisis">owls</a> he represents the mobile experience and <a href="http://alistapart.com/article/smartphone-browser-landscape">smartphones</a>. With a team arranging themselves to fit into a group photo, he makes the concept of <a href="http://alistapart.com/article/responsive-web-design">responsive design</a> easier to grasp.</p>

<p>Not only has Kevin trained his hand and eye to produce the gestures, textures, and compositions that are uniquely his, but he has trained his mind to speak in a distinctive visual language—and he can do it on deadline. That is some serious mastery of the art.</p>

<p>—Rose Weisburd, <em>Columns editor</em></p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/kLBd_yugW7w" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-08-18T12:30:47+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:57:"http://alistapart.com/blog/post/my-favorite-kevin-cornell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:18;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"Measure Twice, Cut Once";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:17:"Anthony Colangelo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:84:"http://feedproxy.google.com/~r/alistapart/main/~3/lA0mO7gh2u0/measure-twice-cut-once";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:54:"http://alistapart.com/blog/post/measure-twice-cut-once";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2986:"<p>Not too long ago, I had a few rough days in support of a client project. The client had a big content release, complete with a media embargo and the like. I woke up on the day of the launch, and things were bad. I was staring straight into a wall of red.</p>

<figure><img src="http://alistapart.com/d/misc-images/Blog_images/crash.png" alt="A response and downtime report"></figure>

<p>Thanks to <a href="http://en.wikipedia.org/wiki/No_Silver_Bullet">the intrinsic complexity of software engineering</a>, these situations happen—<a href="http://cognition.happycog.com/article/under-pressure">I’ve been through them before, and I’ll certainly be through them again</a>. While the particulars change, there are two guiding principles I rely on when I find myself looking up that hopelessly tall cliff of red.</p>

<p>You can’t be at the top of your game while stressed and nervous about the emergency, so unless there’s an obvious, quick-to-deploy resolution, you need to give yourself some cover to work.</p>

<p>What that means will be unique to every situation, but as strange as it may sound, don’t dive into work on the be-all and end-all solution right off the bat. Take a few minutes to find a way to provide a bit of breathing room for you to build and implement the long-term solution in a stable, future-friendly way.</p>

<p>Ideally, the cover you’re providing shouldn’t affect the users too much. Consider beefing up your caching policies to lighten the load on your servers as much as possible. If there’s any functionality that is particularly taxing on your hardware and isn’t mission critical, disable it temporarily. Even if keeping the servers alive means pressing a button every 108 minutes like you’re <a href="http://en.wikipedia.org/wiki/Dharma_Initiative#Station_3:_The_Swan">Desmond from Lost</a>, do it.</p>

<p>After you’ve got some cover, work the problem slowly and deliberately. Think solutions through two or three times to be sure they’re the right course of action.</p>

<p>With the pressure eased, you don’t have to rush through a cycle of building, deploying, and testing potential fixes. Rushing leads to oversight of important details, and typically, that cycle ends the first time a change fixes (or seemingly fixes) the issue, which can lead to sloppy code and weak foundations for the future.</p>

<p>If the environment doesn’t allow you to ease the pressure enough to work slowly, go ahead and cycle your way to a hacky solution. But don’t forget to come back and work the root issue, or else temporary fixes will pile up and eat away at your system’s architecture like a swarm of termites.</p>

<p>Emergencies often require more thought and planning than everyday development, so be sure to give yourself the necessary time. Reactions alone may patch an issue, but thoughtfulness can solve it.</p>

<p>&nbsp;</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/lA0mO7gh2u0" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-08-15T16:50:12+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:54:"http://alistapart.com/blog/post/measure-twice-cut-once";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:19;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"How We Read";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:17:"Jason Santa Maria";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:73:"http://feedproxy.google.com/~r/alistapart/main/~3/C3Q4XTZD2ME/how-we-read";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:41:"http://alistapart.com/article/how-we-read";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:12445:"<p>I want you to think about what you’re doing right now. I mean <em>really</em> think about it. As your eyes move across these lines and funnel information to your brain, you’re taking part in a conversation I started with you. The conveyance of that conversation is the type you’re reading on this page, but you’re also filtering it through your experiences and past conversations. You’re putting these words into context. And whether you’re reading this book on paper, on a device, or at your desk, your environment shapes your experience too. Someone else reading these words may go through the same motions, but their interpretation is inevitably different from yours.</p>

<p>This is the most interesting thing about typography: it’s a chain reaction of time and place with you as the catalyst. The intention of a text depends on its presentation, but it needs you to give it meaning through reading.</p>

<p>Type and typography wouldn’t exist without our need to express and record information. Sure, we have other ways to do those things, like speech or imagery, but type is efficient, flexible, portable, and translatable. This is what makes typography not only an art of communication, but one of nuance and craft, because like all communication, its value falls somewhere on a spectrum between success and failure.</p>

<p>The act of reading is beautifully complex, and yet, once we know how, it’s a kind of muscle memory. We rarely think about it. But because reading is so intrinsic to every other thing about typography, it’s the best place for us to begin. We’ve all made something we wanted someone else to read, but have you ever thought about that person’s reading experience?</p>

<p>Just as you’re my audience for this book, I want you to look at your audience too: your readers. One of design’s functions is to entice and delight. We need to welcome readers and convince them to sit with us. But what circumstances affect reading?</p>

<h2>Readability</h2>

<p>Just because something is legible doesn’t mean it’s readable. <em>Legibility</em> means that text can be interpreted, but that’s like saying tree bark is edible. We’re aiming higher. <em>Readability</em> combines the emotional impact of a design (or lack thereof ) with the amount of effort it presumably takes to read. You’ve heard of <em>TL;DR</em> (too long; didn’t read)? Length isn’t the only detractor to reading; poor typography is one too. To <a href="http://bkaprt.com/owt/2/">paraphrase Stephen Coles</a>, the term readability doesn’t ask simply, “Can you read it?” but “Do you want to read it?”</p>

<p>Each decision you make could potentially hamper a reader’s understanding, causing them to bail and update their Facebook status instead. Don’t let your design deter your readers or stand in the way of what they want to do: <em>read</em>.</p>

<p>Once we bring readers in, what else can we do to keep their attention and help them understand our writing? Let’s take a brief look at what the reading experience is like and how design influences it.</p>

<h2>The act of reading</h2>

<p>When I first started designing websites, I assumed everyone read my work the same way I did. I spent countless hours crafting the right layout and type arrangements. I saw the work as a collection of the typographic considerations I made: the lovingly set headlines, the ample whitespace, the typographic rhythm (fig 1.1). I assumed everyone would see that too.</p>

<figure>
<img src="http://d.alistapart.com/400/OWT-fig1-1.png" alt="A normal paragraph of text">
<figcaption>Fig 1.1: A humble bit of text. But what actually happens when someone reads it?</figcaption></figure>

<p>It’s appealing to think that’s the case, but reading is a much more nuanced experience. It’s shaped by our surroundings (am I in a loud coffee shop or otherwise distracted?), our availability (am I busy with something else?), our needs (am I skimming for something specific?), and more. Reading is not only informed by what’s going on with us at that moment, but also governed by how our eyes and brains work to process information. What you <em>see</em> and what you’re <em>experiencing</em> as you read these words is quite different.</p>

<p>As our eyes move across the text, our minds gobble up the type’s <em>texture</em>—the sum of the positive and negative spaces inside and around letters and words. We don’t linger on those spaces and details; instead, our brains do the heavy lifting of parsing the text and assembling a mental picture of what we’re reading. Our eyes see the type and our brains see Don Quixote chasing a windmill.</p>

<p>Or, at least, that’s what we hope. This is the ideal scenario, but it depends on our design choices. Have you ever been completely absorbed in a book and lost in the passing pages? Me too. Good writing can do that, and good typography can grease the wheels. Without getting too scientific, let’s look at the physical process of reading.</p>

<h3>Saccades and fixations</h3>

<p>Reading isn’t linear. Instead, our eyes perform a series of back and forth movements called <em>saccades</em>, or lightning-fast hops across a line of text (fig 1.2). Sometimes it’s a big hop; sometimes it’s a small hop. Saccades help our eyes register a lot of information in a short span, and they happen many times over the course of a second. A saccade’s length depends on our proficiency as readers and our familiarity with the text’s topic. If I’m a scientist and reading, uh, science stuff, I may read it more quickly than a non-scientist, because I’m familiar with all those science-y words. Full disclosure: I’m not really a scientist. I hope you couldn’t tell.</p>

<figure>
<img src="http://d.alistapart.com/400/OWT-fig1-2.png" alt="Paragraph showing saccades or the movement our eyes make as we read a line of text">
<figcaption>Fig 1.2: Saccades are the leaps that happen in a split second as our eyes move across a line of text.</figcaption></figure>

<p>Between saccades, our eyes stop for a fraction of a second in what’s called a <em>fixation</em> (fig 1.3). During this brief pause we see a couple of characters clearly, and the rest of the text blurs out like ripples in a pond. Our brains assemble these fixations and decode the information at lightning speed. This all happens on reflex. Pretty neat, huh?</p>

<figure>
<img src="http://d.alistapart.com/400/OWT-fig1-3.png" alt="Paragraph showing the fixations or stopping points our eyes make as we read a paragraph">
<figcaption>Fig 1.3: Fixations are the brief moments of pause between saccades.</figcaption></figure>

<p>The shapes of letters and the shapes they make when combined into words and sentences can significantly affect our ability to decipher text. If we look at an average line of text and cover the top halves of the letters, it becomes very difficult to read. If we do the opposite and cover the bottom halves, we can still read the text without much effort (fig 1.4).</p>

<figure>
<img src="http://d.alistapart.com/400/OWT-fig1-4.png" alt="Paragraph showing how the upper half of letters are still readable to the human eyes">
<figcaption>Fig 1.4: Though the letters’ lower halves are covered, the text is still mostly legible, because much of the critical visual information is in the tops of letters.</figcaption></figure>

<p>This is because letters generally carry more of their identifying features in their top halves. The sum of each word’s letterforms creates the word shapes we recognize when reading.</p>

<p>Once we start to subconsciously recognize letters and common words, we read faster. We become more proficient at reading under similar conditions, an idea best encapsulated by type designer Zuzana Licko: “Readers read best what they read most.”</p>

<p>It’s not a hard and fast rule, but close. The more foreign the letterforms and information are to us, the more slowly we discern them. If we traveled back in time to the Middle Ages with a book typeset in a super-awesome sci-fi font, the folks from the past might have difficulty with it. But here in the future, we’re adept at reading that stuff, all whilst flying around on hoverboards.</p>

<p>For the same reason, we sometimes have trouble deciphering someone else’s handwriting: their letterforms and idiosyncrasies seem unusual to us. Yet we’re pretty fast at reading our own handwriting (fig 1.5).</p>

<figure>
<img src="http://d.alistapart.com/400/1-5.jpg" alt="Three paragraphs of handwritten text">
<figcaption>Fig 1.5: While you’re very familiar with your own handwriting, reading someone else’s (like mine!) can take some time to get used to.</figcaption></figure>

<p>There have been many studies on the reading process, with only a bit of consensus. Reading acuity depends on several factors, starting with the task the reader intends to accomplish. Some studies show that we read in <em>word shapes</em>—picture a chalk outline around an entire word—while others suggest we decode things letter by letter. Most findings agree that ease of reading relies on the visual feel and <em>precision</em> of the text’s setting (how much effort it takes to discern one letterform from another), combined with the reader’s own proficiency.</p>

<p>Consider a passage set in all capital letters (fig 1.6). You can become adept at reading almost anything, but most of us aren’t accustomed to reading lots of text in all caps. Compared to the normal sentence-case text, the all-caps text feels pretty impenetrable. That’s because the capital letters are blocky and don’t create much contrast between themselves and the whitespace around them. The resulting word shapes are basically plain rectangles (fig 1.7).</p>

<figure>
<img src="http://d.alistapart.com/400/OWT-fig1-6.png" alt="Paragraph illustrating the difficulty of reading text in all caps">
<figcaption>Fig 1.6: Running text in all caps can be hard to read quickly when we’re used to sentence case.</figcaption></figure>

<figure>
<img src="http://d.alistapart.com/400/OWT-fig1-7.png" alt="Paragraph showing how words are recognizable by the shapes they form">
<figcaption>Fig 1.7: Our ability to recognize words is affected by the shapes they form. All-caps text forms blocky shapes with little distinction, while mixed-case text forms irregular shapes that help us better identify each word.</figcaption></figure>

<p>Realizing that the choices we make in typefaces and typesetting have such an impact on the reader was eye-opening for me. Small things like the size and spacing of type can add up to great advantages for readers. When they don’t notice those choices, we’ve done our job. We’ve gotten out of their way and helped them get closer to the information.</p>

<h2>Stacking the deck</h2>

<p>Typography on screen differs from print in a few key ways. Readers deal with two reading environments: the physical space (and its lighting) and the device. A reader may spend a sunny day at the park reading on their phone. Or perhaps they’re in a dim room reading subtitles off their TV ten feet away. As designers, we have no control over any of this, and that can be frustrating. As much as I would love to go over to every reader’s computer and fix their contrast and brightness settings, this is the hand we’ve been dealt.</p>

<p>The best solution to unknown unknowns is to make our typography perform as well as it can in all situations, regardless of screen size, connection, or potential lunar eclipse. We’ll look at some methods for making typography as sturdy as possible later in this book.</p>

<p>It’s up to us to keep the reading experience unencumbered. At the core of typography is our audience, our readers. As we look at the building blocks of typography, I want you to keep those readers in mind. Reading is something we do every day, but we can easily take it for granted. Slapping words on a page won’t ensure good communication, just as mashing your hands across a piano won’t make for a pleasant composition. The experience of reading and the effectiveness of our message are determined by both <em>what</em> we say and <em>how</em> we say it. Typography is the primary tool we use as designers and visual communicators to speak.</p>

<p>&nbsp;</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/C3Q4XTZD2ME" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-08-05T11:00:05+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:41:"http://alistapart.com/article/how-we-read";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:20;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:47:"The Most Dangerous Word In Software Development";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:17:"Anthony Colangelo";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:109:"http://feedproxy.google.com/~r/alistapart/main/~3/Ei6wVulYF1M/the-most-dangerous-word-in-software-development";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:79:"http://alistapart.com/blog/post/the-most-dangerous-word-in-software-development";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:2991:"<p>“Just put it up on a server somewhere.”</p>

<p>“Just add a favorite button to the right side of the item.”</p>

<p>“Just add [insert complex option here] to the settings screen.”</p>

<p>Usage of the word “just” points to a lot of assumptions being made. A few months ago, <a href="https://twitter.com/brad_frost">Brad Frost</a> <a href="https://the-pastry-box-project.net/brad-frost/2014-january-28">shared some thoughts</a> on how the word applies to knowledge.</p><figure class="quote">
<blockquote>“Just” makes me feel like an idiot. “Just” presumes I come from a specific background, studied certain courses in university, am fluent in certain technologies, and have read all the right books, articles, and resources.</blockquote>
</figure>

<p>He points out that learning is never as easy as it is made to seem, and he’s right. But there is a direct correlation between the amount of knowledge you’ve acquired and the danger of the word “just.” The more you know, the bigger the problems you solve, and the bigger the assumptions are that are hiding behind the word.</p>

<p>Take the comment, “Just put it up on a server somewhere.” How many times have we heard that? But taking a side project running locally and deploying it on real servers requires time, money, and hard work. Some tiny piece of software somewhere will probably be the wrong version, and will need to be addressed. The system built locally probably isn’t built to scale perfectly.</p>

<p>“Just” implies that all of the thinking behind a feature or system has been done. Even worse, it implies that all of the decisions that will have to be made in the course of development have already been discovered—and that’s never the case.</p>

<p>Things change when something moves from concept to reality. As <a href="https://twitter.com/dwiskus">Dave Wiskus</a> said on a <a href="http://www.imore.com/debug-37-simmons-wiskus-gruber-and-vesper-sync">recent episode of Debug</a>, “everything changes when fingers hit glass.”</p>

<p>The favorite button may look fine on the right side, visually, but it might be in a really tough spot to touch. What about when favoriting isn’t the only action to be taken? What happens to the favorite button then?</p>

<p>Even once favoriting is built and in testing, it should be put through its paces again. In use, does favoriting provide enough value to warrant is existence? After all, <a href="http://gettingreal.37signals.com/ch05_Start_With_No.php">“once that feature’s out there, you’re stuck with it.”</a></p>

<p>When you hear the word “just” being thrown around, dig deep into that statement and find all of the assumptions made within it. Zoom out and think slow.</p>

<p>Your product lives and dies by the decisions discovered between ideation and creation, so don’t just put it up on a server somewhere.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/Ei6wVulYF1M" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-07-30T12:30:26+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:79:"http://alistapart.com/blog/post/the-most-dangerous-word-in-software-development";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:21;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"Gardens, Not Graves";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:9:"Allen Tan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:80:"http://feedproxy.google.com/~r/alistapart/main/~3/brzpjVbp608/gardens-not-graves";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:48:"http://alistapart.com/article/gardens-not-graves";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:11822:"<p>The stream—that great glut of ideas, opinions, updates, and ephemera that pours through us every day—is the dominant way we organize content. It makes sense; the stream’s popularity springs from the days of the early social web, when a huge number of users posted all types of content on unpredictable schedules. The simplest way to show updates to new readers focused on reverse chronology and small, discrete chunks, as sorting by newness called for content quick to both produce and digest. This approach saw wide adoption in blogs, social networks, notification systems, etc., and ever since we’ve flitted from one stream to another like sugar-starved hummingbirds.</p>

<p>Problem is, the stream’s emphasis on the new above all else imposes a short lifespan on content. Like papers piled on your desk, the stream makes it easy to find the last thing you’ve added, while anything older than a day effectively disappears. Solely relying on reverse-chronology turns our websites into graveyards, where things pile up atop each other until they fossilize. We need to start treating our websites as gardens, as places worthy of cultivation and renewal, where new things can bloom from the old.</p>

<h2>The stream, in print</h2>

<p>The stream’s focus on the <em>now</em> isn’t novel, anyway. Old-school modes of publishing like newspapers and magazines shared a similar disposability: periodic updates went out to subscribers and were then thrown away. No one was expected to hang onto them for long.</p>

<p>Over the centuries with print, however, we came up with a number of ways to preserve and showcase older material. Newspapers put out <a href="http://en.wikipedia.org/wiki/New_York_Times_Index">annual indexes</a> cataloguing everything they print ordered by subject and frequency. Magazines get rebound into <a href="http://us.macmillan.com/theparisreviewinterviewsboxedsetiiv/TheParisReview">larger, more substantial anthologies</a>. Publishers frequently reach into their back catalogue and reprint books with new forewords or even chapters. These acts serve two purposes: to maintain widespread and cheap access to material that has gone out of print, and to ensure that material is still relevant and useful today.</p>

<p>But we haven’t yet developed patterns for slowing down on the web. In some ways, access is simpler. As long as the servers stay up, content remains a link away from interested readers. But that same ease of access makes the problem of outdated or redundant content more pronounced. Someone looking at an old magazine article also holds the entire issue it was printed with. With an online article, someone can land directly on the piece with little indication of who it’s by, what it’s for, and whether it’s gone out of date. Providing sufficient context for content already out there is a vital factor to consider and design for.</p>

<p>You don’t need to be a writer to help fix this. Solutions can come from many fields, from targeted writing and design tweaks to more overarching changes in content strategy and information architecture.</p>

<p>Your own websites are good places to start. Here are some high-level guidelines, ordered by the amount of effort they’ll take. Your site will demand its own unique set of approaches, though, so recombine and reinvent as needed.</p>

<h2>Reframe</h2>

<p><em>Emma is a travel photographer. She keeps a blog, and many years ago she wrote a series about visiting Tibet. Back then, she was required to travel with a guided tour. That’s no longer the case, as visitors only need to obtain a permit.</em></p>

<p>The most straightforward thing to do is to look through past content and identify what’s outdated: pieces you’ve written, projects you worked on, things you like. The goal is triage: sorting things into what needs attention and what’s still fine.</p>

<p>Once you’ve done that, find a way to signal their outdated status. Perhaps you have a design template for “archived” content that has a different background color, more strongly emphasizes when it was written, or <a href="http://24ways.org/2008/geotag-everywhere-with-fire-eagle/">adds a sentence or two at the top of your content that explains why it’s outdated</a>. If entire groups of content need mothballing, see whether it makes sense to pull them into separate areas. (Over time, you may have to overhaul the way your entire site is organized—a complicated task we’ll address below.)</p>

<p><em>Emma adds an <code>&lt;outdated&gt;</code> tag to her posts about her guided tour and configures the site’s template to show a small yellow notification at the top telling visitors that her information is from 2008 and may be irrelevant. She also adds a link on each post pointing to a site that explains the new visa process and ways to obtain Tibetan permits.</em></p>

<p>On the flip side, separate the pieces that you’re particularly proud of. Your “best-of” material is probably getting scattered by the reverse-chronology organization of your website, so list all of them in a prominent place for people visiting for the first time.</p>

<h2>Recontextualize</h2>

<p>I hope that was easy! The next step is to look for old content you feel differently about today.</p>

<p><em>When Emma first started traveling, she hated to fly. She hated waiting in line, hated sitting in cramped seats, and especially hated the food. There are many early blog posts venting about this.</em></p>

<p>Maybe what you wrote needs additional nuance or more details. Or maybe you’ve changed since then. Explain why—lead readers down the learning path you took. It’s a chance for you to reflect on the delta.</p>

<p><em>Now that she’s gotten more busy and has to frequently make back-to-back trips for clients, she finds that planes are the best time for her to edit photos from the last trip, catch up on email, and have some space for reflection. So she writes about how she fills up her flying time now, leaving more time when she’s at her destination to shoot and relax.</em></p>

<p>Or expand on earlier ideas. What started as a rambling post you began at midnight can turn into a series or an entire side project. Or, if something you wrote provokes a big response online, you could gather those links at the bottom of your piece. It’s a service to your new readers to collect connected pieces together, so that they don’t have to hunt around to find them all.</p>

<h2>Revise and reorganize</h2>

<p>Hopefully that takes care of most of your problematic content. But for content so dire you’re embarrassed to even look at it, much less having other people reading it, consider more extreme measures: the act of culling, revising, and rewriting.</p>

<p>Looking back: maybe you were <em>completely wrong</em> about something, and you would now argue the opposite. Rewrite it! Or you’re shocked to find code you wrote one rushed Friday afternoon—well, set aside some time to start from the ground up and do it right.</p>

<p><em>Emma started her website years ago as a typical reverse-chron blog, but has started to work on a redesign based around the concepts of LOCATIONS and TRIPS. Appearing as separate items in the navigation, they act as different ways for readers to approach and make sense of her work. The locations present an at-a-glance view of where she’s been and how well-traveled she is. The trips (labeled Antarctica: November 2012, Bangkok: Fall 2013, Ghana: early 2014, etc.) retain the advantages of reverse-chronology by giving people updates on what she’s done recently, but these names are more flexible and easier to explain than dates and timestamps on their own. Someone landing directly on a post from a trip two years ago can easily get to the other posts from that trip, but they would be lost if the entries were only timestamped.</em></p>

<p>If the original structure no longer matches the reality of what’s there, it’s also the best case for redesigning and reorganizing your website. Now is the time to consider your content as a whole. Think about how you’d explain your website to someone you’re having lunch with. Are you a writer, photographer, artist, musician, cook? What kind? What sorts of topics does your site talk about? What do you want people to see first? How do they go deeper on the things they find interesting? This gets rather existential, but it’s important to ask yourself.</p>

<h2>Remove</h2>

<p>If it’s really, truly <em>foul</em>, you can throw it out. (It’s okay. You officially have permission.) Not everything needs to live online forever, but throwing things out doesn’t have to be your first option when you get embarrassed by the past.</p>

<p>Deploying the internet equivalent of space lasers does, I must stress, come with some responsibility. Other sites can be affected by changes in your links:</p>

<ul>
<li>If you’re consolidating or moving content, it’s important to set up redirects for affected URLs to the new pages.</li>
<li>If someone links to a tutorial you wrote, it may be better to archive it and link to more updated information, rather than outright deleting it.</li>
</ul>

<h2>Conclusion</h2>

<p>Everything we’ve done so far applies to more than personal websites, of course. Where else?</p>

<p>Businesses have to maintain scores of announcements, documentation, and customer support. Much of it is subject to greatly change over time, and many need help looking at things from a user’s perspective. Content strategy has been leading the charge on this, from <a href="http://alistapart.com/article/content-modelling-a-master-skill">developing content models and relationships</a>, to <a href="http://alistapart.com/article/dont-poke-the-bear-creating-content-for-sensitive-situations">communicating with empathy in touchy situations</a>, to <a href="http://alistapart.com/column/responsive-design-wont-fix-your-content-problem">working out content standards</a>.</p>

<p>Newspapers and magazines relentlessly publish new pieces and sweep the old away from public view. Are there opportunities to <a href="http://nprchives.tumblr.com">highlight</a> <a href="http://livelymorgue.tumblr.com">material</a> from their archives? What about content that can always <a href="http://www.theparisreview.org/interviews">stay</a> <a href="http://www.lettersofnote.com">interesting</a>? How can selections be best brought together to <a href="http://aworkinglibrary.com/writing/some-thoughts-on-the-anthology">generate new connections and meaning</a>?</p>

<p>Museums and libraries, as they step into their digital shoes, will have to think about building places online for histories and archives for the long term. Are there <a href="http://snarkmarket.com/blog/snarkives/books_writing_such/paleoblogging">new roles and practices</a> that bridge the old world with the networked, digital one? How do they preserve <a href="http://www.aaronland.info/weblog/2013/07/25/verb">entirely new categories of things</a> for the public?</p>

<p>No one has all the answers. But these are questions that come from leaving the stream and approaching content from the long view. These are problems that the shapers and caretakers of the web are uniquely positioned to think about and solve.</p>

<p>As a community, we take pride in being makers and craftsmen. But for years, we’ve neglected the disciplines of stewardship—the invisible and unglamorous work of collecting, restoring, safekeeping, and preservation. Maybe the answer isn’t to post more, to add more and more streams. Let’s return to our existing content and make it more durable and useful.</p>

<p>You don’t even have to pick up a shovel.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/brzpjVbp608" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-07-29T14:00:06+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:48:"http://alistapart.com/article/gardens-not-graves";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:22;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"Radio-Controlled Web Design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:9:"Art Lawry";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:89:"http://feedproxy.google.com/~r/alistapart/main/~3/2wXn4oMCOXo/radio-controlled-web-design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:57:"http://alistapart.com/article/radio-controlled-web-design";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:31146:"<p>Interactive user interfaces are a necessity in our responsive world. Smaller screens constrain the amount of content that can be displayed at any given time, so we need techniques to keep navigation and secondary information out of the way until they’re needed. From tabs and modal overlays to hidden navigation, we’ve created many powerful design patterns that show and hide content using JavaScript. </p>

<p>JavaScript comes with its own mobile challenges, though. Network speeds and data plans vary wildly, and every byte we deliver has an impact on the render speed of our pages or applications. When we add JavaScript to a page, we’re typically adding an external JavaScript file and an optional (usually large) library like jQuery. These interfaces won’t become usable until all the content, JavaScript files included, is downloaded—creating a slow and sluggish first impression for our users. </p>

<p>If we could create these content-on-demand patterns with no reliance on JavaScript, our interfaces would render earlier, and users could interact with them as soon as they were visible. By shifting some of the functionality to CSS, we could also reduce the amount of JavaScript needed to render the rest of our page. The result would be smaller file sizes, faster page-load times, interfaces that are available earlier, and the same functionality we’ve come to rely on from these design patterns. </p>

<p>In this article, I’ll explore a technique I’ve been working on that does just that. It’s still a bit experimental, so use your best judgment before using it in your own production systems.</p>

<h2>Understanding JavaScript’s role in maintaining state</h2>

<p>To understand how to accomplish these design patterns without JavaScript at all, let’s first take a look at the role JavaScript plays in maintaining state for a simple tabbed interface.</p>

<figure class="text full-width">
<p data-height="310" data-theme-id="0" data-slug-hash="aKbBf" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/1-javascript-tabs-no-aria/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<p>Let’s take a closer look at the underlying code.</p>

<pre><code class="language-markup">&lt;div class=&quot;js-tabs&quot;&gt;

    &lt;div class=&quot;tabs&quot;&gt;
        &lt;a href=&quot;#starks-panel&quot; id=&quot;starks-tab&quot;
            class=&quot;tab active&quot;&gt;Starks&lt;/a&gt;
        &lt;a href=&quot;#lannisters-panel&quot; id=&quot;lannisters-tab&quot;
            class=&quot;tab&quot;&gt;Lannisters&lt;/a&gt;
        &lt;a href=&quot;#targaryens-panel&quot; id=&quot;targaryens-tab&quot;
            class=&quot;tab&quot;&gt;Targaryens&lt;/a&gt;
    &lt;/div&gt;

    &lt;div class=&quot;panels&quot;&gt;
        &lt;ul id=&quot;starks-panel&quot; class=&quot;panel active&quot;&gt;
            &lt;li&gt;Eddard&lt;/li&gt;
            &lt;li&gt;Caitelyn&lt;/li&gt;
            &lt;li&gt;Robb&lt;/li&gt;
            &lt;li&gt;Sansa&lt;/li&gt;
            &lt;li&gt;Brandon&lt;/li&gt;
            &lt;li&gt;Arya&lt;/li&gt;
            &lt;li&gt;Rickon&lt;/li&gt;
        &lt;/ul&gt;
        &lt;ul id=&quot;lannisters-panel&quot; class=&quot;panel&quot;&gt;
            &lt;li&gt;Tywin&lt;/li&gt;
            &lt;li&gt;Cersei&lt;/li&gt;
            &lt;li&gt;Jamie&lt;/li&gt;
            &lt;li&gt;Tyrion&lt;/li&gt;
        &lt;/ul&gt;
        &lt;ul id=&quot;targaryens-panel&quot; class=&quot;panel&quot;&gt;
            &lt;li&gt;Viserys&lt;/li&gt;
            &lt;li&gt;Daenerys&lt;/li&gt;
        &lt;/ul&gt;
    &lt;/div&gt;

&lt;/div&gt;
</code></pre>

<p>Nothing unusual in the layout, just a set of tabs and corresponding panels that will be displayed when a tab is selected. Now let’s look at how tab state is managed by altering a tab’s class:</p>

<pre><code class="language-css">...

.js-tabs .tab {
    /* inactive styles go here */
}
.js-tabs .tab.active {
    /* active styles go here */
}

.js-tabs .panel {
    /* inactive styles go here */
}
.js-tabs .panel.active {
    /* active styles go here */
}

...
</code></pre>

<p>Tabs and panels that have an active class will have additional CSS applied to make them stand out. In our case, active tabs will visually connect to their content while inactive tabs remain separate, and active panels will be visible while inactive panels remain hidden.</p>

<p>At this point, you’d use your preferred method of working with JavaScript to listen for click events on the tabs, then manipulate the <code>active</code> class, removing it from all tabs and panels and adding it to the newly clicked tab and corresponding panel. This pattern is pretty flexible and has worked well for a long time. We can simplify what’s going on into two distinct parts:</p>

<ol>
<li>JavaScript binds events that manipulate classes.</li>
<li>CSS restyles elements based on those classes.</li>
</ol>

<h3>State management without JavaScript</h3>

<p>Trying to replicate event binding and class manipulation in CSS and HTML alone would be impossible, but if we define the process in broader terms, it becomes:</p>

<ol>
<li>User input changes the system’s active state.</li>
<li>The system is re-rendered when the state is changed.</li>
</ol>

<p>In our HTML- and CSS-only solution, we’ll use radio buttons to allow the user to manipulate state, and the <code>:checked</code> pseudo-class as the hook to re-render.</p>

<p>The solution has its roots in Chris Coyier’s <a href="http://css-tricks.com/the-checkbox-hack/">checkbox hack</a>, which I was introduced to via my colleague Scott O’Hara in his <a href="http://www.scottohara.me/article/css-morph-menu-button.html">morphing menu button</a> demo. In both cases, checkbox inputs are used to maintain two states without JavaScript by styling elements using the <code>:checked</code> pseudo-class. In this case, we’ll be using radio buttons to increase the number of states we can maintain beyond two.</p>

<h2>Wait, radio buttons?</h2>

<p>Using radio buttons to do something other than collect form submission data may make some of you feel a little uncomfortable, but let’s look at <a href="http://www.w3.org/TR/html5/forms.html#the-input-element">what the W3C says</a> about input use and see if we can ease some concerns:</p>

<figure class="quote">
<blockquote>The <code>&lt;input&gt;</code> element represents a typed data field, usually with a form control to <strong>allow the user to edit</strong> the <strong>data</strong>. (emphasis mine)</blockquote>
</figure>

<p>“Data” is a pretty broad term—it has to be to cover the multitude of types of data that forms collect. We’re <em>allowing the user to edit</em> the <em>state</em> of a part of the page. State is just data about that part of the page at any given time. This may not have been the intended use of <code>&lt;input&gt;</code>, but we’re holding true to the specification.</p>

<p>The W3C also states that inputs may be rendered wherever “phrasing content” can be used, which is basically anywhere you could put standalone text. This allows us to use radio buttons outside of a form.</p>

<h2>Radio-controlled tabs</h2>

<p>So now that we know a little more about whether we <em>can</em> use radio buttons for this purpose, let’s dig into an example and see <em>how</em> they can actually remove or reduce our dependency on JavaScript by modifying the original tabs example.</p>

<h3>Add radio buttons representing state</h3>

<p>Each radio button will represent one state of the interactive component. In our case, we have three tabs and each tab can be active, so we need three radio buttons, each of which will represent a particular tab being active. By giving the radio buttons the same name, we’ll ensure that only one may be checked at any time. Our JavaScript example had the first tab active initially, so we can add the <code>checked</code> attribute to the radio button representing the first tab, indicating that it is currently active.</p>

<p>Because CSS selectors can only style sibling or child selectors based on the state of another element, these radio buttons must come <em>before</em> any content that needs to be visually manipulated. In our case, we’ll put our radio buttons just before the tabs <code>div</code>: </p>

<pre><code class="language-markup">    &lt;input class=&quot;state&quot; type=&quot;radio&quot; name=&quot;houses-state&quot;
        id=&quot;starks&quot; checked /&gt;
    &lt;input class=&quot;state&quot; type=&quot;radio&quot; name=&quot;houses-state&quot;
        id=&quot;lannisters&quot; /&gt;
    &lt;input class=&quot;state&quot; type=&quot;radio&quot; name=&quot;houses-state&quot;
        id=&quot;targaryens&quot; /&gt;

    &lt;div class=&quot;tabs&quot;&gt;
    ...
</code></pre>

<h3>Replace click and touch areas with labels</h3>

<p>Labels naturally respond to click and touch events. We can’t tell them <em>how</em> to react to those events, but the behavior is predictable and we can leverage it. When a label associated with a radio button is clicked or touched, the radio button is checked while all other radio buttons in the same group are unchecked.</p>

<p>By setting the <code>for</code> attribute of our labels to the <code>id</code> of a particular radio button, we can place labels wherever we need them while still inheriting the touch and click behavior.</p>

<p>Our tabs were represented with anchors in the earlier example. Let’s replace them with labels and add <code>for</code> attributes to wire them up to the correct radio buttons. We can also remove the <code>active</code> class from the tab and panel as the radio buttons will be maintaining state:</p>

<pre><code class="language-markup">...
    &lt;input class=&quot;state&quot; type=&quot;radio&quot; title=&quot;Targaryens&quot;
        name=&quot;houses-state&quot; id=&quot;targaryens&quot; /&gt;

    &lt;div class=&quot;tabs&quot;&gt;
        &lt;label for=&quot;starks&quot; id=&quot;starks-tab&quot;
            class=&quot;tab&quot;&gt;Starks&lt;/label&gt;
        &lt;label for=&quot;lannisters&quot; id=&quot;lannisters-tab&quot;
            class=&quot;tab&quot;&gt;Lannisters&lt;/label&gt;
        &lt;label for=&quot;targaryens&quot; id=&quot;targaryens-tab&quot;
            class=&quot;tab&quot;&gt;Targaryens&lt;/label&gt;
    &lt;/div&gt;

    &lt;div class=&quot;panels&quot;&gt;
...
</code></pre>

<h3>Hide radio buttons with CSS</h3>

<p>Now that our labels are in place, we can safely hide the radio buttons. We still want to keep the tabs keyboard accessible, so we’ll just move the radio buttons offscreen:</p>

<pre><code class="language-css">...

.radio-tabs .state {
    position: absolute;
    left: -10000px;
}

...
</code></pre>

<h3>Style states based on <code>:checked</code> instead of <code>.active</code></h3>

<p>The <code>:checked</code> pseudo-class allows us to apply CSS to a radio button when it is checked. The sibling selector <code>~</code> allows us to style elements that follow an element in the same level. Combined, we can style anything after the radio buttons based on the buttons’ state.</p>

<p>The pattern is <code>#radio:checked ~ .something-after-radio</code> or optionally <code>#radio:checked ~ .something-after-radio .something-nested-deeper</code>:</p>

<pre><code class="language-css">...

.tab {
    ...
}
#starks:checked ~ .tabs #starks-tab,
#lannisters:checked ~ .tabs #lannisters-tab,
#targaryens:checked ~ .tabs #targaryens-tab {
    ...
}

.panel {
    ...
}
#starks:checked ~ .panels #starks-panel,
#lannisters:checked ~ .panels #lannisters-panel,
#targaryens:checked ~ .panels #targaryens-panel {
    ...
}

...
</code></pre>

<p>Now when the tab labels are clicked, the appropriate radio button will be checked, which will style the correct tab and panel as active. The result: </p>

<figure class="text full-width">
<p data-height="310" data-theme-id="0" data-slug-hash="domFD" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/2-radio-controlled-tabs-no-aria/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script></figure>

<h2>Browser support</h2>

<p>The requirements for this technique are pretty low. As long as a browser supports the <code>:checked</code> pseudo-class and <code>~</code> sibling selector, we’re good to go. Firefox, Chrome, and mobile Webkit have always supported these selectors. Safari has had support since version 3, and Opera since version 9. Internet Explorer started supporting the sibling selector in version 7, but didn’t add support for <code>:checked</code> until IE9. Android supports <code>:checked</code> but has a bug which impedes it from being aware of changes to a checked element after page load.</p>

<p>That’s decent support, but with a little extra work we can get Android and older IE working as well.</p>

<h3>Fixing the Android 2.3 <code>:checked</code> bug</h3>

<p>In some versions of Android, <code>:checked</code> won’t update as the state of a radio group changes. Luckily, <a href="http://stackoverflow.com/questions/8320530/webkit-bug-with-hover-and-multiple-adjacent-sibling-selectors/8320736#8320736">there’s a fix for that</a> involving a webkit-only infinite animation on the body, which Tim Pietrusky points out in his <a href="http://timpietrusky.com/advanced-checkbox-hack">advanced checkbox hack</a>: </p>

<pre><code class="language-css">...

/* Android 2.3 :checked fix */
@keyframes fake {
    from {
        opacity: 1;
    }
    to {
        opacity: 1
    }
}
body {        
    animation: fake 1s infinite;
}

...
</code></pre>

<h3>JavaScript shim for old Internet Explorer</h3>

<p>If you need to support IE7 and IE8, you can add this shim to the bottom of your page in a script tag: </p>

<pre><code class="language-javascript">document.getElementsByTagName('body')[0]
.addEventListener('change', function (e) {
    var radios, i;
    if (e.target.getAttribute('type') === 'radio') {
        radios = document.querySelectorAll('input[name=&quot;' +
            e.target.getAttribute('name') + '&quot;]');
        for (i = 0; i &lt; radios.length; i += 1) {
            radios[ i ].className = 
                radios[ i ].className.replace(
                    /(^|\s)checked(\s|$)/,
                    ' '
                );
            if (radios[ i ] === e.target) {
                radios[ i ].className += ' checked';
            }
        }
    }
});
</code></pre>

<p>This adds a <code>checked</code> class to the currently checked radio button, allowing you to double up your selectors and keep support. Your selectors would have to be updated to include <code>:checked</code> and <code>.checked</code> versions like this:</p>

<pre><code class="language-css">...

.tab {
    ...
}
#starks:checked ~ .tabs #starks-tab,
#starks.checked ~ .tabs #starks-tab,
#lannisters:checked ~ .tabs #lannisters-tab,
#lannisters.checked ~ .tabs #lannisters-tab,
#targaryens:checked ~ .tabs #targaryens-tab,
#targaryens.checked ~ .tabs #targaryens-tab {
    ...
}

.panel {
    ...
}
#starks:checked ~ .panels #starks-panel,
#starks.checked ~ .panels #starks-panel,
#lannisters:checked ~ .panels #lannisters-panel,
#lannisters.checked ~ .panels #lannisters-panel,
#targaryens:checked ~ .panels #targaryens-panel,
#targaryens.checked ~ .panels #targaryens-panel {
    ...
}

...
</code></pre>

<p>Using an inline script still saves a potential http request and speeds up interactions on newer browsers. When you choose to drop IE7 and IE8 support, you can drop the shim without changing any of your code.</p>

<h2>Maintaining accessibility</h2>

<p>While our initial JavaScript tabs exhibited the state management between changing tabs, a more robust example would use progressive enhancement to change three titled lists into tabs. It should also handle adding all the ARIA roles and attributes that screen readers and other assistive technologies use to navigate the contents of a page. A better JavaScript example might look like this:</p>

<figure class="text full-width">
<p data-height="310" data-theme-id="0" data-slug-hash="DazeL" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/3-javascript-tabs-aria/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<p>Parts of the HTML are removed and will now be added by additional JavaScript; new HTML has been added and will be hidden by additional JavaScript; and new CSS has been added to manage the pre-enhanced and post-enhanced states. In general, our code has grown by a good amount.</p>

<p>In order to support ARIA, particularly managing the <code>aria-selected</code> state, we’re going to have to bring some JavaScript back into our radio-controlled tabs. However, the amount of progressive enhancement we need to do is greatly reduced.</p>

<p>If you aren’t familiar with ARIA or are a little rusty, you may wish to refer to the <a href="http://www.w3.org/TR/wai-aria-practices/#tabpanel">ARIA Authoring Practices for tabpanel</a>.</p>

<h3>Adding ARIA roles and attributes</h3>

<p>First, we’ll add the role of <code>tablist</code> to the containing <code>div</code>.</p>

<pre><code class="language-markup">&lt;div class=&quot;radio-tabs&quot; role=&quot;tablist&quot;&gt;
  
    &lt;input class=&quot;state&quot; type=&quot;radio&quot; name=&quot;houses-state&quot;
        id=&quot;starks&quot; checked /&gt;
    ...
</code></pre>

<p>Next, we’ll add the role of <code>tab</code> and attribute <code>aria-controls</code> to each radio button. The <code>aria-controls</code> value will be the <code>id</code> of the corresponding panel to show. Additionally, we’ll add titles to each radio button so that screen readers can associate a label with each tab. The checked radio button will also get <code>aria-selected=&quot;true&quot;</code>:</p>

<pre><code class="language-markup">&lt;div class=&quot;radio-tabs&quot; role=&quot;tablist&quot;&gt;
  
    &lt;input class=&quot;state&quot; type=&quot;radio&quot; title=&quot;Starks&quot;
        name=&quot;houses-state&quot; id=&quot;starks&quot; role=&quot;tab&quot;
        aria-controls=&quot;starks-panel&quot; aria-selected=&quot;true&quot;checked /&gt;
    &lt;input class=&quot;state&quot; type=&quot;radio&quot; title=&quot;Lanisters&quot; 
        name=&quot;houses-state&quot; id=&quot;lannisters&quot; role=&quot;tab&quot; 
        aria-controls=&quot;lannisters-panel&quot; /&gt;
    &lt;input class=&quot;state&quot; type=&quot;radio&quot; title=&quot;Targaryens&quot; 
        name=&quot;houses-state&quot; id=&quot;targaryens&quot; role=&quot;tab&quot; 
        aria-controls=&quot;targaryens-panel&quot; /&gt;

    &lt;div class=&quot;tabs&quot;&gt;
</code></pre>

<p>We’re going to hide the visual tabs from assistive technology because they are shallow interfaces to the real tabs (the radio buttons). We’ll do this by adding <code>aria-hidden=&quot;true&quot;</code> to our <code>.tabs</code> <code>div</code>:</p>

<pre><code class="language-markup">    ...
    &lt;input class=&quot;state&quot; type=&quot;radio&quot; title=&quot;Targaryens&quot;
        name=&quot;houses-state&quot; id=&quot;targaryens&quot; role=&quot;tab&quot;
        aria-controls=&quot;targaryens-panel&quot; /&gt;

    &lt;div class=&quot;tabs&quot; aria-hidden=&quot;true&quot;&gt;
        &lt;label for=&quot;starks&quot; id=&quot;starks-tab&quot;
            class=&quot;tab&quot;&gt;Starks&lt;/label&gt;
    ...
</code></pre>

<p>The last bit of ARIA support we need to add is on the panels. Each panel will get the role of <code>tabpanel</code> and an attribute of <code>aria-labeledby</code> with a value of the corresponding tab’s id:</p>

<pre><code class="language-markup">   ...
   &lt;div class=&quot;panels&quot;&gt;
        &lt;ul id=&quot;starks-panel&quot; class=&quot;panel active&quot;
            role=&quot;tabpanel&quot; aria-labelledby=&quot;starks-tab&quot;&gt;
            &lt;li&gt;Eddard&lt;/li&gt;
            &lt;li&gt;Caitelyn&lt;/li&gt;
            &lt;li&gt;Robb&lt;/li&gt;
            &lt;li&gt;Sansa&lt;/li&gt;
            &lt;li&gt;Brandon&lt;/li&gt;
            &lt;li&gt;Arya&lt;/li&gt;
            &lt;li&gt;Rickon&lt;/li&gt;
        &lt;/ul&gt;
        &lt;ul id=&quot;lannisters-panel&quot; class=&quot;panel&quot;
            role=&quot;tabpanel&quot; aria-labelledby=&quot;lannisters-tab&quot;&gt;
            &lt;li&gt;Tywin&lt;/li&gt;
            &lt;li&gt;Cersei&lt;/li&gt;
            &lt;li&gt;Jamie&lt;/li&gt;
            &lt;li&gt;Tyrion&lt;/li&gt;
        &lt;/ul&gt;
        &lt;ul id=&quot;targaryens-panel&quot; class=&quot;panel&quot;
            role=&quot;tabpanel&quot; aria-labelledby=&quot;targaryens-tab&quot;&gt;
            &lt;li&gt;Viserys&lt;/li&gt;
            &lt;li&gt;Daenerys&lt;/li&gt;
        &lt;/ul&gt;
    &lt;/div&gt;
    ...
</code></pre>

<p>All we need to do with JavaScript is to set the <code>aria-selected</code> value as the radio buttons change:</p>

<pre><code class="language-js">$('.state').change(function () {
    $(this).parent().find('.state').each(function () {
        if (this.checked) {
            $(this).attr('aria-selected', 'true');
        } else {
            $(this).removeAttr('aria-selected');
        }       
    });
});
</code></pre>

<p>This also gives an alternate hook for IE7 and IE8 support. Both browsers support attribute selectors, so you could update the CSS to use <code>[aria-selected]</code> instead of <code>.checked</code> and remove the support shim.</p>

<pre><code class="language-css">...

#starks[aria-selected] ~ .tabs #starks-tab,
#lannisters[aria-selected] ~ .tabs #lannisters-tab,
#targaryens[aria-selected] ~ .tabs #targaryens-tab,
#starks:checked ~ .tabs #starks-tab,
#lannisters:checked ~ .tabs #lannisters-tab,
#targaryens:checked ~ .tabs #targaryens-tab {
    /* active tab, now with IE7 and IE8 support! */
}

...
</code></pre>

<p>The result is full ARIA support with minimal JavaScript—and you still get the benefit of tabs that can be used as soon as the browser paints them.</p>

<figure class="text full-width">
<p data-height="310" data-theme-id="0" data-slug-hash="gbLev" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/4-radio-controlled-tabs-aria/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<p>That’s it. Note that because the underlying HTML is available from the start, unlike the initial JavaScript example, we didn’t have to manipulate or create any additional HTML. In fact, aside from adding ARIA roles and parameters, we didn’t have to do much at all.</p>

<h2>Limitations to keep in mind</h2>

<p>Like most techniques, this one has a few limitations. The first and most important is that the state of these interfaces is transient. When you refresh the page, these interfaces will revert to their initial state. This works well for some patterns, like modals and offscreen menus, and less well for others. If you need persistence in your interface’s state, it is still better to use links, form submission, or AJAX requests to make sure the server can keep track of the state between visits or page loads.</p>

<p>The second limitation is that there is a scope gap in what can be styled using this technique. Since you cannot place radio buttons before the <code>&lt;body&gt;</code> or <code>&lt;html&gt;</code> elements, and you can only style elements following radio buttons, you cannot affect either element with this technique.</p>

<p>The third limitation is that you can only apply this technique to interfaces that are triggered via click, tap, or keyboard input. You can use progressive enhancement to listen to more complex interactions like scrolling, swipes, double-tap, or multitouch, but if your interfaces rely on these events alone, standard progressive enhancement techniques may be better.</p>

<p>The final limitation involves how radio groups interact with the tab flow of the document. If you noticed in the tab example, hitting tab brings you to the tab group, but hitting tab again leaves the group. This is fine for tabs, and is the expected behavior for ARIA tablists, but if you want to use this technique for something like an open and close button, you’ll want to be able to have both buttons in the tab flow of the page independently based on the button location. This can be fixed through a bit of JavaScript in four steps:</p>

<ol>
<li>Set the radio buttons and labels to <code>display: none</code> to take them out of the tab flow and visibility of the page.</li>
<li>Use JavaScript to add <code>buttons</code> after each <code>label</code>.</li>
<li>Style the buttons just like the labels.</li>
<li>Listen for clicks on the <code>button</code> and trigger clicks on their neighboring <code>label</code>.</li>
</ol>

<p>Even using this process, it is highly recommended that you use a standard progressive enhancement technique to make sure users without JavaScript who interact with your interfaces via keyboard don’t get confused with the radio buttons. I recommend the following JavaScript in the head of your document:</p>

<pre><code class="language-markup">&lt;script&gt;document.documentElement.className+=&quot; js&quot;;&lt;/script&gt;
</code></pre>

<p>Before any content renders, this will add the <code>js</code> class to your <code>&lt;html&gt;</code> element, allowing you to style content depending on whether or not JavaScript is turned on. Your CSS would then look something like this:</p>

<pre><code class="language-css">.thing {
    /* base styles - when no JavaScript is present
       hide radio button labels, show hidden content, etc. */
}

.js .thing {
    /* style when JavaScript is present
       hide content, show labels, etc. */
}
</code></pre>

<p>Here’s an example of an offscreen menu implemented using the above process. If JavaScript is disabled, the menu renders open at all times with no overlay:</p>

<figure class="text full-width">
<p data-height="388" data-theme-id="0" data-slug-hash="wsbfC" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/5-and-7-radio-controlled-offscreen-menu/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<h2>Implementing other content-on-demand patterns</h2>

<p>Let’s take a quick look at how you might create some common user interfaces using this technique. Keep in mind that a robust implementation would address accessibility through ARIA roles and attributes.</p>

<h3>Modal windows with overlays</h3>

<ul>
<li>Two radio buttons representing modal visibility</li>
<li>One or more labels for modal-open which can look like anything</li>
<li>A label for modal-close styled to look like a semi-transparent overlay</li>
<li>A label for modal-close styled to look like a close button</li>
</ul>

<figure class="text full-width">
<p data-height="388" data-theme-id="0" data-slug-hash="npsCd" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/6-radio-controlled-modal-window/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<h3>Off-screen menu</h3>

<ul>
<li>Two radio buttons representing menu visibility</li>
<li>A label for menu-open styled to look like a menu button</li>
<li>A label for menu-close styled to look like a semi-transparent overlay</li>
<li>A label for menu-close styled to look like a close button</li>
</ul>

<figure class="text full-width">
<p data-height="388" data-theme-id="0" data-slug-hash="wsbfC" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/5-and-7-radio-controlled-offscreen-menu/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<h3>Switching layout on demand</h3>

<ul>
<li>Radio buttons representing each layout</li>
<li>Labels for each radio button styled like buttons</li>
</ul>

<figure class="text full-width">
<p data-height="490" data-theme-id="0" data-slug-hash="gbehv" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/8-radio-controlled-layout-manipulation/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<h3>Switching style on demand</h3>

<ul>
<li>Radio buttons representing each style</li>
<li>Labels for each radio button styled like buttons</li>
</ul>

<figure class="text full-width">
<p data-height="490" data-theme-id="0" data-slug-hash="ncAfK" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/9-radio-controlled-style-manipulation/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<h3>Content carousels</h3>

<ul>
<li>X radio buttons, one for each panel, representing the active panel</li>
<li>Labels for each panel styled to look like next/previous/page controls</li>
</ul>

<figure class="text full-width">
<p data-height="350" data-theme-id="0" data-slug-hash="ioCaA" data-default-tab="result" class='codepen'>See the demo: <a href='http://alistapart.com/d/399/10-radio-controlled-carousel/'>Show/hide example</a></p>
<script async src="//codepen.io/assets/embed/ei.js"></script>
</figure>

<h3>Other touch- or click-based interfaces</h3>

<p>As long as the interaction does not depend on adding new content to the page or styling the <code>&lt;body&gt;</code> element, you should be able to use this technique to accomplish some very JavaScript-like interactions.</p>

<p>Occasionally you may want to manage multiple overlapping states in the same system—say the color and size of a font. In these situations, it may be easier to maintain multiple sets of radio buttons to manage each state.</p>

<p>It is also <em>highly</em> recommended that you use <code>autocomplete=&quot;off&quot;</code> with your radio buttons to avoid conflict with browser form autofill switching state on your users.</p>

<h2>Radio-control the web?</h2>

<p>Is your project right for this technique? Ask yourself the following questions:</p>

<ol>
<li>Am I using complex JavaScript on my page/site that can’t be handled by this technique?</li>
<li>Do I need to support Internet Explorer 6 or other legacy browsers?</li>
</ol>

<p>If the answer to either of those question is “yes,” you probably shouldn’t try to integrate radio control into your project. Otherwise, you may wish to consider it as part of a robust progressive enhancement technique.</p>

<p>Most of the time, you’ll be able to shave some bytes off of your JavaScript files and CSS. Occasionally, you’ll even be able to remove Javascript completely. Either way, you’ll gain the appearance of speed—and build a more enjoyable experience for your users.</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/2wXn4oMCOXo" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-07-29T14:00:47+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:57:"http://alistapart.com/article/radio-controlled-web-design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:23;a:6:{s:4:"data";s:54:"
      
      
      
      
      
      
      
    ";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:45:"Matt Griffin on How We Work: Being Profitable";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"author";a:1:{i:0;a:5:{s:4:"data";s:12:"Matt Griffin";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:78:"http://feedproxy.google.com/~r/alistapart/main/~3/xNL_biRVME4/being-profitable";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:45:"http://alistapart.com/column/being-profitable";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:10951:"<p>When I recently read <a href="http://alistapart.com/article/living-up-to-your-business-ideals">Geoff Dimasi’s excellent article</a> I thought: this is great—values-based business decisions in an efficient fashion. But I had another thought, too: where, in that equation, is the money?</p>

<p>If I’m honest with myself, I’ve always felt that on some level it’s wrong to be profitable. That making money on top of your costs somehow equates to bilking your clients. I know, awesome trait for a business owner, right?</p>

<p>Because here’s the thing: a business can’t last forever skating on the edge of viability. And that’s what not being profitable means. This is a lesson I had to learn with Bearded the hard way. Several times. Shall we have a little bit of story time? “Yes, Matt Griffin,” you say, “let’s!” Well OK, then.</p>

<p>At Bearded, our philosophy from the beginning was to focus on doing great web work for clients we believed in. The hope was that all the sweat and care we put into those projects and relationships would show, and that profit would naturally follow quality. For four years we worked our tails off on project after project, and as we did so, we lived pretty much hand-to-mouth. On several occasions we were within weeks and a couple of thousand bucks from going out of business. I would wake up in the night in a panic, and start calculating when bills went out and checks would come in, down to the day. I loved the work and clients, but the other parts of the business were frankly pretty miserable.</p>

<p>Then one day, I went to the other partners at Bearded and told them I’d had it. In the immortal words of <cite>Lethal Weapon’s</cite> Sergeant Murtaugh, I was getting too old for this shit. I told them I could put in one more year, and if we weren’t profitable by the end of it I was out, and we should all go get well-paid jobs somewhere else. They agreed.</p>

<p>That decision lit a fire under us to pay attention to the money side of things, change our process, and effectively do whatever it took to save the best jobs we’ve ever had. By the end of the next quarter, we had three months of overhead in the bank and were on our way to the first profitable year of our business, with a 50 percent growth in revenue over the previous year and raises for everyone. All without compromising our values or changing the kinds of projects we were doing.</p>

<p>This did not happen on its own. It happened because we started designing the money side of our business the way we design everything else we care about. We stopped neglecting our business, and started taking care.</p>

<p>“So specifically,” you ask, “what did you do to turn things around? I am interested in these things!” you say. Very good, then, let’s take a look.</p>

<h2>Now it’s time for a breakdown</h2>

<p>Besides my arguably weird natural aversion to profit, there are plenty of other motivations not to examine the books. Perhaps math and numbers are scary to you. Maybe finances just seem really boring (they’re no CSS pseudo-selectors, amiright?). Or maybe it’s that when we don’t pay attention to a thing, it’s easier to pretend that it’s not there. But in most cases, the unknown is far scarier than fact.</p>

<p>When it comes down to it, your businesses finances are made up of two things: money in and money out. Money in is revenue. Money out is overhead. And the difference? That’s profit (or lack thereof). Let’s take a look at the two major components of that equation.</p>

<h3>Overhead Overheels</h3>

<p>First let’s roll up our sleeves and calculate your overhead. Overhead includes loads of stuff like:</p>

<ul>
<li>Staff salaries</li>
<li>Health insurance</li>
<li>Rent</li>
<li>Utilities</li>
<li>Equipment costs</li>
<li>Office supplies</li>
<li>Snacks, meals, and beverages</li>
<li>Service fees (hosting, web services, etc.)</li>
</ul>

<p>In other words: it’s all the money you pay out to do your work. You can assess these items over whatever period makes sense to you: daily, weekly, annually, or even by project.</p>

<p>For Bearded, we asked our bookkeeper to generate a monthly budget in Quicken based on an average of the last six months of actual costs that we have, broken down by type. This was super helpful in seeing where our money goes. Not surprisingly, most of it was paying staff and covering their benefits.</p>

<p>Once we had that number it was easy to derive whatever variations were useful to us. The most commonly used number in our arsenal is weekly overhead. Knowing that variable is very helpful for us to know how much we cost every week, and how much average revenue needs to come in each week before we break even.</p>

<h3>Everything old is revenue again</h3>

<p>So how do we bring in that money? You may be using pricing structures that are fixed-fee, hourly, weekly, monthly, or value-based. But at the end of the day you can always divide the revenue gained by the time you spent, and arrive at a period-based rate for the project (whether monthly, weekly, hourly, or project length). This number is crucial in determining profitability, because it lines up so well with the overhead number we already determined.</p>

<p>Remember: money in minus money out is profit. And that’s the number we need to get to a point where it safely sustains the business.</p>

<p>If we wanted to express this idea mathematically, it might look something like this:</p>

<pre>(Rate × Time spent × Number of People) - (Salaries + Expenses) = Profit</pre>

<p>Here’s an example:</p>

<p>Let’s say that our ten-person business costs $25,000 a week to run. That means each person, on average, needs to do work that earns $2,500 per week for us to break even. If our hourly rate is $100 per hour, that means each person needs to bill 25 hours per week just to maintain the business. If everyone works 30 billable hours per week, the business brings in $30,000—a profit of 20 percent of that week’s overhead. In other words, it takes five good weeks to get one extra week of overhead in the bank.</p>

<p>That’s not a super great system, is it? How many quality billable hours can a person really do in a week—30? Maybe 36? And is it likely that all ten people will be able to do that many billable hours each week? After all, there are plenty of non-billable tasks involved in running a business. Not only that, but there will be dry periods in the work cycle—gaps between projects, not to mention vacations! We won’t all be able to work full time every week of the year. Seems like this particular scenario has us pretty well breaking even, if we’re lucky.</p>

<p>So what can we do to get the balance a little more sustainable? Well, everyone could just work more hours. Doing 60-hour weeks every week would certainly take care of things. But how long can real human beings keep that up?</p>

<p>We can lower our overhead by cutting costs. But seeing as most of our costs are paying salaries, that seems like an unlikely place to make a big impact. To truly be more profitable, the business needs to bring in more revenue per hour of effort expended by staff. That means higher rates. Let’s look at a new example:</p>

<p>Our ten-person business still costs $25,000 a week. Our break-even is still at $2,500 per week per person. Now let’s set our hourly rate at $150 per hour. This means that each person has to work just under 17 billable hours per week for the business to break even. If everyone bills 30 hours in a week, the business will now bring in $45,000—or $20,000 in profit. That’s 80 percent of a week’s overhead.</p>

<p>That scenario seems a whole lot more sustainable—a good week now pays for itself, and brings in 80 percent of the next week’s overhead. With that kind of ratio we could, like a hungry bear before hibernation, start saving up to protect ourselves from less prosperous times in the future.</p>

<p>Nature metaphors aside, once we know how these parts work, we can figure out any one component by setting the others and running the numbers. In other words, we don’t just have to see how a specific hourly rate changes profit. We can go the other way, too.</p>

<h2>Working for a living or living to work</h2>

<p>One way to determine your system is to start with desired salaries and reasonable work hours for your culture, and work backwards to your hourly rate. Then you can start thinking about pricing systems (yes, even fixed price or value-based systems) that let you achieve that effective rate.</p>

<p>Maybe time is the most important factor for you. How much can everyone work? How much does everyone want to work? How much must you then charge for that time to end up with salaries you can be content with?</p>

<p>This is, in part, a lifestyle question. At Bearded, we sat down not too long ago and did an exercise adapted from an IA exercise we learned from <a href="http://kevinmhoffman.com/">Kevin M. Hoffman</a>. We all contributed potential qualities that were important to our business—things like “high quality of life,” “high quality of work,” “profitable,” “flexible,” “clients who do good in the world,” “efficient,” and “collaborative.” As a group we ordered those qualities by importance, and decided we’d let those priorities guide us for the next year, at which point we’d reassess.</p>

<p>That exercise really helped us make decisions about things like what rate we needed to charge, how many hours a week we wanted to work, as well as more squishy topics like what kinds of clients we wanted to work for and what kind of work we wanted to do. Though finances can seem like purely quantitative math, that sort of qualitative exercise ended up significantly informing how we plugged numbers into the profit equation.</p>

<h2>Pricing: Where the rubber meets the road</h2>

<p>Figuring out the basics of overhead, revenue, and profit, is instrumental in giving you an understanding of the mechanics of your business. It lets you plan knowledgeably for your future. It allows you to make plans and set goals for the growth and maintenance of your business.</p>

<p>But once you know what you want to charge there’s another question—how do you charge it?</p>

<p>There are plenty of different pricing methods out there (time unit-based, deliverable-based, time period-based, value-based, and combinations of these). They all have their own potential pros and cons for profitability. They also create different motivations for clients and vendors, which in turn greatly affect your working process, day-to-day interactions, and project outcomes.</p>

<p>But that, my friends, is a topic for our next column. Stay tuned for part two of my little series on the money side of running a web business: pricing!</p><img src="http://feeds.feedburner.com/~r/alistapart/main/~4/xNL_biRVME4" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:2:{s:7:"subject";a:1:{i:0;a:5:{s:4:"data";s:38:"<a href="/topic/business">Business</a>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-07-24T12:29:02+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:45:"http://alistapart.com/column/being-profitable";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:32:"http://purl.org/dc/elements/1.1/";a:4:{s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:2:"en";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:30:"The fine folks at A List Apart";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:6:"rights";a:1:{i:0;a:5:{s:4:"data";s:14:"Copyright 2014";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"date";a:1:{i:0;a:5:{s:4:"data";s:25:"2014-09-25T17:00:13+00:00";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:2:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";s:4:"href";s:43:"http://feeds.feedburner.com/alistapart/main";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:2:{s:3:"rel";s:3:"hub";s:4:"href";s:32:"http://pubsubhubbub.appspot.com/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:4:"info";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:3:"uri";s:15:"alistapart/main";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:11:{s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:4:"etag";s:27:"G/f0bGrK7pbR7Bw+MQci0fdeLDE";s:13:"last-modified";s:29:"Fri, 26 Sep 2014 09:22:37 GMT";s:16:"content-encoding";s:4:"gzip";s:4:"date";s:29:"Fri, 26 Sep 2014 09:31:00 GMT";s:7:"expires";s:29:"Fri, 26 Sep 2014 09:31:00 GMT";s:13:"cache-control";s:18:"private, max-age=0";s:22:"x-content-type-options";s:7:"nosniff";s:16:"x-xss-protection";s:13:"1; mode=block";s:6:"server";s:3:"GSE";s:18:"alternate-protocol";s:15:"80:quic,p=0.002";}s:5:"build";s:14:"20121030095402";}