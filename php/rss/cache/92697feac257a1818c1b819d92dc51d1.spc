a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:52:"
	
	
	
	
	
	
		
		
	

	
		
		
		
		
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:5:{s:0:"";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:17:"I LOVE TYPOGRAPHY";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:26:"http://ilovetypography.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Sep 2014 08:38:01 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"en-US";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:27:"http://wordpress.org/?v=4.0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:5:"cloud";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:5:{s:6:"domain";s:19:"ilovetypography.com";s:4:"port";s:2:"80";s:4:"path";s:17:"/?rsscloud=notify";s:17:"registerProcedure";s:0:"";s:8:"protocol";s:9:"http-post";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:51:"
		
		
		
		
		
				
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:24:"This Month in Typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/rmDFNi4yeUI/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:82:"http://ilovetypography.com/2014/09/26/monthly-typography-fonts-roundup-1/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Sep 2014 08:38:01 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:11:"calligraphy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:9:"new fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:5:"video";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15315";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:986:"Welcome to a new monthly roundup of type-related info and entertainment. (This will be in addition to Sean Mitchell&#8217;s excellent This Week in Fonts feature.) This month, we have movies (about sign painters, and Eric and John Gill), books (about Porchez, Baskerville, Spiekermann, and typewriter art, as well as books by Steven Heller and Gail [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/09/26/monthly-typography-fonts-roundup-1/">This Month in Typography</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:11:"Alec Julien";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:27620:"<p>Welcome to a new monthly roundup of type-related info and entertainment. (This will be in addition to Sean Mitchell&#8217;s excellent This Week in Fonts feature.) This month, we have movies (about sign painters, and Eric and John Gill), books (about Porchez, Baskerville, Spiekermann, and typewriter art, as well as books by Steven Heller and Gail Anderson), a Flickr group that points at things, a free typography course with Ellen Lupton, ladies of letterpress, medieval marginalia, all sorts of lettering and typography, some fun new fonts, and more. </p>
<p><span id="more-15315"></span></p>
<h3><a href="https://www.youtube.com/watch?v=IXNbWqnvXuM">Sign Painters: The Movie</a></h3>
<p><a href="http://ilovetypography.com/2014/09/26/monthly-typography-fonts-roundup-1/"><em>Click here to view the embedded video.</em></a></p>
<p>Weathered by time, distinct characteristics shining through, hand-painted signs are a product of a fascinating 150 year-old American history. What was once a common job has now become a highly specialized trade, a unique craft struggling with technological advances. Sign Painters, directed by Faythe Levine and Sam Macon, stylistically explores this unacknowledged art form through anecdotal accounts from artists across the country including Ira Coyne, Bob Dewhurst, Keith Knecht, Norma Jeane Maloney and Stephen Powers.</p>
<p>Check out the <a href="http://www.signpaintersfilm.com/">film&#8217;s website</a>, or learn more over at <a href="http://www.imdb.com/title/tt3358998/">IMDB</a>.</p>
<h3><a href="https://www.flickr.com/groups/manicule/">Manicules</a></h3>
<p><a href="https://www.flickr.com/groups/manicule/"><img src="http://cdn.ilovetypography.com/img/2014/09/manicules-500.jpg" alt="Manicules" width="500" height="667" class="alignnone size-full wp-image-15318" /></a></p>
<p>The term &#8220;manicule&#8221; is from the Latin <em>maniculum</em>, or &#8220;little hand,&#8221; and denotes the typographic finger-pointing hands you&#8217;ve no doubt seen on retro posters and other typographic paraphernalia.</p>
<p>Here&#8217;s a lovely type specimen demonstrating a series of oversized manicules, as captured by Nick Sherman at the <a href="https://www.flickr.com/groups/manicule/">Flickr ☞ Manicule Pool</a>.</p>
<p>(As seen at <a href="http://www.shadycharacters.co.uk/2014/07/miscellany-52-eric-gill-manicules/">Shady Characters</a>. For more information on manicules, here&#8217;s a good article over at <a href="http://www.slate.com/blogs/lexicon_valley/2013/09/30/the_manicule_a_hand_with_a_pointing_index_finger_becomes_a_common_marginalia.html">Slate</a>.)</p>
<h3><a href="http://www.calameo.com/read/0002199635148ee18fd4e">New Book: Jean François Porchez&#8217;s Monograph</a></h3>
<p><a href="http://www.calameo.com/read/0002199635148ee18fd4e"><img src="http://cdn.ilovetypography.com/img/2014/09/porchez-500.jpg" alt="porchez-500" width="500" height="313" class="alignnone size-full wp-image-15319" /></a></p>
<p>This monograph on the work of Jean François Porchez, published by Atelier Perrousseaux éditeur, features contributions by Karen Cheng, Aaron Levin, Muriel Paris and Sumner Stone. Porchez is a French type designer and founder of Typofonderie. He was president of ATypI (Association Typographique Internationale), the leading organisation of type designers from 2004 to 2007.</p>
<p>(Found at: <a href="http://typofonderie.com/gazette/post/jean-francois-porchez-perrousseaux-artazart-book-launch/">Typofonderie.com</a>. Preview the book at <a href="http://www.calameo.com/read/0002199635148ee18fd4e">Calameo.com</a>, or purchase at <a href="http://www.adverbum.fr/jean-francois-porchez--atelier-perrousseaux_ouvrage-perrousseaux_4wpinkqrydcp.html">Adverbum.fr</a>.)</p>
<h3><a href="http://bartvollebregt.com/projects/lettering">Bart Vollebregt Lettering</a></h3>
<p><a href="http://bartvollebregt.com/projects/lettering"><img src="http://cdn.ilovetypography.com/img/2014/09/bart-vollebregt-500.jpg" alt="bart-vollebregt-500" width="500" height="380" class="alignnone size-full wp-image-15320" /></a></p>
<p>Bart Vollebregt is a typographic designer based in the Netherlands. His work is characterized by lively experimental typography. He strives to tell stories with pure letterforms often with a poetic approach.</p>
<h3><a href="http://opentypecookbook.com/">The OpenType Cookbook</a></h3>
<p><a href="http://opentypecookbook.com/"><img src="http://cdn.ilovetypography.com/img/2014/09/open-type-cookbook-500.jpg" alt="open-type-cookbook-500" width="500" height="322" class="alignnone size-full wp-image-15321" /></a></p>
<p>The OpenType Cookbook is a creation of font creator and software developer Tal Leming. OpenType features allow fonts to behave smartly. This behavior can do simple things (e.g. change letters to small caps) or they can do complex things (e.g. insert swashes, alternates, and ligatures to make text set in a script font feel handmade). This cookbook aims to be a designer friendly introduction to understanding and developing these features.</p>
<h3><a href="https://www.youtube.com/watch?v=JfdTZDNE3Xw">Yves Peters: Sexy Type</a></h3>
<p><iframe width="500" height="281" src="https://www.youtube.com/embed/JfdTZDNE3Xw?feature=oembed" frameborder="0" allowfullscreen></iframe></p>
<p><a href="https://twitter.com/BaldCondensed">Yves Peters</a> at <a href="http://creativemornings.com/talks">CreativeMornings</a> in Minneapolis, April 2014, talking about how typography can, er, service erotic movie poster design. One of his key conclusions: &#8220;Cooper Black is one of the sexiest typefaces ever.&#8221;</p>
<h3><a href="https://www.kickstarter.com/projects/louis-jack/gill-and-gill-a-film-comparing-bouldering-and-ston?ref=ksr_staff">Gill &amp; Gill: a film comparing bouldering and stone carving</a></h3>
<p><a href="https://www.kickstarter.com/projects/louis-jack/gill-and-gill-a-film-comparing-bouldering-and-ston?ref=ksr_staff"><img src="http://cdn.ilovetypography.com/img/2014/09/gills-500.jpg" alt="gills-500" width="500" height="466" class="" /></a></p>
<p>A visual essay exploring humanity&#8217;s relationship with stone by juxtaposing two masters: one of rock climbing, the other of letter cutting. The film looks at the way these two seemingly very different practices, united by a common material, share basic principles such as: creativity, discipline, dedication, muscle memory and balance. Created by Louis-Jack Horton-Stephens, an artist and filmmaker working in London.</p>
<h3><a href="https://www.flickr.com/photos/internetarchivebookimages">Internet Archive Book Images</a></h3>
<p><a href="https://www.flickr.com/photos/internetarchivebookimages"><img src="http://cdn.ilovetypography.com/img/2014/09/internet-archive.jpg" alt="internet-archive" width="500" height="299" /></a></p>
<p>A vast new resource of images including many typographic gems, thanks to Kalev Leetaru, a Yahoo! Fellow in Residence at Georgetown University. Head over to Flickr and search through an archive of 2.6 million public domain images, all extracted from books, magazines and newspapers published over a 500 year period. The Flickr project draws on 600 million pages that were originally scanned by the Internet Archive. And it uses special software to extract images from those pages, plus the text that surrounds the images.</p>
<p>(Found on <a href="http://www.openculture.com/2014/08/new-flickr-archive-makes-available-2-6-million-images-from-books.html">OpenCulture.com</a>.)</p>
<h3><a href="http://spitalfieldslife.com/2014/08/23/at-the-caslon-letter-foundry-2/">At The Caslon Letter Foundry</a></h3>
<p><a href="http://spitalfieldslife.com/2014/08/23/at-the-caslon-letter-foundry-2/"><img src="http://cdn.ilovetypography.com/img/2014/09/caslon-500.jpg" alt="caslon-500" width="500" height="418" class="alignnone size-full wp-image-15324" /></a></p>
<p>A collection of photographs of the Caslon Letter Foundry in the St Bride Printing Library, found while researching the work of William Caslon, the first British type founder.</p>
<h3><a href="http://www.myfonts.com/fonts/kyle-wayne-benson/kansas-casual/">Kansas Casual &mdash; A New Font</a></h3>
<p><a href="http://www.myfonts.com/fonts/kyle-wayne-benson/kansas-casual/"><img src="http://cdn.ilovetypography.com/img/2014/09/kansas-500.jpg" alt="kansas-500" width="500" height="250" class="alignnone size-full wp-image-15325" /></a></p>
<p>Kansas Casual offers a more upright, gothic, and modern alternative to the conventional sign painter’s one stroke. Kansas provides a completely unique take on a overdone classic with proportions and crossbar heights inspired by the more friendly Chicago style.</p>
<h3><a href="http://www.skillshare.com/classes/design/Typography-That-Works-Typographic-Composition-and-Fonts/1694217981">Free Online Typography Course with Ellen Lupton</a></h3>
<p><a href="http://www.skillshare.com/classes/design/Typography-That-Works-Typographic-Composition-and-Fonts/1694217981"><img src="http://cdn.ilovetypography.com/img/2014/09/lupton-500.jpg" alt="lupton-500" width="500" height="280" class="alignnone size-full wp-image-15347" /></a></p>
<p>Have you checked out Skillshare? It&#8217;s an online learning community to master real-world skills through project-based classes. They have a <a href="http://www.skillshare.com/search?query=typography">bunch of typo-centric courses</a>, but one in particular stands out: a free course from world-class typographer, designer, and educator Ellen Lupton. The course is titled &#8220;Typography That Works: Typographic Composition and Fonts&#8221;, and delves into typographic terminology, practice, and artistry through lectures and design projects.</p>
<h3><a href="http://www.amazon.com/Typographic-Universe-Steven-Heller/dp/0500241457">Typographic Universe &mdash; New Book</a></h3>
<p><a href="http://www.amazon.com/Typographic-Universe-Steven-Heller/dp/0500241457"><img src="http://cdn.ilovetypography.com/img/2014/09/heller-500.jpg" alt="heller-500" width="500" height="701" class="alignnone size-full wp-image-15326" /></a></p>
<p><em>Typographic Universe</em> by Steven Heller and Gail Anderson. Unlike most books on typography that present the “best” and most refined examples, the object here is to reveal the &#8220;lost&#8221; or &#8220;unseen&#8221; typographies in nature and our cities. From machine-made and sculptural forms to flora and fauna, from the fading ghost types on buildings from a pre-digital age to the subterranean forms found beneath our urban centers, from crowd-sourced creations to the popular vernacular, there is a universe of letterforms all around us.</p>
<p>Steven Heller is co-chair of the MFA Design: Designer as Author program at the School of Visual Arts, New York. His many previous books include <em>New Modernist Type</em> and <em>Scripts</em>. Gail Anderson is creative director of design at SpotCo, New York, and a former senior art director of <em>Rolling Stone</em> magazine.</p>
<h3><a href="http://ladiesofletterpress.com/">Ladies of Letterpress</a></h3>
<p><a href="http://ladiesofletterpress.com/"><img src="http://cdn.ilovetypography.com/img/2014/09/lolp-500.jpg" alt="lolp-500" width="500" height="344" class="alignnone size-full wp-image-15327" /></a></p>
<p>Ladies of Letterpress is an international trade organization for letterpress printers and print enthusiasts. Their mission is to promote the art and craft of letterpress printing and to encourage the voice and vision of women printers. They strive to maintain the cultural legacy of fine press printing while advancing it as a living, contemporary art form, as well as a viable commercial printing method. Membership is open to both men and women. This is a community where you can read about adventures in commercial, fine press, art and zine printing, ask for advice and learn from other printers, share resources, and get inspiration for your own business and work &mdash; all for the love of letterpress. </p>
<h3><a href="https://affinity.serif.com/">Affinity Designer</a></h3>
<p><a href="https://affinity.serif.com/"><img src="http://cdn.ilovetypography.com/img/2014/09/affinity-500.jpg" alt="affinity-500" width="500" height="292" class="alignnone size-full wp-image-15338" /></a></p>
<p>In open beta for Mac, Affinity Designer touts itself as an incredibly accurate vector illustrator that feels fast and at home in the hands of creative professionals. It intuitively combines rock solid and crisp vector art with flexible layer management and an impressive range of high quality raster tools for finishing. With accuracy, quality and speed at the heart of every single design task, and the ability to finesse designs without switching apps, this fresh-faced multi-discipline illustrator lets creatives shine.</p>
<p>With full OpenType support, Affinity Designer just might be able eventually to dent the Adobe vector editing monopoly.</p>
<h3><a href="http://www.myfonts.com/fonts/context/elise/">Elise &mdash; A New Font</a></h3>
<p><a href="http://www.myfonts.com/fonts/context/elise/"><img src="http://cdn.ilovetypography.com/img/2014/09/elise-500.jpg" alt="elise-500" width="500" height="250" class="alignnone size-full wp-image-15328" /></a></p>
<p>Elise is a sweet natured, layered display typeface, with a few layers but a wealth of options. From the feminine to the fun to the nostalgic, Elise is a capable and personable set. Best used big and with color, you’ll always find an occasion for Elise’s charm.</p>
<h3><a href="http://books.google.com/books?id=dTwuAAAAYAAJ">John Baskerville: Type-founder and Printer, 1706-1775 (Google eBook)</a></h3>
<p><a href="http://books.google.com/books?id=dTwuAAAAYAAJ"><img src="http://cdn.ilovetypography.com/img/2014/09/baskerville-500.jpg" alt="baskerville-500" width="500" height="288" class="alignnone size-full wp-image-15329" /></a></p>
<p>This short biography of John Baskerville was published in 1914 by Josiah Henry Benton, an American lawyer and author. Baskerville, born in Worcestershire, set up as a writing-master and letter-cutter in Birmingham, but later built up a business in &#8216;japanning&#8217;, the imitation of Japanese lacquer work, from which he made his fortune. He began working as a type-founder and printer around 1750, and made innovations not only in typefaces but also in paper, ink and printing machines. The quality of his books made them collectors&#8217; items.</p>
<p>If you want a hard copy of the book, <a href="http://www.cambridge.org/us/academic/subjects/literature/english-literature-1700-1830/john-baskerville-type-founder-and-printer-17061775">Cambridge University Press is planning</a> to publish one in October. </p>
<h3><a href="http://sploid.gizmodo.com/these-amazing-architectural-alphabet-engravings-should-1614474690">Antonio Basoli&#8217;s Alfabeto Pittorico</a></h3>
<p><a href="http://sploid.gizmodo.com/these-amazing-architectural-alphabet-engravings-should-1614474690"><img src="http://cdn.ilovetypography.com/img/2014/09/basoli-500.jpg" alt="basoli-500" width="500" height="388" class="alignnone size-full wp-image-15330" /></a></p>
<p>Antonio Basoli was an Italian artist that lived during the 18th and the 19th century, and worked mostly in Bologna. Among other things, he created these beautiful architectural alphabet engravings called <em>Alfabeto Pittorico</em>.</p>
<h3><a href="http://www.mr-cup.com/blog/varied-others/item/the-neo-museum-of-las-vegas.html">The Neon Museum of Las Vegas</a></h3>
<p><a href="http://www.mr-cup.com/blog/varied-others/item/the-neo-museum-of-las-vegas.html"><img src="http://cdn.ilovetypography.com/img/2014/09/vegas-500.jpg" alt="vegas-500" width="500" height="375" class="alignnone size-full wp-image-15331" /></a></p>
<p>The <a href="http://www.neonmuseum.org/">Neon Museum Boneyard</a> in Las Vegas has a two-acre campus which includes an outdoor exhibition space, known as the Boneyard, featuring more than 150 signs, a visitors&#8217; center housed inside the former La Concha Motel lobby, and the Neon Boneyard North Gallery which houses additional rescued signs and is used for weddings, special events, photo shoots and educational programs.</p>
<h3><a href="http://instagram.com/arabictypography">Arabic Lettering on Instagram</a></h3>
<p><a href="http://instagram.com/arabictypography"><img src="http://cdn.ilovetypography.com/img/2014/09/hand-500.jpg" alt="hand-500" width="500" height="501" class="alignnone size-full wp-image-15332" /></a></p>
<p>Cataloging arabic typography from all corners of the world.</p>
<h3><a href="http://www.stonetypefoundry.com/typohaug2014.html">Sumner Stone on Trajan</a></h3>
<p><a href="http://www.stonetypefoundry.com/typohaug2014.html"><img src="http://cdn.ilovetypography.com/img/2014/09/trajan-500.jpg" alt="trajan-500" width="500" height="334" class="alignnone size-full wp-image-15333" /></a></p>
<p>Type designer Sumner Stone has a new blog &mdash; and the first post details the creation of Adobe Type&#8217;s Trajan.</p>
<h3><a href="http://spiekermann.com/en/letterpress-in-berlin/">Erik Spiekermann on Letterpress</a></h3>
<p><iframe src="//player.vimeo.com/video/95682050" width="500" height="281" frameborder="0" title="Letterpress printing at P98a" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>
<p>Letterpress printing in Berlin, with the illustrious Erik Spiekermann.</p>
<h3><a href="http://usshop.gestalten.com/hello-erik.html">A New Book About Erik Spiekermann</a></h3>
<p><a href="http://usshop.gestalten.com/hello-erik.html"><img src="http://cdn.ilovetypography.com/img/2014/09/erik-500.jpg" alt="erik-500" width="500" height="324" class="alignnone size-full wp-image-15334" /></a></p>
<p>The visual biography <em>Hello, I am Erik</em> is the first comprehensive exploration of Spiekermann&#8217;s more than 30-year career, his body of work, and his mindset. Contributions by Michael Bierut, Neville Brody, Mirko Borsche, Wally Olins, Stefan Sagmeister, Christian Schwartz, Erik van Blokland, and others round out this insightful publication.</p>
<h3><a href="http://www.brainpickings.org/2014/05/23/typewriter-art-laurence-king/">A Visual History of Typewriter Art from 1893 to Today</a></h3>
<p><a href="http://www.brainpickings.org/2014/05/23/typewriter-art-laurence-king/"><img src="http://cdn.ilovetypography.com/img/2014/09/typewriter-500.jpg" alt="typewriter-500" width="500" height="671" class="alignnone size-full wp-image-15335" /></a></p>
<p>A fascinating chronicle of the development of the typewriter as a medium for creating work far beyond anything envisioned by the machine&#8217;s makers, embedded in which is a beautiful allegory for how all technology is eventually co-opted as an unforeseen canvas for art and political statement.</p>
<h3><a href="http://medievalbooks.nl/2014/09/05/getting-personal-in-the-margin/">Medieval Marginalia</a></h3>
<p><a href="http://medievalbooks.nl/2014/09/05/getting-personal-in-the-margin/"><img src="http://cdn.ilovetypography.com/img/2014/09/margin-500.jpg" alt="margin-500" width="500" height="277" class="alignnone size-full wp-image-15336" /></a></p>
<p>Word and images are often found in the margins of medieval books, placed there by scribes and readers. In most books, there was ample room  to add such details, because on average a stunning fifty percent of the medieval page was left blank. It is in this vast emptiness, so often overlooked in editions of texts, that we may pick up key information about the long life of the book.</p>
<h3><a href="http://vimeo.com/103907606">Dan Rhatigan on type&#8230; Type on Dan Rhatigan</a></h3>
<p><iframe src="//player.vimeo.com/video/103907606" width="500" height="281" frameborder="0" title="Dan Rhatigan on type...Type on Dan Rhatigan" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>
<p>Monotype&#8217;s Dan Rhatigan on his passion for typography &mdash; and the fonts he loves so much he&#8217;s had them tattooed onto his body.</p>
<h3><a href="https://www.youtube.com/watch?v=3BmpriscLu8">Scarlett Naming Fonts</a></h3>
<p><iframe width="500" height="281" src="https://www.youtube.com/embed/3BmpriscLu8?feature=oembed" frameborder="0" allowfullscreen></iframe></p>
<p>A precocious typographer!</p>
<h3><a href="http://next.fontshop.com/content/from-compressed-light-to-extended-ultra/">From Compressed Light to Extended Ultra</a></h3>
<p><a href="http://next.fontshop.com/content/from-compressed-light-to-extended-ultra/"><img src="http://cdn.ilovetypography.com/img/2014/09/typesystem-500.jpg" alt="typesystem-500" width="500" height="246" class="alignnone size-full wp-image-15349" /></a></p>
<p>A great article on visual systems in type designs: anything from a series of widths to a typeface with a spectrum of more than two or four weights (regular, bold and respective italics used to be common) to establishing parameters which allow for an interrelated combination of different styles (mixing sans with serif or with slab or any other hybrid version of these). </p>
<h3><a href="http://showcase.commercialtype.com/">Commercial Type Webfonts Showcase</a></h3>
<p><a href="http://showcase.commercialtype.com/"><img src="http://cdn.ilovetypography.com/img/2014/09/webfonts-500.jpg" alt="webfonts-500" width="500" height="258" class="alignnone size-full wp-image-15350" /></a></p>
<p>A really lovely (and useful) webfonts showcase of 16 microsites, to try to show Commercial Type&#8217;s webfonts in a different context and do something different from the usual &#8216;interactive type specimen&#8217;.</p>
<h3><a href="http://khan.github.io/KaTeX/">KaTeX &mdash; math typesetting for the web</a></h3>
<p><a href="http://khan.github.io/KaTeX/"><img src="http://cdn.ilovetypography.com/img/2014/09/katex.jpg" alt="katex" width="500" height="161" class="alignnone size-full wp-image-15361" /></a></p>
<p>Do you need complex mathematical expressions rendered on your web pages? There are a few options out there, but KaTeX claims to be the fastest math typesetting library for the web. A free and open source Javascript library, it does indeed render beautiful output.</p>
<h3><a href="http://www.marksimonson.com/notebook/view/adventures-in-roller-calligraphy">Adventures in Roller Calligraphy with Mark Simonson</a></h3>
<p><a href="http://www.marksimonson.com/notebook/view/adventures-in-roller-calligraphy"><img src="http://cdn.ilovetypography.com/img/2014/09/simonson.jpg" alt="simonson" width="500" height="357" class="alignnone size-full wp-image-15362" /></a></p>
<p>Type designer and self-proclaimed calligraphy avoider Mark Simonson traveled to the Gutenberg Museum&#8217;s print shop and created some lovely calligraphic pieces, using inking rollers. Read all about his adventure on Mark&#8217;s blog.</p>
<h3><a href="http://stoneletters.com/video">An interview with stone cutter Fergus Wessel</a></h3>
<p><iframe class="wistia_embed" name="wistia_embed" src="http://fast.wistia.net/embed/iframe/g1e12xcjj2?canonicalUrl=http%3A%2F%2Fstoneletters.com%2Fvideo&#038;canonicalTitle=video%20%7C%20Stoneletters" allowtransparency="true" frameborder="0" scrolling="no" width="480" height="298"></iframe><br/></p>
<p><em>The Spirit of Letter Carving</em></p>
<p>There’s also an <a href="http://ilovetypography.com/2012/03/09/letters-stone-interview-fergus-wessel/">interview with Fergus</a> here on ILT.</p>
<h3><a href="https://medium.com/re-form/a-refutation-of-the-elements-of-typographic-style-3b18c07977f3">A Refutation of the Elements of Typographic Style</a></h3>
<p><a href="https://medium.com/re-form/a-refutation-of-the-elements-of-typographic-style-3b18c07977f3"><img src="http://cdn.ilovetypography.com/img/2014/09/bringhurst-refute-500.jpg" alt="bringhurst-refute-500" width="500" height="268" class="alignnone size-full wp-image-15379" /></a></p>
<p>A long read, but worth it, is this treatise critiquing Bringhurst&#8217;s seminal reference text.</p>
<p>It’s the typography reference book you’ve heard of. The one everyone recommends to everyone else. If you’re a student it’s probably at the top of your list of resources; if you’re a teacher, you probably put it there. But is it a <em>good</em> book?</p>
<h3><a href="http://matadornetwork.com/abroad/5-beautiful-endangered-alphabets/">Five Beautiful Endangered Alphabets</a></h3>
<p><a href="http://matadornetwork.com/abroad/5-beautiful-endangered-alphabets/"><img src="http://cdn.ilovetypography.com/img/2014/09/burmese-aphabet-script.jpg" alt="burmese-aphabet-script" width="500" height="332" class="alignnone size-full wp-image-15368" /></a></p>
<p>Check out Felipe Sant&#8217;Ana Pereira’s take on, what for some, will be unfamiliar scripts.</p>
<p class="footnote">Photo credit: <a href="https://www.flickr.com/photos/yoanngd/">Yoann Gruson-Daniel.</a></p>
<h3><a href="http://vimeo.com/106269260">Martina Flor on Lettering Techniques</a></h3>
<p><iframe src="//player.vimeo.com/video/106269260" width="500" height="281" frameborder="0" title="BerlinLettering" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></p>
<p>Martina Flor’s short and instructive video demo on lettering sketching techniques.</p>
<h3><a href="http://youtu.be/COZl4EwETqs">Caslon Invaders</a></h3>
<p><iframe width="500" height="281" src="https://www.youtube.com/embed/COZl4EwETqs?feature=oembed" frameborder="0" allowfullscreen></iframe></p>
<p>And to conclude, something completely different and rather silly. <a href="http://youtu.be/COZl4EwETqs">Caslon Invaders</a>.</p>
<hr />
<p></p>
<p><em>Edited by <a href="http://www.myfonts.com/foundry/Haiku_Monkey/">Alec Julien</a></em></p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/09/26/monthly-typography-fonts-roundup-1/">This Month in Typography</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=rmDFNi4yeUI:sXOYlX4pGL8:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=rmDFNi4yeUI:sXOYlX4pGL8:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=rmDFNi4yeUI:sXOYlX4pGL8:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=rmDFNi4yeUI:sXOYlX4pGL8:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=rmDFNi4yeUI:sXOYlX4pGL8:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=rmDFNi4yeUI:sXOYlX4pGL8:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=rmDFNi4yeUI:sXOYlX4pGL8:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=rmDFNi4yeUI:sXOYlX4pGL8:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=rmDFNi4yeUI:sXOYlX4pGL8:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=rmDFNi4yeUI:sXOYlX4pGL8:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/rmDFNi4yeUI" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:78:"http://ilovetypography.com/2014/09/26/monthly-typography-fonts-roundup-1/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:73:"http://ilovetypography.com/2014/09/26/monthly-typography-fonts-roundup-1/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:45:"
		
		
		
		
		
				
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"This Week in Fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/LOXhR4eu-IE/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:69:"http://ilovetypography.com/2014/09/09/this-week-in-fonts-29/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 09 Sep 2014 08:44:31 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"new fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15302";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:937:"A highly functional sans by HVD Fonts, a powerful script from Dai Foldes, a hard working slab by Fort Foundry, a charming script from Drew Melton, an energetic script by Laura Worthington, a unique text face from Canada Type, a versatile sans by Stone Type Foundry, and an ambitious slab from Hoftype. HVD Fonts: Brix [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/09/09/this-week-in-fonts-29/">This Week in Fonts</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sean Mitchell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6118:"<p>A highly functional sans by <a href="http://tprls.me/1oUYwzE">HVD Fonts</a>, a powerful script from <a href="http://tprls.me/1lFaDAY">Dai Foldes</a>, a hard working slab by <a href="http://tprls.me/ZaNt0D">Fort Foundry</a>, a charming script from <a href="http://tprls.me/1xnsdUF">Drew Melton</a>, an energetic script by <a href="http://tprls.me/1pMI0D5">Laura Worthington</a>, a unique text face from <a href="http://tprls.me/1lM1b4q">Canada Type</a>, a versatile sans by <a href="http://tprls.me/1u73ekz">Stone Type Foundry</a>, and an ambitious slab from <a href="http://tprls.me/Yjgs26">Hoftype</a>.</p>
<p><span id="more-15302"></span></p>
<h3><a href="http://tprls.me/1oUYwzE">HVD Fonts: Brix Sans</a></h3>
<p><em>Designed by Hannes von Döhren &amp; Livius Dietzel</em></p>
<p><a href="http://tprls.me/1oUYwzE"><img src="http://cdn.ilovetypography.com/img/2014/09/brix-sans.png" alt="" /></a></p>
<p><a href="http://tprls.me/1oUYwzE">Brix Sans</a> is a family of 6 weights with matching italics, which works perfectly for corporate &amp; editorial design. Combined with <a href="http://tprls.me/1oOMfx8">Brix Slab</a>, high and complex typographical challenges can be solved.</p>
<h3><a href="http://tprls.me/1lFaDAY">Dai Foldes: Globe Script</a></h3>
<p><em>Designed by Dai Foldes</em></p>
<p><a href="http://tprls.me/1lFaDAY"><img src="http://cdn.ilovetypography.com/img/2014/09/globe-script.png" alt="" /></a></p>
<p>With tenacious rhythm and dynamic connections, <a href="http://tprls.me/1lFaDAY">Globe Script</a> gives power to your headings and overlays.</p>
<h3><a href="http://tprls.me/ZaNt0D">Fort Foundry: Factoria</a></h3>
<p><em>Designed by Mattox Shuler</em></p>
<p><a href="http://tprls.me/ZaNt0D"><img src="http://cdn.ilovetypography.com/img/2014/09/factoria.png" alt="" /></a></p>
<p>Born out of <a href="http://tprls.me/1updLH2">Industry</a>, <a href="http://tprls.me/ZaNt0D">Factoria</a> is a geometric, square slab. The hard-working family can jump from the side of an industrial building and into a sports magazine in a jiffy.</p>
<h3><a href="http://tprls.me/1xnsdUF">Drew Melton: Ballpoint Script</a></h3>
<p><em>Designed by Drew Melton</em></p>
<p><a href="http://tprls.me/1xnsdUF"><img src="http://cdn.ilovetypography.com/img/2014/09/ballpoint-script.png" alt="" /></a></p>
<p><a href="http://http://tprls.me/1xnsdUF">Ballpoint Script&#8217;s</a> smooth, single-weight lines and charming variations provide the ideal balance of humanity and clarity.</p>
<h3><a href="http://tprls.me/1pMI0D5">Laura Worthington: Voltage</a></h3>
<p><em>Designed by Laura Worthington</em></p>
<p><a href="http://tprls.me/1pMI0D5"><img src="http://cdn.ilovetypography.com/img/2014/09/voltage.png" alt="" /></a></p>
<p><a href="http://tprls.me/1pMI0D5">Voltage</a> is an unexpected and energetic standout in the world of script fonts, breaking free from formal classifications while retaining the degree of personality we treasure in hand lettering.</p>
<h3><a href="http://tprls.me/1lM1b4q">Canada Type: Dokument Pro</a></h3>
<p><em>Designed by Jim Rimmer</em></p>
<p><a href="http://tprls.me/1lM1b4q"><img src="http://cdn.ilovetypography.com/img/2014/09/dokument-pro.png" alt="" /></a></p>
<p><a href="http://tprls.me/1lM1b4q">Dokument Pro&#8217;s</a> range of weights, styles and features allows for multi-application versatility and clear, precise emotional delivery.</p>
<h3><a href="http://tprls.me/1u73ekz">Stone Type Foundry: Magma II</a></h3>
<p><em>Designed by Sumner Stone</em></p>
<p><a href="http://tprls.me/1u73ekz"><img src="http://cdn.ilovetypography.com/img/2014/09/magma.png" alt="" /></a></p>
<p><a href="http://tprls.me/1u73ekz">Magma II</a> is a rare sans serif typeface family designed explicitly for use in both text and display applications.</p>
<h3><a href="http://tprls.me/Yjgs26">Hoftype: Orgon Slab</a></h3>
<p><em>Designed by Dieter Hofrichter</em></p>
<p><a href="http://tprls.me/Yjgs26"><img src="http://cdn.ilovetypography.com/img/2014/09/orgon-slab.png" alt="" /></a></p>
<p><a href="http://tprls.me/Yjgs26">Orgon Slab</a> consists of 16 styles and is well suited for ambitious typography.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/09/09/this-week-in-fonts-29/">This Week in Fonts</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=LOXhR4eu-IE:nHKkQxggi4E:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=LOXhR4eu-IE:nHKkQxggi4E:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=LOXhR4eu-IE:nHKkQxggi4E:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=LOXhR4eu-IE:nHKkQxggi4E:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=LOXhR4eu-IE:nHKkQxggi4E:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=LOXhR4eu-IE:nHKkQxggi4E:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=LOXhR4eu-IE:nHKkQxggi4E:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=LOXhR4eu-IE:nHKkQxggi4E:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=LOXhR4eu-IE:nHKkQxggi4E:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=LOXhR4eu-IE:nHKkQxggi4E:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/LOXhR4eu-IE" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:65:"http://ilovetypography.com/2014/09/09/this-week-in-fonts-29/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:60:"http://ilovetypography.com/2014/09/09/this-week-in-fonts-29/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:45:"
		
		
		
		
		
				
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"This Week in Fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/sUHUaPZ1V50/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:69:"http://ilovetypography.com/2014/08/19/this-week-in-fonts-28/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Mon, 18 Aug 2014 18:15:48 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"new fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15280";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:959:"A fast moving script by Process Type Foundry, a versatile sans from The Northern Block, a decorative serif by Zeune Ink, an energetic script from Sudtipos, a characterful grotesque by Commercial Type, a harmonious slab from Dada Studio, a comfortable sans by Font Bureau, a spirited grotesque from Latinotype, a condensed sans by MCKL, and [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/08/19/this-week-in-fonts-28/">This Week in Fonts</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sean Mitchell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6895:"<p>A fast moving script by <a href="http://tprls.me/WQtraq">Process Type Foundry</a>, a versatile sans from <a href="http://tprls.me/Ua06Gj">The Northern Block</a>, a decorative serif by <a href="http://tprls.me/1ouWhTG">Zeune Ink</a>, an energetic script from <a href="http://tprls.me/1n5VkVx">Sudtipos</a>, a characterful grotesque by <a href="http://tprls.me/1oYNSbK">Commercial Type</a>, a harmonious slab from <a href="http://tprls.me/1lk6l1M">Dada Studio</a>, a comfortable sans by <a href="http://tprls.me/1l1IzYe">Font Bureau</a>, a spirited grotesque from <a href="http://tprls.me/1kqEeTI">Latinotype</a>, a condensed sans by <a href="http://tprls.me/1pUyQ8Q">MCKL</a>, and a neutral face from <a href="http://tprls.me/1jTzSnr">Type Dynamic</a>.</p>
<p><span id="more-15280"></span></p>
<h3><a href="http://tprls.me/WQtraq">Process Type Foundry: Pique</a></h3>
<p><em>Designed by Nicole Dotin</em></p>
<p><a href="http://tprls.me/WQtraq"><img src="http://cdn.ilovetypography.com/img/2014/08/pique.png" alt="" /></a></p>
<p>A script with a crisp energy and buoyancy that only the collaboration of paper and screen can lay claim to.</p>
<h3><a href="http://tprls.me/Ua06Gj">The Northern Block: Loew</a></h3>
<p><em>Designed by Jonathan Hill</em></p>
<p><a href="http://tprls.me/Ua06Gj"><img src="http://cdn.ilovetypography.com/img/2014/08/loew.png" alt="" /></a></p>
<p><a href="http://tprls.me/Ua06Gj">Loew</a> is a versatile sans serif with simple and honest geometry aimed at a wide range of modern applications.</p>
<h3><a href="http://tprls.me/1ouWhTG">Zeune Ink: Wallington</a></h3>
<p><em>Designed by Sandi Dez</em></p>
<p><a href="http://tprls.me/1ouWhTG"><img src="http://cdn.ilovetypography.com/img/2014/08/wallington.png" alt="" /></a></p>
<p><a href="http://tprls.me/1ouWhTG">Wallington</a> is a decorative serif embodying vintage &amp; elegant curves with functional structure.</p>
<h3><a href="http://tprls.me/1n5VkVx">Sudtipos: Horizontes Script</a></h3>
<p><em>Designed by Panco Sassano &amp; Alejandro Paul</em></p>
<p><a href="http://tprls.me/1n5VkVx"><img src="http://cdn.ilovetypography.com/img/2014/08/horizontes-script.png" alt="" /></a></p>
<p>Relaxed, energic and very natural. With different alternatives of proportion, a wide range of ligatures, initial letters, terminals, floritures, <a href="http://tprls.me/1n5VkVx">Horizontes Script</a> comes in two weight for large and small formats.</p>
<h3><a href="http://tprls.me/1oYNSbK">Commercial Type: Marr Sans</a></h3>
<p><em>Designed by Paul Barnes &amp; Dave Foster</em></p>
<p><a href="http://tprls.me/1oYNSbK"><img src="http://cdn.ilovetypography.com/img/2014/08/marr-sans.png" alt="" /></a></p>
<p><a href="http://tprls.me/1oYNSbK">Marr Sans</a> revels in the individuality of the nineteenth century, and is like an eccentric British uncle to Morris Fuller Benton&#8217;s Franklin and News Gothics.</p>
<h3><a href="http://tprls.me/1lk6l1M">Dada Studio: Servus Slab</a></h3>
<p><em>Designed by Michał Jarociński</em></p>
<p><a href="http://tprls.me/1lk6l1M"><img src="http://cdn.ilovetypography.com/img/2014/08/servus-slab.png" alt="" /></a></p>
<p>The light and bold weights are perfect for display use and the regular weights create a harmonious structure that provides good legibility in long texts.</p>
<h3><a href="http://tprls.me/1l1IzYe">Font Bureau: Apres</a></h3>
<p><em>Designed by David Berlow</em></p>
<p><a href="http://tprls.me/1l1IzYe"><img src="http://cdn.ilovetypography.com/img/2014/08/apres.png" alt="" /></a></p>
<p>The plain-spoken geometry is regular and balanced, without being static or mechanical, for a friendly and forthright familiarity.</p>
<h3><a href="http://tprls.me/1kqEeTI">Latinotype: Grota Sans</a></h3>
<p><em>Designed by Eli Hernández &amp; Daniel Hernández</em></p>
<p><a href="http://tprls.me/1kqEeTI"><img src="http://cdn.ilovetypography.com/img/2014/08/grota-sans.png" alt="" /></a></p>
<p>A complete family of 40 fonts, 10 different weights and their respective cursives, and an alt version, <a href="http://tprls.me/1kqEeTI">Grota Sans</a> is a grotesque font with a latin spirit.</p>
<h3><a href="http://tprls.me/1pUyQ8Q">MCKL: Fort Condensed</a></h3>
<p><em>Designed by Jeremy Mickel</em></p>
<p><a href="http://tprls.me/1pUyQ8Q"><img src="http://cdn.ilovetypography.com/img/2014/08/fort-condensed.png" alt="" /></a></p>
<p>Neutral enough to take on information design, corporate identity, and small text sizes, the refined details and personality of <a href="http://tprls.me/1pUyQ8Q">Fort Condensed</a> shine in display.</p>
<h3><a href="http://tprls.me/1jTzSnr">Type Dynamic: Sailec</a></h3>
<p><a href="http://tprls.me/1jTzSnr"><img src="http://cdn.ilovetypography.com/img/2014/08/sailec.png" alt="" /></a></p>
<p><a href="http://tprls.me/1jTzSnr">Sailec</a> is a low contrast, neutral typeface that includes 7 weights, from hairline to black, with corresponding italics.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/08/19/this-week-in-fonts-28/">This Week in Fonts</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=sUHUaPZ1V50:WyrhD7a2VEA:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=sUHUaPZ1V50:WyrhD7a2VEA:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=sUHUaPZ1V50:WyrhD7a2VEA:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=sUHUaPZ1V50:WyrhD7a2VEA:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=sUHUaPZ1V50:WyrhD7a2VEA:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=sUHUaPZ1V50:WyrhD7a2VEA:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=sUHUaPZ1V50:WyrhD7a2VEA:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=sUHUaPZ1V50:WyrhD7a2VEA:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=sUHUaPZ1V50:WyrhD7a2VEA:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=sUHUaPZ1V50:WyrhD7a2VEA:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/sUHUaPZ1V50" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:65:"http://ilovetypography.com/2014/08/19/this-week-in-fonts-28/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:60:"http://ilovetypography.com/2014/08/19/this-week-in-fonts-28/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:51:"
		
		
		
		
		
				
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"Learning to Love letters!";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/MeMyn-4Yh0E/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:72:"http://ilovetypography.com/2014/08/08/learning-to-love-letters/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 07 Aug 2014 16:04:07 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:4:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:11:"calligraphy";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:11:"make a font";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:11:"type design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15255";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:994:"I love letters. All kinds and types of letters: small, large, drawn, sketched, painted, rough, smooth, serif, sans serif, script, roman, italic, oblique, digitized, old and new, uppercase, lowercase, all materials and media, three dimensional… Yes, I love letters, except for those that are poorly or incorrectly proportioned. For those poor ugly letters, I feel [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/08/08/learning-to-love-letters/">Learning to Love letters!</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"johno";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:12521:"<p class="intro"><em>I love letters.</em> All kinds and types of letters: small, large, drawn, sketched, painted, rough, smooth, serif, sans serif, script, roman, italic, oblique, digitized, old and new, uppercase, lowercase, all materials and media, three dimensional… Yes, I love letters, <em>except </em>for those that are poorly or incorrectly proportioned. <em>For those poor ugly letters, I feel pity and sadness.</em></p>
<p><span id="more-15255"></span></p>
<div id="theDeck-1"></div>
<p>In the hope of creating an appreciation of beautiful letters and avoiding further letterform abuse, I try to share my passion for letterform design with my graphic design, typography and calligraphy students at the Rochester Institute of Technology.</p>
<p>My challenge in teaching calligraphy and typography is to make letters and words come to life: to make letters exciting, tangible and meaningful. My objective is to instill in my students a passion for the inherent beauty and integrity of letterforms, an understanding of history, terminology, structure and proportional relationships, and a highly developed sensitivity to the selection and application of alphabet designs for effective and powerful communication.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/08/2983_Frear_079.jpg" alt="2983_Frear_079" /></p>
<p class="footnote">Helping students with letters. Photograph by Elizabeth Torgerson-lamark</p>
<p>It has been my experience that not all students come to typography or graphic design with an innate love of letters or type. Over the years, I have tried many methods of infecting students with the <em>type virus</em>…the term I use to describe the typographic obsession acquired by designers. I have found that introducing calligraphy in my typography courses has been the most successful, fun and interactive way to reinforce fundamentals and to develop a high level of typographic sensitivity. It is a far more satisfying approach than tracing letters, which is viewed by most students as torture and which they pursue as such. Calligraphy, on the other hand, is a rewarding and meaningful skill that reinforces lessons learned in typography lectures and demonstrations. Graphic design student Elizabeth McGrail said: “Calligraphy is very beautiful and it was fun to learn about it as well as learning how to do it. I love knowing that I know calligraphy and I can do it whenever I want!” This year’s Hand Lettering Club President and graphic design student Elizabeth Wells stated: “After studying calligraphy, I felt that I understood the anatomy of typography better. Making characters by hand makes you think about every aspect of the letter and the words as a whole.”</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/08/2983_Frear_094.jpg" alt="2983_Frear_094" /></p>
<p class="footnote">Photograph by Elizabeth Torgerson-lamark</p>
<p>Drawing letters is an interactive, physical experience in which students learn proportions, stroke sequences, anatomy, and letter, word and line spacing without even realizing it. They begin to see letters as beautiful and functional symbols that have artistic and expressive potential. Students develop a comprehensive understanding of the systematic nature of typographic design. They see letters as physical forms and not just as shapes on a screen or a page. This in turn helps students understand the process of designing an alphabet and the challenges faced by a typographer in creating a new typeface. Many design students have an AHA! moment when drawing an oblique letter such as a W, finding that they understand stroke weight variation for the first time. Similarly, a student drawing an O by hand may comprehend the meaning of stress for the first time, although he or she may have traced an O or read previously about typographic stress in a book. Mysteries of letterform design and structure are solved when students have firsthand experience drawing letterforms. As graphic design student Lauren Spath said, “Calligraphy was one of the most eye-opening things I have done in awhile. It showed me how to appreciate the letters in typefaces I use every day and changed how I think about letterforms and their intricacy.”</p>
<p>In this 6–9 hour exposure (two or three studio sessions), students use broad-edged steel nibs and ink to draw the minuscules of Chancery Cursive. Drawing families of similar letters (such as n, h, m, r, u) helps students see the systematic foundation of alphabet design. When creating bowls, counters, serifs and flourishes, students become highly aware of the tiny, yet critical details and nuances that give typefaces personality and uniqueness. As they begin to write words, students practice the letter proportions, and incorporate letter and word spacing. In writing sentences and paragraphs, students begin to understand the importance of line length, sense breaks, and line spacing. Graphic design student Emily Butler expressed this well when she said: “Learning calligraphy helped me to understand how the letters flow together. Each character needs to work with the surrounding characters to create words and to look beautiful as a whole. Calligraphy opened my eyes to the different widths and heights of individual characters as well as the terminology that goes with typography. Calligraphy was a great introduction to learning the mechanics of typography.”</p>
<p>Studying calligraphy also provides students with great respect for the challenges faced by lettering artists and typographers in the creation of alphabets. This makes it less likely that they will bastardize typefaces in the future. <em>(Bastardization is the term I use for</em> <em>pseudo-italicizing or scaling type on the computer without making visual adjustments.)</em></p>
<p>At various times during this process, students have an AHA! moment when they see the connections between the past and present and the hand-drawn and the machine-made. Letters become exciting, tangible and meaningful.</p>
<p>Although today’s students have little experience in drawing letters, I have found that they <em>LOVE</em> working with their hands. In fact, they CRAVE it. Certainly, a part of the attraction is that it is a new experience and a welcome break from the digital world. As graphic design student Giovanni Leoni said: “The art of calligraphy is extremely zen and relaxing. It causes the artist to focus on each and every stroke, revealing the secrets of typography.” And graphic design student Samantha Watson described the experience: “Having calligraphy in a course was a very relaxing way to end the day… the kind of homework I would save as a treat because I looked forward to it.”</p>
<p>In addition to the change of pace drawing by hand provides, I believe the current trends in calligraphy and expressive lettering represent the human need for personal, unique and expressive communication that is difficult to achieve exclusively through digital media. Inspired by lettering seen on websites and blogs, students regularly experiment with lettering in their projects. A fundamental understanding of letterform structure, proportions and relationships is more important than ever if we are to help students create gorgeous, competent letters and not more ugly ones. It is critical that students have a fundamental background in order to create alphabets that are cohesive and aesthetically pleasing.</p>
<p>This is one of the many benefits of the the RIT Hand Lettering Club, a multidisciplinary group consisting of illustrators, fine artists and photographers in addition to graphic designers. Offering lectures, demonstrations and workshops, the club provides students with no typographic background with the key fundamentals of good letterform design with feedback and encouragement. Hopefully, this contributes to the creation of some beautiful new letters!</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/08/IMG_5129.jpg" alt="calligraphy practice" /></p>
<p class="footnote">Image of demo letters. Photograph &#038; letters by Lorrie Frear</p>
<p>Through the years of incorporating calligraphy in my typography courses, I have used the terminology and information from both areas in a reciprocal, back-and-forth dance. This has assisted students in gaining sensitivity to typography that occurs naturally and with a sense of personal accomplishment. I have also found that introducing calligraphy as part of a university typographic education increases refinement of design issues such as the use of negative space and composition. Graphic design student Autumn Wadsworth stated: “Learning calligraphy and lettering by hand makes a huge difference in your design skills. Every little typographic detail just makes so much more sense, and my typographic and design skills have improved because of it.”</p>
<p>In conclusion, graphic design student Rachel Nicholson provided her viewpoint:</p>
<p>“I have found an outlet in lettering. Whether it’s calligraphy or custom hand lettering, there is something so powerful about language and words in general and then to pair that with the artistic ideals of composition and balance and flow…there is a real a beauty in that. The natural connection between the pen, hand and paper is relaxing, rewarding and immediately gratifying. Lettering allows you to see the alphabets as shapes and lines. I found this new way of “seeing” not only to be a challenge, but a wonderful lesson in design: to simplify the world around you into contours and forms that have endless potential to inspire, create and function.”</p>
<p>It is greatly rewarding for me to introduce students to calligraphy and type, to witness their genuine enthusiasm for letterform design, and to observe their growth as they explore and improve. It’s an inspiring, humbling and magical experience.</p>
<p class="footnote">———</p>
<p class="footnote">Header: Photograph by Elizabeth Torgerson-lamark</p>
<p class="footnote" style="margin-bottom:2em;">AUTHOR BIO:<br />Lorrie Frear is an Associate Professor in the School of Design/College of Imaging Arts and Sciences at the Rochester Institute of Technology in Rochester, New York, where she teaches graphic design and calligraphy. Lorrie also conducts calligraphy and lettering lectures and workshops nationally that are tailored to the needs and interests of the audience. Her professional and student work can be viewed at the <a href="http://cias.rit.edu/faculty-staff/141">RIT website</a>.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/08/08/learning-to-love-letters/">Learning to Love letters!</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=MeMyn-4Yh0E:3WW72zjwUNg:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=MeMyn-4Yh0E:3WW72zjwUNg:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=MeMyn-4Yh0E:3WW72zjwUNg:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=MeMyn-4Yh0E:3WW72zjwUNg:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=MeMyn-4Yh0E:3WW72zjwUNg:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=MeMyn-4Yh0E:3WW72zjwUNg:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=MeMyn-4Yh0E:3WW72zjwUNg:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=MeMyn-4Yh0E:3WW72zjwUNg:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=MeMyn-4Yh0E:3WW72zjwUNg:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=MeMyn-4Yh0E:3WW72zjwUNg:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/MeMyn-4Yh0E" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:68:"http://ilovetypography.com/2014/08/08/learning-to-love-letters/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:63:"http://ilovetypography.com/2014/08/08/learning-to-love-letters/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:45:"
		
		
		
		
		
				
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"This Week in Fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/w7KNnSkWjnU/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:69:"http://ilovetypography.com/2014/07/23/this-week-in-fonts-27/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 22 Jul 2014 15:29:16 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"new fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15235";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:976:"A friendly brush script by Nikola Giacintová, a new classic from Jason Vandenberg, a flexible sans by Fatype, fluid &#38; friendly structure from Latinotype, a new sans by Monotype, a multi-colored face from Underware, an elegant serif by FontFont, and a practical sans from Ludwig Type. Nikola Giacintová: Rukola Designed by Nikola Giacintová Rukola is [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/07/23/this-week-in-fonts-27/">This Week in Fonts</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sean Mitchell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5723:"<p>A friendly brush script by <a href="http://tprls.me/1nZufjq">Nikola Giacintová</a>, a new classic from <a href="http://tprls.me/1mbFfcr">Jason Vandenberg</a>, a flexible sans by <a href="http://tprls.me/1o8Mc0q">Fatype</a>, fluid &amp; friendly structure from <a href="http://tprls.me/1xbEojY">Latinotype</a>, a new sans by <a href="http://tprls.me/1lOQpZ2">Monotype</a>, a multi-colored face from <a href="http://tprls.me/1pTrv9P">Underware</a>, an elegant serif by <a href="http://tprls.me/1nZypaZ">FontFont</a>, and a practical sans from <a href="http://tprls.me/1pTsneF">Ludwig Type</a>.</p>
<p><span id="more-15235"></span></p>
<h3><a href="http://tprls.me/1nZufjq">Nikola Giacintová: Rukola</a></h3>
<p><em>Designed by Nikola Giacintová</em></p>
<p><a href="http://tprls.me/1nZufjq"><img src="http://cdn.ilovetypography.com/img/2014/07/rukola.png" alt="" /></a></p>
<p><a href="http://tprls.me/1nZufjq">Rukola</a> is a friendly brush script that follows in the footsteps of sign painting.</p>
<h3><a href="http://tprls.me/1mbFfcr">Jason Vandenberg: Bodoni Sans</a></h3>
<p><em>Designed by Jason Vandenberg</em></p>
<p><a href="http://tprls.me/1mbFfcr"><img src="http://cdn.ilovetypography.com/img/2014/07/bodoni-sans.png" alt="" /></a></p>
<p><a href="http://tprls.me/1mbFfcr">Bodoni Sans</a> is a new classic built on the foundation of two centuries of history.</p>
<h3><a href="http://tprls.me/1o8Mc0q">Fatype: Beausite</a></h3>
<p><em>Designed by Yassin Baggar</em></p>
<p><a href="http://tprls.me/1o8Mc0q"><img src="http://cdn.ilovetypography.com/img/2014/07/beausite.png" alt="" /></a></p>
<p>A flexible, medium to high contrast, sans serif — less about designing a stylish decorative design and more about applying contrast onto a neo-grotesk skeleton.</p>
<h3><a href="http://tprls.me/1xbEojY">Latinotype: Modernica</a></h3>
<p><em>Designed by Javier Quintana</em></p>
<p><a href="http://tprls.me/1xbEojY"><img src="http://cdn.ilovetypography.com/img/2014/07/modernica.png" alt="" /></a></p>
<p><a href="http://tprls.me/1xbEojY">Modernica</a> seeks to go beyond the grotesque style and instigate a more fluid and friendly structure while remaining solid in its use.</p>
<h3><a href="http://tprls.me/1lOQpZ2">Monotype: Quire Sans</a></h3>
<p><em>Designed by Jim Ford</em></p>
<p><a href="http://tprls.me/1lOQpZ2"><img src="http://cdn.ilovetypography.com/img/2014/07/quire-sans.png" alt="" /></a></p>
<p><a href="http://tprls.me/1lOQpZ2">Quire Sans</a> performs with confidence in virtually any setting.</p>
<h3><a href="http://tprls.me/1pTrv9P">Underware: Tripper Tricolor</a></h3>
<p><a href="http://tprls.me/1pTrv9P"><img src="http://cdn.ilovetypography.com/img/2014/07/tripper-tricolor.png" alt="" /></a></p>
<p>With <a href="http://tprls.me/1pTrv9P">Tripper Tricolor</a> all patriots can set text in their national colors, or in any other color they prefer.</p>
<h3><a href="http://tprls.me/1nZypaZ">FontFont: FF Franziska</a></h3>
<p><em>Designed by Jakob Runge</em></p>
<p><a href="http://tprls.me/1nZypaZ"><img src="http://cdn.ilovetypography.com/img/2014/07/ff-franziska.png" alt="" /></a></p>
<p><a href="http://tprls.me/1nZypaZ">FF Franziska</a> elegantly closes the gap between the artistic formulation of the individual glyphs and the rational functionality of the overall type design.</p>
<h3><a href="http://tprls.me/1pTsneF">Ludwig Type: Riga</a></h3>
<p><em>Designed by Ludwig Übele</em></p>
<p><a href="http://tprls.me/1pTsneF"><img src="http://cdn.ilovetypography.com/img/2014/07/riga.png" alt="" /></a></p>
<p>Clear and practical, yet warm and polite, <a href="http://tprls.me/1pTsneF">Riga</a> is a space-saving and legible typeface designed to work equally well on paper and on the computer screen.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/07/23/this-week-in-fonts-27/">This Week in Fonts</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=w7KNnSkWjnU:49VPi6FdM2I:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=w7KNnSkWjnU:49VPi6FdM2I:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=w7KNnSkWjnU:49VPi6FdM2I:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=w7KNnSkWjnU:49VPi6FdM2I:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=w7KNnSkWjnU:49VPi6FdM2I:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=w7KNnSkWjnU:49VPi6FdM2I:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=w7KNnSkWjnU:49VPi6FdM2I:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=w7KNnSkWjnU:49VPi6FdM2I:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=w7KNnSkWjnU:49VPi6FdM2I:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=w7KNnSkWjnU:49VPi6FdM2I:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/w7KNnSkWjnU" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:65:"http://ilovetypography.com/2014/07/23/this-week-in-fonts-27/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:60:"http://ilovetypography.com/2014/07/23/this-week-in-fonts-27/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:45:"
		
		
		
		
		
				
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:57:"BundesSans and BundesSerif — truly democratic typefaces";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/I-mu7e-P50w/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:101:"http://ilovetypography.com/2014/07/12/bundessans-and-bundesserif-truly-democratic-typefaces/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 12 Jul 2014 06:57:06 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:11:"make a font";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15139";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:1034:"Three years ago MetaDesign Berlin asked us to design a custom Serif and Sans typeface for the German federal government. They had been assigned to redevelop the government’s corporate design with the typefaces as part of the update. The project was to cover all communication issued by the government and their ministries, online or offline, [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/07/12/bundessans-and-bundesserif-truly-democratic-typefaces/">BundesSans and BundesSerif — truly democratic typefaces</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:5:"johno";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:14595:"<p class="intro">Three years ago MetaDesign Berlin asked us to design a custom Serif and Sans typeface for the German federal government. They had been assigned to redevelop the government’s corporate design with the typefaces as part of the update. The project was to cover all communication issued by the government and their ministries, online or offline, national or international. It was a demanding and interesting task. Though we were accustomed to working on projects like these for corporations, we were now asked to design “for the people”.</p>
<p><span id="more-15139"></span></p>
<div id="theDeck-1"></div>
<p>A custom type design job begins with the definition of aesthetic and technical goals, dictated to a large extent by the target group. A typeface for an art school can be more liberally designed than the typeface for a company of financial advisers or a newspaper font, though all three have easily definable target groups. Assumptions about a client’s target group are based on lifestyle, age group, likes and dislikes, etc. and shape the development of an aesthetic design.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/05/I_love_Typography_05-01-161.png" alt="BundesSans and BundesSerif" /></p>
<p class="footnote">The new typefaces for the German federal government</p>
<p>The technical savvy of the target group — how current their technology is, what devices, browsers, etc. they use — is also crucial. It informs us on the font technology required: font formats, hinting, and so on. The more homogenous the target group, the more straightforward the definitions are upon which we base our design.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-04-25-update.jpg" alt="I_love_Typography_05-04-25-update" /></p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-03-22-update.png" alt="I_love_Typography_05-03-22-update" /></p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-05-28-update.jpg" alt="I_love_Typography_05-05-28-update" /></p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-06-31-update.jpg" alt="I_love_Typography_05-06-31-update" /></p>
<p class="footnote">Custom typefaces for the Glasgow School of Art, the northern Italian region of Südtirol, the Dutch weekly newspaper Staatscourant SC, and the Berlin Lottery (an assignment from <a href="http://www.connex-werbung.de/">Connex Advertising, Berlin</a>). Custom type designs with set target groups and applications.</p>
<p>For this project we were faced with the challenge of mapping a goal for a target group that encompassed the general public — all citizens and persons coming into contact with information issued by the German government and its agencies, ranging from the age of 9 to 99, from all educational and cultural backgrounds — which was basically everyone. The written word, whether just a footnote or the headline on a billboard, had to be accessible and user-friendly to this widely diverse group.</p>
<p>Besides the public audience, the other target group for the typeface was the government itself: employees and officials using the fonts, creating information and communicating with them in an office environment. Though smaller than the first target group, it was crucial that their needs be met as well.</p>
<p>Our foremost goal in considering this broad audience was outstanding legibility — creating glyph shapes that made for pleasant reading. Not only because the user group was so diverse but also because the official text issued by the government could at times be complex and detailed.</p>
<p>The second aim was a consequence of the first: the typeface should have no extroverted details. By ornamenting information we would run the risk of distracting the reader. We chose therefore not to give the typeface too much personality, and aimed to design an unbiased yet friendly “transmitter”. This impulse was the opposite as that for a corporate typeface design, where we would enhance the profile with strong recognizable details. We were not competing with another brand in the free market, the typeface did not have to look “cooler” than the one before, it just needed to work “better” by serving a public function.</p>
<p>Translating our next two goals into definable shapes was more straightforward. We wanted to balance femininity and masculinity as well as infuse it with determination and sure-footedness. Both aspects have visual correlations and reflect the values implicit in “democracy”. One recognizes the equality of both sexes, while the other acknowledges the authority of government, tempered by democratic and humanitarian ideals. We strove to make an inclusive typeface, not an exclusive one.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-10-34.png" alt="I_love_Typography_05-10-34" /></p>
<p class="footnote">Both writing and construction were part of the design concept for the two families.</p>
<p>The typefaces the government previously used were a combination of Neue Praxis and Neue Demos by <a href="http://gerardunger.com">Gerard Unger</a>. These typefaces, originally designed in the 1970s, were built up of fairly coarse pixels and made to function within a specific technical environment where letters were formed by a cathode ray tube. This meant that the design had to match the technology and not the other way around.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-12-37.png" alt="I_love_Typography_05-12-37" /></p>
<p class="footnote">Previously in use for the corporate design of the government: Neue Demos by Gerard Unger. BundesSerif has more definite and dynamic features.</p>
<p>The technical requirements for the BundesSans and BundesSerif were more demanding, requiring cross media, cross platform and cross browser usage. Again, with their heterogenous target group, our aim was to foster the government’s obligation to make information accessible to everyone. As not everyone has the latest computer or software, we had to create backwards-compatible typefaces. That meant the fonts would require hinting on various levels for good display on screens, especially under difficult conditions with font smoothing switched off.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-14-40-update.jpg" alt="I_love_Typography_05-14-40-update" /></p>
<p class="footnote">The fonts rendered in Firefox Windows (left) and Mac OS. The ClearType and grayscale hinting for the web and office fonts was made by Monika Bartels from <a href="http://fontwerk.de">fontwerk.de</a></p>
<p>We approached the actual design of the typefaces on the macro and the micro levels. The macro meant defining the general profile or cornerstone of our design. Only after that did we start sketching and drawing letters on the micro level.</p>
<p>The foundation of our design began with general proportions, vertically (especially the ratio uppercase, ascender and x-height) and horizontally. We knew the typefaces should not demand too much space but also should not appear too compact or cramped. Then we looked at the possible shape of the letters for Sans and Serif, and at the level of individuality — recognizability — of the characteristic shapes.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-08-43.png" alt="I_love_Typography_05-08-43" /></p>
<p class="footnote">Our first macro approach to vertical and horizontal proportions.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-09-46.png" alt="I_love_Typography_05-09-46" /></p>
<p class="footnote">Balancing the proportions within the letters, design options (top and middle, blue is our choice) and some problem letters and combinations we needed to aware of.</p>
<p>On the micro level, we methodically considered how pointy/round the curves should be, compared dynamic (humanist) with more solid (constructed) shapes, and tested symmetrical/asymmetrical serifs. At that point in the process we gave great thought to each detail, questioned and discussed — sometimes fiercely — all the features that led us in the end to conclusions and possible design options. These were discussed with MetaDesign who assisted in streamlining our decisions to correspond with their modernized corporate design. Only after that did we present the development to the clients.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-13-49.png" alt="I_love_Typography_05-13-49" /></p>
<p class="footnote">The glyphs’ individuality increases recognizability.</p>
<p>It is not easy to present individual letters to a client, laymen of type, and ask them to make decisions on details. Commonly, the client finds certain features strange because they have never been exposed to them close up. Characteristics of Times New Roman and Arial might be not questioned because they are never examined in detail, but when viewing a new alphabet through a magnifying glass, questions on forms and proportions suddenly arise — “why isn’t the letter ‘t’ as tall as the ‘h’ or ‘b’?” For this reason we always use meaningful words in our presentations — in this case, Bildung, Berlin, Demokratie — rather than individual letters.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-15-52.png" alt="I_love_Typography_05-15-52" /></p>
<p class="footnote">Presentation of the typeface to the client.</p>
<p>After finalizing the last design decisions, we presented the complete glyph set of each weight/style on A2 boards (tabloid format x 2) to the client and after a few more detail tweaks, received the final “go ahead” signature on each — a green light for the last step of the production process. The result was two families for DTP, web and office use with each available weight in Roman and Italic, containing about 580 glyphs covering the European languages that use Latin script.</p>
<p>We have learned from this assignment that the usual corporate type design reasoning only applies to a certain extent. It is not a quest for the most groundbreaking “winning” solution or a visualisation of a company profile, because it is outside ‘commercialistic’ thinking where the sole objective is to increase and maximise financial gain. Our objective here was to create an understated design with a sense of integration, not exclusivity — universality instead of selectivity.</p>
<p>The project ran smoothly and our progress was viewed with great interest by the Bundespresseamt (German government press office) who mediated the assignment. Though their communication specialists were familiar with the processes of corporate design, the development of a typeface was something new to them. They came to see great advantages in a new, custom typeface over having their previous typeface overhauled. The latter would have meant an upgrade from Type1 to OpenType and web fonts, resulting in considerable license fees. The custom font allowed them to freely use and distribute it within the governmental bodies and ministries.</p>
<p>BundesSerif and BundesSans received awards from the International Forum Design and the German Designers Club.</p>
<p><img src="http://cdn.ilovetypography.com/img/2014/07/I_love_Typography_05-18-64.png" alt="I_love_Typography_05-18-64" /></p>
<p class="footnote">Requested by the client: the design of an uppercase ß, the German double s (a ligature of ſs, a long s followed by a regular one) which typically becomes SS in uppercase writing. The letter has been included in the Unicode standard in 2008 as U+1E9E.</p>
<p class="footnote" style="margin-bottom:3em;">Prof. Jürgen Huber and Martin Wenzel are two experienced and enthusiastic type designers forming the custom type design partnership, <a href="http://supertype.de">supertype.de</a>.<br />
Jürgen studied Communication Design at the Folkwang University in Essen with Prof. Volker Küster before he worked for MetaDesign until 2004. Since 2012 he runs http://typedepartment.de together with Malte Herok. He currently teaches typography at the University of Applied Sciences HTW, Berlin.<br />
Martin studied at the Royal Academy of Art, The Hague, while embarking on his own type design projects, eventually launching his foundry http://martinplusfonts.com in 2011. Martin also teaches typography part-time at the University of Applied Sciences HTW in Berlin.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/07/12/bundessans-and-bundesserif-truly-democratic-typefaces/">BundesSans and BundesSerif — truly democratic typefaces</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=I-mu7e-P50w:37KwRruMTCQ:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=I-mu7e-P50w:37KwRruMTCQ:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=I-mu7e-P50w:37KwRruMTCQ:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=I-mu7e-P50w:37KwRruMTCQ:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=I-mu7e-P50w:37KwRruMTCQ:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=I-mu7e-P50w:37KwRruMTCQ:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=I-mu7e-P50w:37KwRruMTCQ:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=I-mu7e-P50w:37KwRruMTCQ:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=I-mu7e-P50w:37KwRruMTCQ:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=I-mu7e-P50w:37KwRruMTCQ:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/I-mu7e-P50w" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:97:"http://ilovetypography.com/2014/07/12/bundessans-and-bundesserif-truly-democratic-typefaces/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:92:"http://ilovetypography.com/2014/07/12/bundessans-and-bundesserif-truly-democratic-typefaces/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:45:"
		
		
		
		
		
				
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"This Week in Fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/Iin9YDuqsBI/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:69:"http://ilovetypography.com/2014/06/27/this-week-in-fonts-26/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 26 Jun 2014 16:07:57 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"new fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15202";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:954:"An Erik Spiekermann exclusive from Hamilton Wood Type, a sturdy slab by Rene Bieder, a high-class display from Avondale Type Co, a brush script by Mika Melvas, a modest slab serif from Type Me Fonts, a monospaced family by Matthew Butterick, a contemporary script from Petra Dočekalová, and a super family by Playtype. Hamilton Wood [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/06/27/this-week-in-fonts-26/">This Week in Fonts</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sean Mitchell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5777:"<p>An Erik Spiekermann exclusive from <a href="http://tprls.me/1ubFmZS">Hamilton Wood Type</a>, a sturdy slab by <a href="http://tprls.me/1oUPdlr">Rene Bieder</a>, a high-class display from <a href="http://tprls.me/1lAvI3J">Avondale Type Co</a>, a brush script by <a href="http://tprls.me/1oHRwNg">Mika Melvas</a>, a modest slab serif from <a href="http://tprls.me/1imGZnL">Type Me Fonts</a>, a monospaced family by <a href="http://tprls.me/1myWrce">Matthew Butterick</a>, a contemporary script from <a href="http://tprls.me/1kP8c1A">Petra Dočekalová</a>, and a super family by <a href="http://tprls.me/1s8sJTI">Playtype</a>.</p>
<p><span id="more-15202"></span></p>
<h3><a href="http://tprls.me/1ubFmZS">Hamilton Wood Type: HWT Artz</a></h3>
<p><em>Designed by Erik Spiekermann</em></p>
<p><a href="http://tprls.me/1ubFmZS"><img src="http://cdn.ilovetypography.com/img/2014/06/hwt-artz.png" alt="" /></a></p>
<p><a href="http://tprls.me/1ubFmZS">HWT Artz</a> was designed by venerable type designer <em>Erik Spiekermann</em> exclusively for his own print studio, specifically to be cut into large size wood type.</p>
<h3><a href="http://tprls.me/1oUPdlr">Rene Bieder: Choplin</a></h3>
<p><em>Designed by Rene Bieder</em></p>
<p><a href="http://tprls.me/1oUPdlr"><img src="http://cdn.ilovetypography.com/img/2014/06/choplin.png" alt="" /></a></p>
<p><a href="http://tprls.me/1oUPdlr">Choplin</a> is a modern and clear geometric slab serif with a sturdy heart.</p>
<h3><a href="http://tprls.me/1lAvI3J">Avondale Type Co: ATC Timberline</a></h3>
<p><em>Designed by Alex Sheyn</em></p>
<p><a href="http://tprls.me/1lAvI3J"><img src="http://cdn.ilovetypography.com/img/2014/06/atc-timberline.png" alt="" /></a></p>
<p><a href="http://tprls.me/1lAvI3J">ATC Timberline</a> is a wide display font, evoking the high class side of speed and mechanics.</p>
<h3><a href="http://tprls.me/1oHRwNg">Mika Melvas: Sanelma</a></h3>
<p><em>Designed by Mika Melvas</em></p>
<p><a href="http://tprls.me/1oHRwNg"><img src="http://cdn.ilovetypography.com/img/2014/06/sanelma.png" alt="" /></a></p>
<p><a href="http://tprls.me/1oHRwNg">Sanelma</a> is a brush script inspired by hot rod lettering and sign painting.</p>
<h3><a href="http://tprls.me/1imGZnL">Type Me Fonts: Muriza</a></h3>
<p><em>Designed by Jürgen Schwarz &amp; Jakob Runge</em></p>
<p><a href="http://tprls.me/1imGZnL"><img src="http://cdn.ilovetypography.com/img/2014/06/muriza.png" alt="" /></a></p>
<p><a href="http://tprls.me/1imGZnL">Muriza</a> is a modest slab serif with temptious curves.</p>
<h3><a href="http://tprls.me/1myWrce">Matthew Butterick: Triplicate</a></h3>
<p><em>Designed by Matthew Butterick</em></p>
<p><a href="http://tprls.me/1myWrce"><img src="http://cdn.ilovetypography.com/img/2014/06/triplicate.png" alt="" /></a></p>
<p><a href="http://tprls.me/1myWrce">Triplicate</a> is modeled on several faces from the golden age of the typewriter &#8212; a time when designers treated monospacing not merely as a limitation, but also an opportunity.</p>
<h3><a href="http://tprls.me/1kP8c1A">Petra Dočekalová: Monolina</a></h3>
<p><em>Designed by Petra Dočekalová</em></p>
<p><a href="http://tprls.me/1kP8c1A"><img src="http://cdn.ilovetypography.com/img/2014/06/monolina.png" alt="" /></a></p>
<p><a href="http://tprls.me/1kP8c1A">Monolina</a> is a contemporary monolinear script that is based on the contrast between classical calligraphy and quickly jotted manuscript.</p>
<h3><a href="http://tprls.me/1s8sJTI">Playtype: Berlingske</a></h3>
<p><em>Designed by Jonas Hecksher</em></p>
<p><a href="http://tprls.me/1s8sJTI"><img src="http://cdn.ilovetypography.com/img/2014/06/berlingske.png" alt="" /></a></p>
<p>The design is carefully balanced to deliver significant modernization while paying homage to a unique heritage.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/06/27/this-week-in-fonts-26/">This Week in Fonts</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=Iin9YDuqsBI:pE7PkvAGu3o:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=Iin9YDuqsBI:pE7PkvAGu3o:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=Iin9YDuqsBI:pE7PkvAGu3o:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=Iin9YDuqsBI:pE7PkvAGu3o:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=Iin9YDuqsBI:pE7PkvAGu3o:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=Iin9YDuqsBI:pE7PkvAGu3o:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=Iin9YDuqsBI:pE7PkvAGu3o:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=Iin9YDuqsBI:pE7PkvAGu3o:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=Iin9YDuqsBI:pE7PkvAGu3o:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=Iin9YDuqsBI:pE7PkvAGu3o:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/Iin9YDuqsBI" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:65:"http://ilovetypography.com/2014/06/27/this-week-in-fonts-26/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"3";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:60:"http://ilovetypography.com/2014/06/27/this-week-in-fonts-26/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:45:"
		
		
		
		
		
				
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"This Week in Fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/pI0zdZwNts4/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:69:"http://ilovetypography.com/2014/06/08/this-week-in-fonts-25/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Sat, 07 Jun 2014 16:57:25 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"new fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15175";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:970:"A versatile sans from TipoType, a dynamic script by Sudtipos, an extreme display face from Hoefler &#38; Co, a quirky hand-drawn family by Thinkdust, a geometric sans from Mint Type, a pair of playful stencils by Font Bureau, a multi-sources-inspired titling family from Kyle Wayne Benson, and a robust stencil by House Industries. TipoType: Libertad [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/06/08/this-week-in-fonts-25/">This Week in Fonts</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sean Mitchell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6265:"<p>A versatile sans from <a href="http://tprls.me/1kAAcpA">TipoType</a>, a dynamic script by <a href="http://tprls.me/1xjXUM4">Sudtipos</a>, an extreme display face from <a href="http://tprls.me/1p4FYQM">Hoefler &amp; Co</a>, a quirky hand-drawn family by <a href="http://tprls.me/1oRGKR8">Thinkdust</a>, a geometric sans from <a href="http://tprls.me/1rpgd21">Mint Type</a>, a pair of playful stencils by <a href="http://tprls.me/SfvWkd">Font Bureau</a>, a multi-sources-inspired titling family from <a href="http://tprls.me/S8Ij0X">Kyle Wayne Benson</a>, and a robust stencil by <a href="http://tprls.me/1hvWZ6E">House Industries</a>.</p>
<p><span id="more-15175"></span></p>
<h3><a href="http://tprls.me/1kAAcpA">TipoType: Libertad</a></h3>
<p><em>Designed by Fernando Díaz</em></p>
<p><a href="http://tprls.me/1kAAcpA"><img src="http://cdn.ilovetypography.com/img/2014/06/libertad.png" alt="" /></a></p>
<p><a href="http://tprls.me/1kAAcpA">Libertad</a> is a sans-serif that mixes humanist and grotesk models.</p>
<h3><a href="http://tprls.me/1xjXUM4">Sudtipos: Abelina</a></h3>
<p><em>Designed by Yani Arabena &amp; Guille Vizzari</em></p>
<p><a href="http://tprls.me/1xjXUM4"><img src="http://cdn.ilovetypography.com/img/2014/06/abelina.png" alt="" /></a></p>
<p><a href="http://tprls.me/1xjXUM4">Abelina</a> can be used in display sizes for titles where part of the central premise is to emulate certain features of gestural handwriting.</p>
<h3><a href="http://tprls.me/1p4FYQM">Hoefler &amp; Co: Nitro &amp; Turbo</a></h3>
<p><a href="http://tprls.me/1p4FYQM"><img src="http://cdn.ilovetypography.com/img/2014/06/nitro-turbo.png" alt="" /></a></p>
<p>Most type families begin with a roman font of moderate weight, and build outwards toward their peripheral bolds and italics. <a href="http://tprls.me/1p4FYQM">Nitro</a> starts from the extreme &#8212; an aggressively sloped italic of massive weight &#8212; and adds an equal and opposite form, a backslanted style called <a href="http://tprls.me/1p4FYQM">Turbo</a>.</p>
<h3><a href="http://tprls.me/1oRGKR8">Thinkdust: Nanami Handmade</a></h3>
<p><em>Designed by Alex Haigh</em></p>
<p><a href="http://tprls.me/1oRGKR8"><img src="http://cdn.ilovetypography.com/img/2014/06/nanami-handmade.png" alt="" /></a></p>
<p><a href="http://tprls.me/1oRGKR8">Nanami Handmade</a> comes in two styles &#8212; a solid and a hand-drawn, each of which has eight weights &#8212; and carries a quirky, mischievous charm.</p>
<h3><a href="http://tprls.me/1rpgd21">Mint Type: Proba Pro</a></h3>
<p><em>Designed by Andriy Konstantynov</em></p>
<p><a href="http://tprls.me/1rpgd21"><img src="http://cdn.ilovetypography.com/img/2014/06/proba-pro.png" alt="" /></a></p>
<p><a href="http://tprls.me/1rpgd21">Proba Pro</a> is a geometric sans with lowered x-height, prominent ascenders &amp; descenders, and subtle humanist touch.</p>
<h3><a href="http://tprls.me/SfvWkd">Font Bureau: Tick &amp; Tock</a></h3>
<p><em>Designed by Cyrus Highsmith</em></p>
<p><a href="http://tprls.me/SfvWkd"><img src="http://cdn.ilovetypography.com/img/2014/06/tick-tock.png" alt="" /></a></p>
<p><a href="http://tprls.me/SfvWkd">Tick &amp; Tock</a> play the same game in two different ways; they&#8217;re distinct typefaces that see themselves in each other.</p>
<h3><a href="http://tprls.me/S8Ij0X">Kyle Wayne Benson: Good News Sans</a></h3>
<p><em>Designed by Kyle Wayne Benson</em></p>
<p><a href="http://tprls.me/S8Ij0X"><img src="http://cdn.ilovetypography.com/img/2014/06/good-news-sans.png" alt="" /></a></p>
<p>The inspiration for <a href="http://tprls.me/S8Ij0X">Good News Sans</a> is rooted in early twentieth century titling, gothic woodtype, geometrics like <a href="http://tprls.me/1xon4cs">Futura Display</a>, poster fonts of that era, and strong lowercase sets like <a href="http://tprls.me/TrlsiA">Din</a>.</p>
<h3><a href="http://tprls.me/1hvWZ6E">House Industries: Yorklyn Stencil</a></h3>
<p><em>Designed by Ken Barber</em></p>
<p><a href="http://tprls.me/1hvWZ6E"><img src="http://cdn.ilovetypography.com/img/2014/06/yorklyn-stencil.png" alt="" /></a></p>
<p><a href="http://tprls.me/1hvWZ6E">Yorklyn Stencil&#8217;s</a> robust curves and deceptively delicate breaks will withstand a wide variety of harsh conditions with unprecedented aplomb.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/06/08/this-week-in-fonts-25/">This Week in Fonts</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=pI0zdZwNts4:-nYB__skXjc:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=pI0zdZwNts4:-nYB__skXjc:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=pI0zdZwNts4:-nYB__skXjc:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=pI0zdZwNts4:-nYB__skXjc:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=pI0zdZwNts4:-nYB__skXjc:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=pI0zdZwNts4:-nYB__skXjc:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=pI0zdZwNts4:-nYB__skXjc:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=pI0zdZwNts4:-nYB__skXjc:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=pI0zdZwNts4:-nYB__skXjc:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=pI0zdZwNts4:-nYB__skXjc:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/pI0zdZwNts4" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:65:"http://ilovetypography.com/2014/06/08/this-week-in-fonts-25/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:60:"http://ilovetypography.com/2014/06/08/this-week-in-fonts-25/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:45:"
		
		
		
		
		
				
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"This Week in Fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/-sjYl5PV2GI/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:69:"http://ilovetypography.com/2014/05/29/this-week-in-fonts-24/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 29 May 2014 13:25:54 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"new fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15160";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:962:"A graceful sans from Typotheque, a modern grotesk by Suitcase Type, a contemporary serif from Bold Monday, a letterpress family by Yellow Design, a versatile sans from Milieu Grotesque, a brush script by Doubletwo, a bold display face from Monokrom, and a modern sign painter family by Kyle Wayne Benson. Typotheque: Valter Designed by Nikola [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/05/29/this-week-in-fonts-24/">This Week in Fonts</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sean Mitchell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:6035:"<p>A graceful sans from <a href="http://tprls.me/1prYBOg">Typotheque</a>, a modern grotesk by <a href="http://tprls.me/TvMNAy">Suitcase Type</a>, a contemporary serif from <a href="http://tprls.me/1jW1DoC">Bold Monday</a>, a letterpress family by <a href="http://tprls.me/1knEdxy">Yellow Design</a>, a versatile sans from <a href="http://tprls.me/1qWb1Cj">Milieu Grotesque</a>, a brush script by <a href="http://tprls.me/1vMvFEU">Doubletwo</a>, a bold display face from <a href="http://tprls.me/1nfXLqN">Monokrom</a>, and a modern sign painter family by <a href="http://tprls.me/1vMyplM">Kyle Wayne Benson</a>.</p>
<p><span id="more-15160"></span></p>
<h3><a href="http://tprls.me/1prYBOg">Typotheque: Valter</a></h3>
<p><em>Designed by Nikola Djurek</em></p>
<p><a href="http://tprls.me/1prYBOg"><img src="http://cdn.ilovetypography.com/img/2014/05/valter.png" alt="" /></a></p>
<p><a href="http://tprls.me/1prYBOg">Valter</a> is a graceful and slightly cheeky collection of sans-serif display fonts inspired by pointed-pen writing.</p>
<h3><a href="http://tprls.me/TvMNAy">Suitcase Type Foundry: Urban Grotesk</a></h3>
<p><em>Designed by Tomáš Brousil</em></p>
<p><a href="http://tprls.me/TvMNAy"><img src="http://cdn.ilovetypography.com/img/2014/05/urban-grotesk.png" alt="" /></a></p>
<p><a href="http://tprls.me/TvMNAy">Urban Grotesk</a> attempts to follow the best of traditions of Grotesk typefaces: rounded arches, slightly thinner connecting strokes and a vertical shadowing axis, where outstrokes are terminated strictly in perpendicular to the stroke direction.</p>
<h3><a href="http://tprls.me/1jW1DoC">Bold Monday: Brando</a></h3>
<p><em>Designed by Mike Abbink</em></p>
<p><a href="http://tprls.me/1jW1DoC"><img src="http://cdn.ilovetypography.com/img/2014/05/brando.png" alt="" /></a></p>
<p><a href="http://tprls.me/1jW1DoC">Brando</a> is a contemporary serif with humanist proportions, exploring the balance between mechanical and egyptian forms.</p>
<h3><a href="http://tprls.me/1knEdxy">Yellow Design Studio: Eveleth</a></h3>
<p><em>Designed by Ryan Martinson</em></p>
<p><a href="http://tprls.me/1knEdxy"><img src="http://cdn.ilovetypography.com/img/2014/05/eveleth.png" alt="" /></a></p>
<p><a href="http://tprls.me/1knEdxy">Eveleth</a> is a high-resolution letterpress family with exceptional realism and vintage charm.</p>
<h3><a href="http://tprls.me/1qWb1Cj">Milieu Grotesque: Patron</a></h3>
<p><em>Designed by Timo Gaessner</em></p>
<p><a href="http://tprls.me/1qWb1Cj"><img src="http://cdn.ilovetypography.com/img/2014/05/patron.png" alt="" /></a></p>
<p><a href="http://tprls.me/1qWb1Cj">Patron</a> is an expressive yet versatile grotesk, characterized by a generous x-height, distinctive stroke endings and an unconventional shift in balance.</p>
<h3><a href="http://tprls.me/1vMvFEU">Doubletwo Studios: XXII YeahScript</a></h3>
<p><em>Designed by Lecter Johnson</em></p>
<p><a href="http://tprls.me/1vMvFEU"><img src="http://cdn.ilovetypography.com/img/2014/05/xxii-yeahscript.png" alt="" /></a></p>
<p><a href="http://tprls.me/1vMvFEU">XXII YeahScript</a> is a brush script with a large range of alternates &#8212; a great fit for any sign painter job.</p>
<h3><a href="http://tprls.me/1nfXLqN">Monokrom: Mønster</a></h3>
<p><em>Designed by Sindre Bremnes</em></p>
<p><a href="http://tprls.me/1nfXLqN"><img src="http://cdn.ilovetypography.com/img/2014/05/monster.png" alt="" /></a></p>
<p><a href="http://tprls.me/1nfXLqN">Mønster&#8217;s</a> odd letter forms and exaggerated shapes combine into powerful, vigorous patterns, making a bold statement of any title.</p>
<h3><a href="http://tprls.me/1vMyplM">Kyle Wayne Benson: Kansas Casual</a></h3>
<p><em>Designed by Kyle Benson</em></p>
<p><a href="http://tprls.me/1vMyplM"><img src="http://cdn.ilovetypography.com/img/2014/05/kansas-casual.png" alt="" /></a></p>
<p><a href="http://tprls.me/1vMyplM">Kansas Casual</a> provides a completely unique take on a overdone classic with proportions and crossbar heights inspired by the more friendly <em>Chicago</em> style.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/05/29/this-week-in-fonts-24/">This Week in Fonts</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=-sjYl5PV2GI:HuOa-Mgq03s:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=-sjYl5PV2GI:HuOa-Mgq03s:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=-sjYl5PV2GI:HuOa-Mgq03s:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=-sjYl5PV2GI:HuOa-Mgq03s:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=-sjYl5PV2GI:HuOa-Mgq03s:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=-sjYl5PV2GI:HuOa-Mgq03s:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=-sjYl5PV2GI:HuOa-Mgq03s:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=-sjYl5PV2GI:HuOa-Mgq03s:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=-sjYl5PV2GI:HuOa-Mgq03s:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=-sjYl5PV2GI:HuOa-Mgq03s:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/-sjYl5PV2GI" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:65:"http://ilovetypography.com/2014/05/29/this-week-in-fonts-24/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"2";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:60:"http://ilovetypography.com/2014/05/29/this-week-in-fonts-24/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:45:"
		
		
		
		
		
				
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:18:"This Week in Fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:62:"http://feedproxy.google.com/~r/ILoveTypography/~3/6UxG1M-tNjU/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:69:"http://ilovetypography.com/2014/05/13/this-week-in-fonts-23/#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 13 May 2014 14:07:42 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:2:{i:0;a:5:{s:4:"data";s:10:"typography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:9:"new fonts";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:35:"http://ilovetypography.com/?p=15147";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:978:"An &#8220;old Hollywood&#8221; inspired sans from Jessica Hische, a harmonious family by Laura Worthington, a contemporary serif from Grilli Type, a stylish slab by FaceType, a gentle sans from Production Type, a versatile sans by Tour De Force, a brush inspired face from Commercial Type, and a calligraphic script by Aerotype. Jessica Hische: Silencio Sans [&#8230;]<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/05/13/this-week-in-fonts-23/">This Week in Fonts</a></p>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:13:"Sean Mitchell";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:5851:"<p>An <em>&#8220;old Hollywood&#8221;</em> inspired sans from <a href="http://tprls.me/1kFHOFL">Jessica Hische</a>, a harmonious family by <a href="http://tprls.me/1jSRai5">Laura Worthington</a>, a contemporary serif from <a href="http://tprls.me/1jaTZHi">Grilli Type</a>, a stylish slab by <a href="http://tprls.me/1hcYSCy">FaceType</a>, a gentle sans from <a href="http://tprls.me/1q15DgZ">Production Type</a>, a versatile sans by <a href="http://tprls.me/STUIHs">Tour De Force</a>, a brush inspired face from <a href="http://tprls.me/1h8L1cc">Commercial Type</a>, and a calligraphic script by <a href="http://tprls.me/1jEHNA4">Aerotype</a>.</p>
<p><span id="more-15147"></span></p>
<h3><a href="http://tprls.me/1kFHOFL">Jessica Hische: Silencio Sans</a></h3>
<p><em>Designed by Jessica Hische</em></p>
<p><a href="http://tprls.me/1kFHOFL"><img src="http://cdn.ilovetypography.com/img/2014/05/silencio-sans.png" alt="" /></a></p>
<p>The name <em>Silencio</em> references silent films, but this font would feel as at home in magazines, invitations, and fancy food packaging as it does on the silver screen.</p>
<h3><a href="http://tprls.me/1jSRai5">Laura Worthington: Adorn</a></h3>
<p><em>Designed by Laura Worthington</em></p>
<p><a href="http://tprls.me/1jSRai5"><img src="http://cdn.ilovetypography.com/img/2014/05/adorn.png" alt="" /></a></p>
<p><a href="http://tprls.me/1jSRai5">Adorn</a> arms designers with a breathtakingly large number of faces that work harmoniously, despite the distinctiveness of each.</p>
<h3><a href="http://tprls.me/1jaTZHi">Grilli Type: ￼￼GT Sectra</a></h3>
<p><em>Designed by Marc Kappeler, Dominik Huber &amp; Noël Leu</em></p>
<p><a href="http://tprls.me/1jaTZHi"><img src="http://cdn.ilovetypography.com/img/2014/05/gt-sectra.png" alt="" /></a></p>
<p>A contemporary serif typeface combining the calligraphy of the broad nip pen with the sharpness of the scalpel.</p>
<h3><a href="http://tprls.me/1hcYSCy">FaceType: Adria Slab</a></h3>
<p><em>Designed by Marcus Sterz</em></p>
<p><a href="http://tprls.me/1hcYSCy"><img src="http://cdn.ilovetypography.com/img/2014/05/adria-slab.png" alt="" /></a></p>
<p><a href="http://tprls.me/1hcYSCy">Adria Slab</a> is a stylish slab serif that comes in seven weights and charming upright italics.</p>
<h3><a href="http://tprls.me/1q15DgZ">Production Type: Cogito</a></h3>
<p><em>Designed by Jean-Baptiste Levée</em></p>
<p><a href="http://tprls.me/1q15DgZ"><img src="http://cdn.ilovetypography.com/img/2014/05/cogito.png" alt="" /></a></p>
<p>Initially designed as a personal remix of mechanically engineered typefaces, <a href="http://tprls.me/1q15DgZ">Cogito</a> has all the clarity of its models but with a calmer tone.</p>
<h3><a href="http://tprls.me/STUIHs">Tour De Force: Hedon</a></h3>
<p><em>Designed by Dusan Jelesijevic</em></p>
<p><a href="http://tprls.me/STUIHs"><img src="http://cdn.ilovetypography.com/img/2014/05/hedon.png" alt="" /></a></p>
<p><a href="http://tprls.me/STUIHs">Hedon</a> is a neutral, versatile and legible partner for any kind of publication.</p>
<h3><a href="http://tprls.me/1h8L1cc">Commercial Type: Gabriello</a></h3>
<p><em>Designed by Paul Barnes &amp; Miguel Reyes</em></p>
<p><a href="http://tprls.me/1h8L1cc"><img src="http://cdn.ilovetypography.com/img/2014/05/gabriello.png" alt="" /></a></p>
<p><a href="http://tprls.me/1h8L1cc">Gabriello</a> is slanted on two axes, both horizontally and vertically, giving the energy of a script without causing production problems.</p>
<h3><a href="http://tprls.me/1jEHNA4">Aerotype: Arbordale</a></h3>
<p><em>Designed by Stephen Miggas</em></p>
<p><a href="http://tprls.me/1jEHNA4"><img src="http://cdn.ilovetypography.com/img/2014/05/arbordale.png" alt="" /></a></p>
<p><a href="http://tprls.me/1jEHNA4">Arbordale</a> communicates with casual confidence, a calligraphic script with roots in the midwest.</p>
<p><br /><br />
<a class="noborder" href="http://www.typography.com/cloud/welcome/?affiliateID=509"><img src="http://cdn.ilovetypography.com/img/2014/03/ILT-WebfontsbyH_Co-2014-03.png" /></a>
<br>
Sponsored by <a href="http://www.typography.com/cloud/welcome/?affiliateID=509">Hoefler & Co.</a><br>
and<br>
<a href="http://www.elegantthemes.com/affiliates/idevaffiliate.php?id=190_0_1_7" target="_blank"><img  src="http://cdn.ilovetypography.com/img/2014/02/468x60.gif" width="468" height="60" alt=""></a><br><br><a href="http://ilovetypography.com/2014/05/13/this-week-in-fonts-23/">This Week in Fonts</a></p>
<div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=6UxG1M-tNjU:f_wDQdbFu88:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=6UxG1M-tNjU:f_wDQdbFu88:gIN9vFwOqvQ" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=6UxG1M-tNjU:f_wDQdbFu88:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=6UxG1M-tNjU:f_wDQdbFu88:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=6UxG1M-tNjU:f_wDQdbFu88:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=6UxG1M-tNjU:f_wDQdbFu88:V_sGLiPBpWU"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=6UxG1M-tNjU:f_wDQdbFu88:V_sGLiPBpWU" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=6UxG1M-tNjU:f_wDQdbFu88:D7DqB2pKExk"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?i=6UxG1M-tNjU:f_wDQdbFu88:D7DqB2pKExk" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/ILoveTypography?a=6UxG1M-tNjU:f_wDQdbFu88:TzevzKxY174"><img src="http://feeds.feedburner.com/~ff/ILoveTypography?d=TzevzKxY174" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/ILoveTypography/~4/6UxG1M-tNjU" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:65:"http://ilovetypography.com/2014/05/13/this-week-in-fonts-23/feed/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:60:"http://ilovetypography.com/2014/05/13/this-week-in-fonts-23/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:6:"hourly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:2:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";s:4:"href";s:43:"http://feeds.feedburner.com/ILoveTypography";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:2:{s:3:"rel";s:3:"hub";s:4:"href";s:32:"http://pubsubhubbub.appspot.com/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:5:{s:4:"info";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:3:"uri";s:15:"ilovetypography";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:14:"emailServiceId";a:1:{i:0;a:5:{s:4:"data";s:15:"ILoveTypography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:18:"feedburnerHostname";a:1:{i:0;a:5:{s:4:"data";s:29:"https://feedburner.google.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"feedFlare";a:8:{i:0;a:5:{s:4:"data";s:24:"Subscribe with My Yahoo!";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:83:"http://add.my.yahoo.com/rss?url=http%3A%2F%2Ffeeds.feedburner.com%2FILoveTypography";s:3:"src";s:59:"http://us.i1.yimg.com/us.yimg.com/i/us/my/addtomyyahoo4.gif";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:24:"Subscribe with NewsGator";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:107:"http://www.newsgator.com/ngs/subscriber/subext.aspx?url=http%3A%2F%2Ffeeds.feedburner.com%2FILoveTypography";s:3:"src";s:42:"http://www.newsgator.com/images/ngsub1.gif";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:21:"Subscribe with My AOL";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:87:"http://feeds.my.aol.com/add.jsp?url=http%3A%2F%2Ffeeds.feedburner.com%2FILoveTypography";s:3:"src";s:108:"http://o.aolcdn.com/favorites.my.aol.com/webmaster/ffclient/webroot/locale/en-US/images/myAOLButtonSmall.gif";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:24:"Subscribe with Bloglines";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:72:"http://www.bloglines.com/sub/http://feeds.feedburner.com/ILoveTypography";s:3:"src";s:48:"http://www.bloglines.com/images/sub_modern11.gif";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:23:"Subscribe with Netvibes";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:93:"http://www.netvibes.com/subscribe.php?url=http%3A%2F%2Ffeeds.feedburner.com%2FILoveTypography";s:3:"src";s:44:"http://www.netvibes.com/img/add2netvibes.gif";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:21:"Subscribe with Google";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:88:"http://fusion.google.com/add?feedurl=http%3A%2F%2Ffeeds.feedburner.com%2FILoveTypography";s:3:"src";s:51:"http://buttons.googlesyndication.com/fusion/add.gif";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:25:"Subscribe with Pageflakes";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:96:"http://www.pageflakes.com/subscribe.aspx?url=http%3A%2F%2Ffeeds.feedburner.com%2FILoveTypography";s:3:"src";s:87:"http://www.pageflakes.com/ImageFile.ashx?instanceId=Static_4&fileName=ATP_blu_91x17.gif";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:7;a:5:{s:4:"data";s:23:"Subscribe with Live.com";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:76:"http://www.live.com/?add=http%3A%2F%2Ffeeds.feedburner.com%2FILoveTypography";s:3:"src";s:141:"http://tkfiles.storage.msn.com/x1piYkpqHC_35nIp1gLE68-wvzLZO8iXl_JMledmJQXP-XTBOLfmQv4zhj4MhcWEJh_GtoBIiAl1Mjh-ndp9k47If7hTaFno0mxW9_i3p_5qQw";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"browserFriendly";a:1:{i:0;a:5:{s:4:"data";s:90:"Thanks for subscribing to ILT.
I’m also on twitter:
http://twitter.com/ilovetypography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:52:"http://backend.userland.com/creativeCommonsRssModule";a:1:{s:7:"license";a:1:{i:0;a:5:{s:4:"data";s:49:"http://creativecommons.org/licenses/by-nc-sa/2.0/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:11:{s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:4:"etag";s:27:"Yv2AetJx4XZLSnxY5RhVbHs6qJY";s:13:"last-modified";s:29:"Fri, 26 Sep 2014 09:25:36 GMT";s:16:"content-encoding";s:4:"gzip";s:4:"date";s:29:"Fri, 26 Sep 2014 09:31:03 GMT";s:7:"expires";s:29:"Fri, 26 Sep 2014 09:31:03 GMT";s:13:"cache-control";s:18:"private, max-age=0";s:22:"x-content-type-options";s:7:"nosniff";s:16:"x-xss-protection";s:13:"1; mode=block";s:6:"server";s:3:"GSE";s:18:"alternate-protocol";s:15:"80:quic,p=0.002";}s:5:"build";s:14:"20121030095402";}