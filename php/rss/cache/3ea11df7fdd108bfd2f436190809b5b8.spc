a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:4:"
  
";s:7:"attribs";a:2:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"base";s:21:"http://abduzeedo.com/";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:61:"
    
    
    
    
          
  
  
  
  
  
  
  
  
  
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:65:"Abduzeedo Design Inspiration - Design Inspiration &amp; Tutorials";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:21:"http://abduzeedo.com/";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:2:"en";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"Interview with Brain Mash";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:76:"http://feedproxy.google.com/~r/abduzeedo/~3/QkhHHAVFGEs/interview-brain-mash";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:12685:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/cover_brainmash_interview.jpg" width="1500" height="450" alt="Interview with Brain Mash" title="Interview with Brain Mash" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>Today we interview the minds behind the art design studio Brain Mash. Directly from Novosibirsk, Siberia, Russia, Brain Mash is a multimedia community that makes awesome interior and exterior mural projects usign their abilities from graffiti and graphic design background.  </h3>

<p>You can se more from Brainmash studio on the following links:</p>
<p><a href="https://www.behance.net/brainmash">Behance</a></p>
<p><a href="http://brain-mash.com/">Website</a></p>
<p><a href="https://www.facebook.com/StudioBrainMash">Facebook</a></p>
<p><a href="http://instagram.com/brain_mash">Instagram</a></p>

<br />

<h3>1) First of all I would like to thank you for doing this interview, it's an honor for us to present more about you to our readers. I would like to start asking you about when your interest for mural art and street art started?</h3>

<p>My interest to mural art started from graffiti. During my school years there was the first wave of hip-hop in Russia, thanks to Da Boogie Crew. Although we did not have internet access and spray cans, we had a TV programme with Da Boogie Crew once a week. I was highly interested in hip-hop because I had never seen anything like that before. I painted with sprays for shoes coloring at that time and left tags with ice-cream in the metro. I still have warm memories about those moments. Several years ago I was working on fonts but could not succeed quickly just because of the lack of any information. I took up art courses in 9th year of school and went to the art university after graduation. I chose art faculty on purpose to improve my graffiti skills then I became keen on art and it brought sense to my life.</p>

<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/1.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/2.jpg" /></a></p>

<h3>2) Which artists do you use as reference?</h3>

<p>Today I am always on-line; I constantly monitor the works of lots of art-people. I am not only interested in those who paint graffiti but also computer illustrators, tattoo masters, canvas artists, photographers, graphic designers, calligraphers etc.</p>

<p>I consider it is useless to point at far-famed artists but I can give a list of my friends from Russia who are really worth to look at:</p>

<p><a href="http://tak-nado.com">http://tak-nado.com</a></p>
<p><a href="https://www.facebook.com/basil.lst?fref=ts">https://www.facebook.com/basil.lst?fref=ts</a></p>
<p><a href="https://www.facebook.com/marat.morik?fref=ts">https://www.facebook.com/marat.morik?fref=ts</a></p>
<p><a href="https://www.facebook.com/AndreyAdno?fref=ts">https://www.facebook.com/AndreyAdno?fref=ts</a></p>
<p><a href="https://www.facebook.com/zakhar.evseev?fref=ts">https://www.facebook.com/zakhar.evseev?fref=ts</a></p>
<p><a href="https://www.facebook.com/pavel.roch?fref=ts">https://www.facebook.com/pavel.roch?fref=ts</a></p>
<p><a href="https://www.facebook.com/floksy?fref=ts">https://www.facebook.com/floksy?fref=ts</a></p>
<p><a href="https://www.facebook.com/parisincrew">https://www.facebook.com/parisincrew</a></p>
<p><a href="https://www.facebook.com/evgeni.dolzhanskiy?fref=ts">https://www.facebook.com/evgeni.dolzhanskiy?fref=ts</a></p>
<p><a href="https://www.facebook.com/max.toropov?fref=ts">https://www.facebook.com/max.toropov?fref=ts</a></p>
<p><a href="https://www.facebook.com/trunskee?fref=ts">https://www.facebook.com/trunskee?fref=ts</a></p>
<p><a href="http://instagram.com/vadikvoodoo">http://instagram.com/vadikvoodoo</a></p>
<p><a href="http://instagram.com/chervi1">http://instagram.com/chervi1</a></p>

<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/3.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/4.jpg" /></a></p>


<h3>3) Your style is quite influenced by digital art / photography / animation. How did you develop this style and how would you describe it?</h3>

<p>As I have already said I am fascinated by various art works. I think that is due to my love to absorb every piece of information. So it is not time to define my style, I think I am just at the beginning of my path and there are still too many things to be done.</p>

<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/5.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/6.jpg" /></a></p>


<h3>4) Describe us a bit about your creative process while creating a piece.</h3>
<p>Most of my works have come out of music. I am a huge fan of music and people say that music is always around me. Music gives me certain emotions, which I try to express through my works. Sometimes ideas come spontaneously and I fix everything to refer later to it. Also I do interior mural for money. Clients have ideas and my work is to make them real and beautiful. My drafts could be both on paper or painted in photoshop. Occasionally I do collage of different pictures, if the task is to draw a person, just to make a portrait more resembling.  I am trying to pay more attention to sketches and drafts. I even can draw them for some days and then need only few hours to paint the wall.</p>


<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/7.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/8.jpg" /></a></p>

<h3>5) What would you consider the best moment on your career till now?</h3>
<p>It was the moment when I picked up a can. It is a unique material that has no boarders in use. I believe that painting with can will definitely be an academic subject along with canvas and acryl painting.</p>


<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/9.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/10.jpg" /></a></p>

<h3>6) How do you describe your daily routine?</h3>
<p>My day starts at 9 a.m. and comes to its end at 3 or 4 o’clock at night. I go to the gym in the morning, I like keeping fit. Then I go to paint an order or prepare sketches at home. I also do graphic design. I enjoy living in Siberia where cities are not so big so you can manage to do a lot of things during the day: to meet with a client, buy cans and other materials, see my friends and discuss projects we share… Before going to bed I spend couple of hours surfing the internet or I searching for new music.</p>


<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/11.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/12.jpg" /></a></p>

<h3>7) Being a multimedia artist, please tell us what's your favorite media to work with? Why?</h3>
<p>I use Adobe Photoshop to create drafts and sketches, web-sites and process photos. I have been working in that programme for many years because it copes with everything. If I have to do a logo, print or other vector image I open Illustrator. Another passion of mine is 3d graphic. I am trying to master Zbrush, Real flow, Cinema 4d but there is not so much time left for it. I wish I had much more time to know everything I adore. That is the only my regret in life.</p>


<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/13.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/14.jpg" /></a></p>

<h3>8) Tell us five lessons you believe are really important for every illustrator.</h3>

<p>1. Do not lose your interest to life. Always stay hungry for new ideas.</p>

<p>2. Make more experiments. Try yourself in different art spheres.</p>

<p>3. Contribute to your mind and soul development, visit theatre, cinema, read books.</p>

<p>4. In case you do not have strength or patience to achieve your goals create stricter rules for yourself.</p>

<p>5. Do not believe to mushrooms.</p>


<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/15.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/16.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/17.jpg" /></a></p>

<h3>9) Tell us five websites that you like to visit.</h3>

<p><a href="http://behance.net">behance.net</a></p>
<p><a href="http://graffart.eu">graffart.eu</a></p>
<p><a href="http://cghub.com">cghub.com….Unfortunately, this has been closed recently</a></p>
<p><a href="http://awwwards.com">awwwards.com</a></p>
<p><a href="http://500px.co">500px.com</a></p>



<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/18.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/19.jpg" /></a></p>

<h3>10) Thanks again for your time, please leave a final message for the ones who are starting out on this kind of business.</h3>


<p>Art creation is a praiseworthy activity. It could help you to become free and happy person. I recommend you pay attention on the fact that you can not take without giving in return. In case with art you should be totally involved in it. Be inspired and be the source of inspiration for others. Be surprised and be a surprise for others. Pull your socks to create something outstanding and you will definitely be fully rewarded.</p>



<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/20.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/21.jpg" /></a></p>
<p class="imgC"><a href="http://brain-mash.com/"><img src="http://imgs.abduzeedo.com/files/interview/brainmash/22.jpg" /></a></p></div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/brain-mash">brain mash</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/russia">russia</a></div><div class="field-item even"><a href="http://abduzeedo.com/tags/interview">interview</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/inspiration">inspiration</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/marcos333" title="Read marcos333&#039;s latest blog entries.">marcos333&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QkhHHAVFGEs:9lDQjpZQCNo:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QkhHHAVFGEs:9lDQjpZQCNo:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=QkhHHAVFGEs:9lDQjpZQCNo:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QkhHHAVFGEs:9lDQjpZQCNo:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=QkhHHAVFGEs:9lDQjpZQCNo:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QkhHHAVFGEs:9lDQjpZQCNo:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QkhHHAVFGEs:9lDQjpZQCNo:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=QkhHHAVFGEs:9lDQjpZQCNo:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/QkhHHAVFGEs" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Sep 2014 05:20:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79538 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"marcos333";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:41:"http://abduzeedo.com/interview-brain-mash";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:31:"Beautiful Houses: House in Ohno";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://feedproxy.google.com/~r/abduzeedo/~3/fNxNFIbi5Ko/beautiful-houses-house-ohno";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:7877:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/bh227.jpg" width="1280" height="600" alt="" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>Today we will show you a beautiful house in Ohno, Japan, a project by <u><a href="http://www.airhouse.jp">Airhouse Design Office</a></u>. This place is great. The layout, furniture, colors, the wood and white combination, everything works perfectly. The house is simple, modern and really cozy. Certainly a great place to live. </h3>	

<p>Make sure to check out <u><a href="http://www.airhouse.jp">Airhouse Design Office</a></u> website for further information about this and other inspiring projects. See you next week :) </p>

<blockquote>Description from the architects: A piece of land in a Japanese persimmon (kaki) orchard was offered as the project area for a residential house. As the client wanted wide open spaces with high ceilings, a structure composed of a large roof set on top of seven thick columns was used. To secure copious glass windows, private areas are located inside the columns while intervals between columns are designated as living, dining and kitchen spaces. With this composition, only the sky and the kaki orchard are visible from the inside, giving the area an open feeling while the kaki trees provide a sense of privacy from the outside world, all of which makes for a relaxed atmosphere in the interior areas. As the landscape transforms with the changing of the kaki seasons, a generous living space is produced in the house nestled among the kaki trees.</blockquote>

<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho01.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho02.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho03.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho04.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho05.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho06.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho07.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho08.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho09.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho10.jpg" alt="Beautiful Houses: House in Ohno"/></p>

<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho11.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho12.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho13.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho14.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho15.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho16.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho17.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho18.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho19.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho20.jpg" alt="Beautiful Houses: House in Ohno"/></p>

<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho21.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho22.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho23.jpg" alt="Beautiful Houses: House in Ohno"/></p>
<p class="imgC"><a href="http://www.airhouse.jp"><img src="http://imgs.abduzeedo.com/files/gismullr/beautifulhouses/bh227/ho24.jpg" alt="Beautiful Houses: House in Ohno"/></p>

<p><strong>We found this house at <u><a href="http://www.archdaily.com/546088/house-in-ohno-airhouse-design-office/">ArchDaily</a></u>.</strong></p>
<p><strong>Photos by Toshiyuki Yano.</strong></p>
</div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/houses">houses</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/architecture">architecture</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/gismullr" title="Read GisMullr&#039;s latest blog entries.">GisMullr&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=fNxNFIbi5Ko:MV5FdOuMCyU:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=fNxNFIbi5Ko:MV5FdOuMCyU:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=fNxNFIbi5Ko:MV5FdOuMCyU:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=fNxNFIbi5Ko:MV5FdOuMCyU:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=fNxNFIbi5Ko:MV5FdOuMCyU:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=fNxNFIbi5Ko:MV5FdOuMCyU:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=fNxNFIbi5Ko:MV5FdOuMCyU:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=fNxNFIbi5Ko:MV5FdOuMCyU:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/fNxNFIbi5Ko" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Sep 2014 04:49:11 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79792 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:8:"GisMullr";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:48:"http://abduzeedo.com/beautiful-houses-house-ohno";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"Daily Inspiration #1880";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:78:"http://feedproxy.google.com/~r/abduzeedo/~3/wBq4bFuBi-o/daily-inspiration-1880";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:20682:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/tumblr_ncftus7gmu1r46py4o1_1280.jpg" width="1240" height="698" alt="Daily Inspiration #1880" title="Daily Inspiration #1880" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>This post is part of our daily series of posts showing the most inspiring images selected by some of the Abduzeedo's writers and users. If you want to participate and share your graphic design inspiration, You can submit your images and inspiration to RAWZ via <a href="http://raw.abduzeedo.com">http://raw.abduzeedo.com</a> and don't forget to send your Abduzeedo username; or via Twitter sending to <a href="http://twitter.com/abduzeedo">http://twitter.com/abduzeedo</a>.</h3>

<h3>Andreea Niculae</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426659106/late-night-egyptian-tales-ep-4-sekhmet"><img src="http://33.media.tumblr.com/f94bbda6898f3140635934714f243b71/tumblr_ncgf1rNsXd1r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426661331/late-night-egyptian-tales-ep-4-sekhmet"><img src="http://38.media.tumblr.com/6e16621613be411ee2f365595ffa7307/tumblr_ncgf3cjOyi1r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Ashley</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426076931"><img src="http://33.media.tumblr.com/f8b5b690d6edacc60d748921c6841978/tumblr_nch4wqOEDN1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>bullhorncreative</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426633821/october-night-market-poster-by-curt-rice-at"><img src="http://33.media.tumblr.com/000c567a7a8247ae7ff2e7eb205b7be5/tumblr_ncgwrzB6iZ1r46py4o1_500.png"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Cheng Ling Wei</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426310346/hi-im-lingwei-im-a-freelance"><img src="http://38.media.tumblr.com/7ded828587f9dcb3e0fa712eb98ff753/tumblr_ncgc2fJlJ61r46py4o1_1280.gif"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Chris Rw </h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426286456/minimalist-poster-design-for-the-strain-based"><img src="http://38.media.tumblr.com/6bb483ab2428e31c087f4b2f8de9ab25/tumblr_ncgmak8iz91r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>constantine-sith4brains</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426617826/so-with-the-new-dracula-film-coming-up-i-decided"><img src="http://38.media.tumblr.com/02192e8bf3ab878f65acbafbfcb4dd20/tumblr_ncgrleqN781r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Corey Bohorquez</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426292311"><img src="http://38.media.tumblr.com/3279073cc198ade45597a5c652f95070/tumblr_ncgmvfEsdu1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Daniel Bech</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426757366"><img src="http://33.media.tumblr.com/a39b9089070bbc72d21168dd8a5bb84b/tumblr_ncgayfVzFI1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Daniel Tolentino</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426283811"><img src="http://33.media.tumblr.com/3c738e70c1641fbf158732f69d2ba900/tumblr_ncglh5R8mP1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Danilo Itty</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426683561/https-www-behance-net-gallery-16622515-the-anders"><img src="http://38.media.tumblr.com/9b7339d76d110d46a87e51f8d7dcffd5/tumblr_ncgixnbboR1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>darkdesigngraphics</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426738426/check-out-the-thought-process-behind-this-custom"><img src="http://38.media.tumblr.com/868407331446a1f32e864e4c7fa4b705/tumblr_ncg72izWVR1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Dropdog clothing</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426681636/dropdog-clothing-is-a-new-blasting-independent"><img src="http://31.media.tumblr.com/ecdb864679dfa74debe3714b0e376b54/tumblr_ncgiwegwMJ1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Egan</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426251531/egan-corporate-identity-by-alessandro-risso"><img src="http://31.media.tumblr.com/bcfb8653b75234ead772c907d60c939c/tumblr_nchbt7iLD71r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Eivind Holum</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426267366"><img src="http://38.media.tumblr.com/9fb37a935d89df269a0df468035fa19a/tumblr_nch2thxBbR1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Flowhynot</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426636311"><img src="http://38.media.tumblr.com/f95f52e9e2a2e044d35b4af73227f45c/tumblr_ncgwvmmecI1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Ignacio Estñebanez</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426294996/hi-my-name-is-ignacio-estebanez-aka-mao-dam-and"><img src="http://33.media.tumblr.com/26e069a0d554701a245184652c63de59/tumblr_ncgoh5WESv1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>inspirationde</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426751141/leadership-by-noel-shiveley"><img src="http://38.media.tumblr.com/0565beb57a857fc99663e22d6f9aad70/tumblr_ncg8u0ZBKm1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426679721/positive-vibes-only-by-sergio-malashenko"><img src="http://33.media.tumblr.com/9c9048d13e523c8320d13a170d9c575b/tumblr_ncgiu78Vcn1r46py4o1_500.png"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426275141/creative-juices-by-james-graves"><img src="http://38.media.tumblr.com/a57721539aecd84c97e0a260d92c5f3d/tumblr_ncgkm05Yge1r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426259526/emd-diamond"><img src="http://38.media.tumblr.com/cd607ff7c074d4feb03cd621cff150d5/tumblr_ncgyazGPZ21r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426262771/make-waves"><img src="http://33.media.tumblr.com/f72d322113175189891133b93d18409c/tumblr_ncgypl7Ogm1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Jim Banne</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426740811"><img src="http://31.media.tumblr.com/bfeb906fa00ad921a88be79c0502aca1/tumblr_ncg7ye0ZLa1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>joaoguapo</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426752936"><img src="http://33.media.tumblr.com/2a27989a470dc9e6afe697f5e167a87c/tumblr_ncg9l4WgQ81r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>João Marcos Britto</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426251271/https-www-behance-net-gallery-20045837-vivo-hands"><img src="http://38.media.tumblr.com/c7fc078b4caa4a4450f8f19236809462/tumblr_nchaou4txO1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Jose Checa</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426301626/hype-by-jose-checa"><img src="http://33.media.tumblr.com/ea583829534d645e2258452c3387d1f5/tumblr_ncgp6y1Org1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>juan diego johns</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426257336/hayabuza-1-300"><img src="http://33.media.tumblr.com/0ab4081cdceff10652e7afa13ee20ba7/tumblr_ncgy2nxFc71r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>lelinho1991</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426685486/icon-casio-in-style-ios8-coming-soon-the-other"><img src="http://31.media.tumblr.com/7a87521f10000dc4de49f59eda0f8149/tumblr_ncgkb0OTRd1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Lloyd Sebalane</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426276266/one-of-my-favorite-rappers-at-the-moment-the"><img src="http://38.media.tumblr.com/bd377b9ff3cb11911a9da168d31a23ba/tumblr_ncgl5joY9X1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426280181/self-portrait-of-a-low-poly-i-wanted-something"><img src="http://38.media.tumblr.com/3c4065d5790b849bc71b75c820d16262/tumblr_ncgldaJ37s1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>luxuryaccommodations</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426289801/hideaway-beach-resort"><img src="http://38.media.tumblr.com/ab3484ad27c8bd37c4fc4cc20d9b1993/tumblr_ncgmvhBHZc1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426665586/hotel-odyssey"><img src="http://33.media.tumblr.com/b4e58f237aab1e63d1ea266526f3373f/tumblr_ncgien2kOp1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426665586/hotel-odyssey"><img src="http://33.media.tumblr.com/b4e58f237aab1e63d1ea266526f3373f/tumblr_ncgien2kOp1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>makhzouum</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426226431/its-just-design-theres-no-coffee-shop-called"><img src="http://38.media.tumblr.com/180c2f306228723ec32a51fb02d5eae8/tumblr_nchaiqMPPn1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Mark</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426621126/old-tin-toys-come-back-to-life-as-lights"><img src="http://33.media.tumblr.com/214b25e0c67b43bc45ae95ee0c5cf05b/tumblr_ncgs0v45gX1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426625811/dive-bombing-red-baron-plane-becomes-a-light"><img src="http://33.media.tumblr.com/37123edaf4110bb4c04281139d1efe5e/tumblr_ncgs6ileRP1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426623101/vintage-tin-toy-evolves-into-a-ceiling-pendant"><img src="http://38.media.tumblr.com/f76f4a4ba0ce816054de2735fb49307c/tumblr_ncgs3wbN0M1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Mirko Càmia</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426320651"><img src="http://38.media.tumblr.com/c48e7eb8705e641453fd30a4fba131df/tumblr_ncgc6kwuw41r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426759361"><img src="http://33.media.tumblr.com/d15a96bbf5ae9c88a2a04393b884837f/tumblr_ncgb9nF1z91r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426755001"><img src="http://38.media.tumblr.com/e642e09d8492754da7befed32d94dc95/tumblr_ncgavdUp0z1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426302741"><img src="http://33.media.tumblr.com/ad1d5b40f51a904a7e7e0272f0ed8b22/tumblr_ncgbihJcO51r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>naoconsigonaodizer</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426766891/pedro-giunti"><img src="http://33.media.tumblr.com/3367f1027c7a72ff4e621e39021b92d3/tumblr_ncfklajgLP1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Nate Hicks</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426605691/a-striking-show-poster-created-from-traditional"><img src="http://33.media.tumblr.com/8f261a82d931c72dad4fc8c6eec1a3f2/tumblr_ncgqhp7SWM1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Nick Myers</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426677831/wacom-australia-is-giving-away-a-cintiq-companion"><img src="http://38.media.tumblr.com/cc7b8f357a1d91e6168e9b1821bedda9/tumblr_ncfkgcaxqa1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Odinio</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426056556"><img src="http://38.media.tumblr.com/3bc7946300c467e0af4dec6768c813e2/tumblr_nch6jmFmrR1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Orlando Korzo</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426265771/ohh-la-la-orlando-korzo"><img src="http://33.media.tumblr.com/23f706d3c9c80a3e42139128b3419b12/tumblr_nch18er44I1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>r.sands</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426553371/the-black-toe-xiv-beast-one-of-the-beasts-in"><img src="http://33.media.tumblr.com/9b4b5d37d4a6bdbe2a646e4dbcd9b079/tumblr_ncfkrtBtW01r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426736226/the-black-toe-xiv-beast-one-of-the-beasts-in"><img src="http://38.media.tumblr.com/9b4b5d37d4a6bdbe2a646e4dbcd9b079/tumblr_ncfkqyPetz1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>samadarag</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426522131/proposed-logo-concept-for-a-music-producer-based"><img src="http://33.media.tumblr.com/010211562d8251f93a476205613b7457/tumblr_ncfza3lbxt1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426526996/proposed-logo-concept-for-a-music-producer-based"><img src="http://38.media.tumblr.com/86fd933897056faa25f3cef847c22dbb/tumblr_ncfzmcF9le1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426515551/proposed-logo-concept-for-a-music-producer-based"><img src="http://38.media.tumblr.com/86fd933897056faa25f3cef847c22dbb/tumblr_ncfyvtKC0c1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Sebastian</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426748641/summer-in-the-city"><img src="http://33.media.tumblr.com/699ded83e6aa799e52ba10e91f7b7f2e/tumblr_ncg89cSiqX1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426743226/making-war"><img src="http://33.media.tumblr.com/08718bfb34f0b4f027d44b2bb4c36d1b/tumblr_ncg86wUxJw1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>svpermachine</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426512141/vanitas-by-fatkur-rokhim-serving-desktopography"><img src="http://33.media.tumblr.com/1635e020a492efcd55d2ede601dc0046/tumblr_ncftus7gMU1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>The cash maker</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426630526"><img src="http://33.media.tumblr.com/cc94222796a5cbdcc414134abe36e588/tumblr_ncgvys4OsV1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>ueeo-media</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426269096"><img src="http://31.media.tumblr.com/6f3825c5b57131983dcb8d2c40932f71/tumblr_nch47cdhVy1r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>Vinicius Costa</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426518106/hi-i-d-like-to-share-with-you-my-best"><img src="http://31.media.tumblr.com/2755c5e8b3f43df8993eb644a6ee89c7/tumblr_ncfz7dxKrg1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>WhatAnArt</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426654921/clever-street-art-makes-truck-body-look-like-a"><img src="http://31.media.tumblr.com/d61a43e927d9fce83e3eb472d5b7fff3/tumblr_ncgexaA2tl1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>

<h3>ZAKI Abdelmounim</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426531711/curiosity-2-my-entry-for-desktopography-exhibition"><img src="http://31.media.tumblr.com/58f5a4440765dd9bd28683e1bc696bc9/tumblr_ncgcpdHXMM1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98426324851/curiosity-my-entry-for-desktopography-exhibition"><img src="http://33.media.tumblr.com/378015436a5bbd98d20a2121fec8d2c2/tumblr_ncgch6ekRB1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1880" width="550" /></a></p></div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/daily-inspiration">daily inspiration</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/fabiano" title="Read fabiano&#039;s latest blog entries.">fabiano&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=wBq4bFuBi-o:7_KtaUlLJcI:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=wBq4bFuBi-o:7_KtaUlLJcI:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=wBq4bFuBi-o:7_KtaUlLJcI:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=wBq4bFuBi-o:7_KtaUlLJcI:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=wBq4bFuBi-o:7_KtaUlLJcI:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=wBq4bFuBi-o:7_KtaUlLJcI:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=wBq4bFuBi-o:7_KtaUlLJcI:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=wBq4bFuBi-o:7_KtaUlLJcI:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/wBq4bFuBi-o" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Sep 2014 00:23:29 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79826 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"fabiano";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:43:"http://abduzeedo.com/daily-inspiration-1880";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"Case Study: Just Around the Corner";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:85:"http://feedproxy.google.com/~r/abduzeedo/~3/u8Rwr-X5R4c/case-study-just-around-corner";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6755:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/c18c810a603083e4fe3cc001de05f417.jpg" width="1240" height="1043" alt="Case Study: Just Around the Corner" title="Case Study: Just Around the Corner" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>Just Around the Corner is a campaign for HSBC to communicate the merging of HSBC Oman with Oman International Bank. <strong>Staudinger + Franke</strong> is a visual studio from Austria that took this project and created an amazing case study. Enjoy!</h3>

<p>For more from Staudinger + Franke visit <a href="http://www.staudinger-franke.com/">staudinger-franke.com/</a>.</p>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/00.jpg" alt="Case Study: Just Around the Corner" />
</a></p>

<blockquote>HSBC is a worldwide operating banking and financial services company.
To communicate the merging of HSBC Oman with Oman International Bank we were commissioned to realise the visuals for the campaign.</blockquote>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/01.jpg" alt="Case Study: Just Around the Corner" />
</a></p>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/02.jpg" alt="Case Study: Just Around the Corner" />
</a></p>

<h2>The Idea</h2>
<p>HSBC is operating global and connecting Oman to the rest of the world and wherever you are, you will find a branch that connects you to your home bank, just around the corner.</p>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/03.jpg" alt="Case Study: Just Around the Corner" />
</a><br/>The layout from JWT Dubai.</p>

<h2>The Realization</h2>
<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/04.jpg" alt="Case Study: Just Around the Corner" />
</a><br/>First rough composition before we went to Oman to shoot additional elements.</p>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/05.jpg" alt="Case Study: Just Around the Corner" />
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/06.jpg" alt="Case Study: Just Around the Corner" />
</a><br/>Shooting in and around Muscat/Oman.</p>



<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/07.jpg" alt="Case Study: Just Around the Corner" />
</a><br/>First rough composition after the shoot in Oman. </p>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/08.jpg" alt="Case Study: Just Around the Corner" />
</a><br/>Work in progress.</p>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/09.jpg" alt="Case Study: Just Around the Corner" />
</a><br/>Final background with rough color corrections.</p>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/10.jpg" alt="Case Study: Just Around the Corner" />
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/11.jpg" alt="Case Study: Just Around the Corner" />
</a><br/>Most elements are from our stock library.</p>

<object height="320" width="512">
              <param name="movie" value="https://a2.behance.net/swf/osmf_strobe.swf">
              <param name="flashvars" value="src=https%3A%2F%2Fm1.behance.net%2Fprofiles12%2F292702%2Fprojects%2F15788149%2F079d01a9c7647312d0bf7b35e7b01c37.flv&amp;height=320&amp;width=512&amp;poster=https%3A%2F%2Fm1.behance.net%2Fprofiles12%2F292702%2Fprojects%2F15788149%2F079d01a9c7647312d0bf7b35e7b01c37-0000.png&amp;controlBarMode=floating&amp;streamType=recorded&amp;controlBarAutoHideTimeout=2&amp;controlBarAutoHide=true">
              <param name="allowfullscreen" value="true">
              <param name="allowscriptaccess" value="never">
              <embed src="https://a2.behance.net/swf/osmf_strobe.swf" type="application/x-shockwave-flash" width="512" height="320" flashvars="src=https%3A%2F%2Fm1.behance.net%2Fprofiles12%2F292702%2Fprojects%2F15788149%2F079d01a9c7647312d0bf7b35e7b01c37.flv&amp;height=320&amp;width=512&amp;poster=https%3A%2F%2Fm1.behance.net%2Fprofiles12%2F292702%2Fprojects%2F15788149%2F079d01a9c7647312d0bf7b35e7b01c37-0000.png&amp;controlBarMode=floating&amp;streamType=recorded&amp;controlBarAutoHideTimeout=2&amp;controlBarAutoHide=true" allowfullscreen="true" wmode="transparent" style="">
            </object>

<p class="imgC"><a href="http://bit.ly/1qenDRE">
<img src="http://imgs.abduzeedo.com/files/paul0v2/hsbc/12.jpg" alt="Case Study: Just Around the Corner" />
</a><br/>Final Version</p></div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/case-study">case study</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/photo-illustration">photo illustration</a></div><div class="field-item even"><a href="http://abduzeedo.com/tags/ad-campaign">ad campaign</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/paul0v2" title="Read paul0v2&#039;s latest blog entries.">paul0v2&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=u8Rwr-X5R4c:OO5w9fF703o:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=u8Rwr-X5R4c:OO5w9fF703o:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=u8Rwr-X5R4c:OO5w9fF703o:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=u8Rwr-X5R4c:OO5w9fF703o:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=u8Rwr-X5R4c:OO5w9fF703o:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=u8Rwr-X5R4c:OO5w9fF703o:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=u8Rwr-X5R4c:OO5w9fF703o:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=u8Rwr-X5R4c:OO5w9fF703o:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/u8Rwr-X5R4c" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 25 Sep 2014 14:44:13 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79825 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"paul0v2";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:50:"http://abduzeedo.com/case-study-just-around-corner";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:20:"Logo Design: Folders";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:75:"http://feedproxy.google.com/~r/abduzeedo/~3/QhZ1DJ3OUp0/logo-design-folders";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5824:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/logo-folders.jpg" width="1000" height="700" alt="Logo Design: Folders" title="Logo Design: Folders" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>We keep going through our weekly journey checking out great logos! We're thinking ahead and we want to find new topics, good ones, to share with you guys. Last week we featured logos with swords, and today we're featuring logos with folders!</h3>

<p>Every week we search through our favorite galleries: <a href="http://logopond.com">Logopond</a> and <a href="http://dribbble.com">Dribbble</a>. Both are a great communities for designers everywhere. Also, if you you'd like to suggest ideas for the next subjects, please, tell me by sending me sending me a tweet: <a href="http://twitter.com/paulogabriel">@paulogabriel</a>. Cheers! ;)</p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/67248" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/6feb747fadf507c0395373670fe07bea.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/185776" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/40737ced3014b8e3a9977b4605156d99.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/177751" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/0d75af9253a6ca2033a300f9675643ac.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/172148" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/37932c762cba46ae124f8a63b35fb809.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/135789" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/beb95fee9f489e25b79a1110e77e0c17.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/171743" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/cc763fa498f394d2c445e7548eed5b21.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/122215" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/3347dd7f4e30c4512abeef4ff42debce.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/96841" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/909a68a78dc7bca543556bf6e16282de.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/63488" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/5889db0a33f51adca0db8b4f060ee48d.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="http://logopond.com/gallery/detail/15545" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/5c8249352479bf080a6c4484fb5daa0f.png" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="https://dribbble.com/shots/1150330-Mindfolder?list=searches&offset=15" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/mindfolder-dribbble_1x.jpg" alt="Logo Design: Folders"/>
</a></p>

<p class="imgC">
<a href="https://dribbble.com/shots/955749-Casework?list=searches&offset=39" title="Logo Design: Folders">
<img src="http://imgs.abduzeedo.com/files/articles/logo-design-folders/caseworkdribbble.png" alt="Logo Design: Folders"/>
</a></p></div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/logo">logo</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/logo-design">logo design</a></div><div class="field-item even"><a href="http://abduzeedo.com/tags/folders">folders</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/paulogabriel" title="Read PauloGabriel&#039;s latest blog entries.">PauloGabriel&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QhZ1DJ3OUp0:R6rEjcdili0:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QhZ1DJ3OUp0:R6rEjcdili0:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=QhZ1DJ3OUp0:R6rEjcdili0:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QhZ1DJ3OUp0:R6rEjcdili0:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=QhZ1DJ3OUp0:R6rEjcdili0:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QhZ1DJ3OUp0:R6rEjcdili0:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=QhZ1DJ3OUp0:R6rEjcdili0:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=QhZ1DJ3OUp0:R6rEjcdili0:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/QhZ1DJ3OUp0" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 25 Sep 2014 11:33:55 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79824 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"PauloGabriel";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:40:"http://abduzeedo.com/logo-design-folders";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:16:"RADA Neon Lights";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"http://feedproxy.google.com/~r/abduzeedo/~3/_b-EOt5WYf0/rada-neon-lights";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:4675:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/0f53e9107c6c3dcd77c4839794072bd2.jpg" width="1240" height="698" alt="RADA Neon Lights" title="RADA Neon Lights" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>I love neon light and the light effects that they create. I also wrote a few Photoshop tutorials back in the day showing how to reproduce this kind of effect, nothing as fancy and realistic as the work that RADA put together in Cinema 4D for a personal project inspired by the work of Mathias Nösel. The outcome is quite beautiful and the first time I saw I was sure that it was real neon and not 3d.</h3>

<p>For more information and to check out Rada's full portfolio check out his webiste on Behance at <a href="https://www.behance.net/rada">https://www.behance.net/rada</a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132593015/disp/c1eee6e9fe411526d0e00eb00bd36b10.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132593009/disp/84c27b90feda75a524baba364a4e49c7.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132593013/disp/d74188ff4111f6aa2da1beca25976ab0.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132593011/disp/dd63831ccc1411dbdf19510f1db8161c.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132592989/disp/f5a3fcfa6da5f0e02d65b5a807e2f657.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132592991/disp/3a47f46655101d72a086ab79396be2a9.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132592993/disp/9519ddb38a91770f53c59bc6617cb599.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132592995/disp/ffd7582017d71490d9e2d00a3bd7ba8e.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132597281/disp/0f53e9107c6c3dcd77c4839794072bd2.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132593001/disp/6a61cab18723d9932e0206a6966caf67.jpg" /></a></p>

<p class="imgC"><a href="https://www.behance.net/rada"><img src="https://m1.behance.net/rendition/modules/132593005/disp/b547b80106dc001a76978597bbd2c10a.jpg" /></a></p>
</div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/neon">neon</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/light-effects">light effects</a></div><div class="field-item even"><a href="http://abduzeedo.com/tags/typography">Typography</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/abduzeedo" title="Read abduzeedo&#039;s latest blog entries.">abduzeedo&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=_b-EOt5WYf0:4-jAMVoQqOI:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=_b-EOt5WYf0:4-jAMVoQqOI:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=_b-EOt5WYf0:4-jAMVoQqOI:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=_b-EOt5WYf0:4-jAMVoQqOI:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=_b-EOt5WYf0:4-jAMVoQqOI:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=_b-EOt5WYf0:4-jAMVoQqOI:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=_b-EOt5WYf0:4-jAMVoQqOI:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=_b-EOt5WYf0:4-jAMVoQqOI:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/_b-EOt5WYf0" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 25 Sep 2014 05:08:16 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79823 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:9:"abduzeedo";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:37:"http://abduzeedo.com/rada-neon-lights";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:23:"Daily Inspiration #1879";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:78:"http://feedproxy.google.com/~r/abduzeedo/~3/oPoGSC7Vxv4/daily-inspiration-1879";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:24337:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/tumblr_ncdt19pdok1r46py4o1_1280.jpg" width="1012" height="1181" alt="Daily Inspiration #1879" title="Daily Inspiration #1879" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>This post is part of our daily series of posts showing the most inspiring images selected by some of the Abduzeedo's writers and users. If you want to participate and share your graphic design inspiration, You can submit your images and inspiration to RAWZ via <a href="http://raw.abduzeedo.com">http://raw.abduzeedo.com</a> and don't forget to send your Abduzeedo username; or via Twitter sending to <a href="http://twitter.com/abduzeedo">http://twitter.com/abduzeedo</a>.</h3>

<h3>admin</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342547126/the-nature-of-victorian-by-kevin-cantrell-is-the"><img src="http://38.media.tumblr.com/18294acf64a205a69d73c4924fe4cd01/tumblr_ncf32zr1VR1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Angel Alejandro</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342598956"><img src="http://38.media.tumblr.com/50e825535b2f4462c999c090b4aeea36/tumblr_nce8i8R4f21r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>artofsandwiches</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341991041/the-art-of-sandwiches-more-than-your-average"><img src="http://33.media.tumblr.com/70c9583ecffd12dba41e7401cf637dca/tumblr_nceo5zHr3S1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>ATOTEIXEIRA</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341935836"><img src="http://33.media.tumblr.com/434bb23a29baa916441d45b068df2fd9/tumblr_ncf5fubW3m1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>beemifun</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342597051/skateboy-color-study"><img src="http://38.media.tumblr.com/54ac1772870638e496a53dba8bc177a8/tumblr_nce4ypR6zW1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Ben Hewitt</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342579336/ben-hewitt-desktopography-2014"><img src="http://33.media.tumblr.com/7a6ab9f48a8fb5d814c15059d1e5e462/tumblr_nceituRTaV1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>bluethumbart</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342586146/embrace-and-incredibly-detailed-drawing-by-rhys"><img src="http://31.media.tumblr.com/471e60d42cb53fe26a23041f28f7378d/tumblr_nce0xtRKmM1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342668051/jasper-acrylic-on-canvas-by-jac-clark"><img src="http://33.media.tumblr.com/014e6dc8171f2ef77cbb765be4629c4a/tumblr_ncdzzvLeVZ1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342670041/satori-acrylic-on-canvas-by-leon-lester"><img src="http://38.media.tumblr.com/ad85540452684b86cb5068ddedf94f41/tumblr_nce0hohjnv1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342588381/whistful-and-incredibly-detailed-drawing-by"><img src="http://33.media.tumblr.com/d871716a3c77c9994a1a0749927f8e78/tumblr_nce17rWKHm1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Deoxys</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342610701/deoxys-pokemon"><img src="http://33.media.tumblr.com/01b84d84b519775e4ff8609d480279f6/tumblr_ncdpu617JT1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>erreacorreia</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341966376"><img src="http://38.media.tumblr.com/44c769c134fe4aab3519592ee4dde675/tumblr_nceohb2GCP1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Frank Offman</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341946946"><img src="http://38.media.tumblr.com/5835d14074ec6ce8d0fff52d4b76bae1/tumblr_ncf7quB6Hc1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341948471"><img src="http://31.media.tumblr.com/bb13bfb706b9f55b3fdfc23d5ce07ff7/tumblr_ncf9m8uFiE1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341941381"><img src="http://33.media.tumblr.com/07ed8e0e26bd6020f5d02492e2b036b3/tumblr_ncf7dnQ9Ye1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Henrique Cassab</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341920551/in-love-with-a-ferris-wheel-desktopography-2014"><img src="http://38.media.tumblr.com/7ed33f1dfeb32332fbf57004d1336f8d/tumblr_ncfe5iTXiD1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>hotamr</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341925546/hotamr-aka-amr-elshamy"><img src="http://38.media.tumblr.com/753fe56ef318190687725070d150ff4e/tumblr_ncff0r4fpX1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341924936"><img src="http://38.media.tumblr.com/30022a25083ae9b4d206de12de5b5985/tumblr_ncfey3Lbtr1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Ingus Eisaks</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341962051/exclusive-customizable-logo-at-eisaks-logo-design"><img src="http://33.media.tumblr.com/0b4c0ca42190648a834ca610b9a14f06/tumblr_ncemju07hm1r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Ink Ration</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98258545146"><img src="http://33.media.tumblr.com/410bfa140d1dbb14f06976a0a4c762f2/tumblr_nccyqcjVn11r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>inspirationde</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342643746/grain-and-gristle"><img src="http://38.media.tumblr.com/286ded60d54dc638659c7129f80f7bfb/tumblr_ncec564pfm1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342645961/cook-book-training-stuff"><img src="http://38.media.tumblr.com/671b8e1a7aca15177747efd5d4158bd1/tumblr_ncechhXBrg1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342571646/scandinavian-design-menu-copenhagen-designtrade"><img src="http://33.media.tumblr.com/579d87325dfcccf88074e2a667e16e6c/tumblr_ncemdsFS191r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342544936/syrup-speckled-camp-mug-syrup-souvenir-shop"><img src="http://38.media.tumblr.com/29b52c7ed73ec46e65338c368ac13ca6/tumblr_ncf1ljtA871r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342567816/r-road-cone-logo-deign-by-deividas-bielskis"><img src="http://33.media.tumblr.com/a5ab6f7f189661527b825782ad8a212b/tumblr_ncelncntsC1r46py4o1_400.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342569196/porcelain-by-fin-dac"><img src="http://33.media.tumblr.com/5e03fe12ca1d0319cf17c28c44c5c380/tumblr_ncem9in5QY1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341939341/real-not-perfect-by-jay-roeder"><img src="http://33.media.tumblr.com/3aa34ee99b637905a12798ab9a7946be/tumblr_ncf723fr2k1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>jaidrop</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342552941/https-www-behance-net-gallery-20016810-spirit-man"><img src="http://33.media.tumblr.com/35cf50b9eeb86ebac5e62d4687a28811/tumblr_ncf4djt8u21r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>James George</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342594556/i-created-this-free-vintage-flyer-template-as-a"><img src="http://33.media.tumblr.com/f1eb6dde5d822c1d07305e4cf293ea9d/tumblr_nce4853AM51r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Jeff Wright</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341929601/touchwood-for-life-a-lifestyle-brand-touchwood"><img src="http://31.media.tumblr.com/e2e5c6a199f9142e350584d753cbdf49/tumblr_ncfgjt7Vpe1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342687126/touchwood-for-life-is-a-premium-and-fashionable"><img src="http://38.media.tumblr.com/42ea7bd31b05d494445def772a7c4978/tumblr_ncdst9ShZL1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>jerald saddle</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342531271"><img src="http://38.media.tumblr.com/9c3d985bb9aae2f5eb9ac212fd4f1288/tumblr_ncfdg7SInZ1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Jim Banne</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342638271/http-www-artstation-com-artwork-mean-martian"><img src="http://33.media.tumblr.com/00771cf6eed79268d96dade9f94002ad/tumblr_ncebljHYa51r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342641086"><img src="http://33.media.tumblr.com/00771cf6eed79268d96dade9f94002ad/tumblr_ncec0u1nvk1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>John Rebaud</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341950821/skateboard-art-prints"><img src="http://33.media.tumblr.com/0b186e59178008fe6e2bf2d4347c3604/tumblr_ncf9x87aGQ1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Joshua Phillips</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98258549071"><img src="http://33.media.tumblr.com/714eb0382d2abfe480b3871f2c60118a/tumblr_nccyv6ALyj1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Liam</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98258653311/the-butcher-snowpiercer-character-illustration"><img src="http://38.media.tumblr.com/dcab437e4bf53e068daa84c03af0dfc9/tumblr_ncc8umKjeY1r46py4o1_500.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>lmscreed</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341975276/herbie-from-the-love-bug-1968-as-a-kaiser-jeep"><img src="http://33.media.tumblr.com/ec305cc30fa0353dd284fe7684f2fcc2/tumblr_ncerlrQQMT1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Lucas Vogelmann</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341982846/duparc-brand-identity"><img src="http://38.media.tumblr.com/1474c0c6b248b42d01b1acdcb6550abd/tumblr_nces7v8b1c1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>marcopuccini</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342557531/marco-puccini-a-d-o-r-s-o-d-i-u-n-p-e-s-c-e"><img src="http://33.media.tumblr.com/347ab7b07b823182f2b18447610f6e45/tumblr_ncefocJ1551r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Marc Schönn</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342539716"><img src="http://38.media.tumblr.com/aaffca7230d2532e0dfe1a023bf1bc8f/tumblr_ncemeo0Xix1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342653191"><img src="http://33.media.tumblr.com/ad9ef94df11e154e9d81aa762c435668/tumblr_ncemh2D4NQ1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341964011"><img src="http://33.media.tumblr.com/26deb3f3fd3cd4e99c8140eb000e1747/tumblr_ncemkksAXC1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341959051"><img src="http://33.media.tumblr.com/26deb3f3fd3cd4e99c8140eb000e1747/tumblr_ncemjnF9Dd1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>maya rodriguez</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341927576/dance-to-life-more-of-my-upcycled-work-at"><img src="http://31.media.tumblr.com/440aa00183bbdb0ee7c262c82e4a37d1/tumblr_ncffu3gu9b1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Miguel Andrade</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341916176/http-migueandrade-blogspot-com"><img src="http://31.media.tumblr.com/e31642cb0fd273ca38ad3f0b350df95d/tumblr_ncfdtrqZvz1r46py4o1_500.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>moriarty111</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98258536981"><img src="http://31.media.tumblr.com/d9d9e2331498485ed5b572b608f8c403/tumblr_nccwey6CMI1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342602081/starblax-plotation"><img src="http://38.media.tumblr.com/a830899d577b548939d1a3291a802f65/tumblr_ncea0lxX6H1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>M&E</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98258534511/we-are-m-e-and-we-were-recently-asked-to-design-a"><img src="http://31.media.tumblr.com/93e28c0e3981c74abcbcbc2f4cd5b9b3/tumblr_nccughJwG01r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Nathan Clegg</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342674651/the-bull-in-chicago"><img src="http://38.media.tumblr.com/ead4c32f528fbce3d4f2cd117d01c6e6/tumblr_nce0j94PNb1r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Nectar</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342543171/new-interactive-campaign-of-lois-jeans-for-the"><img src="http://38.media.tumblr.com/5a223e98d880ba7d8ef588fb822acbb2/tumblr_ncezzybhlD1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>ODON</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342661346/crystal-font"><img src="http://38.media.tumblr.com/5e83f1355daf1eae0f382f750ed89cc1/tumblr_ncduu0L7X21r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342659141/i-hate-k-drama"><img src="http://38.media.tumblr.com/fd13d0a8455d24ae6e3080774130480d/tumblr_ncduf7as7R1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Pascal Meyer</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341954741/www-pascalmeyer-net"><img src="http://33.media.tumblr.com/054d1a23ce77e9226aab9a6a73e2ead3/tumblr_ncfcnv3EJI1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>paulvonexcite</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342636611/http-fb-com-paulvonexcite"><img src="http://31.media.tumblr.com/327b8340f7fcc0a996616accddbe4a5e/tumblr_ncebg6zLpL1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342634761/http-fb-com-paulvonexcite"><img src="http://38.media.tumblr.com/54edff02463105661d2c78be338c43bf/tumblr_ncebfmlthP1r46py4o1_400.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342632886/http-fb-com-paulvonexcite"><img src="http://33.media.tumblr.com/18d4dc3430eedb48867579acf04d8487/tumblr_ncebd2hn8f1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Piotr Makarewicz</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342560201/ive-prepared-a-100-simple-icons-along-with-a"><img src="http://38.media.tumblr.com/6a702279406ee2fcee50e2e37760ad9c/tumblr_ncegysodFZ1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Raul Aguiar</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98258532306/wild-numbers"><img src="http://33.media.tumblr.com/f2019309847f547418034a453a6ed59b/tumblr_ncctgzmk9Z1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Robert Silva</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341937901/printed-look-book-for-and-by-jls-agency"><img src="http://38.media.tumblr.com/5700e9d8b6f9149aef8ba208fa95607f/tumblr_ncf64nvEhq1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>sarah2611</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342656636/to-the-stars"><img src="http://38.media.tumblr.com/3f1b8e71a408c4845b031c24bfd97bc5/tumblr_ncdt19Pdok1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Soul Branding</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341984861/bolo-integral-banana-orquidea-ambientacao"><img src="http://31.media.tumblr.com/106c7e2df0fb078d1e62e2a68151266d/tumblr_nceth2RdUa1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Steve </h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342562711/http-www-memyselfandi-uk-com-typography-map-of"><img src="http://31.media.tumblr.com/c5eb87d983aeb18fffc4eca0425549c7/tumblr_ncejtaxZAs1r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342566146/stahl-house-los-angeles"><img src="http://33.media.tumblr.com/4155969d0986345f7b814c36c03f42f6/tumblr_ncejyd5nIC1r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>tecnohaus</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342551331/living-room-design-50-living-room-in-gray"><img src="http://33.media.tumblr.com/f863648c33440b7b7863c19d316343bc/tumblr_ncf4c0HJJk1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>thomahh</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342592151/road-trip-in-chili-photo-by-thomas-hermelin"><img src="http://38.media.tumblr.com/3016eb6a6e08d82aad7748887ee800aa/tumblr_nce2w1O5AF1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Tiago Elias</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342614216"><img src="http://38.media.tumblr.com/29381fe744ecdc67dcf1390b705be1cc/tumblr_ncdpj0hMD41r46py4o1_1280.png"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Tiberiu Sirbu</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342678721/flick-magazine"><img src="http://33.media.tumblr.com/f9fd44eb9d55321dc8f216e7f74bb255/tumblr_nceb9mzuMe1r46py4o1_500.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Veezy Vibetime</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342663726/http-veezyvibetimecreations-wordpress-com"><img src="http://38.media.tumblr.com/aa721b31262ed1299d09cb3c43d20a8e/tumblr_ncdxp7WNvC1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
<p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342665591/http-veezyvibetimecreations-wordpress-com"><img src="http://38.media.tumblr.com/38bb764c63e8d2df6691ae160567d370/tumblr_ncdxu3nsZd1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>wearviral</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98258648246/iron-chef-t-shirt-by-michael-holmes-wear-viral"><img src="http://33.media.tumblr.com/b9f0c5c272cd42499bf3733cdf8bc9e4/tumblr_nccshqyqtx1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>YanniBunny</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98342555886"><img src="http://38.media.tumblr.com/41ef3c0e2377794518d7c115c0f21a31/tumblr_ncedqa6lQu1r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>

<h3>Yoko</h3><p class="imgC" ><a href="http://raw.abduzeedo.com/post/98341952901"><img src="http://38.media.tumblr.com/92df2631f899dd724a4e529f576a70eb/tumblr_ncfa4wAk651r46py4o1_1280.jpg"  alt="Digital art selected for the Daily Inspiration #1879" width="550" /></a></p>
</div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/daily-inspiration">daily inspiration</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/fabiano" title="Read fabiano&#039;s latest blog entries.">fabiano&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=oPoGSC7Vxv4:NN21DL4IIsw:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=oPoGSC7Vxv4:NN21DL4IIsw:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=oPoGSC7Vxv4:NN21DL4IIsw:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=oPoGSC7Vxv4:NN21DL4IIsw:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=oPoGSC7Vxv4:NN21DL4IIsw:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=oPoGSC7Vxv4:NN21DL4IIsw:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=oPoGSC7Vxv4:NN21DL4IIsw:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=oPoGSC7Vxv4:NN21DL4IIsw:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/oPoGSC7Vxv4" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 25 Sep 2014 01:23:17 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79822 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"fabiano";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:43:"http://abduzeedo.com/daily-inspiration-1879";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Typography Mania #255";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:76:"http://feedproxy.google.com/~r/abduzeedo/~3/JLNG_L1DTBI/typography-mania-255";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:6443:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/tumblr_ncegb7jbre1ryq9ado1_1280.jpg" width="1200" height="1200" alt="Typography Mania #255" title="Typography Mania #255" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>Typography Mania is a weekly post series that comes around once a week with the best of Typography design submitted by our users at <a href="http://typography-mania.com/">typography-mania.com</a>, from type videos to images everything is full of great design and typography inspiration. Submit your typography designs too and be part of this post.</h3>

<p><a title="http://abduzeedo.com/tags/typography-mania" href="http://abduzeedo.com/tags/typography-mania">Click here to check out all the previous Typography Mania</a></p>

<h2>Submit your work to <a href="http://typography-mania.com/">typography-mania.com</a></h2>

<h3>kissmytat2s</h3><p class="imgC" ><a href="http://typography-mania.com/post/97646161138/always-or-never-a-way"><img src="http://38.media.tumblr.com/31e4d77db7908c1aeab915eaf5eec2ca/tumblr_nbp1sz21vW1ryq9ado1_1280.jpg" /></a></p>

<h3>theboredkids</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305607603/surround-yourself-with-good-company-and-good"><img src="http://31.media.tumblr.com/5347b7472f19767252c300af677dbed4/tumblr_ncegb7jbre1ryq9ado1_1280.jpg" /></a></p>

<h3>pascualredmen</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305642748/pascual-redondo"><img src="http://33.media.tumblr.com/9af6374befa747ed01623f89ea7beb41/tumblr_ncc4gkO6kj1ryq9ado1_1280.jpg" /></a></p>

<h3>ahjoboy</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305640778/http-instagram-com-p-tdsq2ybrt2-modal-true"><img src="http://38.media.tumblr.com/bccf028ad06bda638d3314a3e3501d11/tumblr_ncck1tp9Wf1ryq9ado1_1280.jpg" /></a></p>

<h3>LuisEsteban</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305730933/i-hope-you-like-it-you-can-check-the-full-project"><img src="http://33.media.tumblr.com/c59f91ebffa330a56439485c31220865/tumblr_ncagx5FkT41ryq9ado1_1280.png" /></a></p>

<h3>Todd Fooshee</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305739063/answered-artwork"><img src="http://33.media.tumblr.com/2a0c5b418caaceed4b95c11757523933/tumblr_nca3jjlPI71ryq9ado1_1280.jpg" /></a></p>

<h3>Melvin Leidelmeijer</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305823713/through-persistence-labor-made-with-a-brushpen"><img src="http://31.media.tumblr.com/4f3a459ba4184da2e6de333b8bfd63ea/tumblr_nc8ruw14bO1ryq9ado1_1280.jpg" /></a></p>


<h3>Melvin Leidelmeijer</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305836058/seriously-made-with-a-brush-pen-and-vectorized"><img src="http://38.media.tumblr.com/7e63737aa7d2ec647dddeeacfc126448/tumblr_nc8rrpZIDm1ryq9ado1_1280.jpg" /></a></p>

<h3>Melvin Leidelmeijer</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305840548/arrivederci-italy-made-with-a-brush-pen-and"><img src="http://33.media.tumblr.com/a635975b53a92ca0a5750337ec4ccab9/tumblr_nc8rjmouIX1ryq9ado1_1280.jpg" /></a></p>

<h3>Alejandro Solórzano</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305844583/under-the-sea-by-alejandro-solorzano-just"><img src="http://38.media.tumblr.com/90d25a6d9c13e32d93b585f3533b969c/tumblr_nc5ry3R8LI1ryq9ado1_1280.jpg" /></a></p>

<h3>calvinatordesigns</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305846528/i-decided-to-explore-outside-of-my-comfort-zone-by"><img src="http://33.media.tumblr.com/fcfb59d30cc7c9bbdb4b10922a53287d/tumblr_nc4rcmTQxv1ryq9ado1_1280.jpg" /></a></p>

<h3>_fisheater</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305848493/more-typography-in-my-portfolio-here"><img src="http://38.media.tumblr.com/ba13ddae6bac92878420a8b22e77fade/tumblr_nc3q7vAG8z1ryq9ado1_1280.jpg" /></a></p>

<h3>maghriblab</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305852718"><img src="http://38.media.tumblr.com/a1334c842ac88e8468378ddcb96ee61f/tumblr_nc2sqiAyLu1ryq9ado1_500.jpg" /></a></p>

<h3>Wedding Invitation</h3><p class="imgC" ><a href="http://typography-mania.com/post/98305858448/https-www-behance-net-gallery-19477779-wedding-in"><img src="http://38.media.tumblr.com/28a71b8d8b2cc00d878ddf189a95812c/tumblr_nc22r5epYi1ryq9ado1_1280.png" /></a></p></div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/typography-mania">Typography Mania</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/typography">Typography</a></div><div class="field-item even"><a href="http://abduzeedo.com/tags/lettering">lettering</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/type">type</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/paul0v2" title="Read paul0v2&#039;s latest blog entries.">paul0v2&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=JLNG_L1DTBI:1Phvt9u8-KY:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=JLNG_L1DTBI:1Phvt9u8-KY:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=JLNG_L1DTBI:1Phvt9u8-KY:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=JLNG_L1DTBI:1Phvt9u8-KY:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=JLNG_L1DTBI:1Phvt9u8-KY:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=JLNG_L1DTBI:1Phvt9u8-KY:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=JLNG_L1DTBI:1Phvt9u8-KY:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=JLNG_L1DTBI:1Phvt9u8-KY:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/JLNG_L1DTBI" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 24 Sep 2014 14:12:02 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79821 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"paul0v2";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:41:"http://abduzeedo.com/typography-mania-255";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:48:"Weekly Apps: The Firm, Transmit, Manual and more";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:97:"http://feedproxy.google.com/~r/abduzeedo/~3/rbHIvI-ZI9c/weekly-apps-firm-transmit-manual-and-more";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:5039:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/wa140924.jpg" width="1440" height="700" alt="Weekly Apps: The Firm, Transmit, Manual and more" title="Weekly Apps: The Firm, Transmit, Manual and more" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>Today we're featuring a new app for photo, it's called Manual, and its name says everything! Also take a special look at Carddi and the new Transmit app. Hope you guys like these apps and all the whole selection, and stay tuned for the next week!</h3>

<p>You can keep sending me your suggestions via Twitter <a href="http://twitter.com/FabianoMe">twitter.com/FabianoMe</a> and include #abdz_apps in the message.<p>	

<h3>Manual - <a href="http://shootmanual.co/">shootmanual.co</a></h3> 
<blockquote>A powerful camera app with full control over your image. Quickly and simply adjust all parameters of your image. No more tapping and hoping automatic can understand what you want. Take control.</blockquote><p class="imgC"><a href="https://itunes.apple.com/us/app/manual-custom-exposure-camera/id917146276?ls=1&mt=8" title="Manual"><img src="http://imgs.abduzeedo.com/files/apps_week/140924/Manual.jpg" width="100%" alt="Manual" /></a></p><br/>

<h3>Carddi - <a href="http://www.carddi.mobi/">carddi.mobi</a></h3> 
<blockquote>Carddi is the most convenient way to find restaurants near you, allowing you to view detailed menus and their services when the establishment is associated with us.</blockquote><p class="imgC"><a href="https://itunes.apple.com/us/app/carddi-seu-cardapio-digital/id636348272?l=pt&ls=1&mt=8" title="Carddi"><img src="http://imgs.abduzeedo.com/files/apps_week/140924/Carddi.jpg" width="100%" alt="Carddi" /></a></p><br/>
	
<h3>lappsy - <a href="https://lappsy.com/presskit">lappsy.com</a></h3> 
<blockquote>Your interests, holidays, shopping, beautiful memories! Bring it to life, create amazing story and share it with you friends using lappsy! </blockquote><p class="imgC"><a href="https://itunes.apple.com/us/app/lappsy/id906065923" title="lappsy"><img src="http://imgs.abduzeedo.com/files/apps_week/140924/lappsy.jpg" width="100%" alt="lappsy" /></a></p><br/>

<h3>Transmit - <a href="http://panic.com/transmit-ios/">panic.com</a></h3> 
<blockquote>Transmit iOS allows you to connect to that server and manage all your files, exactly as you'd expect. Transfer. Make folders. Rename. Delete. Set Permissions. You know how it works, and Transmit does it.</blockquote><p class="imgC"><a href="https://itunes.apple.com/us/app/transmit-ios/id917432930?ls=1" title="Transmit"><img src="http://imgs.abduzeedo.com/files/apps_week/140924/Transmit.jpg" width="100%" alt="Transmit" /></a></p><br/>

<h3>The Firm - <a href="http://www.sunnysidegames.ch/portfolio/thefirm/">sunnysidegames.ch</a></h3> 
<blockquote>THE FIRM, first game developed by a small swiss indie team, is a twitchy fast-paced ARCADE video game where you work as a trader inside a big corporation.</blockquote><p class="imgC"><a href="http://www.pttrns.com/p/391" title="The Firm"><img src="http://imgs.abduzeedo.com/files/apps_week/140924/theFirm.jpg" width="100%" alt="The Firm" /></a></p><br/></div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/weekly-apps">weekly apps</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/iphone-app">iPhone app</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/fabiano" title="Read fabiano&#039;s latest blog entries.">fabiano&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=rbHIvI-ZI9c:w6x6hyKfi0w:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=rbHIvI-ZI9c:w6x6hyKfi0w:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=rbHIvI-ZI9c:w6x6hyKfi0w:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=rbHIvI-ZI9c:w6x6hyKfi0w:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=rbHIvI-ZI9c:w6x6hyKfi0w:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=rbHIvI-ZI9c:w6x6hyKfi0w:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=rbHIvI-ZI9c:w6x6hyKfi0w:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=rbHIvI-ZI9c:w6x6hyKfi0w:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/rbHIvI-ZI9c" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 24 Sep 2014 13:10:47 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79820 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:7:"fabiano";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:62:"http://abduzeedo.com/weekly-apps-firm-transmit-manual-and-more";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:28:"
    
    
    
     
 
 
  ";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";s:5:"child";a:3:{s:0:"";a:5:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"Pop Culture 3D Wallpapers by SLiD3";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:87:"http://feedproxy.google.com/~r/abduzeedo/~3/OMvDzoMAo6s/pop-culture-3d-wallpapers-slid3";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:9869:"<div class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img src="http://abduzeedo.com/files/originals/happydayz_1920_1600.jpg" width="900" height="563" alt="Pop Culture 3D Wallpapers by SLiD3" title="Pop Culture 3D Wallpapers by SLiD3" /></div></div></div><div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><h3>Wanna change your desktop wallpaper but you can't find a cool image for this? Don't worry, SLiD3 is here to help! This guy is really awesome because he's not only talented, but his work is totally pop culture inspired, with tons of homages to games, music, tv, comics and movies.</h3>

<p>So, just pick your favorite wallpapers, click on it and pay him <a href="http://cargocollective.com/SLiD3" target="_blank"><strong>a little visit</strong></a> to download full resolution images, because these are totally worth it. Also, his take on Mike Mitchell's pieces are just priceless! So stop losing time and go for it! Cheers! ;)</p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Standby-for-Tinyfall-mk3" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/SFTF_mk3_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Happy-DayZ" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/happyDayZ_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Little-Left-4-Dead" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/LittleLeft4Dead_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Songbird-Liz" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/BI_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Yoohoo-Botanicula" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/yoohoo_botanicula_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Andy-Helms-Skeletor" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/AndyHelms_Skeletor_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Tiny-TeamFortress-2" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/tinyTF2_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Rhinosaur" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/Canto_RhinoSaur_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Standby-for-Tinyfall-mk2" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/SFTF_mk2_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Mark-Facey-s-Tendry" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/MarkFacey_tendry_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Mike-Mitchell-s-Robocop" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/MikeMitchell_RoboCop_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Lil-bit-lucky" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/DaftPunk_GetLucky_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Le-Journey" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/LeJourney_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Nina-Edlund-s-Ron-Swanson" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/NinaEdlund_RonSwanson_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Hatboy-s-Moss" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/hatBoy_Moss01_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Mike-Mitchell-s-Walt-Jesse" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/MikeMitchell_WaltAndJesse_Meth_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Standby-for-Tinyfall-mk1" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/SFTF_mk1_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/LoogieSnail" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/Canto_LoogieSnail_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Petite-MIA" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/PetiteMIA_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/fffuuu-statue" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/fffuuu-statue_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Andy-Helms-Chell" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/AndyHelms_Chell_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Portal-2-s-P-body" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/Portals_Pbody_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Buggalope" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/Canto_Buggalope_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p>

<p class="imgC">
<a href="http://cargocollective.com/SLiD3/Mike-Mitchell-s-Treehugger" title="Pop Culture 3D Wallpapers by SLiD3">
<img src="http://imgs.abduzeedo.com/files/articles/pop-culture-3d-wallpapers-SLiD3/MikeMitchell_Treehugger_1920_1600.jpg" alt="Pop Culture 3D Wallpapers by SLiD3"/>
</a></p></div></div></div><div class="field field-name-field-tags field-type-taxonomy-term-reference field-label-above"><div class="field-label">Tags:&nbsp;</div><div class="field-items"><div class="field-item even"><a href="http://abduzeedo.com/tags/wallpaper">wallpaper</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/digital-art">digital art</a></div><div class="field-item even"><a href="http://abduzeedo.com/tags/computer-graphics">computer graphics</a></div><div class="field-item odd"><a href="http://abduzeedo.com/tags/3d">3d</a></div></div></div><ul class="links inline"><li class="blog_usernames_blog first last"><a href="http://abduzeedo.com/blog/paulogabriel" title="Read PauloGabriel&#039;s latest blog entries.">PauloGabriel&#039;s blog</a></li>
</ul><div class="feedflare">
<a href="http://feeds.feedburner.com/~ff/abduzeedo?a=OMvDzoMAo6s:nGw49_5m3aU:yIl2AUoC8zA"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=yIl2AUoC8zA" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=OMvDzoMAo6s:nGw49_5m3aU:F7zBnMyn0Lo"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=OMvDzoMAo6s:nGw49_5m3aU:F7zBnMyn0Lo" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=OMvDzoMAo6s:nGw49_5m3aU:-BTjWOF_DHI"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=OMvDzoMAo6s:nGw49_5m3aU:-BTjWOF_DHI" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=OMvDzoMAo6s:nGw49_5m3aU:qj6IDK7rITs"><img src="http://feeds.feedburner.com/~ff/abduzeedo?d=qj6IDK7rITs" border="0"></img></a> <a href="http://feeds.feedburner.com/~ff/abduzeedo?a=OMvDzoMAo6s:nGw49_5m3aU:gIN9vFwOqvQ"><img src="http://feeds.feedburner.com/~ff/abduzeedo?i=OMvDzoMAo6s:nGw49_5m3aU:gIN9vFwOqvQ" border="0"></img></a>
</div><img src="http://feeds.feedburner.com/~r/abduzeedo/~4/OMvDzoMAo6s" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 24 Sep 2014 11:30:43 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:29:"79819 at http://abduzeedo.com";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:12:"PauloGabriel";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:52:"http://abduzeedo.com/pop-culture-3d-wallpapers-slid3";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:2:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";s:4:"href";s:37:"http://feeds.feedburner.com/abduzeedo";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:2:{s:3:"rel";s:3:"hub";s:4:"href";s:32:"http://pubsubhubbub.appspot.com/";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:4:{s:4:"info";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:3:"uri";s:9:"abduzeedo";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:14:"emailServiceId";a:1:{i:0;a:5:{s:4:"data";s:9:"abduzeedo";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:18:"feedburnerHostname";a:1:{i:0;a:5:{s:4:"data";s:29:"https://feedburner.google.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}s:9:"feedFlare";a:9:{i:0;a:5:{s:4:"data";s:21:"Subscribe with Plusmo";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:75:"http://www.plusmo.com/add?url=http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:43:"http://plusmo.com/res/graphics/fbplusmo.gif";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:34:"Subscribe with The Free Dictionary";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:95:"http://www.thefreedictionary.com/_/hp/AddRSS.aspx?http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:48:"http://img.tfd.com/hp/addToTheFreeDictionary.gif";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:28:"Subscribe with Bitty Browser";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:107:"http://www.bitty.com/manual/?contenttype=rssfeed&contentvalue=http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:48:"http://www.bitty.com/img/bittychicklet_91x17.gif";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:23:"Subscribe with Live.com";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:70:"http://www.live.com/?add=http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:141:"http://tkfiles.storage.msn.com/x1piYkpqHC_35nIp1gLE68-wvzLZO8iXl_JMledmJQXP-XTBOLfmQv4zhj4MhcWEJh_GtoBIiAl1Mjh-ndp9k47If7hTaFno0mxW9_i3p_5qQw";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:25:"Subscribe with Excite MIX";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:78:"http://mix.excite.eu/add?feedurl=http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:42:"http://image.excite.co.uk/mix/addtomix.gif";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:21:"Subscribe with Webwag";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:83:"http://www.webwag.com/wwgthis.php?url=http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:40:"http://www.webwag.com/images/wwgthis.gif";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:28:"Subscribe with Podcast Ready";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:99:"http://www.podcastready.com/oneclick_bookmark.php?url=http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:58:"http://www.podcastready.com/images/podcastready_button.gif";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:7;a:5:{s:4:"data";s:20:"Subscribe with Wikio";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:80:"http://www.wikio.com/subscribe?url=http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:45:"http://www.wikio.com/shared/img/add2wikio.gif";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}i:8;a:5:{s:4:"data";s:29:"Subscribe with Daily Rotation";s:7:"attribs";a:1:{s:0:"";a:2:{s:4:"href";s:89:"http://www.dailyrotation.com/index.php?feed=http%3A%2F%2Ffeeds.feedburner.com%2Fabduzeedo";s:3:"src";s:40:"http://www.dailyrotation.com/rss-dr2.gif";}}s:8:"xml_base";s:21:"http://abduzeedo.com/";s:17:"xml_base_explicit";b:1;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:11:{s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:4:"etag";s:27:"LDHy7vUxaQmiy/B7HnoP40o7wic";s:13:"last-modified";s:29:"Fri, 26 Sep 2014 08:57:21 GMT";s:16:"content-encoding";s:4:"gzip";s:4:"date";s:29:"Fri, 26 Sep 2014 09:31:06 GMT";s:7:"expires";s:29:"Fri, 26 Sep 2014 09:31:06 GMT";s:13:"cache-control";s:18:"private, max-age=0";s:22:"x-content-type-options";s:7:"nosniff";s:16:"x-xss-protection";s:13:"1; mode=block";s:6:"server";s:3:"GSE";s:18:"alternate-protocol";s:15:"80:quic,p=0.002";}s:5:"build";s:14:"20121030095402";}