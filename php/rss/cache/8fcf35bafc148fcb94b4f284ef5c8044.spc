a:4:{s:5:"child";a:1:{s:0:"";a:1:{s:3:"rss";a:1:{i:0;a:6:{s:4:"data";s:3:"


";s:7:"attribs";a:1:{s:0:"";a:1:{s:7:"version";s:3:"2.0";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:1:{s:0:"";a:1:{s:7:"channel";a:1:{i:0;a:6:{s:4:"data";s:51:"
	
	
	
	
	
	
		
		
	
	
		
		
		
		
		
		
		
		
		
	";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:4:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:57:"Whitezine | Design Graphic &amp; Photography Inspirations";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:24:"http://www.whitezine.com";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:11:"Inspiration";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:13:"lastBuildDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Sep 2014 09:00:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"language";a:1:{i:0;a:5:{s:4:"data";s:5:"fr-FR";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:27:"http://wordpress.org/?v=4.0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"item";a:10:{i:0;a:6:{s:4:"data";s:57:"
		
		
		
		
		
				
		
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"John Duncan – Beautiful Scotland";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:68:"http://feedproxy.google.com/~r/Whitezinefr/~3/sK5mDX1LcIg/49925.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:54:"http://www.whitezine.com/fr/motion/49925.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Fri, 26 Sep 2014 09:00:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:6:{i:0;a:5:{s:4:"data";s:6:"Motion";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:18:"Beautiful Scotland";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:11:"John Duncan";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:8:"Scotland";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:11:"short movie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:9:"timelapse";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49925";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:382:"John Duncan est un réalisateur britannique basé à Edimbourg. Il a récemment fini une magnifique vidéo très inspirante intitulée Beautiful Scotland, une timelapse autour des lieux les plus incroyables des Highlands quand le soleil se lève à peine au petit matin. Découvrez tout le travail de John sur john-duncan.co.uk. Beautiful Scotland from John Duncan on Vimeo. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1133:"<p><strong>John Duncan</strong> est un réalisateur britannique basé à Edimbourg. Il a récemment fini une magnifique vidéo très inspirante intitulée <strong>Beautiful Scotland</strong>, une timelapse autour des lieux les plus incroyables des Highlands quand le soleil se lève à peine au petit matin. Découvrez tout le travail de John sur <a href="http://www.john-duncan.co.uk/">john-duncan.co.uk</a>.</p>
<p><iframe src="//player.vimeo.com/video/100426447?title=0&amp;byline=0&amp;portrait=0&amp;badge=0" width="620" height="361" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="http://vimeo.com/100426447">Beautiful Scotland</a> from <a href="http://vimeo.com/johnduncan">John Duncan</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/motion/49925.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/sK5mDX1LcIg" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:50:"http://www.whitezine.com/fr/motion/49925.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:45:"http://www.whitezine.com/fr/motion/49925.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:1;a:6:{s:4:"data";s:66:"
		
		
		
		
		
				
		
		
		
		
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:56:"Devinsupertramp – Robotic Dolphin and Flying Water Car";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:115:"http://feedproxy.google.com/~r/Whitezinefr/~3/QqOoGE75UNw/devinsupertramp-robotic-dolphin-and-flying-water-car.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:101:"http://www.whitezine.com/fr/motion/devinsupertramp-robotic-dolphin-and-flying-water-car.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 25 Sep 2014 13:00:03 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:9:{i:0;a:5:{s:4:"data";s:6:"Motion";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:17:"american director";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:12:"Devin Graham";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:15:"Devinsupertramp";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:9:"Jetovator";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:36:"Robotic Dolphin and Flying Water Car";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:11:"Seabreacher";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:7;a:5:{s:4:"data";s:6:"sports";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:8;a:5:{s:4:"data";s:12:"water sports";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49914";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:464:"Devin Graham alias Devinsupertramp est un jeune réalisateur américain. Pour sa dernière vidéo intitulée Robotic Dolphin and Flying Water Car, il a suivi une bande de jeunes qui s&#8217;amusent avec le dauphin robotique de Seabreacher la moto volante de Jetovator. Et maintenant, vous aussi vous voulez essayer ces engins, n&#8217;est-ce pas ? Découvrez les autres vidéos de Devinsupertramp sur youtube. Découvrez aussi la vidéo Behind the scenes : English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1085:"<p>Devin Graham alias <strong>Devinsupertramp</strong> est un jeune réalisateur américain. Pour sa dernière vidéo intitulée<strong> Robotic Dolphin and Flying Water Car</strong>, il a suivi une bande de jeunes qui s&rsquo;amusent avec le dauphin robotique de Seabreacher la moto volante de Jetovator. Et maintenant, vous aussi vous voulez essayer ces engins, n&rsquo;est-ce pas ? Découvrez les autres vidéos de Devinsupertramp sur <a href="https://www.youtube.com/channel/UCwgURKfUA7e0Z7_qE3TvBFQ">youtube</a>.</p>
<p>Découvrez aussi la vidéo Behind the scenes :<br />
<iframe width="580" height="335" src="//www.youtube.com/embed/0UEo0SRI8_s" frameborder="0" allowfullscreen></iframe>
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/motion/devinsupertramp-robotic-dolphin-and-flying-water-car.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/QqOoGE75UNw" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:97:"http://www.whitezine.com/fr/motion/devinsupertramp-robotic-dolphin-and-flying-water-car.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:92:"http://www.whitezine.com/fr/motion/devinsupertramp-robotic-dolphin-and-flying-water-car.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:2;a:6:{s:4:"data";s:57:"
		
		
		
		
		
				
		
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:75:"Ezequiel Galasso &amp; Gianfranco de Gennaro Gilmour – Skateboard Guitars";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:128:"http://feedproxy.google.com/~r/Whitezinefr/~3/Wnbq-PTKZr8/ezequiel-galasso-gianfranco-de-gennaro-gilmour-skateboard-guitars.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:114:"http://www.whitezine.com/fr/design/ezequiel-galasso-gianfranco-de-gennaro-gilmour-skateboard-guitars.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 25 Sep 2014 11:00:42 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:6:{i:0;a:5:{s:4:"data";s:6:"Design";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:16:"electric guitars";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:16:"Ezequiel Galasso";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:29:"Gianfranco de Gennaro Gilmour";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:10:"skateboard";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:18:"Skateboard Guitars";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49903";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:443:"L&#8217;artiste Ezequiel Galasso collabore avec le skater professionnel Gianfranco de Gennaro Gilmour pour créer des guitares électriques avec de vieilles planches de skate. Et le résultat est magnifiques. Il n&#8217;y a pas plus à dire, ils ont trouvé le meilleur moyen de recycler des planches de skate usés pour en faire de belles guitares et c&#8217;est tout. Allez faire un tour sur leur page facebook pour en commander une. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:2272:"<p>L&rsquo;artiste <strong>Ezequiel Galasso</strong> collabore avec le skater professionnel <strong>Gianfranco de Gennaro Gilmour</strong> pour créer des guitares électriques avec de vieilles planches de skate. Et le résultat est magnifiques. Il n&rsquo;y a pas plus à dire, ils ont trouvé le meilleur moyen de recycler des planches de skate usés pour en faire de belles guitares et c&rsquo;est tout. Allez faire un tour sur leur page <a href="https://www.facebook.com/sktgtr">facebook</a> pour en commander une.</p>
<p><img src="http://img.whitezine.com/Ezequiel-Galasso-Gianfranco-de-Gennaro-Gilmour-skateboard-guitar-2-580x580.jpg" alt="Ezequiel Galasso Gianfranco de Gennaro Gilmour skateboard guitar 2" width="580" height="580" class="aligncenter size-medium wp-image-49906" /></p>
<p><img src="http://img.whitezine.com/Ezequiel-Galasso-Gianfranco-de-Gennaro-Gilmour-skateboard-guitar-3-580x580.jpg" alt="Ezequiel Galasso Gianfranco de Gennaro Gilmour skateboard guitar 3" width="580" height="580" class="aligncenter size-medium wp-image-49907" /></p>
<p><img src="http://img.whitezine.com/Ezequiel-Galasso-Gianfranco-de-Gennaro-Gilmour-skateboard-guitar-4-580x580.jpg" alt="Ezequiel Galasso Gianfranco de Gennaro Gilmour skateboard guitar 4" width="580" height="580" class="aligncenter size-medium wp-image-49908" /></p>
<p><img src="http://img.whitezine.com/Ezequiel-Galasso-Gianfranco-de-Gennaro-Gilmour-skateboard-guitar-5-580x580.jpg" alt="Ezequiel Galasso Gianfranco de Gennaro Gilmour skateboard guitar 5" width="580" height="580" class="aligncenter size-medium wp-image-49909" /></p>
<p><img src="http://img.whitezine.com/Ezequiel-Galasso-Gianfranco-de-Gennaro-Gilmour-skateboard-guitar-6-580x870.jpg" alt="Ezequiel Galasso Gianfranco de Gennaro Gilmour skateboard guitar 6" width="580" height="870" class="aligncenter size-medium wp-image-49910" />
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/design/ezequiel-galasso-gianfranco-de-gennaro-gilmour-skateboard-guitars.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/Wnbq-PTKZr8" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:110:"http://www.whitezine.com/fr/design/ezequiel-galasso-gianfranco-de-gennaro-gilmour-skateboard-guitars.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:105:"http://www.whitezine.com/fr/design/ezequiel-galasso-gianfranco-de-gennaro-gilmour-skateboard-guitars.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:3;a:6:{s:4:"data";s:60:"
		
		
		
		
		
				
		
		
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:27:"Bibo X – My cat’s daily";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:83:"http://feedproxy.google.com/~r/Whitezinefr/~3/MN5H0b-YzcA/bibo-x-my-cats-daily.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:70:"http://www.whitezine.com/fr/graphic/bibo-x-my-cats-daily.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Thu, 25 Sep 2014 09:00:33 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:7:{i:0;a:5:{s:4:"data";s:7:"Graphic";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:6:"Bibo X";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:3:"cat";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:14:"chinese artist";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:6:"comics";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:14:"My cat's daily";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:8:"webcomic";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49895";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:393:"Bibo X est un illustrateur chinois spécialisé dans l&#8217;art digital et le dessin. Avec beaucoup d&#8217;humour, il dessine sa vie quotidienne d&#8217;artiste vivant avec des chats. Et comme tous les chats, les siens trouvent toujours le moyen de venir calmement l&#8217;ennuyer lorsqu&#8217;il veut travailler ou simplement se reposer. Découvrez tout son travail sur behance.net. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1189:"<p><strong>Bibo X</strong>  est un illustrateur chinois spécialisé dans l&rsquo;art digital et le dessin. Avec beaucoup d&rsquo;humour, il dessine sa vie quotidienne d&rsquo;artiste vivant avec des chats. Et comme tous les chats, les siens trouvent toujours le moyen de venir calmement l&rsquo;ennuyer lorsqu&rsquo;il veut travailler ou simplement se reposer. Découvrez tout son travail sur <a href="https://www.behance.net/biboX">behance.net</a>.</p>
<p><img src="http://img.whitezine.com/bibo-X-my-cats-daily--428x2000.jpg" alt="bibo X  my cat&#039;s daily" width="428" height="2000" class="aligncenter size-medium wp-image-49897" /></p>
<p><img src="http://img.whitezine.com/bibo-X-my-cats-daily-2-428x2000.jpg" alt="bibo X  my cat&#039;s daily 2" width="428" height="2000" class="aligncenter size-medium wp-image-49898" />
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/graphic/bibo-x-my-cats-daily.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/MN5H0b-YzcA" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:66:"http://www.whitezine.com/fr/graphic/bibo-x-my-cats-daily.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:61:"http://www.whitezine.com/fr/graphic/bibo-x-my-cats-daily.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:4;a:6:{s:4:"data";s:60:"
		
		
		
		
		
				
		
		
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:28:"Volvic – Badass Commercial";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:87:"http://feedproxy.google.com/~r/Whitezinefr/~3/CrBz5pfneP0/volvic-badass-commercial.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:73:"http://www.whitezine.com/fr/motion/volvic-badass-commercial.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 24 Sep 2014 16:00:31 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:7:{i:0;a:5:{s:4:"data";s:6:"Motion";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:10:"commercial";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:4:"Iron";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:11:"short movie";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:6:"Volvic";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:5:"water";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:7:"woodkid";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49888";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:370:"Volvic vient juste de diffuser une magnifique publicité vidéo produite par Gang Film. L&#8217;eau minérale a réalisé un court métrage comprenant des dinosaures, des guerriers en plein combat et bien sur des volcans. Appuyé par la musique Iron de Woodkid, cette pub est tout simplement épique. Découvrez les autres projets de gang film sur gangfilms.com. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:802:"<p><strong>Volvic</strong> vient juste de diffuser une magnifique publicité vidéo produite par<strong> Gang Film</strong>. L&rsquo;eau minérale a réalisé un court métrage comprenant des dinosaures, des guerriers en plein combat et bien sur des volcans. Appuyé par la musique Iron de Woodkid, cette pub est tout simplement épique. Découvrez les autres projets de gang film sur <a href="http://www.gangfilms.com/">gangfilms.com</a>.
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/motion/volvic-badass-commercial.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/CrBz5pfneP0" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:69:"http://www.whitezine.com/fr/motion/volvic-badass-commercial.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:64:"http://www.whitezine.com/fr/motion/volvic-badass-commercial.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:5;a:6:{s:4:"data";s:63:"
		
		
		
		
		
				
		
		
		
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:25:"Jason Paul – Arcade Run";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:104:"http://feedproxy.google.com/~r/Whitezinefr/~3/O1Fs7zoP3bM/jason-paul-arcade-run-freerunning-in-8bit.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:90:"http://www.whitezine.com/fr/motion/jason-paul-arcade-run-freerunning-in-8bit.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 24 Sep 2014 11:00:44 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:8:{i:0;a:5:{s:4:"data";s:6:"Motion";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:2:"2D";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:6:"arcade";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:7:"freerun";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:10:"Jason Paul";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:7:"parkour";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:8:"red bull";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:7;a:5:{s:4:"data";s:11:"video games";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49882";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:369:"Jason Paul est Team Farang se sont associés pour crée un superbe parcours de freerun 2D rendant hommage aux jeux vidéos d&#8217;arcade des années 80-90. Tout le poarcous est placé sur un train et Jason doit finir les différents niveaux et battre le boss pour gagner. Ce court métrage est sponsorisé par Red Bull, découvrez tout leur projet sur youtube. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:849:"<p><strong>Jason Paul</strong> est <strong>Team Farang</strong> se sont associés pour crée un superbe parcours de freerun 2D rendant hommage aux jeux vidéos d&rsquo;arcade des années 80-90. Tout le poarcous est placé sur un train et Jason doit finir les différents niveaux et battre le boss pour gagner. Ce court métrage est sponsorisé par Red Bull, découvrez tout leur projet sur <a href="https://www.youtube.com/channel/UCblfuW_4rakIf2h6aqANefA">youtube</a>.
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/motion/jason-paul-arcade-run-freerunning-in-8bit.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/O1Fs7zoP3bM" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:86:"http://www.whitezine.com/fr/motion/jason-paul-arcade-run-freerunning-in-8bit.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:81:"http://www.whitezine.com/fr/motion/jason-paul-arcade-run-freerunning-in-8bit.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:6;a:6:{s:4:"data";s:60:"
		
		
		
		
		
				
		
		
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:26:"DB Burkeman – Star Warps";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:85:"http://feedproxy.google.com/~r/Whitezinefr/~3/SNO9P-zzzis/dk-burkeman-star-warps.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:76:"http://www.whitezine.com/fr/inspiration/dk-burkeman-star-warps.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Wed, 24 Sep 2014 09:00:28 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:7:{i:0;a:5:{s:4:"data";s:11:"Inspiration";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:7:"artwork";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:11:"DK Burkeman";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:7:"fan art";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:11:"online book";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:10:"Star Warps";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:6;a:5:{s:4:"data";s:9:"star wars";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49872";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:380:"DB Burkeman ne se définit pas comme un véritable fan de la saga Star Wars mais il a crée une étrange connection entre notre monde et cette galaxie très éloignée. Après plusieurs magnifiques fan arts, il décida de créer un livre en ligne intitulé Star Warps reprenant tous les travaux inspiré de Star Wars à travers le monde. Jetez-y un oeil sur starwarps.com. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1639:"<p><strong>DB Burkeman</strong> ne se définit pas comme un véritable fan de la saga Star Wars mais il a crée une étrange connection entre notre monde et cette galaxie très éloignée. Après plusieurs magnifiques fan arts, il décida de créer un livre en ligne intitulé <strong>Star Warps</strong> reprenant tous les travaux inspiré de Star Wars à travers le monde. Jetez-y un oeil sur <a href="http://www.starwarps.com/">starwarps.com</a>.</p>
<p><img src="http://img.whitezine.com/DB-Burkeman-star-wars-star-warps-2-580x386.jpg" alt="DB Burkeman star wars star warps 2" width="580" height="386" class="aligncenter size-medium wp-image-49875" /></p>
<p><img src="http://img.whitezine.com/DB-Burkeman-star-wars-star-warps-3-580x386.jpg" alt="DB Burkeman star wars star warps 3" width="580" height="386" class="aligncenter size-medium wp-image-49876" /></p>
<p><img src="http://img.whitezine.com/DB-Burkeman-star-wars-star-warps-4-580x386.jpg" alt="DB Burkeman star wars star warps 4" width="580" height="386" class="aligncenter size-medium wp-image-49877" /></p>
<p><img src="http://img.whitezine.com/DB-Burkeman-star-wars-star-warps-5-580x386.jpg" alt="DB Burkeman star wars star warps 5" width="580" height="386" class="aligncenter size-medium wp-image-49878" />
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/inspiration/dk-burkeman-star-warps.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/SNO9P-zzzis" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:72:"http://www.whitezine.com/fr/inspiration/dk-burkeman-star-warps.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:67:"http://www.whitezine.com/fr/inspiration/dk-burkeman-star-warps.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:7;a:6:{s:4:"data";s:57:"
		
		
		
		
		
				
		
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:47:"Sandro Miller – Malkovich Malkovich Malkovich";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:106:"http://feedproxy.google.com/~r/Whitezinefr/~3/8ZBDHDtYTgU/sandro-miller-malkovich-malkovich-malkovich.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:97:"http://www.whitezine.com/fr/photography/sandro-miller-malkovich-malkovich-malkovich.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 23 Sep 2014 14:00:41 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:6:{i:0;a:5:{s:4:"data";s:11:"Photography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:21:"american photographer";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:14:"John Malkovich";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:29:"Malkovich Malkovich Malkovich";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:11:"portraiture";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:5;a:5:{s:4:"data";s:13:"Sandro Miller";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49822";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:361:"Sandro Miller est un photographe américain spécialisé dans le portrait, la mode et la photographie lifestyle. Son dernier projet simplement appelé Malkovich Malkovich Malkovich est génial. Sandro a repris de célèbres clichés pour les recréer avec John Malkovich et le résultat est juste superbe. Découvrez tout son travail sur sandrofilm.com. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:4206:"<p><strong>Sandro Miller</strong> est un photographe américain  spécialisé dans le portrait, la mode et la photographie lifestyle. Son dernier projet simplement appelé <strong>Malkovich Malkovich Malkovich</strong> est génial. Sandro a repris de célèbres clichés pour les recréer avec John Malkovich et le résultat est juste superbe. Découvrez tout son travail sur <a href="http://www.sandrofilm.com/main/index.php">sandrofilm.com</a>.</p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch--580x736.jpg" alt="Sandro Miller John malkovitch" width="580" height="736" class="aligncenter size-medium wp-image-49824" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-ali-580x733.jpg" alt="Sandro Miller John malkovitch ali" width="580" height="733" class="aligncenter size-medium wp-image-49825" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-beard-580x725.jpg" alt="Sandro Miller John malkovitch beard" width="580" height="725" class="aligncenter size-medium wp-image-49826" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-che-guevara-580x773.jpg" alt="Sandro Miller John malkovitch che guevara" width="580" height="773" class="aligncenter size-medium wp-image-49827" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-dali-580x787.jpg" alt="Sandro Miller John malkovitch dali" width="580" height="787" class="aligncenter size-medium wp-image-49828" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-david-bowie-580x580.jpg" alt="Sandro Miller John malkovitch david bowie" width="580" height="580" class="aligncenter size-medium wp-image-49829" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-davis-580x580.jpg" alt="Sandro Miller John malkovitch davis" width="580" height="580" class="aligncenter size-medium wp-image-49830" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-einstein-580x580.jpg" alt="Sandro Miller John malkovitch einstein" width="580" height="580" class="aligncenter size-medium wp-image-49831" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-jagger-580x580.jpg" alt="Sandro Miller John malkovitch jagger" width="580" height="580" class="aligncenter size-medium wp-image-49832" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-joker-580x773.jpg" alt="Sandro Miller John malkovitch joker" width="580" height="773" class="aligncenter size-medium wp-image-49833" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-JP-gauthier-580x758.jpg" alt="Sandro Miller John malkovitch JP gauthier" width="580" height="758" class="aligncenter size-medium wp-image-49834" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-marilyn-580x544.jpg" alt="Sandro Miller John malkovitch marilyn" width="580" height="544" class="aligncenter size-medium wp-image-49835" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-migrant-580x750.jpg" alt="Sandro Miller John malkovitch migrant" width="580" height="750" class="aligncenter size-medium wp-image-49836" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-truman-capote-580x773.jpg" alt="Sandro Miller John malkovitch truman capote" width="580" height="773" class="aligncenter size-medium wp-image-49837" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-twin-580x633.jpg" alt="Sandro Miller John malkovitch twin" width="580" height="633" class="aligncenter size-medium wp-image-49838" /></p>
<p><img src="http://img.whitezine.com/Sandro-Miller-John-malkovitch-usa-580x745.jpg" alt="Sandro Miller John malkovitch usa" width="580" height="745" class="aligncenter size-medium wp-image-49839" />
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/photography/sandro-miller-malkovich-malkovich-malkovich.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/8ZBDHDtYTgU" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:93:"http://www.whitezine.com/fr/photography/sandro-miller-malkovich-malkovich-malkovich.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:88:"http://www.whitezine.com/fr/photography/sandro-miller-malkovich-malkovich-malkovich.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:8;a:6:{s:4:"data";s:54:"
		
		
		
		
		
				
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:42:"Sarah Illenberger – Ice Cream Sculptures";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:101:"http://feedproxy.google.com/~r/Whitezinefr/~3/GDe6mpYJ5zM/sarah-illenberger-ice-cream-sculptures.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:92:"http://www.whitezine.com/fr/photography/sarah-illenberger-ice-cream-sculptures.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 23 Sep 2014 11:00:19 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:5:{i:0;a:5:{s:4:"data";s:11:"Photography";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:13:"german artist";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:19:"german photographer";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:20:"Ice Cream Sculptures";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:17:"Sarah Illenberger";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49800";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:429:"Sarah Illenberger est une artiste multi-disciplinaire allemande basée à Berlin. Elle concentre son travail sur les objets de la vie quotidienne les transformant en de véritables oeuvres d&#8217;arts. Cette fois, elle a utilisé de la glace pour créer de superbes sculptures, des photographies puissantes donnant un sens presque poétiques à la crème glacée. Découvrez le travail de Sarah sur sarahillenberger.com. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1742:"<p><strong>Sarah Illenberger</strong> est une artiste multi-disciplinaire allemande basée à Berlin. Elle concentre son travail sur les objets de la vie quotidienne les transformant en de véritables oeuvres d&rsquo;arts. Cette fois, elle a utilisé de la glace pour créer de superbes sculptures, des photographies puissantes donnant un sens presque poétiques à la crème glacée. Découvrez le travail de Sarah sur <a href="http://www.sarahillenberger.com/">sarahillenberger.com</a>.</p>
<p><img src="http://img.whitezine.com/Sarah-Illenberger-ice-cream-sculptures-2-580x773.jpg" alt="Sarah Illenberger ice cream sculptures 2" width="580" height="773" class="aligncenter size-medium wp-image-49803" /></p>
<p><img src="http://img.whitezine.com/Sarah-Illenberger-ice-cream-sculptures-3-580x773.jpg" alt="Sarah Illenberger ice cream sculptures 3" width="580" height="773" class="aligncenter size-medium wp-image-49804" /></p>
<p><img src="http://img.whitezine.com/Sarah-Illenberger-ice-cream-sculptures-4-580x773.jpg" alt="Sarah Illenberger ice cream sculptures 4" width="580" height="773" class="aligncenter size-medium wp-image-49805" /></p>
<p><img src="http://img.whitezine.com/Sarah-Illenberger-ice-cream-sculptures-5-580x782.jpg" alt="Sarah Illenberger ice cream sculptures 5" width="580" height="782" class="aligncenter size-medium wp-image-49806" />
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/photography/sarah-illenberger-ice-cream-sculptures.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/GDe6mpYJ5zM" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:88:"http://www.whitezine.com/fr/photography/sarah-illenberger-ice-cream-sculptures.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:83:"http://www.whitezine.com/fr/photography/sarah-illenberger-ice-cream-sculptures.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}i:9;a:6:{s:4:"data";s:54:"
		
		
		
		
		
				
		
		
		
		

		
		
				
			
		
		";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";s:5:"child";a:6:{s:0:"";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:9:"Os Gemeos";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:72:"http://feedproxy.google.com/~r/Whitezinefr/~3/s5F809Pvrco/os-gemeos.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:59:"http://www.whitezine.com/fr/graphic/os-gemeos.html#comments";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:7:"pubDate";a:1:{i:0;a:5:{s:4:"data";s:31:"Tue, 23 Sep 2014 09:00:26 +0000";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:8:"category";a:5:{i:0;a:5:{s:4:"data";s:7:"Graphic";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:16:"brazilian artist";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:2;a:5:{s:4:"data";s:9:"Os Gemeos";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:3;a:5:{s:4:"data";s:10:"street art";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:4;a:5:{s:4:"data";s:13:"wall painting";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:4:"guid";a:1:{i:0;a:5:{s:4:"data";s:36:"http://www.whitezine.com/en/?p=49787";s:7:"attribs";a:1:{s:0:"";a:1:{s:11:"isPermaLink";s:5:"false";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:11:"description";a:1:{i:0;a:5:{s:4:"data";s:321:"Gustavo et Otavio Pandolfo sont Os Gemeos ce qui signifie les Jumeaux. Ils sont deux street artistes brésiliens très talentueux. Ils créent d&#8217;impressionnantes et gigantesques fresques sur les murs, peignant des géants très colorés à travers le monde. Découvrez tout leur travail sur osgemeos.com.br. English";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:32:"http://purl.org/dc/elements/1.1/";a:1:{s:7:"creator";a:1:{i:0;a:5:{s:4:"data";s:6:"Thomas";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:40:"http://purl.org/rss/1.0/modules/content/";a:1:{s:7:"encoded";a:1:{i:0;a:5:{s:4:"data";s:1775:"<p>Gustavo et Otavio Pandolfo sont <strong>Os Gemeos</strong> ce qui signifie les Jumeaux. Ils sont deux street artistes brésiliens très talentueux. Ils créent d&rsquo;impressionnantes et gigantesques fresques sur les murs, peignant des géants très colorés à travers le monde. Découvrez tout leur travail sur <a href="http://www.osgemeos.com.br/en">osgemeos.com.br</a>.</p>
<p><img src="http://img.whitezine.com/Os-Gemeos-4.jpg" alt="Os Gemeos 4" width="450" height="450" class="aligncenter size-full wp-image-49790" /></p>
<p><img src="http://img.whitezine.com/Os-Gemeos-5.jpg" alt="Os Gemeos 5" width="450" height="313" class="aligncenter size-full wp-image-49791" /></p>
<p><img src="http://img.whitezine.com/Os-Gemeos-6.jpg" alt="Os Gemeos 6" width="450" height="600" class="aligncenter size-full wp-image-49792" /></p>
<p><img src="http://img.whitezine.com/Os-Gemeos-7-.jpeg" alt="Os Gemeos 7" width="450" height="507" class="aligncenter size-full wp-image-49793" /></p>
<p><img src="http://img.whitezine.com/Os-Gemeos-8-.jpg" alt="Os Gemeos 8" width="450" height="300" class="aligncenter size-full wp-image-49794" /></p>
<p><img src="http://img.whitezine.com/Os-Gemeos2.jpg" alt="Os Gemeos2" width="450" height="450" class="aligncenter size-full wp-image-49795" /></p>
<p><img src="http://img.whitezine.com/Os-Gemeos3.jpg" alt="Os Gemeos3" width="450" height="301" class="aligncenter size-full wp-image-49796" />
<ul class="lang_switch">
<li class="lang_switch"><a href="http://www.whitezine.com/en/graphic/os-gemeos.html"><img src="http://www.whitezine.com/wp-content/plugins/zdmultilang/flags/en_US.png" alt="English" title="English" border="0">English</a></li>
</ul>
<img src="http://feeds.feedburner.com/~r/Whitezinefr/~4/s5F809Pvrco" height="1" width="1"/>";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:36:"http://wellformedweb.org/CommentAPI/";a:1:{s:10:"commentRss";a:1:{i:0;a:5:{s:4:"data";s:55:"http://www.whitezine.com/fr/graphic/os-gemeos.html/feed";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:38:"http://purl.org/rss/1.0/modules/slash/";a:1:{s:8:"comments";a:1:{i:0;a:5:{s:4:"data";s:1:"0";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:8:"origLink";a:1:{i:0;a:5:{s:4:"data";s:50:"http://www.whitezine.com/fr/graphic/os-gemeos.html";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}s:44:"http://purl.org/rss/1.0/modules/syndication/";a:2:{s:12:"updatePeriod";a:1:{i:0;a:5:{s:4:"data";s:6:"hourly";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}s:15:"updateFrequency";a:1:{i:0;a:5:{s:4:"data";s:1:"1";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"link";a:2:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"rel";s:4:"self";s:4:"type";s:19:"application/rss+xml";s:4:"href";s:39:"http://feeds.feedburner.com/Whitezinefr";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}i:1;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:2:{s:3:"rel";s:3:"hub";s:4:"href";s:32:"http://pubsubhubbub.appspot.com/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}s:42:"http://rssnamespace.org/feedburner/ext/1.0";a:1:{s:4:"info";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:1:{s:3:"uri";s:11:"whitezinefr";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:0:"";}}}}}}}}}}}}s:4:"type";i:128;s:7:"headers";a:11:{s:12:"content-type";s:23:"text/xml; charset=UTF-8";s:4:"etag";s:27:"RFgLyhvz0MS5i8iYTBHUrMM/iuc";s:13:"last-modified";s:29:"Fri, 26 Sep 2014 09:16:39 GMT";s:16:"content-encoding";s:4:"gzip";s:4:"date";s:29:"Fri, 26 Sep 2014 09:31:06 GMT";s:7:"expires";s:29:"Fri, 26 Sep 2014 09:31:06 GMT";s:13:"cache-control";s:18:"private, max-age=0";s:22:"x-content-type-options";s:7:"nosniff";s:16:"x-xss-protection";s:13:"1; mode=block";s:6:"server";s:3:"GSE";s:18:"alternate-protocol";s:15:"80:quic,p=0.002";}s:5:"build";s:14:"20121030095402";}