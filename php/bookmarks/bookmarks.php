<?php
	include_once '../.././db/database.php';
?>

<div id="section_bookmarks">
	<!--generate menu - can't use below loop without messing up structure-->
	<div id="bookmark_menu">
		<?php foreach($book_cats as $row){ 
			$book_id = $row['ID'];

			if(strpos($row['Title'], " ") > 0){
				$titleStr = substr($row['Title'], 0, strpos($row['Title'], " "));
			}else{$titleStr = $row['Title'];}
			?>
			<div class="bookMenu_<?php echo $book_id;?>">
				<a href="#" data-link="<?php echo $book_id;?>"><?php echo $titleStr;?></a>

				<div id="section_book_<?php echo $book_id; ?>">
					<ul>
						<?php foreach($dbh->query("SELECT 
							Bookmarks.Title,
							Bookmarks.URL,
							Bookmarks.ClassID,
							Bookmarks.ID
							From Bookmarks WHERE Bookmarks.ClassID = $book_id", PDO::FETCH_ASSOC) as $row2){ ?>
						<li>
							<div><a class="external" data-location="default" href="<?php echo $row2['URL']; ?>" data-ID="<?php echo $row2['ID']; ?>" data-Title="<?php echo $row2['Title']; ?>"><?php echo $row2['Title']; ?></a></div>

							<button title="Edit Bookmark" type="button" data-button="2" class="edit_bookmark" onclick="formSubmission.revealForm(this)">e</button>	
							<button title="Delete Bookmark" type="button" class="delete_bookmark" onclick="formSubmission.deleteInlineForm(this)">x</button>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		<?php } ?>	
	</div>

	<form id="delete_bookmark_form" name="delete_bookmark_form" action="./db/bookmark_actions.php" method="post">
		<input type="hidden" name="delete_book_ID" value="">
		<input type="hidden" name="bookmarksDecision" value="delete">
		<input type="submit" name="submit" value="delete">
	</form>
</div>

<div id="bookmark_content">
	<header id="book_cont_header">
		<button title="Show Main Menu" id="revealMenu" onclick="header.revealMenu()" class="mainMenu"><?php include("../../images/buttons/hamburger.svg"); ?></button>
		<button title="Add Bookmark" onclick="bookmarks.bookmarkAdd(this)" class="bookmark"><?php include("../../images/buttons/bookmark.svg"); ?></button>
		<input type="text" name="searchURL" value="http://">
		<button title="Go To Link" onclick="header.iframeChangeLocation(this)" class="search"><?php include("../../images/buttons/search.svg"); ?></button>
		<button title="Close Window" onclick="header.close()" class="close"><?php include("../../images/buttons/delete.svg"); ?></button>

	</header>
	<div id="content">
		<iframe src="about:blank" id="contentFrame" sandbox="allow-forms allow-scripts allow-same-origin"></iframe>
	</div>
</div>

<script>
formSubmission.submitForms('delete_bookmark_form', true);
header.loadExternalLinks('#section_bookmarks');
bookmarks.bookMenu();
</script>
