<?php 
ini_set('display_errors',1);
error_reporting(E_ALL);
?>

<header id="mainHeader">
	<h1 id="logo">
		<?php include("./images/logo/design_logo.svg");?>	
	</h1>
	<nav id="index">
		<div id="stack">
			<a class='navigation' href="./php/stacks.php">Design</a>
		</div>
		<div id="rss">
			<a data-feedCount="0" class='navigation' href="./php/rss/readFeed.php">Feeds</a>
		</div>
		<div id="bookmarks">
			<a class='navigation' href="./php/bookmarks/bookmarks.php">Bookmarks</a>
		</div>

		<div id="globalForms">

			<div class="bookmarks">

				<button title="Add Bookmark(Shortcut: b-o-o-k)" class="add_bookmark" data-button="1" onclick="formSubmission.revealForm(this)"><?php include("./images/buttons/book.svg"); ?></button>

				<form id="add_bookmark_form" name="add_bookmark_form" class="headerForm" action="./db/bookmark_actions.php" method="post">
		            <select name="bookCat">
		            	<?php foreach($book_cats as $bc){
		            		echo '<option data-catColour="bookMenu_'.$bc['ID'].'" value = "'.$bc['ID'].'">'.$bc['Title'].'</option>';
		            	} ?>
		            </select>
		            <p>Add Bookmark</p>
		            <a class="selectList" href="#">Select Category…</a>

					<input type="text" name="title" placeholder="Title…" required>
					<input type="url" name="url" placeholder="URL…" required>
					<input type="hidden" name="bookmarksDecision" value="add">
					<button type="submit" name="edit_submit"><?php include("./images/buttons/submit.svg"); ?></button>
					<button type="button" name="cancel_submit" data-cancel="global" onclick="formSubmission.closeForm(this)"><?php include("./images/buttons/delete.svg"); ?></button>
				</form>
			</div>

			<div class="models">

				<button title="Add Files(Shortcut: u-p)" class="add_file" data-button="1" onclick="formSubmission.revealForm(this)"><?php include("./images/buttons/file.svg"); ?></button>

				<form id="add_file_form" class="headerForm" name="add_file_form" enctype="multipart/form-data" action="./db/photo_actions.php" method="POST">
				    <input type="hidden" name="MAX_FILE_SIZE" value="5242880000" />
				    
				    <select name="add_model_ID">
				    	<?php foreach($modelsList as $ml){ ?>
				    	<option data-catSort="<?php echo $ml['catTitle']?>" value = "<?php echo $ml['ID'];?>"><?php echo $ml['Title']; ?>
				    	</option>
				    	<?php } ?>
				    </select>
				    <p>Add Files</p>
				    <a class="selectList" href="#">Select Subsection…</a>

					<input id="fileUpload" type="file" multiple="true" name="files[]" accept="video/mp4, video/m4v, image/jpeg, image/jpg, image/gif, image/png, image/tiff">
					<input type="hidden" name="photoDecision" value="add">
					<button type="submit" name="edit_submit"><?php include("./images/buttons/submit.svg"); ?></button>
					<button type="button" data-cancel="global" name="cancel_submit" onclick="formSubmission.closeForm(this)"><?php include("./images/buttons/delete.svg"); ?></button>
				</form>
			</div>

			<div class="add_models">

				<button title="Add New Project(Shortcut: n-e-w)" class="add_model" data-button="1" onclick="formSubmission.revealForm(this)"><?php include("./images/buttons/add.svg"); ?></button>

				<form id="add_model_form" class="headerForm" name="add_model_form" action="./db/model_actions.php" method="post">
			        <select name="add_model_cat">
			        	<?php foreach($categoriesArray as $ca){ ?>
			        	<option data-catColour="<?php echo $ca['Title']; ?>" value = "<?php echo $ca['ID']; ?>">
			        	<?php echo $ca['Title']; ?></option>
			        	<?php } ?>
			        </select>
			        <p>Add Design Project</p>
			        <a class="selectList" href="#">Select Category…</a>

			        <input type="text" name="add_model_title" placeholder="Enter Name…" required>
			        <input type="hidden" name="modelDecision" value="add">
					<button type="submit" name="edit_submit"><?php include("./images/buttons/submit.svg"); ?></button>
					<button type="button" data-cancel="global" name="cancel_submit" onclick="formSubmission.closeForm(this)"><?php include("./images/buttons/delete.svg"); ?></button>
				</form>
			</div>
			<!-- forms moved here due to styling nightmares-->
			<form id="edit_model_form" class="headerForm" name="edit_model_form" action="./db/model_actions.php" method="post">
			    <select name= "edit_model_cat">
			    	<?php foreach($categoriesArray as $cArr){ ?>
			        	<option data-catColour="<?php echo $cArr['Title']; ?>" value="<?php echo $cArr['ID']; ?>">
			        	<?php echo $cArr['Title']; ?></option>
		        	<?php } ?>
			    </select>
			    <p>Edit Project</p>
			    <a class="selectList" href="#">Select Category…</a>
			    
			    <input type="text" name="edit_model_title" value="" required>
			    <input type="hidden" name="edit_model_ID"value="">
			    <input type="hidden" name="modelDecision" value="edit">
				<button type="submit" name="edit_submit"><?php include("./images/buttons/submit.svg"); ?></button>
				<button type="button"  data-cancel="midForm" name="cancel_submit" onclick="formSubmission.closeForm(this)"><?php include("./images/buttons/delete.svg"); ?></button>
			</form>
			<form id="edit_bookmark_form" class="headerForm" name="edit_bookmark_form" action="./db/bookmark_actions.php" method="post">
				<p>Edit Bookmark</p>
				<select class="edit_bookCat" name="edit_bookCat">
				<?php foreach($book_cats as $bCa){
            		echo '<option data-catColour="bookMenu_'.$bCa['ID'].'" value = "'.$bCa['ID'].'">'.$bCa['Title'].'</option>';
            	} ?>
				</select>

				<a class="selectList" href="#">Select Category…</a>

				<input type="hidden" name="edit_book_ID" value="" required>
				<input type="text" name="edit_title" value="" required>
				<input type="url" name="edit_link" value="" required>
				<input type="hidden" name="bookmarksDecision" value="edit">
				<button type="submit" name="edit_submit"><?php include("./images/buttons/submit.svg"); ?></button>
				<button type="button" name="cancel_submit" data-cancel="midForm" onclick="formSubmission.closeForm(this)"><?php include("./images/buttons/delete.svg"); ?></button>
			</form>
		</div>
		<span id="tempFiles"><a class="navigation" href="./php/tempDir.php?getDir=../temp">Temp Files</a></span>
	</nav>
</header>