<?php
date_default_timezone_set('UTC');

$in = htmlspecialchars_decode($_GET['linkage']);
$inThumb = htmlspecialchars_decode($_GET['linkThumb']);
$acceptNewThumb = htmlspecialchars($_GET['descision']);

$ffmpeg = '/usr/local/Cellar/ffmpeg/2.2.2/bin/ffmpeg';
$ffprobe = '/usr/local/Cellar/ffmpeg/2.2.2/bin/ffprobe';

$tempFolder = '/Library/WebServer/Documents/Design/temp/';

$inFull = '/Library/WebServer/Documents/Design/' . substr($in, 2);
$inFullescapedSpace = str_replace(" ", "\ ", $inFull);

$inThumbFull = '/Library/WebServer/Documents/Design/' . substr($inThumb, 2);

$command = "$ffprobe -v quiet -show_format -print_format flat $inFullescapedSpace";

$size = '640x360'; 

exec ($command, $results);

foreach ($results as $key=>$value){
	if(strpos($value, "duration")){
		$duration = $value;
	}
}

$truncatePos = strpos($duration, '"');

$truncate = substr($duration, $truncatePos+1, -1);

$finalNumber = round($truncate);

$startingPoint = round($finalNumber/21);

$seconds = date('u');


if($acceptNewThumb == 'false'){ ?>
	<div id="videoThumbnailselect"> 
	<button onclick="videoThumbs.cancelAndClose(this);" id="close_crop"><?php include("../images/buttons/delete.svg"); ?></button>

	<?php

	for($i = 1; $i < 22; $i++){
		$interval = $startingPoint * $i;
		$output = $tempFolder .'thumb-'.$i.'.jpeg';

		$cmd = "$ffmpeg -y -ss $interval -i $inFullescapedSpace -s $size -an -vframes 1 -f image2 $output";

		exec($cmd); ?>

		<div><a href="#"><img data-original="./temp/Videos/*Thumbnails/thumb-<?php echo $i;?>.jpeg" src="./temp/Videos/*Thumbnails/thumb-<?php echo $i;?>.jpeg?<?php echo $seconds;?>" /></a></div>

	<?php } ?>

	</div>
<?php } elseif($acceptNewThumb == 'true'){
	$newThumb = htmlspecialchars_decode($_GET['newThumb']);

	$finalNewThumb = '/Library/WebServer/Documents/Design/' . substr($newThumb, 2);

	rename($finalNewThumb, $inThumbFull);
	echo 'success';

} 

?>




