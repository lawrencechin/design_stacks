<?php

ini_set('display_errors',1);
error_reporting(E_ALL);

function videoThumbs($in, $out) {
	$command = FFPROBE. " -v quiet -show_format -print_format flat $in";

	exec($command, $results);

	foreach ($results as $key=>$value){
		if(strpos($value, "duration")){
			$duration = $value;
		}
	}

	$truncatePos = strpos($duration, '"');

	$truncate = substr($duration, $truncatePos+1, -1);

	$finalNumber = round($truncate);

	$interval = rand(0, $finalNumber); 
	  
	//screenshot size  
	$size = VIDSIZE;  
	
	$cmd = FFMPEG. " -ss $interval -i $in -s $size -an -vframes 1 -f image2 $out";

	exec($cmd);	
}

function makeThumbnail($in, $out) {
    $width = THUMB_WIDTH;
    $height = THUMB_HEIGHT;
    list($w,$h) = getimagesize($in);

    $thumbRatio = $width/$height;
    $inRatio = $w/$h;
    $isLandscape = $inRatio > $thumbRatio;

    $size = ($isLandscape ? '1000x'.$height : $width.'x1000');
    $xoff = ($isLandscape ? floor((($inRatio*$height)-$width)/2) : 0);
    $in = $in . '[0]';
    

    $command = MAGICK_PATH."convert $in -resize $size -crop {$width}x{$height}+{$xoff}+0 ".
        "-quality 85 $out";

    exec($command);

}

$crop = array_key_exists('crop_cor', $_POST) ? $_POST['crop_cor'] : null;

if(isset($crop) && $crop == 'crop'){
  generateModelThumb();
}elseif(isset($crop) && $crop == 'replace'){
	replaceAndCopy();
}
//need to fix with new naming/location schemes
function generateModelThumb(){
	include '../../db/database.php';
	$cropX = htmlspecialchars($_POST['x_cor']);
	$cropY = htmlspecialchars($_POST['y_cor']);
	$cropW = htmlspecialchars($_POST['w_cor']);
	$cropH = htmlspecialchars($_POST['h_cor']);
	$currentImage = htmlspecialchars($_POST['image_cor']);

	$in = ROOT.PAGEURL.substr($currentImage, 1);

	$out = ROOT.PAGEURL.THUMBDIR.'/crop/new_thumb.jpg';

	$command = MAGICK_PATH."convert $in -quality 85 -crop {$cropW}x{$cropH}+{$cropX}+{$cropY} -resize 200x250 -format jpg $out";
    exec($command);
    echo json_encode(array('msg'=>'true', 'value'=>'./content/thumb/crop/new_thumb.jpg'));

}
//need to fix with new naming/location schemes
function replaceAndCopy(){
	include '../../db/database.php';
	$decision = htmlspecialchars($_POST['decision_cor']);
	$modelID = htmlspecialchars($_POST['modelID']);

	$currentImage = ROOT.PAGEURL.THUMBDIR."/$modelID-thumb.jpg";
	$newImage = ROOT.PAGEURL.THUMBDIR."/crop/new_thumb.jpg";

	if(is_file($currentImage) || is_file($newImage)){
    	if($decision == 'true'){
    		rename($newImage, $currentImage);
    		echo json_encode(array('msg'=>'false'));
    	}
    }else{
    	echo json_encode(array('msg'=>'Could not move new thumb. Please try again.'));

    }
}
	
?>