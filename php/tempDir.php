<?php

include_once '../db/database.php';

$getDir = urldecode($_GET['getDir']);
$fileArray;
$dirArray;
$displayThumbArr = ['jpg', 'jpeg', 'gif', 'bmp', 'png', 'svg', 'tiff', 'webp'];

function scanDirectory($directory, &$fileArray, &$dirArray){
	$forbiddenArray = array('.DS_Store', 'category.txt', urldecode('Icon%0D'));
	try{
		foreach (new DirectoryIterator($directory) as $file_or_folder) {
			if($file_or_folder->isFile()){
					$king = $file_or_folder->getBasename();
					if(!in_array($king, $forbiddenArray)){
						$imagePath = $file_or_folder->getPathname();
						$image = $file_or_folder->getBasename();
						$fileArray[] = array($image, $imagePath);
					}
			} elseif($file_or_folder->isDir()){
				$king = $file_or_folder->getBasename();
				if($file_or_folder -> isDot() && $king != '.'){
					$imagePath = "../temp";
					$image = $file_or_folder->getBasename();
					$dirArray[] = array($image, $imagePath);
				}elseif($king != '.'){
					$imagePath = $file_or_folder->getPathname();
					$image = $file_or_folder->getBasename();
					$dirArray[] = array($image, $imagePath);
				}
			}
		}

	} catch(Exception $e){
		//Some Models do not have videos, previously php would throw up an error and wouldn't procede
		//Using the try/catch method we can get all the errors while allowing the full loop to run
		print($e);
	}
	
}
scanDirectory($getDir, $fileArray, $dirArray);


?>	

<div id="temp_files">
	<ul id="dir_list">
		<select name="add_model_ID">
	    	<?php foreach($modelsList as $options){ ?>
	    	<option data-catSort="<?php echo $options['catTitle']?>" value ="<?php echo $options['ID'];?>"><?php echo $options['Title']; ?></option>
	    	<?php } ?>
	    </select>

		<?php foreach($dirArray as $dirs){?>
			<li><a class="folders" href="./php/tempDir.php?getDir=<?php echo urlencode($dirs[1]);?>"><?php include('../images/buttons/folder.svg');?><?php echo $dirs[0];?></a></li> 
		<?php } ?>

		<div class="selectListContainer"><a href="#" class="selectList">Select Subsection…</a></div>

		<li><button title="Move Selected Files" class="move_photo" onclick="temp.tempFunction('move');"><?php include("../images/buttons/move.svg"); ?></button></li>

		<li><button title="Delete Selected Files" class="file_delete" onclick="temp.tempFunction('delete');"><?php include("../images/buttons/trash.svg"); ?></button></li>

		<li><button title="Close Temp Files" onclick="header.removeContent();"class="close_window_temp"><?php include("../images/buttons/delete.svg"); ?></button></li>
	</ul>

	<div id="tempFilesContainer">

		<ul id="file_list">
			<!--check if there are any files in folder-->
			<?php if(count($fileArray) < 1){ ?>
				<div><span>Nothing to see here, move along</span></div>
			<!-- process files -->
			<?php } else{
				foreach($fileArray as $files){?>
				<li><a href="#">

				<!-- get the extension of the file. Strrpos checks for LAST occurance of needle - perfect for file extensions -->
				<?php $searchString = strtolower(substr($files[1], strrpos($files[1], ".") + 1));

				//check if extension is recognised in array
				if(!in_array($searchString, $displayThumbArr)){?>
					<span><img data-notfile="true" data-original="<?php echo urlencode($files[1]); ?>" style="display:none;"/> <?php echo $files[0]; ?></span>

				<?php } else{ ?>
					<img data-original="<?php echo urlencode($files[1]); ?>" src="./php/thumbs/timthumb.php?src=<?php echo urlencode($files[1]); ?>&w=200" />
				<?php } ?>

				</a></li>	
			<?php }
			} ?>	
		</ul>
		<div id="display_panel">
			<div id="tempMessageCont">
				<div><?php include '../images/buttons/stack.svg';?></div>
				<p>Nothing selected</p>
			</div>
		</div>
	</div>
</div>	

<script>
temp.switchFolders();
temp.selectFiles();
formSubmission.selectLists($('#dir_list'), true);
</script>

