<?php
include_once '.././db/database.php';
//ini_set('display_errors',1);
//error_reporting(E_ALL);

$modelID = htmlspecialchars($_GET['ModelID']);

$imgQuery = "SELECT Photos.IMGname, Photos.ID, Photos.ModelID From Photos WHERE Photos.ModelID = $modelID ORDER BY Photos.IMGname ASC";

$vidQuery = "SELECT Videos.VIDname, Videos.ID, Videos.ModelID From Videos WHERE Videos.ModelID = $modelID ORDER BY Videos.VIDname ASC";
?>	

<div id="section_models">
	<div id="model_pictures">
		<?php
		foreach($dbh->query($imgQuery, PDO::FETCH_ASSOC) as $row)
		{
			$favStatus = 'false';
			//check if image is a favourite
			foreach($favsArray as $fav){
				
				if($fav['PhotoID'] === $row['ID']){
					$favStatus = 'true';
					break;
				}
			}
			$location = locationGenerator::gen($row['IMGname']);
			$link = ".".CONTENTDIR.$location;
			$thumb = $link.locationGenerator::thumbLocation($row['IMGname']);
		?>
		<!--set data-attr for fav pic-->
		<div id="<?php echo $row['ID'] ?>-pic" class="modelPictures">
			<a data-id="<?php echo $row['ID'] ?>" data-fav="<?php echo $favStatus;?>" class="onlyThis" href="<?php echo $link.$row['IMGname'];?>" target="_blank"><img class="lazy" src="./images/loader.png" data-original="<?php echo $thumb;?>"/></a>
		</div>
		<?php } ?>
	</div>

	<div id="model_videos">
		<?php
		foreach ($dbh->query($vidQuery, PDO::FETCH_ASSOC) as $row2){

		$location = locationGenerator::gen($row2['VIDname']);
		$link = ".".CONTENTDIR.$location;
		$thumb = $link.locationGenerator::thumbLocation($row2['VIDname']);?>

		<div id="<?php echo $row2['ID'] ?>-vid" class="modelVideos">
		    <a data-id="<?php echo $row2['ID'] ?>" class="onlyThis" target="_blank" href="<?php echo $link.$row2['VIDname'];?>"><img class="lazy" src="./images/loader.png" data-original="<?php echo $thumb;?>"/></a>

		    <button class="video_thumbs" onclick="videoThumbs.init(this)"><?php include("../images/buttons/videos.svg"); ?></button>
		</div>

		<?php } if(empty($row2)){ ?>
		<h4>No videos, sorry!</h4> <?php } ?>
	</div>

	<div id="media">
		<header id="mediaControlBar">
			<button title="Shift Window Right" class="shiftWindow" onclick="mediaView.hide(this)"><?php include("../images/buttons/back.svg"); ?></button>

			<button title="Close Window(Keyboard Shortcut: X)" class="mediaClose close" onclick="mediaView.close(this)"><?php include("../images/buttons/delete.svg"); ?></button>
			<button title="Previous File(Keyboard Shortcut: &larr;)" class="prevPic" onclick="mediaView.back()"><?php include("../images/buttons/browserBack.svg"); ?></button>
			<button title="Next File(Keyboard Shortcut: &rarr;)" class="nextPic" onclick="mediaView.forward()"><?php include("../images/buttons/browserForward.svg"); ?></button>

			<div id="pictureControls">
				<button title="Slideshow(Keyboard Shortcut: Space)" id="slideShowButton" onclick="mediaView.slideShow(this)"><?php include("../images/buttons/play.svg"); ?></button>
				<button title="Add/Remove Favourite(Keyboard Shortcut: F)" data-id="" class="fav" onclick="mediaView.favourites(this)"><?php include("../images/buttons/favourite.svg"); ?></button>
				<button title="Delete File" class="picTrash close" onclick="mediaView.trash()"><?php include("../images/buttons/trash.svg"); ?></button>
				<button title="Image Original Size" onclick="mediaView.imageOriginal(this)"><?php include("../images/buttons/originalSizenobox.svg"); ?></button>
				<button title="Image Fit Width(Keyboard Shortcut: &darr;)" class="imageFullWidth" onclick="mediaView.imageFullWidth(this)"><?php include("../images/buttons/fullWidthnobox.svg"); ?></button>
				<button title="Image Fit Height(Keyboard Shortcut: &uarr;)" class="imageToFit" onclick="mediaView.imageToFit(this)"><?php include("../images/buttons/fullHeightnobox.svg"); ?></button>
			</div>

			<div id="videoControls">
				<button title="Play(Keyboard Shortcut: Space)" type="button" onclick="mediaView.play(this)" id="play-pause"><?php include("../images/buttons/play.svg"); ?></button>
			    <input type="range" id="seek-bar" value="0">
			    <button title="Mute" onclick="mediaView.mute(this)" type="button" id="mute"><?php include("../images/buttons/audio.svg"); ?></button>
			    <input type="range" id="volume-bar" min="0" max="1" step="0.1" value="1">
			    <button title="Fullscreen" onclick="mediaView.fullscreen()" type="button" id="full-screen"><?php include("../images/buttons/move.svg"); ?></button>
			    <button title="Delete Video" class="vidTrash close" onclick="mediaView.trash()"><?php include("../images/buttons/trash.svg"); ?></button>

			</div>
		</header>

		<div class="holder">
			<img data-id="" src="">
			<video data-id="" id="mainVideo" type="video/mp4" src="" width="1000" height="550"></video>
		</div>
	</div>
</div>
<script>
projects.loadContent('#section_models');
lazyLoad.init('img.lazy', '.modelPictures', '', false);
projects.sorting();
projects.modelID = <?php echo $modelID; ?>;
projects.formIDs();
formSubmission.selectLists($('#move_photo_projects'), true);
</script>