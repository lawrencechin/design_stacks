<?php

$thumbGET = $_GET['tArr'];
$thumbArray = explode("," , $thumbGET);
$pictureGET = $_GET['pArr'];
$pictureArray = explode("," , $pictureGET);

$pathPos = strpos($thumbArray[0], '_Thumb/');

$path = substr($thumbArray[0], 0, $pathPos);

$pathThumb = $path."thumb.png";

?>

<div id="crop_container">

<button onclick="crop.cancelAndClose();" id="close_crop"><?php include("../images/buttons/delete.svg"); ?></button>

<div class="thumbnail_area">
  <ul>
  <?php foreach($thumbArray as $key=>$thumbs){?>
    <li><a href="<?php echo $pictureArray[$key];?>"><img src="<?php echo $thumbs; ?>"/></a></li>
  <?php } ?>
  </ul>

  <h4>Select a thumbnail to crop</h4>
</div>

<div class="main_crop_area">
  <img src="" id="jcrop"/>
  <button id="cancel_crop">cancel</button>
  <button id="submit_crop">submit</button>
</div>

<form id="hidden_crop_form" action="./php/thumbs/thumbnails.php" name="hidden_crop_form" style="display:none;">
<input type="hidden" id="x_cor" name="x_cor" />
<input type="hidden" id="y_cor" name="y_cor" />
<input type="hidden" id="w_cor" name="w_cor" />
<input type="hidden" id="h_cor" name="h_cor" />
<input type="hidden" id="image_cor" name="image_cor" />
<input type="hidden" id="crop_cor" name="crop_cor" value="crop"/>
<input type="hidden" id="path_cor" name="path_cor" value="<?php echo $path;?>" />
<input type="submit" />
</form>

<div id="crop_comparrison">
  <h4>Review Images</h4>
  <div>
    <img src="<?php echo $pathThumb; ?>" width="200px" height="250px"/>
    <h4>Current Image</h4>
  </div>

  <div>
    <img src="" id="new_image" width="200px" height="250px"/>
    <h4>New Image</h4>
  </div>
  <button onclick="crop.finalise();" id="confirm_thumb">Accept new thumb?</button>

  <form id="accept_crop_form" action="./php/thumbs/thumbnails.php" name="accept_crop_form" style="display:none;">
    <input type="hidden" id="image_cor_2" name="image_cor" value="<?php echo $path;?>"/>
    <input type="hidden" id="flag_cor" name="crop_cor" value="replace"/>
    <input type="hidden" id="decision_cor" name="decision_cor" value="true"/>
    <input type="submit" />
  </form>
</div>

<link rel="stylesheet" href="./css/jquery.Jcrop.css" type="text/css" />
<script src="./js/min/jquery.Jcrop.min.js"></script>

<script>
  crop.selectImage('div.thumbnail_area');
  crop.submitForm($('form#hidden_crop_form'));
</script>

</div>







