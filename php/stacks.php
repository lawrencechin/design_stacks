<?php
include_once '.././db/database.php';

$getString = './php/models.php';
?>

<div id="section_categories">
	<header id="stacks_control">
		<button title="Show Sort-Project Menu" onclick="feeds.sort(this)"><?php include '../images/buttons/sort.svg';?></button>
		<ul id="sortStacks">
			<?php foreach($categoriesArray as $ca){ ?>
			<li class="stacksCat <?php echo $ca['Title'];?>"><a class="<?php echo $ca['Title'];?>"><?php echo $ca['Title'];?></a></li>
			<?php } ?>
		</ul>

		<span><strong>sort:</strong> <a href="asc">asc</a> | <a href="desc">desc</a> | <a href="rand">rand</a></span>

		<input id="searchStacks" type="text" name="searchStacks" placeholder="Search Name…">
		<button title="Search Projects" onclick="stacks.showSearch(this)" class="search"><?php include '../images/buttons/search.svg';?></button>
	</header>

	<div id="stacksScrollContainer">
		<?php
		foreach ($modelsList as $sa){ ?>

		<div class="stacked <?php echo $sa['catTitle']; ?>">

			<a class="external" data-location="stacks" data-id="<?php echo $sa['ID'];?>" href="<?php echo $getString ?>" data-title="<?php echo $sa['Title'] ?>">

		    	<img class="lazy" src="./images/loader.svg" data-original="<?php echo '.'.THUMBDIR.'/'.$sa['ID'].'-thumb.jpg';?>">
		    </a>

			<a class="external" data-location="stacks" data-id="<?php echo $sa['ID'];?>" href="<?php echo $getString; ?>" data-title="<?php echo $sa['Title'] ?>">

		    	<h3><?php echo $sa['Title'] ?></h3>
		    	
		    </a>
		    	
	    	<button title="Edit Project" type="button" data-button="2" class="edit_model" onclick="formSubmission.revealForm(this)">e</button>	
			<button title="Delete Project" type="button" class="delete_models" data-stacks = "true" onclick="formSubmission.deleteInlineForm(this)">x</button>
		</div>

		<?php } ?>
	</div>
			
	<form id="delete_models_form" name="delete_models_form" action="./db/model_actions.php" method="post">
		<input type="hidden" name="delete_model_ID" value="">
		<input type="hidden" name="modelDecision" value="delete">
		<input type="submit" name="submit" value="delete">
	</form>
</div>

<div id="section_cat_content">
	<header id="models_header">
		<!--reveal menu-->
		<button title="Reveal Main Menu" id="revealMenu" onclick="header.revealMenu()" class="mainMenu"><?php include("../images/buttons/hamburger.svg"); ?></button>
		<!--photos-->
		<button title="Display Photos" class="display_photos activeButton" data-filetype="images" onclick="projects.reveal(this)"><?php include("../images/buttons/picture2.svg"); ?></button>
		<!--videos-->
		<button title="Display Videos" data-filetype="videos" class="display_videos" onclick="projects.reveal(this)"><?php include("../images/buttons/videos.svg"); ?></button>
		<!--add files-->
		<button title="Add Files To This Project" class="add_file_models" data-add="true" onclick="projects.showForm(this)"><?php include("../images/buttons/add.svg"); ?></button>
		<!--close menu-->
		<button title="Close Window" onclick="header.close(true)" class="close"><?php include("../images/buttons/delete.svg"); ?></button>
		<!--sort options-->
		<span><strong>sort:</strong> <a href="asc">asc</a> | <a href="desc">desc</a> | <a href="rand">rand</a></span>

		<!--move files to another project-->
		<button title="Toggle Edit Mode" class="editProjectFiles" onclick="projects.editMode(this)"><?php include("../images/buttons/edit.svg"); ?></button>
		<!--move files to another project-->
		<button title="Move Selected Files" disabled="disabled" class="move_photo_projects" onclick="projects.showForm(this)"><?php include("../images/buttons/move.svg"); ?></button>
		<!--check all/deselct all-->
		<button title="Select/Deselect All Files" disabled="disabled" onclick="projects.select(this)" class="deselect_checks"><?php include("../images/buttons/checkbox.svg"); ?></button>
		<!--set project main thumb-->
		<button title="Generate Project Thumb for Selected Files" disabled="disabled" onclick="crop.init(this)" class="image_crop"><?php include("../images/buttons/crop.svg"); ?></button>
		<!--delete files-->
		<button title="Delete Selected Files" disabled="disabled" class="file_delete_projects close" onclick="projects.triggerSubmit('file_delete_projects')"><?php include("../images/buttons/trash.svg"); ?></button>

		<form id="file_delete_projects" name="file_delete_projects" action="./db/photo_actions.php" method="post">
		    <input type="hidden" name="IDArray" value="">
		    <input type="hidden" name="photoDecision" value="delete">
		    <input type="hidden" name="videoFlag" value="false">
			<input type="submit" name="delete_photo_submit">
		</form>

		<form id="move_photo_projects" name="move_photo_projects" action="./db/photo_actions.php" method="post">
		        <select name="select_model">
		        	<?php foreach($modelsList as $selectList){ ?>
		        	<option data-catSort="<?php echo $selectList['catTitle']?>" value="<?php echo $selectList['ID'];?>"><?php echo $selectList['Title'];?></option>
		        	<?php } ?>
		        </select>

		        <a class="selectList" href="#">Select Subsection…</a>

		        <input type="hidden" name="IDArray" value="">
		        <input type="hidden" name="photoDecision" value="move">
		        <input type="hidden" name="videoFlag" value="false">
				<button type="submit" name="edit_submit"><?php include("../images/buttons/submit.svg"); ?></button>
				<button type="button" name="cancel_submit" onclick="projects.removeForm(this)"><?php include("../images/buttons/delete.svg"); ?></button>
		</form>
		
		<form id="add_file_models" name ="add_file_models" enctype="multipart/form-data" action="./db/photo_actions.php" method="POST">
		    <!-- MAX_FILE_SIZE must precede the file input field -->
		    <input type="hidden" name="MAX_FILE_SIZE" value="5242880000" />
		    <!-- Name of input element determines name in $_FILES array -->

			<input id="fUpload" type="file" multiple="true" name="files[]" accept="video/mp4, video/m4v, image/jpeg, image/jpg, image/gif, image/png, image/tiff">
			<input type="hidden" name="add_model_ID" value="">
			<input type="hidden" name="photoDecision" value="add">
			<button type="submit" name="edit_submit"><?php include("../images/buttons/submit.svg"); ?></button>
			<button type="button" name="cancel_submit" onclick="projects.removeForm(this)"><?php include("../images/buttons/delete.svg"); ?></button>
		</form>
	</header>
	<div id="content"></div>
</div>

<script>
formSubmission.submitForms('_models', true);
formSubmission.submitForms('_projects', false);
formSubmission.checkFileTypes('#fUpload');
lazyLoad.init('img.lazy', 'a.external', '#section_categories', true);
header.loadExternalLinks('#section_categories');
stacks.init();
</script>