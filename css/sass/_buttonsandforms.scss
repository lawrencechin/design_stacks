select{
	display:none;
}

button:disabled{
	opacity: 0.3;
}

button{
	width: $buttonDimensions;
	height: $buttonDimensions;
	border-radius: $buttonDimensions;
	background-color: transparent;
	border: none;
	color: $borderColour;
	position: relative;
	transition: 0.6s all ease;
	transition-property: background, opacity;

	svg{
		transition: 0.6s all ease;
		width: $buttonDimensions; 
		height: $buttonDimensions;
		#iconBorder{ stroke: none;}
	}
	@media #{$information-screen} {
  		&:hover{
			background: $titleText;
			border: none;
			color: $chromeColour;
			svg{
				rect,path, polygon{fill: $chromeColour;}
				polyline{stroke: $chromeColour};
				#iconBorder{ stroke: none;}
				#submit{
					rect,path, polygon{fill: none; stroke: $chromeColour;}
				}
				#checkbox{
					rect,path, polygon{fill: none;}
				}
			}
		}
	}
	
}

.activeButton{
	background-color: $titleText;
	color: $chromeColour;
	border: none;
	svg{
		rect,path, polygon{fill: $chromeColour;transition: 0.6s all ease;}
		#iconBorder{ stroke: none;}
		polyline{stroke: $chromeColour};
		#submit{
			rect,path, polygon{fill: none; stroke: $chromeColour;}
		}
	}
}

.buttonSubmit{
	background-color: $greenColour;
	svg{
		rect,path,polygon{stroke: $chromeColour;}
	}
}

button[class^="delete_"].activeButton{
	background-color: $book_texture !important;
	color: $chromeColour;
}

button[class^="edit_"].activeButton{
	background-color: $book_css !important;
	color: $chromeColour;
}

button[class^="delete_"], button[class^="edit_"]{
	width: 15px;
	height: 15px;
	border-radius: 15px;
	position: absolute;
	right: 0;
	font-size: 0.9em;
	line-height: 0.9em;
	font-weight: 600;
	border: 1px solid;
	border-color: $book_texture;
	color: $book_texture;
	text-transform: lowercase;
	font-variant: small-caps;
	top: 12px;
	@media #{$information-screen}{
		&:hover{
			background-color: $book_texture !important;
			color: $chromeColour;
		}
	}
	
}

button[class^="edit_"]{
	right: 20px;
	border-color: $book_css;
	color: $book_css;
	@media #{$information-screen}{
		&:hover{
			background-color: $book_css !important;
			color: $chromeColour;
		}
	}
}

input{
	border: none;
	width: 90%;
	height: 35px;
	padding-left: 10px;
	margin: 10px 0;
	font-size: 1rem;
	background-color: $book_prog;
	font-style: italic;
	transition: all 0.6s ease-in-out;
	
	&:focus{
		outline: none;
		color: $book_learning;
		background-color: $book_css !important;
	}
}

input[type^="file"]{
	padding-top: 8px;
	color: $book_prog;
	text-align:center;
	padding-left: 20%;
}

form{
	width: 100%;
	background-color: $chromeColour;
	position: absolute;
	transition: all 0.6s ease-in-out;
	text-align: center;
	z-index: 1;

	@media #{$information-phone} {
  		z-index: 6;
	}

	.selectList{
		width: 100%;
		height: 35px;
		display:block;
		border-bottom: 1px solid $borderColour;
		font-style: italic;
		font-size: 1rem;
		font-weight: 600;
		color: $titleText !important;
		text-transform: lowercase;
		font-variant: small-caps;
		margin: 5px 0;
		transition: 0.6s color ease;
		line-height: 1.3em;
		overflow: hidden;
		text-overflow: ellipsis;
		@media #{$information-screen}{
			&:hover{
				color: darken($titleText, 50%) !important;
			}
		}
	}

	button[type="submit"], button[name^="cancel_"]{
		width:45%;
		margin: 0;
		padding: 0;
		background-color: transparent;
		font-style:normal;

		svg{
			circle{fill: $greenColour;}
			path{ stroke: $chromeColour; fill: none;}
		}

		&:after{
			position: absolute;
			bottom: -20px;
			width: 100%;
			left: 0;
			text-transform: lowercase;
			font-variant: small-caps;
			font-weight: 700;
			font-size: 0.8rem;
			color: #666;
		}
	}

	button[name^="cancel_"]{
		svg{
			circle{fill: $book_texture;}
			path{ stroke: none; fill: $chromeColour;}
		}
	}

	button[type="submit"]:after{
		content: 'submit';
	}

	button[name^="cancel_"]:after{
		content: 'cancel';
	}
}

form[name^='delete']{display:none;}

.headerForm{
	position: absolute;
	width: 180px;
	padding:0px 0px 40px 0px;
	bottom: 26px;
	left: 0;
	transform: translate3d(-180px, 0, 0);
	@media #{$information-phone} {
  		width: 100%;
  		transform: translate3d(-100%, 0, 0);
	}

	p{
		margin: 0px 0 10px 0;
		font-size: 1.1rem;
		border-top: 1px solid $borderColour;
		border-bottom: 1px solid $borderColour;
		padding: 15px 0;
		font-weight: 700;
		text-transform: lowercase;
		font-variant: small-caps;
		color: $linkColour;
		line-height: 0;
	}
}

.displayForm{
	transform: translate3d(0, 0, 0) !important;
	display:block !important;
}

.selectConvert{
	position: fixed;
	width: 38%;
	bottom: 0;
	left: 180px;
	z-index: 100;
	overflow-x: hidden;
	overflow-y: scroll;
	-webkit-overflow-scrolling: touch;
	display: flex;
	align-content: flex-start;
	flex-wrap: wrap;

	@media #{$information-phone} {
	  width: 100%;
	  left: 0;
	  max-height: 100%;
	}

	li{
		flex: 1 25%;
		position: relative;
		border-bottom: 1px solid $borderColour;
		border-right: 1px solid $borderColour;
		transition: 0.2s all ease-in-out;

		&:before{
			content: '';
			display: table;
			padding-top: 100%;
		}
		@media #{$information-screen}{
			&:hover{
				background-color: black;
				a, span{ color: $chromeColour;}
			}
		}

		a{
			position: absolute;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			text-align: center;
			overflow: hidden;
			padding: 5px;
			padding-top: 40%;

			@media #{$information-phone} {
			  padding: 30% 5px;
			}
		}
	}

	li[class^="bookMenu_"], li[class^="feedMenu_"]{
		a{color: $buttonColour; padding-top: 60%; font-size: 0.9em;}
	}

	.Identity{
		a{color: $chromeColour;}
		color: $chromeColour;
	}

}

.selectConvert.largeList{
	width: 90%;
	height: 90%;
	top: 5%;
	left: 5%;

	@media #{$information-phone} {
  		width: 100%;
  		height: 100%;
  		top: 0;
  		left: 0;
	}

	li{
		flex: 1 10%;
		@media #{$information-phone} {
			flex: 1 33%;
		}

		a{
			padding-top: 20%;
			bottom: 20%;
		}

		span{
			position:absolute;
			top: 80%;
			bottom: 0;
			right: 20%;
			left: 20%;
			border-top: 1px solid $borderColour;
			text-align:center;
			text-transform: lowercase;
			font-variant: small-caps;
			font-size: 0.9em;
		}
	}

	.searchBar{ 
		width: 100%;

		input{
			width: 100%;
			height: 45px;
			background-color: $chromeColour;
			margin: 0;
			border: 1px solid $borderColour;
		}

		button{
			position: absolute;
			top: 7px;
			right: 10px;
			background: url('../images/buttons/delete.svg') no-repeat center;
			@media #{$information-phone} {
				&:hover{background: $book_texture url('../images/buttons/delete.svg') no-repeat center;}
			}
		}
	}
}

