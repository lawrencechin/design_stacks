module.exports = function(grunt){
	'use strict';
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		uglify: {
			options: {
				mangle: false,
				preserveComments: false,
				compress : true
			},
				
			compile: {
				files: {
					'js/min/script.min.js' : 'js/script.js',
				}		
			}
		},

		sass: {
			dist: {
				options: {
					style : 'expanded',
					trace : true
				},

				files:[{
					expand: true,
					cwd: 'css/sass/',
					src: '*.scss',
					dest: 'css/',
					ext: '.css'
				}]
			}
		},

		autoprefixer: {
			dist: {
				files: {
					'css/screen-prefixed.css' : 'css/screen.css'
				}
			}
		},

		watch: {
            sass: {
                files: ['css/sass/*.scss'],
                tasks: ['sass', 'autoprefixer']
            },

            uglify: {
            	files: ['js/script.js'],
            	tasks: ['uglify']
            },

            options: {
            	spawn : false,
            	event : 'changed'
            }
        }
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-watch');

	//Default task(s)
	grunt.registerTask('compile', 'uglify');
};