# Require any additional compass plugins here.
require "/Library/Ruby/Gems/2.0.0/gems/compass-normalize-1.5/lib/compass-normalize.rb";
require "/Library/Ruby/Gems/2.0.0/gems/autoprefixer-rails-1.1.20140327/lib/autoprefixer-rails.rb";

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "css/sass"
images_dir = "images"
javascripts_dir = "js"
fonts_dir = "fonts"

output_style = :compressed
environment = :production

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
color_output = false
require 'compass-normalize'

# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass css/sass scss && rm -rf sass && mv scss sass
preferred_syntax = :scss

on_stylesheet_saved do |file|
  css = File.read(file)
  File.open(file, 'w') { |io| io << AutoprefixerRails.compile(css) }
end
