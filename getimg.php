<?php include_once './db/database.php';

$id = $_GET['project_id'];

$models_query1 = "SELECT Categories.Title as catTitle, Models.Title, Models.ID, Models.CatID FROM Categories INNER JOIN Models on Categories.ID = Models.CatID ORDER BY Models.Title ASC";
foreach($dbh->query($models_query1, PDO::FETCH_ASSOC) as $models){
	$modelsList[] = $models;
}

echo '<ul style="width: 200px; float: left; margin: 0; padding: 0;">';
foreach($modelsList as $pro){	
	echo '<li style="border-bottom: 3px double silver; padding: 5px;"><a href="getimg.php?project_id='.$pro[ID].'">'.$pro['Title'].'</a></li>';
}
echo '</ul>';

if(!empty($id)){
	echo '<ul>';
	$imgQuery = "SELECT Photos.IMGname, Photos.ID, Photos.ModelID From Photos WHERE Photos.ModelID = $id ORDER BY Photos.IMGname ASC";

	foreach($dbh->query($imgQuery, PDO::FETCH_ASSOC) as $row){ 
		$location = locationGenerator::gen($row['IMGname']);
		$link = "./content".$location;
		$thumb = $link.locationGenerator::thumbLocation($row['IMGname']);
	?>

	<li style="display: inline-block; margin: 2px; border: 3px double silver; padding: 10px;">
		<a href="<?=$link.$row['IMGname']?>"><img src="<?=$thumb?>"></a>
	</li>

	<?php }
	echo '</ul>';
}