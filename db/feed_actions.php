<?php 
include_once './database.php';

$decision = htmlspecialchars($_POST['feedDecision']);
$error = true;

if($decision === 'add'){
	add();
}else if($decision === 'edit'){
	edit();
}else if($decision === 'delete'){
	delete();
}else{
	echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>$error));
}

function add(){
	global $dbh;
	global $error;
	$add_catID = htmlspecialchars($_POST['add_feed']);
	$add_url = htmlspecialchars($_POST['add_url']);

	if(isset($add_url) && strlen($add_url) > 0 && isset($add_catID) && is_numeric($add_catID)){
		$query = "INSERT INTO Feed_URLS(CatID, URL) VALUES(:CatID, :URL);";

		$addFeedQuery = $dbh->prepare($query);

		$data = array('CatID'=>$add_catID, 'URL'=>$add_url);
		try{
			$addFeedQuery->execute($data);
			$error = false;
			echo json_encode(array('msg'=>'FeedChanged', 'error'=>$error));
		}catch(PDOException $e){
			echo json_encode(array('msg'=>'Could not communicate with database: '.$e->getMessage(), 'error'=>$error));
		}
	}else{
		echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>$error));
	}
}

function edit(){
	global $dbh;
	global $error;
	$edit_catID = htmlspecialchars($_POST['edit_feed']);
	$edit_url = htmlspecialchars($_POST['edit_url']);
	$edit_id = htmlspecialchars($_POST['edit_feed_ID']);

	if(isset($edit_url) && strlen($edit_url) > 0 && isset($edit_catID) && is_numeric($edit_catID) && isset($edit_id) && is_numeric($edit_id)){
		$query = "UPDATE Feed_URLS SET CatID=$edit_catID, URL='$edit_url' WHERE ID = $edit_id;";
		try{
			$dbh->exec($query);
			$error = false;
			echo json_encode(array('msg'=>'FeedChanged', 'error'=>$error));
		}catch(PDOException $e){
			echo json_encode(array('msg'=>'Could not communicate with database: '.$e->getMessage(), 'error'=>$error));
		}
	}else{
		echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>$error));
	}
}

function delete(){
	global $dbh;
	global $error;
	$delete_ID = htmlspecialchars($_POST['delete_feed_ID']);

	if(isset($delete_ID) && is_numeric($delete_ID)){
		$query = "DELETE FROM Feed_URLS WHERE ID = $delete_ID;";
		try{
			$dbh->exec($query);
			$error = false;
			echo json_encode(array('msg'=>'FeedDeleted', 'error'=>$error));
		}catch(PDOException $e){
			echo json_encode(array('msg'=>'Could not communicate with database: '.$e->getMessage(), 'error'=>$error));
		}
	}else{
		echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>$error));
	}
}


?>