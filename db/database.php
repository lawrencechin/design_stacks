<?php
//Constants defined here:
//Paths
date_default_timezone_set('Europe/London');
define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('PAGEURL', '/DesignNxt');
define('CONTENTDIR', '/content');
define('THUMBDIR', CONTENTDIR.'/thumb');
//Thumb Dimensions
define('THUMB_WIDTH', 200);
define('THUMB_HEIGHT', 250);
define('VIDSIZE', '640x360');
//Libraries
define('MAGICK_PATH','/usr/local/Cellar/imagemagick/6.8.9-7/bin/');
define('FFMPEG', '/usr/local/Cellar/ffmpeg/2.3.3/bin/ffmpeg');
define('FFPROBE', '/usr/local/Cellar/ffmpeg/2.3.3/bin/ffprobe');

//simple class with static method to generate folder struction from file name 
class locationGenerator{
	//take file name and split into year, month and first two characters of hashed name
	static public function gen($file){
		$location = "/".substr($file, 0, 4)."/".substr($file, 5, 2)."/".substr($file, 11, 2)."/";
		return $location;
	}

	static public function thumbLocation($file){
		$thumbLocation = "thumb/".substr($file, 0, strripos($file, '.')).".jpg";
		return $thumbLocation;
	}

	static public function nameGen($file, $fileExt){
		$today = date("Y-m-d-");
		$shaName = hash_file('sha1', $file);
		$finalname = $today.$shaName.".".$fileExt;
		return $finalname;
	}

	static public function createDir($location){
		if(!is_dir(ROOT.PAGEURL.CONTENTDIR.$location) && !is_dir(ROOT.PAGEURL.CONTENTDIR.$location."thumb/")){
			$oldmask = umask(0);
			mkdir(ROOT.PAGEURL.CONTENTDIR.$location, 0777, true);
			mkdir(ROOT.PAGEURL.CONTENTDIR.$location."thumb/", 0777, true);
			umask($oldmask);
		}
		
	}
	//helper function to log to console - useful!
	static public function debug_to_console($data) {
		if(is_array($data) || is_object($data)) {
			echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
		}else{
	    	echo("<script>console.log('PHP: $data');</script>");
	  	}
	}
}

$dbPath = ROOT.PAGEURL;
//Connect to database, use for all transactions//
$dbh = new PDO("sqlite:$dbPath/db/stacks.sqlite");
//set PDO exception mode
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//Bookmarks categories
foreach($dbh->query("SELECT Bookmarks_Cats.Title,Bookmarks_Cats.ID FROM Bookmarks_Cats", PDO::FETCH_ASSOC) as $selectList){
	$book_cats[] = $selectList;
}
//Project Categories
foreach($dbh->query("SELECT * FROM Categories", PDO::FETCH_ASSOC) as $cats){
	$categoriesArray[] = $cats;
}
//Projects
$models_query1 = "SELECT Categories.Title as catTitle, Models.Title, Models.ID, Models.CatID FROM Categories INNER JOIN Models on Categories.ID = Models.CatID ORDER BY Models.Title ASC";
foreach($dbh->query($models_query1, PDO::FETCH_ASSOC) as $models){
	$modelsList[] = $models;
}

//Favourites
$favQuery = "SELECT Photos.IMGname, Favourites.PhotoID FROM Photos INNER JOIN Favourites ON Photos.ID = Favourites.PhotoID";

foreach($dbh->query($favQuery, PDO::FETCH_ASSOC) as $fav){
	$favsArray[] = $fav;
}

?>