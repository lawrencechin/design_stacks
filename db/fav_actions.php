<?php

include_once './database.php';

$decision = htmlspecialchars($_GET['decision']);
$photoID = htmlspecialchars($_GET['PhotoID']);
$error = true;

if($decision == 'add'){
	if(isset($photoID) && is_numeric($photoID)){
		$addFav = "INSERT INTO Favourites(PhotoID) VALUES($photoID)";
		try{
			$dbh->exec($addFav);
			$error = false;
			echo json_encode(array('msg'=>'FavAdded', 'error'=>$error));
		}catch(PDOException $e){
			echo json_encode(array('msg'=>'Could not add favourite because: '.$e->getMessage(), 'error'=>$error));
		}
	}else{
		echo json_encode(array('msg'=>'Incorrect ID supplied', 'error'=>$error));
	}

}elseif($decision == 'remove'){
	if(isset($photoID) && is_numeric($photoID)){
		$removeFav = "DELETE FROM Favourites WHERE PhotoID = $photoID";
		try{
			$dbh->exec($removeFav);
			$error = false;
			echo json_encode(array('msg'=>'FavRemoved', 'error'=>$error));
		}catch(PDOException $e){
			echo json_encode(array('msg'=>'Could not remove favourite because: '.$e->getMessage(), 'error'=>$error));
		}
	}else{
		echo json_encode(array('msg'=>'Incorrect ID supplied', 'error'=>$error));
	}
}else{
	echo json_encode(array('msg'=>'There was an error, sorry!', 'error'=>$error));
}
?>