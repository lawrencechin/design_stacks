<?php 
ini_set('display_errors',1);
error_reporting(E_ALL);
include_once './database.php';
include_once './delete_file_function.inc.php';

$modelDecision = htmlspecialchars($_POST['modelDecision']);

$error = true;

if($modelDecision == 'add'){
	addModel();
}elseif($modelDecision == 'delete'){
	deleteModel();
} elseif($modelDecision == 'edit'){
	editModel();
}else{
	echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
}

function addModel(){
	global $dbh;
	global $error;
	//post variables
	$addModelTitle = htmlspecialchars($_POST['add_model_title']);
	$addModelCat = htmlspecialchars($_POST['add_model_cat']);

	$failedTest = 0;

	if(isset($addModelCat) && is_numeric($addModelCat) && strlen($addModelTitle) > 0){

		//Check if name is already used and set failedtest to 1
		foreach($dbh->query("SELECT Models.Title FROM Models")as $modelTitle){
				if($addModelTitle == $modelTitle['Title']){
					$failedTest = 1;
				}
		}
		//run providing failedtest is false
		if($failedTest === 0){
			//add data to database
			$addModelQuery = "INSERT INTO Models(CatID, Title) Values(:CatID, :Title)";

			$data = array('CatID' => $addModelCat, 'Title' => $addModelTitle);

			$modelQuery = $dbh->prepare($addModelQuery);

			try{
				$modelQuery->execute($data);
				//success! return JSON response
				$error = false;
				echo json_encode(array('msg'=>'ModelChanged', 'error'=>$error));

			}catch(PDOException $e){
				$_SESSION["PERCENTAGE_DONE"] = 100;
				echo json_encode(array('msg'=>'Could not insert into database. '.$e->getMessage(), 'error'=>$error));
			}
		}else{
			//if name exists already
			echo json_encode(array('msg'=>'That name already exists. Please change it.', 'error'=>$error));
		}
	}else{
		//if variables not set
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}
	
};

function editModel(){

	global $dbh;
	global $error;
	//post variables
	$editModelTitle = htmlspecialchars($_POST['edit_model_title']);
	$editModelCat = htmlspecialchars($_POST['edit_model_cat']);
	$editModelID = htmlspecialchars($_POST['edit_model_ID']);

	if(isset($editModelCat) && is_numeric($editModelCat)){

		$editModelQuery = "UPDATE Models SET CatID = :CatID, Title = :Title WHERE ID = $editModelID";

		$data = array('CatID' => $editModelCat, 'Title' => $editModelTitle);

		$modelQuery = $dbh->prepare($editModelQuery); 
		try{
			$modelQuery->execute($data);
			$error = false;
			echo json_encode(array('msg'=>'ModelChanged', 'error'=>$error));
		}catch(PDOException $e){
			echo json_encode(array('msg'=>'There was an error inserting into database. '.$e->getMessage(), 'error'=>$error));
		}
	}else{
		//if variables aren't set
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}
};

function deleteModel(){

	global $dbh; 
	global $error;

	$deleteModelID = htmlspecialchars($_POST['delete_model_ID']);

	if(isset($deleteModelID) && is_numeric($deleteModelID)){
		$pIDArr = array();
		$vIDArr = array();
		$pTest = '';
		$vTest = '';
		//get array of IDS from Photos and Videos belonging to project that is to be deleted
		foreach($dbh->query("SELECT ID FROM Photos WHERE ModelID = $deleteModelID", PDO::FETCH_ASSOC) as $ids){
			$pIDArr[] = $ids['ID'];
		}
		foreach($dbh->query("SELECT ID FROM Videos WHERE ModelID = $deleteModelID", PDO::FETCH_ASSOC) as $ids){
			$vIDArr[] = $ids['ID'];
		}
		//implode because delete function expects a string even if it is to be exploded afterwards, shrug
		$photoStr = implode(',', $pIDArr);
		$videoStr = implode(',', $vIDArr);
		//check if there are photos or videos to be deleted and run the deletePhoto function to remove them
		if(strlen($photoStr) > 0){
			$pTest = deletePhoto($photoStr, 'false');
		}
		if(strlen($videoStr) > 0){
			$vTest = deletePhoto($videoStr, 'true');
		}
		//if the function returned TRUE, thus complete, for either photos or videos then delete project from db
		//nb.we still need to run this even if no files have been deleted so we need more checks in place
		if($pTest || $vTest || strlen($photoStr) === 0 || strlen($videoStr) === 0){
			//unlink CAT THUMB if exists
			if(is_file(ROOT.PAGEURL.THUMBDIR."/".$deleteModelID."-thumb.jpg")){
				unlink(ROOT.PAGEURL.THUMBDIR."/".$deleteModelID."-thumb.jpg");
			}

			$deleteProQuery = "DELETE FROM Models WHERE Models.ID = $deleteModelID;";

			try{
				$dbh->exec($deleteProQuery);
			}catch(PDOException $e){
				echo json_encode(array('msg'=>'There was an error communicating with database. '.$e->getMessage(), 'error'=>$error));
				exit();
			}
		}
		//success!
		$error = false;
		echo json_encode(array('msg'=>'ModelDeleted', 'error'=>$error));
	}else{
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}

};

?>