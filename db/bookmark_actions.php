<?php 

include_once './database.php';

$bookmarksDecision = htmlspecialchars($_POST['bookmarksDecision']);

$error = true;

if($bookmarksDecision == 'add'){
	addBookmarks();
}elseif($bookmarksDecision == 'delete'){
	deleteBookmarks();
} elseif($bookmarksDecision == 'edit'){
	renameBookmarks();
}else{
	echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
}


function addBookmarks(){
	global $dbh;
	global $error;
	$book_title = htmlspecialchars($_POST['title']);
	$book_url = htmlspecialchars($_POST['url']);
	$book_classid = htmlspecialchars($_POST['bookCat']);

	if(isset($book_title) && strlen($book_title) > 0 && isset($book_url) && strlen($book_url) > 0 && isset($book_classid) && is_numeric($book_classid)){

		$book_query2 = "INSERT INTO Bookmarks(Title, URL, ClassID) VALUES (:Title, :URL, :ClassID)";

		$bookm_query = $dbh->prepare($book_query2);

		$data = array('Title'=> $book_title, 'URL'=> $book_url, 'ClassID'=> $book_classid);

		$bookm_query->execute($data);

		$error = false;
		echo json_encode(array('msg'=>'BookmarkChanged', 'error'=>$error));
	}else{
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}
};

function deleteBookmarks(){
	global $dbh;
	global $error;
	$delete_book_ID = htmlspecialchars($_POST['delete_book_ID']);

	if(isset($delete_book_ID) && is_numeric($delete_book_ID)){

	$book_query2 = "DELETE FROM Bookmarks WHERE Bookmarks.ID = $delete_book_ID";
	$dbh->exec($book_query2);
	$error = false;
	echo json_encode(array('msg'=>'Bookmark Deleted', 'error'=>$error));
	}else{
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}	
};

function renameBookmarks(){

	global $dbh;
	global $error;
	$edit_book_ID = htmlspecialchars($_POST['edit_book_ID']);
	$edit_book_title = htmlspecialchars($_POST['edit_title']);
	$edit_book_url = htmlspecialchars($_POST['edit_link']);
	$edit_book_classid = htmlspecialchars($_POST['edit_bookCat']);
	if(isset($edit_book_title) && strlen($edit_book_title) > 0 && isset($edit_book_url) && strlen($edit_book_url) > 0 && isset($edit_book_classid) && is_numeric($edit_book_classid)) {

		$edit_query = "UPDATE Bookmarks SET Title = '$edit_book_title', URL = '$edit_book_url', ClassID = $edit_book_classid WHERE ID = $edit_book_ID";
		$dbh->exec($edit_query);
		$error = false;
		echo json_encode(array('msg'=>'BookmarkChanged', 'error'=>$error));
	}else{
		echo json_encode(array('msg'=>'There was an error with your form. Please try again.', 'error'=>$error));
	}	
};

?>