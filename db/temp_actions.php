<?php 
//ini_set('display_errors',1);
//error_reporting(E_ALL);
include_once './database.php';
include_once '.././php/thumbs/thumbnails.php';
date_default_timezone_set('Europe/London');

$move = htmlspecialchars($_POST['move']);
$error = true;

if($move == 'move'){
	moveTempFiles();
}elseif($move == 'delete'){
	deleteTempFiles();
}else{
	echo json_encode(array('msg'=>'No move or delete flag sent', 'error'=>$error));
}

function moveTempFiles(){
	//post data
	$fileArray = $_POST['fileArray'];
	$modelID = $_POST['modelID'];
	$moveArray = explode(',', $fileArray);
	//global variables
	global $error;
	global $dbh;

	//filetype arrays and variables(php errors if not defined here)
	$videoFileTypes = ['mp4','mv4'];
	$pictureFileTypes = ['jpeg', 'jpg', 'gif', 'png'];
	$table = '';
	$columnName = '';	
	$functionExec = '';
	$fullPath = '';
	$fileTypeFlag = '';
	$wrongExt = array();
	$count = 0;

	foreach($moveArray as $moving){
		$count++;
		//check if file is applicable file
		$fileExt = pathinfo($moving, PATHINFO_EXTENSION);

		if(in_array($fileExt, $videoFileTypes)){
			$fileTypeFlag = 'video';
		}elseif(in_array($fileExt, $pictureFileTypes)){
			$fileTypeFlag = 'image';
		}else{
			//collect files that are not of correct type and display later.
			$fileTypeFlag = 'ignore';
			$wrongExt[] = substr($moving, 8);
		}

		//set variables depending on video or photos
		if($fileTypeFlag === 'video'){
			$table = 'Videos';
			$columnName = 'VIDname';
			$functionExec = 'videoThumbs';
		}else if($fileTypeFlag === 'image'){
			$table = 'Photos';
			$columnName = 'IMGname';
			$functionExec = 'makeThumbnail';
		}

		if($fileTypeFlag !== 'ignore'){
			//prepare paths and names
			$newName = locationGenerator::nameGen($moving, $fileExt);
			$location = locationGenerator::gen($newName);
			$fullPathThumb = ROOT.PAGEURL.CONTENTDIR.$location.locationGenerator::thumbLocation($newName);
			$fullPathIn = ROOT.PAGEURL.substr($moving, 2);
			$fullPathOut = ROOT.PAGEURL.CONTENTDIR.$location.$newName;

			//make directories 
			locationGenerator::createDir($location);

			//attempt to move file and then run thumbnail function
			if(copy($fullPathIn, $fullPathOut)){
				unlink($fullPathIn);
				$in = '..'.CONTENTDIR.$location.$newName;
				$out = '..'.CONTENTDIR.$location.locationGenerator::thumbLocation($newName);
				$functionExec($in, $out);
			}

			//prepare queries
			$query = "INSERT INTO $table(ModelID, $columnName) VALUES(:ModelID, :$columnName)";
			$data = array('ModelID'=> $modelID, $columnName=> $newName);

			$tempQuery = $dbh->prepare($query);
			//attempt to insert int db
			try{
				$tempQuery->execute($data);
			}catch(PDOException $e){
				echo json_encode(array('msg'=>'Could not communicate with database. '.$e->getMessage(), 'error'=>$error));
			}
		}
		
	}
	//check if wrong file type was sent
	if(count($wrongExt) > 0){
		$weStr = implode(', ', $wrongExt);
		//check if ONLY wrong files were sent 
		if(count($wrongExt) === $count){
			echo json_encode(array('msg'=>'No files selected were of the correct type. Files: '.$weStr, 'error'=>$error));
			exit();
		}
		$error = false;
		echo json_encode(array('msg'=>'FilesAdded','error'=>$error, 'info'=>'The following files could not be moved as they had the wrong file type: '.$weStr));

	}else{
		//success
		$error = false;
		echo json_encode(array('msg'=>'FilesAdded', 'error'=>$error));
	}
	
};


function deleteTempFiles(){
	$fileArray = htmlspecialchars($_POST['fileArray']);
	$deleteArray = explode(',', $fileArray);
	global $error;
	$wrongExt = array();
	$count = 0;

	foreach($deleteArray as $remove){
		$count++;
		$removeFullPath = ROOT.PAGEURL.substr($remove, 2);
		//check if file exists. If not return error and exit
		if(!file_exists($removeFullPath)){
			$wrongExt[] = substr($removeFullPath, 8);
		}else{
			if(unlink($removeFullPath)){
			}else{
				$wrongExt[] = $removeFullPath;
			}
		}
	}
	//check if files could not be deleted
	if(count($wrongExt) > 0){
		$weStr = implode(', ', $wrongExt);

		//check if ONLY wrong files were not deleted
		if(count($wrongExt) === $count){
			echo json_encode(array('msg'=>'No files selected could be removed. Please check file location or permissions. Files: '.$weStr, 'error'=>$error));
			exit();
		}
		$error = false;
		echo json_encode(array('msg'=>'FilesDeleted','error'=>$error, 'info'=>'These files could be deleted: '.$weStr));

	}else{
		//success!
		$error = false;
		echo json_encode(array('msg'=>'FilesDeleted', 'error'=>$error));
	}
	
};

?>